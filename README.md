# README #

### Introduction

## Purpose

* The main goal of this system is to design and develop a secure and user-friendly administrative data collecting and reviewing application to enhance the administrative ease for the Faculty of Engineering. 
* First version of this system mainly focuses on developing three sub systems of Transcript/Academic Rank Certificate Management System, Verification System and Employee Data Management System.

## Purpose of the document

* This document describes the Software Requirements Specification for the Management and Information System (MIS) of the dean’s office, Faculty of Engineering. The Purpose of this document is to provide guidelines for the development of the system.

## Project Scope

* The scope of this project is to develop an administrative support system to manage transcript/academic rank certificate process, verification process and Employee data management system in an effective way.
