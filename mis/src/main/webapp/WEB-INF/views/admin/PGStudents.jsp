<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="PageDetails">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/dashboard.css" rel="stylesheet">

<link href="css/admin.css" rel="stylesheet">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link rel="stylesheet" href="css/toastr.min.css">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.modal').modal();
		$('#AddStudent').on('click', function() {
		});
		$("#AddStudent").modal({
			dismissible : false
		});

		$('#DeleteStudent').on('click', function() {
		});
		$("#DeleteStudent").modal({
			dismissible : false
		});
		
		$('#ViewStudent').on('click', function() {
		});
		$("#ViewStudent").modal({
			dismissible : false
		});
		
		$('#EditStudent').on('click', function() {
		});
		$("#EditStudent").modal({
			dismissible : false
		});
		
		$('select').formSelect();

	});
	$(document).ready(function() {
		$('.sidenav').sidenav({
			draggable : true,
		});
		$('.modal').modal();
		$('.dropdown-trigger').dropdown();
	});
	$(document).ready(function() {
		$('.tabs').tabs();
		$('.tabs_DLevel').tabs();
	});
</script>
<script>
	function printDiv(divName){
		var printContent = document.getElementById(divName).innerHTML;
		var documentName = document.getElementById("studentRegNo").innerHTML;
		w = window.open("","_blank","");
		w.document.write('<head><title>MIS - PG student details : '+documentName+' </title><style>.studentTable tr, .studentTable td{border-bottom: 1px solid #A9A9A9;)));}</style><link href="css/materialize.css" rel="stylesheet"><link href="css/materialize.min.css" rel="stylesheet"></head><body>');
		w.document.write(printContent);
		w.document.write('</body></html>');
		w.print();
		w.close();
	}
</script>

<style>
.main_page {
	margin: 0% 2% 0% 2%;
}

.sidenav-trigger {
	display: block !important;
}

/* .card {
	width: 100%;
	padding: 0% 0% 15% 0%;
} */

.input-field label {
	color: #212121 !important;
	font-size: 12px !important;
}

.input-field input[type=text]:focus {
	border-bottom: 1px solid #212121 !important;
	box-shadow: 0 1px 0 0 #212121 !important;
}

disabled {
    pointer-events: none;
}
.model_size{
			width: 90% !important;
			height: 90% !important;
		}

</style>
</head>
<body ng-controller="PageData">

	<div id="top">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="/logout">Logout</a></li>
		</ul>
		<nav class="navbar">
			<div class="nav-wrapper">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i
					class="material-icons">menu</i></a>
				<ul class="right">
					<li><a class="dropdown-trigger" href="#!"
						data-target="dropdown1">{{firstName}} {{lastName}}<i
							class="material-icons right">arrow_drop_down</i></a></li>
				</ul>

			</div>

		</nav>

	</div>

	<div id="left-side">
		<ul id="slide-out" class="sidenav z-depth-4">
			<li>

				<p class="MIS-logo">MIS</p>
			</li>
			<li><div class="divider"></div></li>
			<li class="nav-item active "><a class="dashboard_link nav-link"
				href="/adminDashboard"> <i class="material-icons">dashboard</i>
					<p>Dashboard</p>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="/adminDashboard/myAccount"> <i class="material-icons">person</i>
					<p>My Account</p>
			</a></li>
			<!-- <li class="nav-item"><a class="nav-link"
			href="./notifications.html"> <i class="material-icons">notifications</i>
				<p>Notifications</p>
		</a></li> -->
		</ul>
	</div>


	<div id="main_page" class="main_page">
		<div class="row">
			<div class="col s12 m12 l4">
				<div class="card cardHover">
					<a href="#AddStudent" class="black-text modal-trigger"
						ng-click="openAddStudentForm()">
						<div class="card-content  waves-effect waves-block waves-light">
							<span class="card-title activator text-darken-4"> ADD
								STUDENTS<i class="material-icons right"
								style="font-size: 55px;">group_add</i>
							</span>
						</div>
					</a>
				</div>
				<blockquote><div style="font=red;"><h5>{{pleaseTryAgain}}</h5></div></blockquote>
			</div>
		</div>


		<h4>Student Data</h4>
		<div class="divider"></div>

		<div class="row">
			<div class="col s12 m12 l12">
				<div class="card z-depth-4">
					<div class="card-content">
					<span class="card-title"><b>Search Student(s)</b></span>
						
						
						<form>
						<div class = "row">
							
								<div class="input-field col s12 m12 l4">
									<input id="registrationNo" name="registrationNo" type="text" value=""
										class="validate" ng-model="regNo"> <label
										for="registrationNo"> Registration No.</label>
								</div>
								<div class="input-field col s12 m12 l4">
									<input id="name" name="name" type="text" value=""
										class="validate" ng-model="name"> <label
										for="name"> Name</label>
								</div>
								<div class="input-field col s12 m12 l4">
									<input id="department" name="department" type="text" value=""
										class="validate" ng-model="department"> <label
										for="department"> Department</label>
								</div>
								<div class="input-field col s12 m12 l4">
									<input id="degreeLevel" name="degreeLevel" type="text" value=""
										class="validate" ng-model="degreeLevel"> <label
										for="degreeLevel"> Degree Level</label>
								</div>
								<div class="input-field col s12 m12 l4">
									<input id="year" name="year" type="text" value=""
										class="validate" ng-model="year"> <label
										for="year"> Year</label>
								</div>
								
								<div class="input-field col s12 m12 l12 right-align">
								<button class="btn waves-effect waves-light" type="submit"
									name="action" id="pgsSearch"
									ng-click="pgsSearch(regNo,name,department,degreeLevel,year)">
									search <i class="material-icons right">search</i>
								</button>
								
								<a href="{{pgsSheetURL}}" class="btn {{disabled}} waves-effect waves-light" 
									name="action" id="pgsSearchDownload"
									ng-click="pgsSearchDownload(regNo,name,department,degreeLevel,year)">
									download<i class="material-icons right">download</i>
								</a> 
								</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<button class="waves-effect waves-light btn-large #1a237e indigo darken-4 {{disabled}}" id="summary_btn">
		<i class="material-icons left">summarize</i>Summary
		</button>
		<script>
			$('#summary_btn').on('click', function() {
				$('#summary').toggle(1500);
				$("html").animate({scrollTop: $("#summary_btn").offset().top}, 1200);
			});
		</script>
		<div class="divider"></div>
		<br>
		
		<div id="summary" style="display:none;">
			<div class="row">
			<div class="col s12 m12 l12">
				<div class="card z-depth-4">
					<div class="card-content">
					<span class="card-title"><b>Summary</b></span>
							<br>
							<div class="row">
															
								<div class="col s6 m6 l6">
									<table class="responsive-table">
									<tr class="#e0e0e0 grey lighten-2">
										<th>Degree Level</th>
										<th>No of students</th>
									</tr>
									<tr ng-repeat="(key,value) in groupedbydegreeLevel">
										<td>{{key}}</td>
										<td>{{value}}</td>
									</tr>
									</table>
								</div>
								
								<div class="col s6 m6 l6">
								<table class="responsive-table">
									<tr class="#e0e0e0 grey lighten-2">
										<th>Department</th>
										<th>No of students</th>
									</tr>
									<tr ng-repeat="(key,value) in groupedbydepartment">
										<td>{{key}}</td>
										<td>{{value}}</td>
									</tr>
									</table>
								</div>
								
								<div class="col s6 m6 l6">
								<table class="responsive-table">
									<tr class="#e0e0e0 grey lighten-2">
										<th>Year</th>
										<th>No of students</th>
									</tr>
									<tr ng-repeat="(key,value) in groupedbyyear">
										<td>{{key}}</td>
										<td>{{value}}</td>
									</tr>
									</table>
								</div>
								
								<div class="col s12 m12 l12">
									<div class="right-align">
									<h4>Total: <b>{{PGS_Total_count}}</b></h4>
								</div>
							</div>						
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
		
		
		
		<div class="row">
			<div class="col s12 m12 l12">
				<table class="responsive-table">

					<tr class="#bbdefb blue lighten-4">
						<th>Degree level</th>
						<th>Year</th>
						<th>Department</th>
						<th>Reg. No</th>
						<th>Name</th>
						<th>Contact</th>
						<th>Email</th>
						<th>Payment</th>
					</tr>
					<tr ng-repeat="i in pgStudents">
						<td>{{i.type}}</td>
						<td>{{i.year}}</td>
						<td>{{i.department}}</td>
						<td>{{i.registrationNo}}</td>
						<td>{{i.namewithinitials}}</td>
						<td>{{i.contactNumber_s}}</td>
						<td>{{i.emailAddress_s}}</td>
						<td>{{i.payment}}</td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close"
							href="#ViewStudent" value="i.email"
							ng-click="viewPGStudentByRegNo(i.registrationNo)"><i
								class="material-icons">account_box</i></a></td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close blue "
							href="#EditStudent" value="i.email"
							ng-click="editPGStudentByRegNo(i.registrationNo)"><i
								class="material-icons">edit</i></a></td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close  red darken-1"
							href="#DeleteStudent" value="i.email"
							ng-click="deletePGStudent(i.registrationNo)"><i
								class="material-icons">delete</i></a></td>
						
					</tr>
					<tr><td colspan="19" class="center-align"><b>{{NotFoundError}}</b></td></tr>
				</table>
			</div>
		</div>
	</div>

	<!-- ADD STUDENTS -->
	<div id="AddStudent" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12 m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
					<h5>ADD STUDENTS</h5>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s3"><a class="active" href="#one">Add
								Student</a></li>
						<li class="tab col s3"><a href="#two">More Students</a></li>
					</ul>
				</div>
				<div id="one" class="col s12">

					<form id="addStudentForm" enctype="multipart/form-data">
						<div class="raw">
							
							<div class="input-field col s12 m12 l12">
								    <select id="dlevel" name="dlevel" ng-model="ng_dlevel">
								    	<option value="" disabled selected>Choose Degree Level</option>
										<option value="Diploma">Post-graduate Diploma </option>
										<option value="MSc">M.Sc. or M.Sc.Eng.</option>
										<option value="MPhil">MPhil</option>
										<option value="PhD">PhD</option>
								    </select>
								    <label></label>
								</div>
							<div  class="input-field col s12 m12 l6">
								<input id="year" name="year" type="number" 
									class="validate" ng-model="year" >
								<label for="year">Year</label>
							</div>
							<div class="input-field col s12 m12 l6">
								<input id="department" name="department" type="text"
									class="validate" ng-model="department" >
								<label for="department">Department</label>
							</div>
							
							<div class="input-field col s12 m12 l12">
								<input id="registrationNo" name="registrationNo" type="text" class="validate" ng-model="ng_registrationNo" ><label
								
									for="registrationNo"> Registration No </label>
							</div>
							<div id="hideshow_title"
							class="input-field col s12 m12 l4">
								<input id="Title" name="Title" type="text" class="validate"
									ng-model="ng_Title" ><label for="Title">
									Title </label>
							</div>
							<div class="input-field col s12 m12 l8">
								<input id="Namewithinitials" name="Namewithinitials"  type="text" class="validate" ng-model="ng_Namewithinitials" ><label
									for="Namewithinitials"> Name with initials </label>
							</div>
							<div class="input-field col s12 m12 l12">
								<input id="NamedenotedbyInitials" name="NamedenotedbyInitials"
									 type="text" class="validate"
									ng-model="ng_NamedenotedbyInitials" ><label
									for="NamedenotedbyInitials"> Name denoted by Initials
								</label>
							</div>
							<div class="input-field col s12 m12 l12">
								<input id="DateofRegistration" name="DateofRegistration"
									 type="text" class="validate" ng-model="ng_DateofRegistration"
									><label for="DateofRegistration"> Date
									of Registration </label>
							</div>
							<div id="hideshow_id"
							class="input-field col s12 m12 l12">
								<input id="ID" name="ID"  type="text" class="validate"
									ng-model="ng_ID" ><label for="ID"> ID
								</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="ContactNumber_s" name="ContactNumber_s"  type="text" class="validate" ng-model="ng_ContactNumber_s" ><label
									for="ContactNumber_s"> Contact Number/s </label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="EmailAddress_s" name="EmailAddress_s"  type="text" class="validate" ng-model="ng_EmailAddress_s" ><label
									for="EmailAddress_s"> Email Address/s </label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="PostalAddress" name="PostalAddress"  type="text" class="validate" ng-model="ng_PostalAddress" ><label
									for="PostalAddress"> Postal Address </label>
							</div>
							<div id="hideshow_undegree"
							class="input-field col s12 m12 l4">
								<input id="UndergraduateDegree" name="UndergraduateDegree"
									 type="text" class="validate" ng-model="ng_UndergraduateDegree"
									><label for="UndergraduateDegree">
									Undergraduate Degree </label>
							</div>
							<div id="hideshow_unclass"
							class="input-field col s12 m12 l4">
								<input id="UndergraduateClass" name="UndergraduateClass"
									 type="text" class="validate" ng-model="ng_UndergraduateClass"
									><label for="UndergraduateClass">
									Undergraduate Class </label>
							</div>
							<div id="hideshow_ununiversity"
							class="input-field col s12 m12 l4">
								<input id="UndergraduateUniversity"
									name="UndergraduateUniversity"  type="text" class="validate"
									ng-model="ng_UndergraduateUniversity" ><label
									for="UndergraduateUniversity"> Undergraduate
									University </label>
							</div>
							<div id="hideshow_experience"
							class="input-field col s12 m12 l12">
								<input id="Experienceinfield" name="Experienceinfield"
									 type="text" class="validate" ng-model="ng_Experienceinfield"
									><label for="Experienceinfield">
									Experience in field </label>
							</div>
							<div id="hideshow_designation"
							class="input-field col s12 m12 l12">
								<input id="Designation" name="Designation"  type="text" class="validate" ng-model="ng_Designation" ><label
									for="Designation"> Designation </label>
							</div>
							<div id="hideshow_stream"
							class="input-field col s12 m12 l12">
								<input id="Stream" name="Stream"  type="text" class="validate"
									ng-model="ng_Stream" ><label for="Stream">
									Stream </label>
							</div>
							<!-- <div class="input-field col s12 m12 l12">
								<input id="Payment" name="Payment"  type="text" class="validate"
									ng-model="ng_Payment" ><label for="Payment">
									Payment </label>
							</div> -->
							<div id="hideshow_mormeng" class="input-field col s12 m12 l12">
								<input id="MScorMScEng" name="MScorMScEng"  type="text" class="validate" ng-model="ng_MScorMScEng" ><label
									for="MScorMScEng"> M.Sc. Or M.Sc. Eng </label>
							</div>
							<div id="hideshow_partorfull"  class="input-field col s12 m12 l12">
								<input id="PartTimeFullTime" name="PartTimeFullTime"  type="text" class="validate" ng-model="ng_PartTimeFullTime" ><label
									for="PartTimeFullTime"> Part Time Full Time </label>
							</div>
							<div id="hideshow_hdc"
							class="input-field col s12 m12 l12">
								<input id="HDCMeetingNumber" name="HDCMeetingNumber"  type="text" class="validate" ng-model="ng_HDCMeetingNumber" ><label
									for="HDCMeetingNumber"> HDC Meeting Number </label>
							</div>
							<div id="hideshow_alab"
							class="input-field col s12 m12 l12">
								<input id="AffiliatedLaboratory" name="AffiliatedLaboratory"
									 type="text" class="validate" ng-model="ng_AffiliatedLaboratory"
									><label for="AffiliatedLaboratory">
									Affiliated Laboratory </label>
							</div>
							<div id="hideshow_researchtitle"
							class="input-field col s12 m12 l12">
								<input id="ResearchTitle" name="ResearchTitle"  type="text" class="validate" ng-model="ng_ResearchTitle" ><label
									for="ResearchTitle"> Research Title </label>
							</div>
							<div id="hideshow_supervisor_s"
							class="input-field col s12 m12 l12">
								<input id="Supervisor_s" name="Supervisor_s"  type="text" class="validate" ng-model="ng_Supervisor_s" ><label
									for="Supervisor_s"> Supervisor/s </label>
							</div>
							<div class="input-field col s12 m12 l12">
								<input id="Examiner_s" name="Examiner_s"  type="text" class="validate" ng-model="ng_Examiner_s" ><label
									for="Examiner_s"> Examiner/s </label>
							</div>
						</div>
						<div class="row right-align">
							<div class="input-field col s12 m12 l12">
								<input type="submit"
										class="waves-effect waves-light modal-close btn light-blue darken-1 align-right"
										value="submit">
							</div>
						</div>
					</form>
				</div>
				<div id="two" class="col s12">
					<h5>Upload Excel sheet here!</h5>
					<form id="addStudentFormByExcel" enctype="multipart/form-data">
						
						<div class="file-field input-field">
							<div class="btn">
								<span>Excel sheet of Student Details</span> <input type="file"
									accept=".xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
									name="pgStudentExcel" ng-model="pgstudentExcel" required>
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<!-- <div class="raw">
						<div class="input-field col s12 m12 l6">
								<input id="year" name="year" type="number" 
									class="validate" ng-model="year" required="required">
								<label for="year">Year</label>
						</div>
						<div class="input-field col s12 m12 l6">
								<input id="department" name="department" type="text"
									class="validate" ng-model="department" required="required">
								<label for="department">Department</label>
						</div>
						</div> -->
						<div class="input-field col s12 m12 l12">
							<div class="modal-footer">

								<input type="submit"
									class="waves-effect waves-light modal-close btn light-blue darken-1"
									value="submit">

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- DELETE EMPLOYEE -->
	<div id="DeleteStudent" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12  m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>
				<div class="col s11 m11 l11">
					<h5>Do you need to delete this Student's Account?</h5>
					<br> Registration No. : {{deletePGStudentregistrationNo}}<br>
				</div>
			</div>
			<div class="raw">
				<form>
					<div class="right-align">
						<button
							class="btn waves-effect waves-light modal-close blue-text blue lighten-5">
							No</button>
						<button
							class="btn waves-effect waves-light modal-close red darken-1"
							type="submit" name="action"
							ng-click="deleteStudentByRegistrationNo(deletePGStudentregistrationNo)">Delete</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- VIEW PG STUDENT-->
	<div id="ViewStudent" class="modal model_size printable" >
		<div class="modal-content">
			<div class="raw">
				<div class="col s12  m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>
			<div id="studentProfile">
			
				<div class="col s12 m12 l12" >
					<table  class="studentTable responsive-table" >
						<tr>
							<td rowspan="3" ><b>_____________</b></td>
							<td ><b>Registration No:</b></td>
							<td colspan="2">{{V_registrationNo}}</td>
						</tr>
						<tr>
							<td><b>Name with initials:</b></td>
							<td colspan="2">{{V_title}} {{V_nameWithInitials}}</td>
						</tr>
						<tr>
							<td><b>Name denoted by Initials:</b></td>
							<td colspan="2">{{V_nameDenotedByInitials}}</td>
						</tr>
						<tr>
							<td ><b>PhD/Mphil/MSc/Diploma:</b></td>
							<td >{{V_type}}</td>
							<td  style="display:{{display_hideshow_partorfull}};"
							><b>Part Time/Full Time:</b></td>
							<td  style="display:{{display_hideshow_partorfull}};"
							>{{V_partTimeOrFullTime}}</td>
						</tr>
						<tr>
							<td><b>Department:</b></td>
							<td>{{V_department}}</td>
							<td style="display:{{display_hideshow_hdc}};"
							><b>HDC Meeting Number:</b></td>
							<td style="display:{{display_hideshow_hdc}};"
							>{{V_hDCMeetingNumber}}</td>
						</tr>
						<tr>
							<td style="display:{{display_hideshow_stream}};"
							><b>Stream:</b></td>
							<td style="display:{{display_hideshow_stream}};"
							>{{V_stream}}</td>
							<td style="display:{{display_hideshow_alab}};"
							><b>Affiliated Laboratory:</b></td>
							<td style="display:{{display_hideshow_alab}};"
							>{{V_affiliatedLaboratory}}</td>
						</tr>
						<tr>
							<td><b>Programme:</b></td>
							<td>{{V_programme}}</td>
							<td style="display:{{display_hideshow_researchtitle}};"
							><b>Research Title:</b></td>
							<td style="display:{{display_hideshow_researchtitle}};"
							>{{V_researchTitle}}</td>
							
						</tr>
						<tr>
							<td><b>Date of Registration:</b></td>
							<td>{{V_dateofRegistration}}</td>
							<td style="display:{{display_hideshow_supervisor_s}};"
							><b>Supervisor(s):</b></td>
							<td style="display:{{display_hideshow_supervisor_s}};"
							>{{V_supervisor_s}}</td>
							
						</tr>
						<tr>
							<td  ><b>Examiner(s):</b></td>
							<td colspan="3" >{{V_examiner_s}}</td>
						</tr>
						<tr></tr>
						<tr>
							<th colspan="4" style="border: 1px solid black; text-align: center; background-color: #f2f2f2;"> <h5>Personal Information </h5></th>
						</tr>
						<tr>
							<td><b>Postal Address:</b></td>
							<td>{{V_postalAddress}}</td>
							<td style="display:{{display_hideshow_id}};"><b>NIC:</b></td>
							<td style="display:{{display_hideshow_id}};">{{V_nic}}</td>
							
						</tr>
						<tr>
							<td><b>Email Address:</b></td>
							<td>{{V_emailAddress}}</td>
							<td style="display:{{display_hideshow_designation}};"
							><b>Designation:</b></td>
							<td style="display:{{display_hideshow_designation}};"
							>{{V_designation}}</td>
							
						</tr>
						<tr>
							<td><b>Contact Number(s):</b></td>
							<td>{{V_contactNumber_s}}</td>
							<td></td>
							<td></td>
							
						</tr>
						<tr style="display:{{display_hideshow_edu}};">
							<th colspan="4" style="border: 1px solid black; text-align: center; background-color: #f2f2f2;"><h5>Educational Information </h5></th>
						</tr>
						<tr style="display:{{display_hideshow_edu}};">
							<td style="display:{{display_hideshow_undegree}};"
							><b>Undergraduate Degree:</b></td>
							<td style="display:{{display_hideshow_undegree}};"
							>{{V_undergraduateDegree}}</td>
							<td style="display:{{display_hideshow_ununiversity}};"
							><b>Undergraduate University:</b></td>
							<td style="display:{{display_hideshow_ununiversity}};"
							>{{V_undergraduateUniversity}}</td>
							
						</tr>
						<tr>
							<td style="display:{{display_hideshow_unclass}};"
							><b>Undergraduate Class:</b></td>
							<td style="display:{{display_hideshow_unclass}};"
							>{{V_undergraduateClass}}</td>
							<td style="display:{{display_hideshow_experience}};"
							><b>Experience in field:</b></td>
							<td style="display:{{display_hideshow_experience}};"
							>{{V_experienceinfield}}</td>
							
						</tr>
						<tr>
							<th colspan="4" style="border: 1px solid black; text-align: center; background-color: #f2f2f2;"><h5>Payment Information </h5></th>
						</tr>
						<tr>
							<td><b>Course Fee:</b></td>
							<td>{{V_courseFee}}</td>
							<td><b>Payment(first year):</b></td>
							<td>{{V_paymentfirstyear}}</td>
							
						</tr>
						<tr>
							<td><b>Library Fee:</b></td>
							<td>{{V_libraryFee}}</td>
							<td><b>Payment(second year):</b></td>
							<td>{{V_paymentsecondyear}}</td>
							
						</tr>
						<tr>
							<td><b>Tuition Fee:</b></td>
							<td>{{V_tuitionFee}}</td>
							<td><b>Payment(third year):</b></td>
							<td>{{V_paymentthirdyear}}</td>
							
						</tr>
						<tr>
							<td><b>Registration fee:</b></td>
							<td>{{V_registrationfee}}</td>
							<td><b>Payment(fourth year):</b></td>
							<td>{{V_paymentfourthyear}}</td>
							
						</tr>
						<tr>
							<td><b>Programme fee:</b></td>
							<td>{{V_programmefee}}</td>
							<td><b>Total Payment Made:</b></td>
							<td>{{V_totalPaymentMade}}</td>
							
						</tr>
					</table>
				</div>
			</div>
			</div>
			<div class="raw">
				<form>
					<div class="right-align">
						<!-- <button onclick="window.print(); return false;"><i class="material-icons">print</i></button>
						 -->
						 <div id="studentRegNo" style="display: none">{{V_documentName}}</div>
						 <button onclick="printDiv('studentProfile'); return false;" class="btn waves-effect waves-light"><i class="material-icons">print</i></button>
						 <!-- <button
							class="btn waves-effect waves-light modal-close blue-text blue lighten-5">
							No</button>
						<button
							class="btn waves-effect waves-light modal-close red darken-1"
							type="submit" name="action"
							ng-click="deleteStudentByRegistrationNo(deletePGStudentregistrationNo)">Delete</button> -->
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Edit PG STUDENT -->
	<div id="EditStudent" class="modal model_size printable" >
		<div class="modal-content">
			<div class="raw">
				<div class="col s12  m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>
			<div id="studentEditProfile">
				<div class="col s12 m12 l12" >
					<form id="formEditPGStudent" enctype="multipart/form-data">
					
					<input id="registrationNo" name="registrationNo" type="hidden" value="{{V_registrationNo}}">
					<table  class="studentEditTable responsive-table" >
						<tr>
							<td rowspan="3" ><b>_____________</b></td>
							<td ><b>Registration No:</b></td>
							<td colspan="2" >{{V_registrationNo}}</td>
						</tr>
						<tr>
							<td><b>Name with initials:</b></td>
							<td>
							<div style="display:{{display_hideshow_title}};">
								<label><b>{{V_title}}</b> selected</label>
								
								<select  id="title" name="title"> 
												<option value="{{V_title}}" disabled selected>Choose title(Mr/Ms/Rev)</option>
												<option value="Mr.">Mr.</option>
												<option value="Ms." >Ms.</option>
												<option value="Rev.">Rev.</option>
								</select>
							</div>
								
							<input id="namewithinitials" name="namewithinitials" type="text" value="{{V_nameWithInitials}}">
							</td>
						</tr>
						<tr>
							<td><b>Name denoted by Initials:</b></td>
							<td colspan="2"><input id="namedenotedbyInitials" name="namedenotedbyInitials" type="text" value="{{V_nameDenotedByInitials}}"></td>
						</tr>
						<tr>
							<td ><b>PhD/Mphil/MSc/Diploma:</b></td>
							<td ><label>{{V_type}} selected</label>
								<select id="type" name="type" ng-model="V_type">
										<option value="" disabled selected>choose PhD/Mphil/MSc/Diploma</option>
										<option value="PhD">PhD</option>
										<option value="Mphil">Mphil</option>
										<option value="MSc">MSc</option>
										<option value="Diploma">Diploma</option>
								</select>
							</td>
							<td  style="display:{{display_hideshow_partorfull}};"
							><b>Part Time/Full Time:</b></td>
							<td  style="display:{{display_hideshow_partorfull}};"
							><label>{{V_partTimeOrFullTime}}</label>
								<select id="partTimeOrFullTime" name="partTimeOrFullTime" ng-model="V_partTimeOrFullTime">
										<option value="" disabled selected>choose Part Time(PT)/Full Time(FT)</option>
										<option value="PT">Part Time</option>
										<option value="FT">Full Time</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><b>Department:</b></td>
							<td><input id="department" name="department" type="text" value="{{V_department}}"></td>
							<td style="display:{{display_hideshow_hdc}};"
							><b>HDC Meeting Number:</b></td>
							<td style="display:{{display_hideshow_hdc}};"
							><input id="hDCMeetingNumber" name="hDCMeetingNumber" type="text" value="{{V_hDCMeetingNumber}}"></td>
							
						</tr>
						<tr>
							<td style="display:{{display_hideshow_stream}};"
							><b>Stream:</b></td>
							<td style="display:{{display_hideshow_stream}};"
							><input id="stream" name="stream" type="text" value="{{V_stream}}"></td>
							<td style="display:{{display_hideshow_alab}};"
							><b>Affiliated Laboratory:</b></td>
							<td style="display:{{display_hideshow_alab}};"
							><input id="affiliatedLaboratory" name="affiliatedLaboratory" type="text" value="{{V_affiliatedLaboratory}}"></td>
							
						</tr>
						<tr>
							<td><b>Programme:</b></td>
							<td><input id="programme" name="programme" type="text" value="{{V_programme}}"></td>
							<td style="display:{{display_hideshow_researchtitle}};"
							><b>Research Title:</b></td>
							<td style="display:{{display_hideshow_researchtitle}};"
							><input id="researchTitle" name="researchTitle" type="text" value="{{V_researchTitle}}"></td>
							
						</tr>
						<tr>
							<td><b>Date of Registration:</b></td>
							<td><input id="dateofRegistration" name="dateofRegistration" type="text" value="{{V_dateofRegistration}}"></td>
							<td style="display:{{display_hideshow_supervisor_s}};"
							><b>Supervisor(s):</b></td>
							<td style="display:{{display_hideshow_supervisor_s}};"
							><input id="supervisor_s" name="supervisor_s" type="text" value="{{V_supervisor_s}}"></td>
							
						</tr>
						<tr>
							<td><b>Examiner(s):</b></td>
							<td colspan="2"><input id="examiner_s" name="examiner_s" type="text" value="{{V_examiner_s}}"></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<th colspan="4" style="border: 1px solid black; text-align: center; background-color: #f2f2f2;"> <h5>Personal Information </h5></th>
						</tr>
						<tr>
							<td><b>Postal Address:</b></td>
							<td><input id="postalAddress" name="postalAddress" type="text" value="{{V_postalAddress}}"></td>
							<td style="display:{{display_hideshow_id}};"><b>NIC:</b></td>
							<td style="display:{{display_hideshow_id}};"><input id="id" name="id" type="text" value="{{V_nic}}"></td>
						</tr>
						<tr>
							<td><b>Email Address:</b></td>
							<td><input id="emailAddress_s" name="emailAddress_s" type="text" value="{{V_emailAddress}}"></td>
							<td style="display:{{display_hideshow_designation}};"
							><b>Designation:</b></td>
							<td style="display:{{display_hideshow_designation}};"
							><input id="designation" name="designation" type="text" value="{{V_designation}}"></td>
							
						</tr>
						<tr>
							<td><b>Contact Number(s):</b></td>
							<td><input id="contactNumber_s" name="contactNumber_s" type="text" value="{{V_contactNumber_s}}"></td>
							<td></td>
							<td></td>
							
						</tr>
						<tr>
							<th colspan="4" style="border: 1px solid black; text-align: center; background-color: #f2f2f2;"><h5>Educational Information </h5></th>
						</tr>
						<tr>
							<td style="display:{{display_hideshow_undegree}};"
							><b>Undergraduate Degree:</b></td>
							<td style="display:{{display_hideshow_undegree}};"
							><input id="undergraduateDegree" name="undergraduateDegree" type="text" value="{{V_undergraduateDegree}}"></td>
							<td style="display:{{display_hideshow_ununiversity}};"
							><b>Undergraduate University:</b></td>
							<td style="display:{{display_hideshow_ununiversity}};"
							><input id="undergraduateUniversity" name="undergraduateUniversity" type="text" value="{{V_undergraduateUniversity}}"></td>
							
						</tr>
						<tr>
							<td style="display:{{display_hideshow_unclass}};"
							><b>Undergraduate Class:</b></td>
							<td style="display:{{display_hideshow_unclass}};"
							><input id="undergraduateClass" name="undergraduateClass" type="text" value="{{V_undergraduateClass}}"></td>
							<td><b>Experience in field:</b></td>
							<td><input id="experienceinfield" name="experienceinfield" type="text" value="{{V_experienceinfield}}"></td>
							
						</tr>
						<tr>
							<th colspan="4" style="border: 1px solid black; text-align: center; background-color: #f2f2f2;"><h5>Payment Information </h5></th>
						</tr>
						<tr>
							<td><b>Course Fee:</b></td>
							<td><input id="courseFee" name="courseFee" type="number" value="{{V_courseFee}}"></td>
							<td><b>Payment(first year):</b></td>
							<td><input id="firstYearPaymeent" name="firstYearPaymeent" type="number" value="{{V_paymentfirstyear}}"></td>
							
						</tr>
						<tr>
							<td><b>Library Fee:</b></td>
							<td><input id="libraryFee" name="libraryFee" type="number" value="{{V_libraryFee}}"></td>
							<td><b>Payment(second year):</b></td>
							<td><input id="secondYearPaymeent" name="secondYearPaymeent" type="number" value="{{V_paymentsecondyear}}"></td>
							
						</tr>
						<tr>
							<td><b>Tuition Fee:</b></td>
							<td><input id="tuitionFee" name="tuitionFee" type="number" value="{{V_tuitionFee}}"></td>
							<td><b>Payment(third year):</b></td>
							<td><input id="thirdYearPaymeent" name="thirdYearPaymeent" type="number" value="{{V_paymentthirdyear}}"></td>
							
						</tr>
						<tr>
							<td><b>Registration fee:</b></td>
							<td><input id="registrationFee" name="registrationFee" type="number" value="{{V_registrationfee}}"></td>
							<td><b>Payment(fourth year):</b></td>
							<td><input id="fourthYearPaymeent" name="fourthYearPaymeent" type="number" value="{{V_paymentfourthyear}}"></td>
							
						</tr>
						<tr>
							<td><b>Programme fee:</b></td>
							<td><input id="programmeFee" name="programmeFee" type="number" value="{{V_programmefee}}"></td>
							<td><b>Total Payment Made:</b></td>
							<td><input id="totalPaymentMade" name="totalPaymentMade" type="number" value="{{V_totalPaymentMade}}"></td>
							
						</tr>
					</table>
					
					<div class="input-field col s12 m12 l12 right-align">

							<input type="reset"
								class="waves-effect waves-light btn #e8eaf6 indigo lighten-5"
								value="Reset"> <input type="submit"
								class="waves-effect waves-light btn light-blue darken-5"
								value="submit">

						</div>
				</form>
				</div>
			</div>
			</div>
			
		</div>
	</div>
</body>

<script>
	$('#addStudentForm')
	.submit(
			function(e) {
				$
						.ajax(
								{
									url : '/adminDashboard/addPGStudent',
									type : 'POST',
									data : new FormData(this),
									processData : false,
									contentType : false
								})
						.done(
								function(data) {
									
									showToastr('success',
											'Successfully uploaded!',
											"<br><br><b> Dismiss </b>");
									$('#addStudentForm').trigger("reset");
									//$("body").load(location.href + " body");
									location.reload();
								})
						.fail(
								function(errMsg) {
									
									showToastr('error',
											'Something went wrong!',
											"Please try again! <br><br><b> Dismiss </b>");
								});
				e.preventDefault();
			});
	$('#formEditPGStudent')
	.submit(
			function(e) {
				$
						.ajax(
								{
									url : '/adminDashboard/editPGStudent',
									type : 'PUT',
									data : new FormData(this),
									processData : false,
									contentType : false
								})
						.done(
								function(data) {
									
									showToastr('success',
											'Successfully uploaded!',
											"<br><br><b> Dismiss </b>");
									$('#addStudentForm').trigger("reset");
									//$("body").load(location.href + " body");
									location.reload();
								})
						.fail(
								function(errMsg) {
									
									showToastr('error',
											'Something went wrong!',
											"Please try again! <br><br><b> Dismiss </b>");
								});
				e.preventDefault();
			});
	

	$('#addStudentFormByExcel')
	.submit(
			function(e) {
				$
						.ajax(
								{
									url : '/adminDashboard/addPGStudentsByExcel',
									type : 'POST',
									data : new FormData(this),
									processData : false,
									contentType : false
								})
						.done(
								function(data) {
									showToastr('success',
											'Successfully uploaded!',
											"<br><br><b> Dismiss </b>");
									$('#addStudentFormByExcel').trigger("reset");
									location.reload();
								})
						.fail(
								function(errMsg) {
									showToastr('error',
											'Something went wrong!',
											"Please try again! <br><br><b> Dismiss </b>");
								});
				e.preventDefault();
			});
	
	$('#one_form')
	.submit(
			function(e) {
				$("#one_form")[0].reset();
				e.preventDefault();
			});
	
	
	$body = $("body");

	$(document).on({
		ajaxStart : function() {
			$body.addClass("loading");
		},
		ajaxStop : function() {
			$body.removeClass("loading");
		}
	});

	function showToastr(type, title, messsage) {
		let body;
		toastr.options = {
			"closeButton" : false,
			"debug" : false,
			"newestOnTop" : false,
			"progressBar" : false,
			"positionClass" : 'toast-bottom-center',
			"preventDuplicates" : true,
			"showDuration" :"300",
			"hideDuration" :"1000",
			"timeOut" : 5000,
			"onclick" : null,
			"onCloseClick" : null,
			"extendedTimeOut" : 1000,
			"showEasing" :"swing",
			"hideEasing" :"linear",
			"showMethod" :"fadeIn",
			"hideMethod" :"fadeOut",
			"tapToDismiss" : true
		};
		switch (type) {
		case"info":
			body ="<span> <i class='fa fa-spinner fa-pulse'></i></span>";
			break;
		default:
			body = '';
		}
		const content = messsage + body;
		toastr[type](content, title)
	}
	
	$(document).ready(function() {
		 $("#dlevel").on('change',function(){
			 //console.log($("#dlevel").val());
			 if($("#dlevel").val() == 'Diploma'){
				 $("#hideshow_title").show();
				 $("#hideshow_id").show();
				 $("#hideshow_partorfull").hide();
				 $("#hideshow_hdc").hide();
				 $("#hideshow_undegree").show();
				 $("#hideshow_unclass").show();
				 $("#hideshow_ununiversity").show();
				 $("#hideshow_experience").show();
				 $("#hideshow_designation").show();
				 $("#hideshow_stream").show();
				 $("#hideshow_mormeng").hide();
				 $("#hideshow_alab").hide();
				 $("#hideshow_researchtitle").hide();
				 $("#hideshow_supervisor_s").hide();
			 }
			 
			 if($("#dlevel").val() == 'MSc'){
				 $("#hideshow_title").show();
				 $("#hideshow_id").show();
				 $("#hideshow_partorfull").hide();
				 $("#hideshow_hdc").hide();
				 $("#hideshow_undegree").show();
				 $("#hideshow_unclass").show();
				 $("#hideshow_ununiversity").show();
				 $("#hideshow_experience").show();
				 $("#hideshow_designation").show();
				 $("#hideshow_stream").show();
				 $("#hideshow_mormeng").show();
				 $("#hideshow_alab").hide();
				 $("#hideshow_researchtitle").hide();
				 $("#hideshow_supervisor_s").hide();
			 }
			 
			 if($("#dlevel").val() == 'MPhil'){
				 $("#hideshow_title").hide();
				 $("#hideshow_id").hide();
				 $("#hideshow_partorfull").show();
				 $("#hideshow_hdc").show();
				 $("#hideshow_undegree").hide();
				 $("#hideshow_unclass").hide();
				 $("#hideshow_ununiversity").hide();
				 $("#hideshow_experience").hide();
				 $("#hideshow_designation").hide();
				 $("#hideshow_stream").show();
				 $("#hideshow_mormeng").hide();
				 $("#hideshow_alab").show();
				 $("#hideshow_researchtitle").show();
				 $("#hideshow_supervisor_s").show();
			 }
			 
			 if($("#dlevel").val() == 'PhD'){
				 $("#hideshow_title").hide();
				 $("#hideshow_id").hide();
				 $("#hideshow_partorfull").show();
				 $("#hideshow_hdc").show();
				 $("#hideshow_undegree").hide();
				 $("#hideshow_unclass").hide();
				 $("#hideshow_ununiversity").hide();
				 $("#hideshow_experience").hide();
				 $("#hideshow_designation").hide();
				 $("#hideshow_stream").hide();
				 $("#hideshow_mormeng").hide();
				 $("#hideshow_alab").show();
				 $("#hideshow_researchtitle").show();
				 $("#hideshow_supervisor_s").show();
			 }
		 });
	 });
</script>
</html>