<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="PageDetails">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/admin.css" rel="stylesheet">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
	
<link rel="stylesheet" href="css/toastr.min.css">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>

<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $('.modal').modal();
	    $('#AddNotice').on('click', function() {
		});
		$("#AddNotice").modal({
			dismissible : false
		});
		$('#EditNotice').on('click', function() {
		});
		$("#EditNotice").modal({
			dismissible : false
		});
		$('#DeleteNotice').on('click', function() {
		});
		$("#DeleteNotice").modal({
			dismissible : false
		});
	  });
	$(document).ready(function(){
	    $('.sidenav').sidenav({
	    	draggable:true
	    });
	    $('.modal').modal();
	    $('.dropdown-trigger').dropdown();
	  });
	$('#left-side').on("click",".dashboard_link",function(e){ 
		  e.preventDefault(); // cancel click
		  var page = $(this).attr('href');   
		  $('body').fadeIn('slow', function(){
			 // $('body').fadeOut(300);
			  $('body').load(page);
			  window.history.pushState("Details", "Title", "/adminDashboard");
		  });
		});
	$(document).ready(function() {
		$('.tabs').tabs();
	});
	</script>
<body ng-controller="PageData">

	<div id="top">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="/logout">Logout</a></li>
		</ul>
		<nav class="navbar">
			<div class="nav-wrapper">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i
					class="material-icons">menu</i></a>
				<ul class="right">
					<!--  <li><a href="sass.html">Sass</a></li>
		      <li><a href="badges.html">Components</a></li> -->
					<!-- Dropdown Trigger -->
					<li><a class="dropdown-trigger" href="#!"
						data-target="dropdown1">${first_name} ${last_name}<i
							class="material-icons right">arrow_drop_down</i></a></li>
				</ul>
			</div>
		</nav>
	</div>

	<div id="left-side">
		<ul id="slide-out" class="sidenav sidenav-fixed z-depth-4">
			<li>

				<p class="MIS-logo">MIS</p>
			</li>
			<li><div class="divider"></div></li>
			<li class="nav-item active  "><a class="dashboard_link nav-link"
				href="/adminDashboard"> <i class="material-icons">dashboard</i>
					<p>Dashboard</p>
			</a></li>
			<li class="nav-item "><a class="nav-link"
				href="/adminDashboard/myAccount"> <i class="material-icons">person</i>
					<p>My Account</p>
			</a></li>
			<!-- <li class="nav-item "><a class="nav-link"
				href="./notifications.html"> <i class="material-icons">notifications</i>
					<p>Notifications</p>
			</a></li> -->
		</ul>
	</div>


	<div id="main_page" class="main_page">
	<h5>Manage Notices</h5>
		<div class="divider"></div>
		
		<!--  Add Notices Button -->
		<div class="row">
			<div class="col s12 m12 l4">
				<div class="card cardHover">
					<!-- <div class="card-image waves-effect waves-block waves-light">
                   image link 
                 </div> -->
					<a href="#AddNotice" class="black-text modal-trigger">
						<div class="card-content  waves-effect waves-block waves-light">
							<span class="card-title activator text-darken-4"> ADD
								NOTICE<i class="material-icons right"
								style="font-size: 55px;">post_add</i>
							</span>
						</div>
					</a>
				</div>
			</div>
		</div>
		
			<div class="col s12 m12 l12">
				<ul class="tabs" style="font: bold;">
					<li class="tab col s2"><a class="active" href="#Important">Important Notices</a></li>
					<li class="tab col s2"><a href="#General">General Notices</a></li>
				</ul>
			</div>
		<div id="Important">
			<div class="row">
			<div class="col s12 m12 l12">
			<h6>Important Notices</h6>
				<table class="responsive-table">
					<tr class="#bbdefb pink lighten-4">
						<th>Notice Id</th>
						<th>Date Time</th>
						<th>Notice</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
					<tr ng-repeat="i in importantNotices">
						<td>{{i.noticeID}}</td>
						<td>{{i.noticeDateTime}}</td>
						<td>{{i.notice}}</td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close  blue darken-1"
							href="#EditNotice" 
							ng-click="getNoticeDetailsById(i.noticeID)"><i
								class="material-icons">edit</i></a></td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close  red darken-1"
							href="#DeleteNotice" 
							ng-click="getDeleteNoticeDetails(i.noticeID)"><i
								class="material-icons">delete</i></a></td>
								
					</tr>
				</table>
			</div>
			</div>
		</div>
		<div id="General">
			<div class="row">
			<div class="col s12 m12 l12">
			<h6>General Notices</h6>
				<table class="responsive-table">
					<tr class="#bbdefb blue lighten-4">
						<th>Notice Id</th>
						<th>Date Time</th>
						<th>Notice</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
					<tr ng-repeat="i in NotImportantNotices">
						<td>{{i.noticeID}}</td>
						<td>{{i.noticeDateTime}}</td>
						<td>{{i.notice}}</td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close  blue darken-1"
							href="#EditNotice" 
							ng-click="getNoticeDetailsById(i.noticeID)"><i
								class="material-icons">edit</i></a></td>
						<td><a
							class="waves-effect waves-light btn modal-trigger modal-close  red darken-1"
							href="#DeleteNotice" 
							ng-click="getDeleteNoticeDetails(i.noticeID)"><i
								class="material-icons">delete</i></a></td>
								
					</tr>
					<tr><td colspan="19" class="center-align"><b>{{NotFoundError}}</b></td></tr>
				</table>
			</div>
			</div>
		</div>
	</div>
</body>

<!--  Add Notice -->
<div id="AddNotice" class="modal">
	<div class="modal-content">
		<div class="raw">
				<div class="col s12 m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
					<h5>ADD NOTICES</h5>
				</div>
		</div>

		<div class="row">
				<div id="one" class="col s12">
					<form id="addForm">
						<div class="input-field col s12 m12 l4">
							 <p>
						      <label>
						        <input ng-model="noticeType" value="Important" name="noticeType" type="radio" checked/>
						        <span>Important</span>
						      </label>
						    </p>
						    <p>
						      <label>
						        <input ng-model="noticeType" value="Not important" name="noticeType" type="radio" />
						        <span>General (not important)</span>
						      </label>
						    </p>
					    </div>
					    <br>
					    <div class="input-field col s12 m12 l4">
							 <p>
						      <label>
						        <input ng-model="noticeSection" value="Transcript" name="noticeSection" type="radio" checked/>
						        <span>Transcript</span>
						      </label>
						    </p>
						    <p>
						      <label>
						        <input ng-model="noticeSection" value="Verification" name="noticeSection" type="radio" />
						        <span>Verification</span>
						      </label>
						    </p>
						      <p>
						      <label>
						        <input ng-model="noticeSection" value="All" name="noticeSection" type="radio" />
						        <span>For all</span>
						      </label>
						    </p>
					    </div>
					    
						<div class="row">
							<div class="input-field col s12 m12 l12">
								<textarea  id="notice" name="notice" type="text" ng-model="notice" class="materialize-textarea" required="required"></textarea>
          						<label for="notice">Notice text</label>
							</div>
							
							<div class="input-field col s12 m12 l12">
							<div class="modal-footer">

								<input type="submit" class="btn waves-effect waves-light modal-close" name="action"
									ng-click="addNotice(notice, noticeType, noticeSection)">

							</div>
						</div>
						</div>
					</form>
				</div>
		</div>
	</div>
</div>

<!--  Edit Notice -->
<div id="EditNotice" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12 m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
					<h5>UPDATE NOTICE</h5>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s3"><a class="active" href="#one">Edit
								Notice</a></li>
						<!-- <li class="tab col s3"><a href="#two">More Employees</a></li> -->
					</ul>
				</div>
				<div id="one" class="col s12">
					<form id="updateform">
						<div class="input-field col s12 m12 l4">
							 <p>Selected type: <span style="color: red; font-size: 12pt;"><b>{{selected_notice_type}}</b></span></p>
							 <input id="noticeID" name="noticeID" type="hidden"
									class="validate" ng-model="notice_id" value="{{notice_id}}" required>
							 <p>
						      <label>
						        <input ng-model="notice_type" value="Important" name="noticeType" type="radio" checked />
						        <span>Important</span>
						      </label>
						    </p>
						    <p>
						      <label>
						        <input ng-model="notice_type" value="Not important" name="noticeType" type="radio" />
						        <span>General (not important)</span>
						      </label>
						    </p>
					    </div>
						<div class="row">
							<div class="input-field col s12 m12 l12">
								<textarea  id="notice" placeholder="" name="notice" type="text" ng-model="notice_text" value="{{notice_text}}" class="materialize-textarea"></textarea>
          						<label for="notice">Notice text</label>
							</div>
							
							<div class="input-field col s12 m12 l12">
							<div class="modal-footer">

								<input type="submit" class="btn waves-effect waves-light modal-close" name="action"
									ng-click="editNotice(notice_id ,notice_text, notice_type)">

							</div>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<!-- Delete Notice -->
<div id="DeleteNotice" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12  m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>
				<div class="col s11 m11 l11">
					<h5>Do you need to delete this Notice?</h5>
					<br> Notice ID : {{d_notice_id}}<br>
					<br> Notice Text : {{d_notice_text}}
					<br> Notice Type : {{d_notice_type}}
				</div>
			</div>
			<div class="raw">
				<form>
					<div class="right-align">
						<button
							class="btn waves-effect waves-light modal-close blue-text blue lighten-5">
							No</button>
						<button
							class="btn waves-effect waves-light modal-close red darken-1"
							type="submit" name="action"
							ng-click="deleteNotice(d_notice_id)">Delete</button>
					</div>
				</form>
			</div>
		</div>
	</div>


<!-- <script>

$( '#addForm' )
.submit( function( e ) {
  $.ajax( {
    url: '/adminDashboard/Notices/createNotice',
	type : 'POST',
	data : new FormData(this),
	processData : false,
	contentType : false
}).done(function(data){
	showToastr('success', 'Successfull!', "<br><br><b> Dismiss </b>");
	$("#form")[0].reset();})
 .fail(function(errMsg) {showToastr('error', 'Something went wrong!', "<br><br>Please try again!</b>");});
e.preventDefault();

});

$body = $("body");

$(document).on({
ajaxStart : function() {
	$body.addClass("loading");
},
ajaxStop : function() {
	$body.removeClass("loading");
}
});

function showToastr(type, title, messsage) {
    let body;
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": 'toast-bottom-center',
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 5000,
        "onclick": null,
        "onCloseClick": null,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": true
    };
    switch(type){
        case "info": body = "<span> <i class='fa fa-spinner fa-pulse'></i></span>";
            break;
        default: body = '';
    }
    const content = messsage + body;
    toastr[type](content, title)
}
</script> -->

</html>
