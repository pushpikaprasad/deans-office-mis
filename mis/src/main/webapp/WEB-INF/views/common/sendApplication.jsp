<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/materialize.css" rel="stylesheet">
<link href="css/application.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link rel="stylesheet" href="css/toastr.min.css">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.tabs').tabs();
		$('select').formSelect();
		$('input#input_text, textarea#textarea1').characterCounter();
	});
	
	
	
</script>
<style type="text/css">
/*LOADING*/
/* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
.modal-looading {
	display: none;
	position: fixed;
	z-index: 1000;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background: rgba(255, 255, 255, .8) url(/img/ajax-loader.gif) 50% 50%
		no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal-looading {
	overflow: hidden;
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal-looading {
	display: block;
}

.input-field label {
	color: #212121 !important;
}

.material-icons{
    display: inline-flex;
    vertical-align: top;
}

</style>
</head>
<body>
	<div id="main_page">
		<div style="margin: 10% 0% 0% 5%;">
			<h5>Application Form</h5>
			<div class="divider"></div>
		</div>

		<br> <br>
		<div class="row">

			<div class="col s12 m10 offset-m1 l10 offset-l1">
				<ul class="tabs">
					<li class="tab col s3"><a class="active" href="#application1">TRANSCRIPT/ACADEMIC
							RANK</a></li>
				</ul>
			</div>

			<div id="application1"
				class=" card z-depth-5 col s12 m10 offset-m1 l10 offset-l1">
				<div class="card-content">
					<div class="raw transparent">
					<form id="form" enctype="multipart/form-data">
						<div class="row center">
							<h5>
								FACULTY OF ENGINEERING - UNIVERSITY OF PERADENIYA<br>
								APPLICATION FOR TRANSCRIPT/ACADEMIC RANK CERTIFICATE
							</h5>
							<br>
							<br>
						</div>
						<div class=" card z-depth-2 hoverable">
							<!-- <div class="card-content">
								<p class="flow text" style="color: black;">
									<b>IMPORTANT:</b>&nbsp&nbsp Please insert a <b>valid email
									address</b>. After you enter the data, you will receive an email of
									your application. If you will not receive any email please
									submit again or contact Dean's office.
								</p>
							</div> -->
							<div class="card-content">
								<p class="flow text" style="display:inline; color: green;">
									<c:if test="${fn:length(general_noticesTranscript) > 0}">
									 	<b><i class="material-icons">info</i> Application information</b>
									
									<table>
										<c:forEach begin="0" end="${fn:length(general_noticesTranscript) - 1}" var="index">
										   <tr>
										      <td><b><i class="material-icons">star_half</i><c:out value="${general_noticesTranscript[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
									<c:if test="${fn:length(general_noticesAll) > 0}">
									 	
									<table>
										<c:forEach begin="0" end="${fn:length(general_noticesAll) - 1}" var="index">
										   <tr>
										      <td><b><i class="material-icons">star_half</i><c:out value="${general_noticesAll[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
								 </p>
								
							</div>
							
							<div class="card-content">
								<p class="flow text" style="display:inline; color: red;">
									<c:if test="${fn:length(important_noticesTranscript) > 0}">
									 	<b><i class="material-icons">error_outline</i> Important notice</b>
									
									<%-- ${important_notices} --%>
									<table>
										<c:forEach begin="0" end="${fn:length(important_noticesTranscript) - 1}" var="index">
										   <tr>
										      <td><b><i class="material-icons">star_half</i><c:out value="${important_noticesTranscript[index]}" escapeXml="false"/></b></td>
										   </tr>
										   
										</c:forEach>
										
									</table>
									</c:if>
									<c:if test="${fn:length(important_noticesAll) > 0}">
									 	
									<table>
										<c:forEach begin="0" end="${fn:length(important_noticesAll) - 1}" var="index">
										   <tr>
										      <td><b><i class="material-icons">star_half</i><c:out value="${important_noticesAll[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
								 </p>
							</div>
							
							
						</div>
						
						
						<div class="row">
							<h5>Personal Details</h5>
							<div class="divider"></div>
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5">
								<div class="input-field col s12 m6 l6">
									<input id="email" name="email" type="email" required
										class="validate" size="320" maxlength="64"> <label for="email">Email:</label>
								</div>
								<div class="input-field col s12 m6 l6">
									<input id="mobile" name="mobile" type="tel" required pattern="[+0-9]{10,}" title="Please enter enter valid phone number with minimum 10 digits"
										class="validate"> <label for="mobile">Mobile:</label>
								</div>
								<div class="col s6 m1 offset-m1 l1 offset-l1">
									<div class="prefixing">
										<label> <input name="titlesPrefixing" type="radio"
											value="Mr." required /> <span>Mr.</span>
										</label>
									</div>
									<div class="prefixing">
										<label> <input name="titlesPrefixing" type="radio"
											value="Ms." /> <span>Ms.</span>
										</label>
									</div>
								</div>
								<div class="input-field col s12 m10 l10">
									<input id="surnameWithInitials" name="surnameWithInitials"
										type="text" required class="validate" pattern="[A-Za-z .]{1,}" title="Please enter english characters."> <label
										for="surnameWithInitials">Surname with initials:</label>
								</div>
								<div class="input-field col s12 m12 l12">
									<input id="NamesDenotedByInitials"
										name="NamesDenotedByInitials" type="text" required  pattern="[A-Za-z .]{1,}" title="Please enter english characters."
										class="validate"> <label for="NamesDenotedByInitials">Names
										denoted by initials:</label>
								</div>
								<div class="input-field col s12 m12 l12">
									<input id="postalAddress" name="postalAddress" type="text"  pattern="[0-9A-Za-z .-/\/,:&']{3,}" title="Please enter only 0-9, A-Z, a-z, and special characters .-/\/,:&'"
										required class="validate"> <label for="postalAddress">Postal
										address:</label>
								</div>
							</div>
						</div>
						
						
						
						<div class="row">
							<h5>Educational Details</h5>
							<div class="divider"></div>
							<div class="col s12 m12 l12 #f9fbe7 lime lighten-5">
								<div class="input-field col s6 m6 l6">
									<input id="registrationNo" name="registrationNo" type="text" pattern="[0-9A-Za-z .-\/]{8,}" title="Please use capital letters, / , .and numbers only"
										 class="validate"> <label for="registrationNo">Registration
										No:</label>
								</div>
								<div class="input-field col s6 m6 l6">
									<input id="field" name="field" type="text" required pattern="[A-Za-z .]{3,}" title="Please enter english characters."
										class="validate"> <label for="field">Field:</label>
								</div>

								<div class="divider"></div>
								<div class="input-field col s12 m12 l12">
									<h6>Particulars of examinations passed:</h6>
								</div>
								<div class="input-field col s12 m4 l4">
									<div class="card">
										<div class="card-content">
											<h6>Course Unit System (For undergraduate)</h6>

											<input id="cus_generalProgramme" name="cus_generalProgramme"
												type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers."class="validate"> <label
												for="cus_generalProgramme">a). General Programme:</label> <input
												id="cus_specializationProgramme"
												name="cus_specializationProgramme" type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers."
												class="validate"> <label
												for="cus_specializationProgramme">b). Specialization
												Programme:</label>

										</div>
									</div>
								</div>

								<div class="input-field col s12 m4 l4">
									<div class="card cardHover">
										<div class="card-content">
											<h6>Old Regulation Examinations (For undergraduate)</h6>
											<input id="ore_firstExaminEng" name="ore_firstExaminEng"
												type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter english characters." class="validate"> <label
												for="ore_firstExaminEng">a) First Examination In
												Engineering:</label> <input id="ore_finalPart1Exam"
												name="ore_finalPart1Exam" type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers." class="validate">
											<label for="ore_finalPart1Exam">b) Final Part I
												Examination:</label> <input id="ore_finalPart2Exam"
												name="ore_finalPart2Exam" type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers." class="validate">
											<label for="ore_finalPart2Exam">c) Final Part II
												Examination:</label> <input id="ore_finalPart3Exam"
												name="ore_finalPart3Exam" type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers." class="validate">
											<label for="ore_finalPart3Exam">d) Final Part III
												Examination:</label>
										</div>
									</div>
								</div>

								<div class="input-field col s12 m4 l4">
									<div class="card cardHover">
										<div class="card-content">
											<h6>Postgraduate Examinations</h6>
											<input id="pge_nameOfdegreeOrDipl"
												name="pge_nameOfdegreeOrDipl" type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers."class="validate">
											<label for="pge_nameOfdegreeOrDipl">a) Name of the
												Degree/Diploma:</label> <input id="pge_yearCompleted"
												name="pge_yearCompleted" type="text" pattern="[0-9A-Za-z .]{3,}" title="Please enter at least 3 english characters or numbers."class="validate">
											<label for="pge_yearCompleted">b) Year Completed:</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<h5>Payment Details & Clearance</h5>
							<div class="divider"></div>
							<div class="input-field col s12 m12 l12">
								<div class="card #e3f2fd blue lighten-5">
									<div class="card-content black-text">
										<span class="card-title">Notes Fees:</span>
										<p>
											The receipt of payment of the fee prescribed below to the
											Shroff or credit to the Acco. No. <b>0001274688 (BOC)</b> /
											Acco. No. <b>057100121338023 (People's Bank)</b> at any
											branch in favour of the <b><i>'Bursar, University of
													Peradeniya'</i></b> should be submitted with the applications to
											the <b><i>Senior Assistant Registrar, Faculty of
													Engineering, University of Peradeniya</i></b>.
										</p>
										<br> <br>
										<p>1. <spam style="color:red;"><b>Please check above application notices for Payment details</b></spam>
										</p>
										<br> <br>
										<p>2. All applications should be accompanied with a
											clearance note in respect of Vacation / Hall Fees arrears
											from the Senior Assistant Registrar / Student Services and
											Bursar / Accounts Branch and Library clearance note . This is
											applicable only at the first time of the requests of the
											above documents.</p>
										<br> <br>
										<p>3. Documents will be posted under registered cover to
											the given address/es by the university</p>
										<br> <br>
										<p>
											Dean's Office,<br> Faculty of Engineering.
										</p>

									</div>
								</div>
							</div>
							<div class="col s12 m12 l12 #f9fbe7 lime lighten-5">
								<div class="divider"></div><br><br>
								<div class="input-field col s12 m6 l6">
									<div class="card cardHover">
										<div class="card-content">
											<h6 class="center"><b>Request Made for</b></h6>

											<p>
											<h6>
												<b>Number of copies</b>
											</h6>
											<br> Number of Transcripts: <input
												id="numberOfTranscripts" type="number" min="0" 
												class="validate"><br>
											<br>
											<br> Number of Academic Rank Certificates: <input
												id="numberOfAcademicRankCertificates" type="number" min="0"
												 class="validate"> <br>
											<br>
											<p> Certificate to be issued within: 7 Days </p>
											<!-- <p>
												Certificate to be issued within: <br> <label> <input
													id="two_days" name="days" value="2days" type="radio" /> <span>
														2 working days</span></label> <label> <input id="four_days"
													name="days" value="4days" type="radio" /> <span> 4
														working days</span></label>
											</p> -->
											<br>
											<br>
											<p>
												for <br> <label> <input id="local_address"
													name="localOrForieng" value="local" type="radio" /> <span>
														Local Address</span></label> <label> <input id="foreign_address"
													name="localOrForieng" value="foreign" type="radio" /> <span>
														Foreign Address</span></label>
											</p>
											<div id="checkBoxView" style="display: none;">
												<p>
													<label> <br> <input id="checkBox"
														type="checkbox" name="additionalCopy" /> <span>Additional
															copy of a transcript or an Academic Rank Certificate
															posted together in the same envelope for foriegn
															university</span>
													</label>
												</p>
											</div>

											<input id="requestMadefor" name="requestMadefor"
												type="hidden" /> <input id="numberOfCopies"
												name="numberOfCopies" type="hidden" /> <input id="payment"
												name="payment" type="hidden" /> 
												<br>
												<!-- Payment:
											<h5>
												<div id="viewPayment"></div> 
											</h5> -->
											<script>
												
												 $(document).ready(function() {
													 
													 	var paymentValue = 0;
														var paymentValue1 = 0;
														var paymentValue2 = 0;
														var addValue = 0;
														
														var noOfTrans = 0;
														var noOfAcads = 0;
														
														var numberOfCopies = 0;
														var days = "";
														var text = "";
														var numberOfcopiestext = "";
														var withinDaystext = "";
														var addressText = "";
														var additionalText = "";
														
														$('#numberOfTranscripts,#numberOfAcademicRankCertificates').focusout( function() {
															noOfTrans = $('#numberOfTranscripts').val();
															noOfAcads = $('#numberOfAcademicRankCertificates').val();
															numberOfCopies = Number(noOfTrans) + Number(noOfAcads);
															
															if(noOfTrans > 0){
																numberOfcopiestext = noOfTrans+"-Transcritpt/s \n";
															}
															if(noOfAcads > 0){
																numberOfcopiestext = numberOfcopiestext + ""+ noOfAcads +"- Academic Rank Certificate/s \n";
															}
														});
														
														/* $("input[name='days']").click(function() {
															$('#local_address').prop('checked', false);
															$('#foreign_address').prop('checked', false);
															$('#viewPayment').text("");
														      if($('input[name="days"]:checked').val() == '2days'){
														    	  days = "2days";
														    	  withinDaystext = "and need within 2 Days";
														      }
														      if($('input[name="days"]:checked').val() == '4days'){
														    	  days = "4days";
														    	  withinDaystext = "and need within 4 Days";
														      }
														 }); */
														
														$("input[name='localOrForieng']").click(function() {

														      if($('input[name="localOrForieng"]:checked').val() == 'local'){
														    	  addressText =" for local address";
														    	  $("#checkBoxView").hide();
														    	 
														    	  //$('#collectionMethodViewForeign').hide();
														    	 // $("#fromARofficeForeign").hide();
														    	  
														    	  $('#collectionMethodView').slideDown(500);
														    	  $("#collectionMethod").val("");
														    	  $("#addressToSendCertificateView").hide();
														    	  $("#addressToSendCertificate").attr('name', 'addressToSendCertificate');
														    	  $("#addressToSendCertificate2").val("");
														    	  $("#addressToSendCertificate2").attr('name', "");
														    	  
														    	  /* if(days == "2days"){
																	paymentValue1 = noOfTrans*750.00;
																	paymentValue2 = noOfAcads*750.00;
																	paymentValue = paymentValue1 + paymentValue2;
														    	  }
														    	  if(days == "4days"){
														    		paymentValue1 = noOfTrans*250.00;
																	paymentValue2 = noOfAcads*250.00;
																	paymentValue = paymentValue1 + paymentValue2;
														    	  } */
														    	  	paymentValue1 = noOfTrans*1000.00;
																	paymentValue2 = noOfAcads*500.00;
																	paymentValue = paymentValue1 + paymentValue2;
														      }
														      if($('input[name="localOrForieng"]:checked').val() == 'foreign'){
														    	  addressText =" for foreign address";
														    	  $("#checkBoxView").slideDown(500);
														    	  
														    	  
														    	  //$('#collectionMethodViewForeign').slideDown(500);
														    	 // $("#fromARofficeForeign").slideDown(500);
														    	  
														    	  $('#collectionMethodView').slideDown(500);
														    	  $("#addressToSendCertificateView").slideDown(500);
														    	  $("#addressToSendCertificate").val("");
														    	  $("#addressToSendCertificate").attr('name', "");
														    	  $("#addressToSendCertificate2").attr('name', 'addressToSendCertificate');
														    	  $("#collectionMethod").val("From register post");
														    	  
														    	  
																 /*  if(days == "2days"){
																	paymentValue1 = noOfTrans*1500.00;
																	paymentValue2 = noOfAcads*1500.00;
																	paymentValue = paymentValue1 + paymentValue2;
														    	  }
														    	  if(days == "4days"){
														    		paymentValue1 = noOfTrans*750.00;
																	paymentValue2 = noOfAcads*750.00;
																	paymentValue = paymentValue1 + paymentValue2;
														    	  } */
														    	  
														    	  	paymentValue1 = noOfTrans*2000.00;
																	paymentValue2 = noOfAcads*500.00;
																	paymentValue = paymentValue1 + paymentValue2;
														      }
														      
																$("#checkBox").on('change', function() {
																	  if ($(this).is(':checked')) {
																			addValue = 150;
																			additionalText = " and Additional copy of a transcript or an Academic Rank Certificate posted together in the same envelope for foriegn university"
																			//console.log($("#checkBox").val());
																			$('#viewPayment').text("Rs. "+(Number(paymentValue)+Number(addValue)));
																		    text = numberOfcopiestext + withinDaystext + addressText + additionalText;
																			//console.log(text);
																		    $('#requestMadefor').val(text);
																			$('#numberOfCopies').val(numberOfCopies);
																			$('#payment').val(paymentValue+addValue);
																	  }else{
																		  $('#viewPayment').text("Rs. "+paymentValue);
																	      text = numberOfcopiestext + withinDaystext + addressText;
																		  //console.log(text);
																	      $('#requestMadefor').val(text);
																			$('#numberOfCopies').val(numberOfCopies);
																		$('#payment').val(paymentValue);
																	  }
																});
																 $('#viewPayment').text("Rs. "+paymentValue);
															      text = numberOfcopiestext + withinDaystext + addressText;
																  //console.log(text);
															      $('#requestMadefor').val(text);
																	$('#numberOfCopies').val(numberOfCopies);
																$('#payment').val(paymentValue);
														 });
													});
												</script>


											<br>
										</div>
									</div>
								</div>
								<div class="col s12 m6 l6">
									<!-- <div style="color: red;"><h6><b>*Please upload <u>Image files</u> below.</b></h6></div>  -->
									<div class="file-field input-field">
										<div style="color: red;"><h6><b>*Please indicate your registration number and the name with initials in the payment receipt</b></h6></div> 
										<div class="btn">
											<span>Deposit Slip (Image / PDF file) </span> <input type="file" accept="image/png,image/jpeg,application/pdf"
												name="paymentReceipt" >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
									</div>
									<div class="file-field input-field">
										<div class="btn">
											<span>Paying in Voucher (Optional) (Image file) </span> <!-- (*Optional)  -->
											<input
												type="file" accept="image/*" name="paymentReceipt2" >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
									</div>
									<div class="file-field input-field" style="border: 2px solid #26a69a;">
										<div class="btn">
											<span>Clearance Certificate (Image file)</span> <input type="file"
												accept="image/*" name="clearenceForm" >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
										<div style="color: black;"><h6>*NOTE: All applications should be accompanied with a
											clearance note in respect of Vacation / Hall Fees arrears
											from the Senior Assistant Registrar / Student Services and
											Bursar / Accounts Branch and Library clearance note . </h6><div style="color: red;"><b> This is
											applicable only at the first time of the requests of the
											above documents..</b></div></div>
											
										
									</div>
									<p class="flow text" style="color: Black;"><b>NOTE:</b></p>
									<p class="flow text" style="color: Green;"><b>
											* Please send the original document of Clearance Certificate by post to the Senior Assistant Registrar, Faculty of Engineering, University
of Peradeniya, Peradeniya, Sri Lanka.<br><br>
										</b></p>
										
										<p class="flow text" style="color: red;"><b>
											* Application is processed after receipt of the Clearance Certificate.<br><br>
										</b></p>
									<!-- <p class="flow text" style="color: Black;"><b>NOTE:</b></p>
									<p class="flow text" style="color: Green;"><b>
											* Please send the original document(s) (Deposit Slip, Paying in Voucher and/or Clearance Certificate) by post to the Senior Assistant Registrar, Faculty of Engineering, University
of Peradeniya, Peradeniya, Sri Lanka.<br><br>
										</b></p>
										
										<p class="flow text" style="color: red;"><b>
											* Application is processed after receipt of the correct fee.<br><br>
										</b></p> -->
								</div>
							</div>
						</div>
						<div class="row" id="collectionMethodView" style="display: none;">
							<h5>Collection Method</h5>
							<div class="divider"></div>
							<div class="col s12 m12 l12 #e8f5e9 green lighten-5">
								<div class="input-field col s12 m12 l12">
									<select id="collectionMethod" name="collectionMethod"
										ng-model="collectionMethod" required>
										<option value="" disabled selected>Choose Collection Method</option>
										<!-- <option value="From Email">From Email</option> -->
										<option value="From register post">From register post</option>
										<option value="Collect from AR office">Collect from AR office</option>
									</select> <label>Collection Method:</label>
								</div>
								<script>
								$('#collectionMethod').change(function(){
											//console.log("Email:"+$("#email").val());
									        if (this.value == "From Email")
										      {
										        $("#emailTo").slideDown(500);
										        $("#emailAddress").text($('#email').val());
										      	$("#addressToSendCertificate").prop('required',false);
										        $("#addressToSealeddocument").prop('required',false);
										       	$("#localOrForieng").prop('required',false);
										        $('#postTo').hide();
										        $("#fromARoffice").hide();
										      }
									        if ( this.value == 'From register post')
										      {
										        $("#postTo").slideDown(500);
										        $("html").animate({scrollTop: $("#postTo").offset().top}, 1200);
										      	$("#addressToSendCertificate").prop('required',true);
										        $("#addressToSealeddocument").prop('required',false);
										        $("#localOrForieng").prop('required',true);
										        $("#addressToSealeddocument").val("");
										        $('input[name="by_hand"]').attr('checked',false);
										        $('#emailTo').hide();
										        $("#fromARoffice").hide();
										      }
									        if ( this.value == 'Collect from AR office')
										      {
										        $("#postTo").hide();
										        $("#addressToSendCertificate").prop('required',false);
										        //$("#addressToSealeddocument").prop('required',true);
										       	$("#localOrForieng").prop('required',true);
										       	$("#addressToSendCertificate").val("");
										        $('#emailTo').hide();
										        $("#fromARoffice").slideDown(500);
										      }
									 });
								</script>
								<div id="emailTo" style="display: none;">
									<div class="input-field col s12 m12 l12">
										Email To:
										<div id="emailAddress"></div>
									</div>
								</div>
								<div id="postTo" style="display: none;">
									<div id="addressTo" class="input-field col s12 m12 l12">
										<input id="addressToSendCertificate" type="text" 
											class="validate"> <label
											for="addressToSendCertificate">The full address to
											which the transcript/academic rank certificate should be
											sent. And please indicate other requests here (if any)</label>
									</div>
								</div>
								<div id="fromARoffice" style="display: none;">
									
									Please select whether you need sealed or non-sealed document.
									<p>
								      <label>
								        <input name="by_hand" type="radio" id="Sealed" value="Sealed"/>
								        <span>Sealed document</span>
								      </label>
								    </p>
								    <p>
								      <label>
								        <input name="by_hand" type="radio" id="Non-sealed" value="Non-sealed"/>
								        <span>Non-Sealed document</span>
								      </label>
								    </p>
								<div id="byHand" class="input-field col s12 m12 l12">
								    <input id="addressToSealeddocument" type="text" 
											class="validate"> <label
											for="addressToSealeddocument">If sealed Document, please insert the address of the collector. And please indicate other requests here (if any)</label>
									</div>
								</div>
								
							</div>
						</div>
						
						<!-- <div class="row" id="collectionMethodViewForeign" style="display: none;">
							<h5>Collection Method</h5>
							<div class="divider"></div>
							<div class="col s12 m12 l12 #e8f5e9 green lighten-5">
								<div id="fromARofficeForeign" style="display: none;">
									
									Please select whether you need sealed or non-sealed document.
									<p>
								      <label>
								        <input name="by_hand" type="radio" id="Sealed" value="Sealed"/>
								        <span>Sealed document</span>
								      </label>
								    </p>
								    <p>
								      <label>
								        <input name="by_hand" type="radio" id="Non-sealed" value="Non-sealed"/>
								        <span>Non-Sealed document</span>
								      </label>
								    </p>
								<div id="byHand" class="input-field col s12 m12 l12">
								    <input id="addressToSealeddocument" type="text" 
											class="validate"> <label
											for="addressToSealeddocument">If sealed Document, please insert the address of the collector. And please indicate other requests here (if any)</label>
									</div>
								</div>
								
							</div>
						</div> -->
						<div class="row" id="addressToSendCertificateView"
							style="display: none;">
							<h5>Foreign address to send and other details</h5>
							<div class="divider"></div>
							<div class="col s12 m12 l12 #e8f5e9 green lighten-5">
									
								<div class="input-field col s12 m12 l12">
									<p><b>The full address <span style="color: Green;"> with receiver email address ( please use 'AT' word to indicate @ sign [example AT gmail.com]) Or official receiver contact number [example: + 1 819 555 5555] </span> and  please indicate other requests here (if any):</b></p>
							
									<input id="addressToSendCertificate2" type="text" 
										class="validate"> 
								</div>
							</div>
						</div>
						
						<div class="row">
							<h5>Other documents and Remarks (if any)</h5>
							<div class="divider"></div>
							
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5">
							<p style="color: Green;"><b>You are only allowed to upload a maximum of 5 files and 
							Total File size must not be more than 10 MB</b></p>
								<div class="file-field input-field">
										<div class="btn">
											<span>other documents (*PDF)(if any)</span> <input type="file" accept="application/pdf"
												name="additionalDocuments" id="otherdocs"  multiple>
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
          								</div>
								</div>
								<div class="input-field col s10">
										<textarea id="textarea1" class="materialize-textarea" name="remark" length="500" maxlength="500"></textarea>
          								<label for="textarea1">Remark (Max character length is 200)</label>
								</div>
								
							</div>
							
							
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5"><br>
								<p><b>(Optional) Upload other documents separately using Google form and then submit this application:  </b><br>
								<span style="color:red"><b>If you uploaded the document through google form please insert a <u>Remark</u> 
								as "Uploaded to Google Form" above</b></span>
								<br/><a href="https://forms.gle/QdnHW7ravBG1p8r8A" target="_blank" id="checkGoogleSheet"
								class="waves-effect waves-light btn  #424242 green darken-3">Google form<i
									class="material-icons">upload</i></a></p><br>
							</div>
							
						</div>
						
						<div class="input-field col s12 m12 l12 right-align">

							<input type="reset"
								class="waves-effect waves-light btn #e8eaf6 indigo lighten-5"
								value="Reset"> <input type="submit"
								class="waves-effect waves-light btn light-blue darken-5"
								value="submit">

						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal-looading"></div>

</body>
<script>
	$("#otherdocs").on('change', function(){
	    var $fileUpload = $("input[name='additionalDocuments']");
	    var fp = $("input[name='additionalDocuments']");
        var lg = fp[0].files.length;
        var items = fp[0].files;
        var fileSize = 0;
	    
	    if (parseInt($fileUpload.get(0).files.length) > 5){
	    	  showToastr('error', 'You are only allowed to upload a maximum of 5 files', "");
	          //alert("");
	          $("#otherdocs").val('');
	          fileSize = 0;
	    }
	    
	    
	    if (lg > 0) {
	        for (var i = 0; i < lg; i++) {
	            fileSize = fileSize+items[i].size; 
	            console.log(fileSize);
	        }
	        if(fileSize > 10485760) {
	        	showToastr('error', 'Total File size must not be more than 10 MB', "");
		        $("#otherdocs").val('');
		        fileSize = 0;
	        }
	    }
	 }
	);


	$('#form').submit(function(e) {
		var collectionDetails = "";
		var remark = "";
		
		if($('#Sealed').prop("checked")){
			collectionDetails = $('#collectionMethod').val()+" -- "+"Request Sealed Document"+" -- To: "+$('#addressToSealeddocument').val();
		}
		
		if($('#Non-sealed').prop("checked")){
			collectionDetails = $('#collectionMethod').val()+" -- "+"Request Non-Sealed Document";
		}
		//console.log(collectionDetails);
		
		if(collectionDetails != ""){
			//console.log(collectionDetails);
			//$('#collectionMethod').val(collectionDetails).change();
			$("#collectionMethod option[value='Collect from AR office']").attr('value', collectionDetails);
			//console.log($('#collectionMethod').val());
		}
		//console.log($('#collectionMethod').val());
		
		$.ajax({
			url : '/sendApplication',
			type : 'POST',
			data : new FormData(this),
			processData : false,
			contentType : false
		}).done(function(data){
			showToastr('success', 'Successfully sent!', "<br><br><b> Dismiss </b>");
			$("#form")[0].reset();
			$('#viewPayment').text("");
			$(document).scrollTop(0);
			window.location.reload();
			//location.reload();
			})
	     .fail(function(errMsg) {showToastr('error', 'Something went wrong!', "Please try again! <br><br><b> Dismiss </b>");});
		e.preventDefault();
		
	});

	$body = $("body");

	$(document).on({
		ajaxStart : function() {
			$body.addClass("loading");
		},
		ajaxStop : function() {
			$body.removeClass("loading");
			//showToastr('success', 'Successfully sent!', "Dismiss");
		}
	});

	function showToastr(type, title, messsage) {
		let body;
		toastr.options = {
			"closeButton" : false,
			"debug" : false,
			"newestOnTop" : false,
			"progressBar" : false,
			"positionClass" : 'toast-top-center',
			"preventDuplicates" : true,
			"showDuration" : "300",
			"hideDuration" : "1000",
			"timeOut" : 5000,
			"onclick" : null,
			"onCloseClick" : null,
			"extendedTimeOut" : 1000,
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut",
			"tapToDismiss" : true
		};
		toastr.options.onHidden = function(){
			  //$(document).scrollTop(0);
			  //window.location.reload();
			};
		switch (type) {
		case "info":
			body = "<span> <i class='fa fa-spinner fa-pulse'></i></span>";
			break;
		default:
			body = '';
		}
		const content = messsage + body;
		toastr[type](content, title)
	}
</script>

</html>
