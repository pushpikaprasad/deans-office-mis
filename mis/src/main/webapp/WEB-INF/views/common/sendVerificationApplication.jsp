<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/materialize.css" rel="stylesheet">
<link href="css/VeriApplication.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link rel="stylesheet" href="css/toastr.min.css">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.tabs').tabs();
		$('select').formSelect();
		$('.datepicker').datepicker();
		$('.modal').modal();
		$('#addStudents').on('click', function() {});
		$("#addStudents").modal({
			dismissible : false
		});
		$('.datepicker').datepicker({
		      yearRange: 100 
		});
	});
</script>
<style>

.modal { width: 90% !important ; max-height: 100% !important ; margin: -20px auto 0px auto !important;}
</style>
<style type="text/css">
/*LOADING*/
/* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
.modal-looading {
	display: none;
	position: fixed;
	z-index: 1000;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background: rgba(255, 255, 255, .8) url(/img/ajax-loader.gif) 50% 50%
		no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal-looading {
	overflow: hidden;
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal-looading {
	display: block;
}

.input-field label {
	color: #212121 !important;
}

[type="checkbox"]:not(:checked), [type="checkbox"]:checked {
    position: initial;
    opacity: 12;
    pointer-events: all;
}
.material-icons{
    display: inline-flex;
    vertical-align: top;
}

</style>
</head>
<body>
	<div id="main_page">
		<div style="margin: 10% 0% 0% 5%;">
			<h5>Application Form</h5>
			<div class="divider"></div>
		</div>

		<br> <br>
		<div class="row">

			<div class="col s12 m10 offset-m1 l10 offset-l1">
				<ul class="tabs">
					<li class="tab col s3"><a class="active" href="#application1">VERIFICATION</a></li>
				</ul>
			</div>

			<div id="application1"
				class=" card z-depth-5 col s12 m10 offset-m1 l10 offset-l1">
				<div class="card-content">
					<div class="raw transparent">
					<form id="form" enctype="multipart/form-data">
						<div class="row center">
							<h5>
								FACULTY OF ENGINEERING - UNIVERSITY OF PERADENIYA<br>
								APPLICATION FOR RESULT VERIFICATION DOCUMENT
							</h5>
							<br>
							<br>
						</div>
						<div class=" card z-depth-2 hoverable">
							<!-- <div class="card-content">
								<p class="flow text" style="color: black;">
									<b>IMPORTANT:</b>&nbsp&nbsp Please insert a valid email
									address. After you enter the data, you will receive an email from the system. If you will not receive any email please
									submit the form again or contact Dean's office.
								</p>
							</div> -->
							<div class="card-content">
								<p class="flow text" style="display:inline; color: green;">
									<c:if test="${fn:length(general_noticesVerification) > 0}">
									 	<b><i class="material-icons">info</i> Application information</b>
									<%-- ${important_notices} --%>
									<table>
										<c:forEach begin="0" end="${fn:length(general_noticesVerification) - 1}" var="index">
										   <tr>
										      <td><i class="material-icons">star_half</i><b><c:out value="${general_noticesVerification[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
									<c:if test="${fn:length(general_noticesAll) > 0}">
									<table>
										<c:forEach begin="0" end="${fn:length(general_noticesAll) - 1}" var="index">
										   <tr>
										      <td><i class="material-icons">star_half</i><b><c:out value="${general_noticesAll[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
								 </p>
							</div>
							
							<div class="card-content">
								<p class="flow text" style="display:inline; color: red;">
									<c:if test="${fn:length(important_noticesVerification) > 0}">
									 	<b><i class="material-icons">error_outline</i> Important notice</b>
									
									<%-- ${important_notices} --%>
									<table>
										<c:forEach begin="0" end="${fn:length(important_noticesVerification) - 1}" var="index">
										   <tr>
										      <td><i class="material-icons">star_half</i><b><c:out value="${important_noticesVerification[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
									<c:if test="${fn:length(important_noticesAll) > 0}">
									<table>
										<c:forEach begin="0" end="${fn:length(important_noticesAll) - 1}" var="index">
										   <tr>
										      <td><i class="material-icons">star_half</i><b><c:out value="${important_noticesAll[index]}" escapeXml="false"/></b></td>
										   </tr>
										</c:forEach>
									</table>
									</c:if>
								 </p>
							</div>
						</div>
						
						<div class=" card z-depth-2 hoverable">
							<div class="card-content">
							<b>Note:</b><br>
								<h6><b>Verification of Results</b></h6>
								<p class="flow text" style="color: black;">
									
									
									The Faculty of Engineering, University of Peradeniya provides the service of providing the report of Results Verification of the graduates on request made by the application form.
									
									<br>
									Results Verification Report will be sent to the given address by registered post or by email.
								</p>
							</div>
						</div>

						<div class="row">
							<h5>Applicant Details</h5>
							<div class="divider"></div>
							
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5">
								<div class="input-field col s12 m10 l10">
									<input id="applicantName" name="applicantName" pattern="[A-Za-z0-9 '()\/\-&.,]{3,}" 
									title="Please enter english characters, numbers and ' ( ) / - & . and , only."
										type="text"  class="validate" required> <label
										for="applicantName">Institute Name:</label>
								</div>
								 <div class="input-field col s12 m10 l10">
									<input id="applicantAddress" name="applicantAddress" required pattern="[0-9A-Za-z '()\/\-&:.,]{3,}" title="Please enter english characters, numbers and ' ( ) / - & : . and , only."
										type="hidden"  class="validate" value = "" ><!--  <label
										for="applicantAddress">Address of the applicant:</label> -->
								</div> 
								
								<div class="input-field col s12 m6 l6">
									<input id="applicantEmail" name="applicantEmail" type="email" 
										class="validate" size="320" maxlength="64" required> <label for="applicantEmail" >Email to get Notification:</label>
								</div>
								<div class="input-field col s12 m6 l6">
									<input id="applicantMobile" name="applicantMobile"  type="tel" pattern="[+0-9]{10,}" 
									title="Please enter enter valid phone number with minimum 10 digits"
										class="validate" required> <label for="applicantMobile" >Contact No:</label>
								</div>
								
								<!-- <div class="col s12 m12 l12">
								<h6><b>Note: <br>
								<i class="material-icons">star_half</i>Please upload cover letter with other relevant documents. <br>
								<i class="material-icons">star_half</i>Please mention students' details in the cover letter OR You can use following section to include their details.
								(Student registration number is required for the verification process)<br>
								</b></h6>
								</div> -->
							</div>
						</div>
						<div class="row">
							<h5>Add Student Details</h5>
							<div class="divider"></div><br>
							
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5">
								<div id="listofStds" class="col s12 m12 l12">
									<input id="stdData" name="stdData"
										type="hidden" class="validate"> 
									<div id="stdDataView" class="input-field col s12 m12 l12 right-align">
										<!-- <div id="viewAddedstd"></div> -->
									<table id="stdtable" class="responsive-table">
									
									<thead>
									<tr>
									<td></td>
									<!-- <td>studentName</td> -->
									<td>Student Registration No.</td>
									<!-- <td>stream</td> -->
									
									<!-- <td>address</td>
									<td>dob</td>
									<td>contactNumber</td> -->
									<!-- <td>Degree Certificate (PDF)</td> -->
									<!-- <td>email</td> -->
									<!-- <td>Transcript (PDF)</td> -->
									<!-- <td>efectiveDate</td> -->
									</tr>
						            
									</thead>
										<tbody>
										</tbody>
									</table>
										<button type='button' id ="deletebtn" class='delete-row waves-effect waves-light btn red darken-5'>Delete selected Row(s)</button>
									</div>
									
								</div>
								
								<a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#addStudents" >Add Student<i
									class="material-icons">add</i></a>
								
								<div class="file-field input-field">
										<div class="btn">
											<span>other documents (*PDF format) (if any) (2Mb)</span> <input type="file" accept="application/pdf"
												name="coverLetter"  multiple >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
          								</div>
								</div>
							</div>
							
						</div>
						
						
						<div class="row">
							<h5>Payment Details</h5>
							<div class="divider"></div>
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5">
								<div class="input-field col s12 m10 l10 container">
									<select id="methodOfPayment" name="methodOfPayment"
										ng-model="methodOfPayment">
										<option value="" disabled selected>Select payment method</option>
										<option value="Telegraphic Transfer to the Account">Telegraphic Transfer to the Account No. 000 127 4688 (Please refer the Note for more details)</option>
										<option value="Direct Payment to the Account">Direct Payment to the Account No. 000 127 4688 (Bank of Ceylon)</option>
									</select> <label>Method Of Payment:</label>
								</div>
								
								
								<div class="col s12 m5 l5">
								<div class="file-field input-field">
										<div class="btn">
											<span>Payment - Image file</span> <input type="file" accept="image/*"
												name="paymentImage" >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
								</div>
								</div>
								<div class="col s12 m2 l2 center">
								<b>|<br> OR <br>|</b>
								</div>
								<div class="col s12 m5 l5">
								<div class="file-field input-field">
										<div class="btn">
											<span>Payment - PDF file</span> <input type="file" accept="application/pdf"
												name="paymentPdf" >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
								</div>
								
								
								
								</div>
								<!-- <div class="input-field col s12 m10 l10">
									<input id="paymentVerifyCode" name="paymentVerifyCode"
										type="text"  class="validate"> <label
										for="paymentVerifyCode">Payment Verify Code</label>
								</div> -->
								
								<div style="color: black;" class="col s12 m12 l12">
									<h6> <b>Note:</b></h6><br>
										<p>The payment could be made by:</p>
										<ul class="collection">
											<!-- <li  class="collection-item">
											A Cheque or an International Money Order in favour of
												Bursar, University of Peradeniya OR
											</li> -->
											<li  class="collection-item">
											Telegraphic Transfer to
												the Account No. 000 127 4688, (A/C Code: 11-01- 221-207),
												Swift Code: BCEYLKLX, Bank of Ceylon, Peradeniya, Sri Lanka
												OR 
											</li>
											<li  class="collection-item">
											Direct payment to the Account No. 000 127 4688 (Bank of
												Ceylon) at any branch in favour of the Bursar, University of
												Peradeniya
											</li>
										</ul>
										
										<p class="flow text" style="color: Green;"><b>
											* Please send the original payment slip by post to the Senior Assistant Registrar, Faculty of Engineering, University
of Peradeniya, Peradeniya, Sri Lanka.<br><br>
										</b></p>
										
										<p class="flow text" style="color: red;"><b>
											* Application is processed after receipt of the correct fee.<br><br>
										</b></p>
										
										
										
								</div>
							</div>
						</div>
						
						<div class="row">
							<h5>Collection Details</h5>
							<div class="divider"></div>
							
							<div class="col s12 m12 l12 #e3f2fd blue lighten-5">
								<div class="input-field col s12 m10 l10 container">
										<select
											id="methodOfcollection" name="methodOfcollection"
											ng-model="methodOfcollection" required>
											<option value="" disabled selected>Select the method of collection</option>
											<option value="By post">By post</option>
											<option value="By email">By email</option>
										</select> <label>Method Of Collection:</label>
								</div>
								
								
								<div class="input-field col s12 m10 l10">
									<input id="postAddress" name="postAddress" pattern="[0-9A-Za-z .-/\/,:&']{3,}" title="Please enter only 0-9, A-Z, a-z, and special characters .-/\/,:&'"
										type="text"  class="validate"> <label id="postAddressLabel"
										for="postAddress">Post Address</label>
									<input id="postEmail" name="postEmail" type="email" 
										class="validate" size="320" maxlength="64"> <label  id="postEmailLabel"
										for="postEmail">Post Email</label>
								</div>
								
							</div>
						</div>

						<div class="input-field col s12 m12 l12 right-align">

							<input type="reset"
								class="waves-effect waves-light btn #e8eaf6 indigo lighten-5"
								value="Reset"> <input type="submit" 
								class="waves-effect waves-light btn light-blue darken-5"
								value="submit">

						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--  VIEW ADD STUDENRTS-->
	<div id="addStudents" class="modal" >
		
		<div class="row">
					<button
						class="modal-close btn waves-effect waves-light pink darken-1 right">
						<i class="material-icons">close</i>
					</button>
			<div class="col s12 m12 l12">
				<div class="input-field col s6 m6 l6">
					<B>NOTE: Please click add+ button to add student details. </B>
				</div>				
								
										
								<div class="input-field col s12 m12 l12">
								<input id="studentName" name="studentName" pattern="[A-Za-z .]{3,}" title="Please enter english characters." type="hidden"  value ="" class="validate"> <!-- <label for="studentName">Student Name:</label> -->
								<input id="stream" name="stream" pattern="[A-Za-z .]{3,}" title="Please enter english characters." type="hidden"  value="" class="validate"> 
								<input id="contactNumber" name="contactNumber"  pattern="[+0-9]{10,}" title="Please enter enter valid phone number with minimum 10 digits" type="hidden"  value=""  class="validate"> 
								<input id="email" name="email" type="hidden"  value=""  class="validate"> 
								<input id="address" name="address" pattern="[0-9A-Za-z .-/\/,:&']{3,}" title="Please enter only 0-9, A-Z, a-z, and special characters .-/\/,:&'" type="hidden"  value=""  class="validate"> 
								<input id="dob" name="dob" type="hidden"  value=""  class="datepicker"> 
								<input id="effectiveDate" name="effectiveDate" type="hidden"  value =""  class="datepicker">
								</div>
								
								<div class="input-field col s12 m12 l12">
									<input id="studentReg" name="studentReg"  pattern="[0-9A-Za-z .-\/]{8,}" type="text" 
									class="validate"> <label for="studentReg">Student Registration No.:</label>
								</div>
								<!-- <div class="col s12 m12 l12">
								<div class="file-field input-field">
										<div class="btn">
											<span>Transcript </span> <input type="file" accept="application/pdf" onchange="readTranscriptFile(this)"
												id="transcript" name="transcript" >
										</div>
										<div class="file-path-wrapper">
											<input id="transcriptInput" class="file-path validate" type="text">
										</div>
								</div>
								</div>
								<div class="col s12 m12 l12">
								<div class="file-field input-field">
										<div class="btn">
											<span>Degree Certificate</span> <input type="file" accept="application/pdf" onchange="readDegreecertificateFile(this)"
												id="degreeCertificate" name="degreeCertificate" >
										</div>
										<div class="file-path-wrapper">
											<input id="degreeCertificateInput"  class="file-path validate" type="text">
										</div>
								</div>
								</div> -->
								
								<div class="input-field col s12 m12 l12 right-align">
									<button id="addstd" type="button" class="waves-effect modal-close waves-light btn light-blue darken-5" >Add+</button>
									<input type="reset" class="waves-effect  waves-light btn light-blue darken-5" value="Reset" >
								</div>
							
			</div>
		</div>
		
	</div>	
	
	<div class="modal-looading"></div>

</body>
<script>
$(document).ready(function(){
	$("#postAddress").hide();
    $('#postEmail').hide();
    $("#postAddressLabel").hide();
    $('#postEmailLabel').hide();
    
	$('#methodOfcollection').change(function(){
		if (this.value == "By post")
	      {
	        $("#postAddress").slideDown(500);
	        $('#postEmail').hide();
	        $("#postAddressLabel").slideDown(500);
	        $('#postEmailLabel').hide();
	      }
        if ( this.value == 'By email')
	      {
	        $("#postEmail").slideDown(500);
	        $('#postAddress').hide();
	        $("#postAddressLabel").hide();
	        $('#postEmailLabel').slideDown(500);
	      }
	});
});




var fileData = [];
var data = {};
var transcriptName = "";
var degreecertificateName = "";
var registrationNumbers = [];

function readTranscriptFile(input) {
	  var file = input.files[0];
	  transcriptName = file.name;
	  
	  var reader = new FileReader();
	  
	  //reader.readAsArrayBuffer(file);
	  reader.readAsDataURL(file);
	  
	  reader.onload = function() {
		  
		  //var arrayBuffer = reader.result;
		 // var bytes = new Uint8Array(arrayBuffer);
		  //fileData.push(bytes);
		  //fileData.push(arrayBuffer);
		  var TranscriptDataURL = reader.result;
		  fileData.push(TranscriptDataURL);
	  };
	} 

function readDegreecertificateFile(input) {
	  let file = input.files[0];
	  degreecertificateName = file.name;
	  
	  let reader = new FileReader();
	  //reader.readAsArrayBuffer(file);
	  reader.readAsDataURL(file);
	 
	  reader.onload = function() {
		  //var arrayBuffer = reader.result;
		  //var bytes = new Uint8Array(arrayBuffer);
		  //fileData.push(bytes);
		  //fileData.push(arrayBuffer);
		  var DegreecertificateDataURL = reader.result;
		  fileData.push(DegreecertificateDataURL);
	  };
	}  
	
function showToastr(type, title, messsage) {
	let body;
	toastr.options = {
		"closeButton" : false,
		"debug" : false,
		"newestOnTop" : false,
		"progressBar" : false,
		"positionClass" : 'toast-top-center',
		"preventDuplicates" : true,
		"showDuration" : "300",
		"hideDuration" : "1000",
		"timeOut" : 5000,
		"onclick" : null,
		"onCloseClick" : null,
		"extendedTimeOut" : 1000,
		"showEasing" : "swing",
		"hideEasing" : "linear",
		"showMethod" : "fadeIn",
		"hideMethod" : "fadeOut",
		"tapToDismiss" : true
	};
	toastr.options.onHidden = function(){
		  //$(document).scrollTop(0);
		  //window.location.reload();
		};
	switch (type) {
	case "info":
		body = "<span> <i class='fa fa-spinner fa-pulse'></i></span>";
		break;
	default:
		body = '';
	}
	const content = messsage + body;
	toastr[type](content, title);
	
}

$(document).ready(function(){
	//$("#stdDataView").hide();
	$("#deletebtn").hide();
	
	
	var dataArray = [];
	var rawNo = 0;
	
	
$('#addstd').click(function(){
	
	var studentName = $('#studentName').val();
	var stream = $('#stream').val();
	var effectiveDate = $('#effectiveDate').val();
	var address = $('#address').val();
	var dob = $('#dob').val();
	var contactNumber = $('#contactNumber').val();
	var email = $('#email').val();
	
	var transcriptFile = transcriptName;//$('#transcript').val();
	transcriptName = '';
	var transcriptData = fileData[0];
	//console.log(transcriptData);
	
	var degreeCertificateFile = degreecertificateName;//$('#degreeCertificate').val();
	degreecertificateName = '';
	var degreeCertiData = fileData[1];
	//console.log(degreeCertiData);
	
	fileData.pop();
	fileData.pop();
	//console.log("fileData");
	//console.log(fileData);
	
	if(!registrationNumbers.includes($('#studentReg').val())){
		registrationNumbers.push($('#studentReg').val());
		var studentReg = $('#studentReg').val();
		
		data = {
				'studentName': studentName,
				'stream': stream,
				'effectiveDate': effectiveDate, 
				'address': address,
				'dob': dob, 
				'contactNumber': contactNumber, 
				'email': email,
				'studentReg': studentReg,
				'transcript': transcriptData, //null,
				'degreeCertificate': degreeCertiData //null
		};
		
		//dataArray.push(JSON.stringify(data));
		dataArray.push(data);
		//console.log(dataArray);
		
		var markup = "<tr><td><input type='checkbox' name='record' value='"+(rawNo++)+"'>"
		//+ "</td><td>" + studentName 
		+ "</td><td>" + studentReg 
		//+ "</td><td>" + stream 
		
		//+ "</td><td>" + address 
		//+ "</td><td>" + dob 
		//+ "</td><td>" + contactNumber 
		+ "</td><td>" + degreeCertificateFile 
		//+ "</td><td>" + email 
		+ "</td><td>" + transcriptFile 
		+ "</td><td>" + effectiveDate 
		+ "</td></tr>";
		
        $("#stdtable tbody").append(markup);
        if(dataArray.length > 0){
        	$("#deletebtn").show(1000);
        }
        
        
        /* console.log("dataArray");
        console.log(dataArray); */
       
        
		$('#stdData').val([JSON.stringify(dataArray)]);
		//console.log([JSON.stringify(dataArray)]);
		//console.log($('#stdData').val());
		// clear data of students
		
		$('#studentName').val('');
		$('#stream').val('');
		$('#email').val('');
		$('#dob').val('');
		$('#studentReg').val('');
		$('#contactNumber').val('');
		$('#address').val('');
		$('#effectiveDate').val('');
		$('#transcript').val('');
		$('#degreeCertificate').val('');
		$('#transcriptInput').val('');
		$('#degreeCertificateInput').val('');
		
	}else{
		//alert("Registration Number is already added!");
		showToastr('error', 'Registration Number is already added!', "<br><br><b> Dismiss </b>");
	}
	
	//$("#viewAddedstd").append(studentName);
	
	
	
});

 $(".delete-row").click(function(){
	 //console.log(registrationNumbers);
	 studentData = dataArray;
        $("table tbody").find('input[name="record"]').each(function(){
            if($(this).is(":checked")){
            	//console.log($(this).val());
            	dataArray.splice($(this).val(), 1, "Remove");
            	$(this).parents("tr").remove();
                registrationNumbers.splice($(this).val(), 1, "");
            }
            var noOfRemovedValues = 0;
            for(var i = 0; i<registrationNumbers.length; i++ ){
            	if(registrationNumbers[i] == ""){
            		noOfRemovedValues++;
            	}
            }
            if(noOfRemovedValues == registrationNumbers.length){
            	$("#deletebtn").hide(1000);
            }
        });
        //console.log(registrationNumbers);
        //console.log([JSON.stringify(dataArray)]);
        
        
    });
 
 $('#form').submit(function(e) {
	 
	 	if(dataArray.length > 0){
	 	dataArray = jQuery.grep(dataArray, function(value) {
	 		  return value != "Remove";
	 		});
	    $('#stdData').val([JSON.stringify(dataArray)]);
	 	}else{
	 		$('#stdData').val('');
	 	}
		
		//console.log([JSON.stringify(dataArray)]);
		var formData = new FormData(this);
		
		$.ajax({
			url : '/sendVerificationApplication',
			type : 'POST',
			data : formData,//new FormData(this), //formData, /
			processData : false,
			contentType : false
		}).done(function(data){
			showToastr('success', 'Successfully sent!', "<br><br><b> Dismiss </b>");
			$("#form")[0].reset();
			$(document).scrollTop(0);
			//$('html, body').animate({scrollTop:0}, 'slow');
			location.reload();
			})
	     .fail(function(errMsg) {showToastr('error', 'Something went wrong!', "Please try again! <br><br><b> Dismiss </b>");});
		
		
		e.preventDefault();
	});
 
});

	/* function readFile(input) {
	  let file = input.files[0];

	  let reader = new FileReader();

	  reader.readAsArrayBuffer(file);

	  reader.onload = function() {
	    console.log(reader.result);
	  };

	  reader.onerror = function() {
	    console.log(reader.error);
	  };
	//onchange="readFile(this)"
	} */
	
	

	$body = $("body");

	$(document).on({
		ajaxStart : function() {
			$body.addClass("loading");
		},
		ajaxStop : function() {
			$body.removeClass("loading");
			//showToastr('success', 'Successfully sent!', "Dismiss");
		}
	});

	
</script>

</html>
