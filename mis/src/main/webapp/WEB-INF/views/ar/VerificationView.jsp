<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="PageDetails">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/officer.css" rel="stylesheet">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
	$('.modal').modal();
		$('#VerificationApplicationView').on('click', function() {
		});
		$("#VerificationApplicationView").modal({
			dismissible : false
		});
		
	});
	$(document).ready(function() {
		$('.sidenav').sidenav({
			draggable : true
		});

		$('.dropdown-trigger').dropdown();
	});
	$(document).ready(function() {
		$('.tabs').tabs();
	});
</script>
</head>
<body ng-controller="arPageData">

	<div id="top">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="/logout">Logout</a></li>
		</ul>
		<nav class="navbar">
			<div class="nav-wrapper">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i
					class="material-icons">menu</i></a>
				<ul class="right">
					<!--  <li><a href="sass.html">Sass</a></li>
		      <li><a href="badges.html">Components</a></li> -->
					<!-- Dropdown Trigger -->
					<li><a class="dropdown-trigger" href="#!"
						data-target="dropdown1">${first_name} ${last_name}<i
							class="material-icons right">arrow_drop_down</i></a></li>
				</ul>
			</div>
		</nav>
	</div>

	<div id="left-side">
		<ul id="slide-out" class="sidenav sidenav-fixed z-depth-4">
			<li>

				<p class="MIS-logo">MIS</p>
			</li>
			<li><div class="divider"></div></li>
			<li class="nav-item active  "><a class="dashboard_link nav-link"
				href="/arDashboard"> <i class="material-icons">dashboard</i>
					<p>Dashboard</p>
			</a></li>
			<li class="nav-item "><a class="nav-link"
				href="/arDashboard/myAccount"> <i class="material-icons">person</i>
					<p>My Account</p>
			</a></li>
		</ul>
	</div>


	<div id="main_page" class="main_page">
		<h5>Applications</h5>
		<div class="divider"></div>
		<div class="row">
			<div class="col s12 m12 l12">
				<ul class="tabs tabs-fixed-width" style="font: bold;">
					<li class="tab col s1 m1 l1"><a  class="active" href="#all">ALL</a></li>
					
				</ul>
			</div>
			<br><br><br>

			<div id="all">
				<div class="col s12 m12 l12">
					<div class="col s12 m12 l4">
					</div>
					<table class="responsive-table">
						<tr class="#bbdef table_head" style="font-size: 9">
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Application Status</th>
							<th>Method of Collection</th>
							<th></th>
						</tr>
						<tr ng-repeat="i in allVeriApplications">
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.applicationStatus}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView"
								ng-click="viewVerificationApplication(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>

						</tr>
					</table>

				</div>
			</div>
		</div>


	<!--  VIEW APPLICATION WITHOUT ANY OPPTIONS-->
	<div id="VerificationApplicationView" class="modal">
		<div class="row">
			<div class="col s12 m12 l12">
				<div class="col s11 m11 l11">
						<br>
						<h6>Application View</h6>
					</div>
					<div class="col s1 m1 l1">
						<br>
						<button
							class="modal-close btn waves-effect waves-light pink darken-1 right">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>

			<div class="col s12 m12 l12">
				<iframe src="{{arverificationPDFviewURL}}" width="100%" height="500px"></iframe>
			</div>
		</div>
	</div>

	</div>

</body>
</html>