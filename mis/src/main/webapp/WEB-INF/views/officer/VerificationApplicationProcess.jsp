<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="PageDetails">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/officer.css" rel="stylesheet">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
	$('.modal').modal();
		$('#VerificationApplicationView').on('click', function() {
		});
		$("#VerificationApplicationView").modal({
			dismissible : false
		});
		$('#prepareVeriDocuments').on('click', function() {
		});

		$("#prepareVeriDocuments").modal({
			dismissible : false
		});
		$('#VerificationApplicationView2').on('click', function() {
		});

		$("#VerificationApplicationView2").modal({
			dismissible : false
		});
		
		$('#viewVeriMessage').on('click', function() {
		});

		$("#viewVeriMessage").modal({
			dismissible : false
		});
		
		$('#viewVeriMessagetoSendPDF').on('click', function() {
		});

		$("#viewVeriMessagetoSendPDF").modal({
			dismissible : false
		});
	});
	$(document).ready(function() {
		$('.sidenav').sidenav({
			draggable : true
		});

		$('.dropdown-trigger').dropdown();
	});
	$(document).ready(function() {
		$('.tabs').tabs();
	});
</script>

<style>
.input-field > label {
    top: 9px;
}

</style>

</head>
<body ng-controller="officerPageData">

	<div id="top">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="/logout">Logout</a></li>
		</ul>
		<nav class="navbar">
			<div class="nav-wrapper">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i
					class="material-icons">menu</i></a>
				<ul class="right">
					<!--  <li><a href="sass.html">Sass</a></li>
		      <li><a href="badges.html">Components</a></li> -->
					<!-- Dropdown Trigger -->
					<li><a class="dropdown-trigger" href="#!"
						data-target="dropdown1">${first_name} ${last_name}<i
							class="material-icons right">arrow_drop_down</i></a></li>
				</ul>
			</div>
		</nav>
	</div>

	<div id="left-side">
		<ul id="slide-out" class="sidenav sidenav-fixed z-depth-4">
			<li>

				<p class="MIS-logo">MIS</p>
			</li>
			<li><div class="divider"></div></li>
			<li class="nav-item active  "><a class="dashboard_link nav-link"
				href="/officerDashboard"> <i class="material-icons">dashboard</i>
					<p>Dashboard</p>
			</a></li>
			<li class="nav-item "><a class="nav-link"
				href="/officerDashboard/myAccount"> <i class="material-icons">person</i>
					<p>My Account</p>
			</a></li>
			<!-- <li class="nav-item "><a class="nav-link"
				href="./notifications.html"> <i class="material-icons">notifications</i>
					<p>Notifications</p>
			</a></li> -->
		</ul>
	</div>


	<div id="main_page" class="main_page">
		<h5>Applications</h5>
		<div class="divider"></div>
		<div class="row">
			<div class="col s12 m12 l12">
				<ul class="tabs tabs-fixed-width" style="font: bold;">
					<li class="tab col s1 m1 l1"><a class="active" href="#1">NEW</a></li>
					<li class="tab col s1 m1 l1"><a  href="#2">ACCEPTED</a></li>
					<li class="tab col s1 m1 l1"><a  href="#3">PREPARED</a></li>
					<li class="tab col s1 m1 l1"><a  href="#4">MARKED NOT OK</a></li>
					<li class="tab col s1 m1 l1"><a  href="#5">MARKED OK</a></li>
					<li class="tab col s1 m1 l1 disabled" ><a  href="#_"></a></li>
					<li class="tab col s1 m1 l1"><a  href="#6">REJECTED</a></li>
					<li class="tab col s1 m1 l1"><a  href="#7">ALL</a></li>
					
				</ul>
			</div>
			<br><br><br>
			<div id="1">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb table_head">
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th></th>
							<th></th>
						</tr>
						<tr ng-repeat="i in allNewVeriApplications">
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView"
								ng-click="viewVerificationApplication(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="2">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb table_head">
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th>View</th>
							<th>Upload PDF</th>
						</tr>
						<tr ng-repeat="i in allAcceptedVeriApplications">
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView2"
								ng-click="viewVerificationApplication2(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-green darken-1"
								href="#prepareVeriDocuments"
								ng-click="prepareVeriDocuments(i.applicationId)"><i
									class="material-icons">upload</i></a></td>
									
									
						</tr>
					</table>
				</div>
			</div>
			<div id="3">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb table_head">
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th>View</th>
						</tr>
						<tr ng-repeat="i in allPreparedVeriApplications">
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView2"
								ng-click="viewVerificationApplication2(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>
						</tr>
					</table>
				</div>
			</div>
			
			<div id="4">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb table_head">
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th>View</th>
							<th>Upload PDF</th>
						</tr>
						<tr ng-repeat="i in allNotOKVeriApplications">
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView2"
								ng-click="viewVerificationApplication2(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-green darken-1"
								href="#prepareVeriDocuments"
								ng-click="prepareVeriDocuments(i.applicationId)"><i
									class="material-icons">upload</i></a></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="5">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb table_head">
							<th>Applicant ID</th>
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th>View</th>
							<th>Send Message</th>
						</tr>
						<tr ng-repeat="i in allOKVeriApplications">
							<td>{{i.applicationId}}</td>
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView2"
								ng-click="viewVerificationApplication2(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#viewVeriMessage"
								ng-click="viewVeriMessage(i.applicationId)"><i
									class="material-icons">send</i></a><br><br>
									<a
								class="waves-effect waves-light btn modal-trigger modal-close  light-green darken-1"
								href="#viewVeriMessagetoSendPDF"
								ng-click="viewVeriMessage(i.applicationId)"><i
									class="material-icons right">send</i><i
									class="material-icons right">description</i>PDF</a></td>
									
						</tr>
					</table>
				</div>
			</div>
			<div id="6">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb table_head">
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th></th>
						</tr>
						<tr ng-repeat="i in allRejectedVeriApplications">
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView2"
								ng-click="viewVerificationApplication2(i.applicationId)"><i
									class="material-icons">visibility</i></a></td>

						</tr>
					</table>
				</div>
				
			</div>
			
			<div id="7">
				<div class="col s12 m12 l12">
					<div class="col s12 m12 l4">
						<form class="#424242 grey darken-3">
							<div class="row" style="padding: 10px">
								<div class="input-field col s12 m12 l12 ">
									<input id="applicantName" name="searchByName" type="text"
										class="searchByName" ng-model="searchByName" placeholder="Enter Name">
									<label for="applicantName">Applicant Name: </label>
									<button class="btn waves-effect waves-light right"
											type="submit" name="action"
											ng-click="searchAllVeriApplicationsByName(searchByName)">
											Search <i class="material-icons right">search</i>
							        </button>
								</div>
							</div>
						</form>
					</div>
					<table class="responsive-table">
						<tr class="#bbdef table_head" style="font-size: 9">
							
							<th>Applicant ID</th>
							<th>Applicant Status</th>
							<th>Applicant Name</th>
							<th>Applicant Email</th>
							<th>Applicant Mobile</th>
							<th>Applicant Address</th>
							<th>Method of Payment</th>
							<th>Method of Collection</th>
							<th></th>
						</tr>
						<tr ng-repeat="i in allVeriApplications" ng-class="{newVeriApplications: i.applicationStatus=='new',
						accepteVeriApplications: i.applicationStatus=='accepted', 
						rejectedVeriApplications: i.applicationStatus=='rejected', 
						markedOKVeriApplications: i.applicationStatus=='marked_OK',
						doneVeriApplications: i.applicationStatus=='done'}">
							<td>{{i.applicationId}}</td>
							<td>{{i.applicationStatus}}</td>
							<td>{{i.applicantName}}</td>
							<td>{{i.applicantEmail}}</td>
							<td>{{i.applicantMobile}}</td>
							<td>{{i.applicantAddress}}</td>
							<td>{{i.methodOfPayment}}</td>
							<td>{{i.methodOfcollection}}</td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  light-blue darken-1"
								href="#VerificationApplicationView2"
								ng-click="viewVerificationApplication2(i.applicationId)"><i 
									class="material-icons">visibility</i></a></td>

						</tr>
					</table>

				</div>
			</div>
		</div>
		
	<!--  VIEW APPLICATION -->
	<div id="VerificationApplicationView" class="modal">
		<div class="row">
			<div class="col s8 m8 l8">
				<iframe src="{{verificationPDFviewURL}}" width="100%" height="500px"></iframe>
			</div>
			<div class="col s4 m4 l4">
				<div class="col s10 m10 l10">
					<br>
					<h6>Application View</h6>
				</div>
				<div class="col s2 m2 l2">
					<br>
					<button
						class="modal-close btn waves-effect waves-light pink darken-1">
						<i class="material-icons">close</i>
					</button>
				</div>

				<div class="container col s12 m12 l12"
					style="display: {{v_Formdisplay}}">

					<div class="divider"></div>
					<br>
					<br>
					<br> <a
						class="waves-effect waves-light btn modal-close  light-blue darken-1"
						ng-click="acceptVerificationApplication(v_applicationId)">Accept</a> <br>
					<br>
					<div class="divider"></div>
					<br>
					<br>

					<form class="rejectForm">
						<div class="row">
							<div class="input-field col s12 m12 l12">
								<textarea id="reason" name="reason" ng-model="v_reasonMsg"
									class="materialize-textarea" value="" required></textarea>
								<label for="reason">Reason for reject:</label>

								<button
									class="btn waves-effect  modal-close waves-light #424242 grey darken-3"
									type="submit" name="action" id="rejectApplication"
									ng-click="rejectVerificationApplication(v_applicationId, v_reasonMsg)">
									Reject<i class="material-icons right ">sd_card_alert</i>
								</button>

							</div>
						</div>
					</form>

				</div>
				<div class="container col s12 m12 l12" style="display: {{v_OKdisplay}}">
					<br>
					<br>
					<br>
					<h6>{{rejectMessage}}</h6>
					<br>
					<br>
					<br>
					<button
						class="btn waves-effect waves-light modal-close #424242 green darken-3">
						OK<i class="material-icons right ">done</i>
					</button>
				</div>
				<br> <a
						class="waves-effect waves-light btn modal-close  pink right"
						ng-click="duplicateVerificationApplication(v_applicationId)">Duplicate</a> 
			</div>
		</div>
	</div>
	
	<!--  SEND FINAL MESSAGE -->
	<div id="viewVeriMessagetoSendPDF" class="modal">
		<div class="row">
			<div class="col s8 m8 l8">
				<iframe src="{{VeripdfURLInMessage}}" width="100%" height="1200px"></iframe>
			</div>
			<div class="col s4 m4 l4">
				<div class="col s10 m10 l10">
					<br>
					<h6>Send final message to the applicant 2</h6>
				</div>
				<div class="col s2 m2 l2">
					<br>
					<button
						class="modal-close btn waves-effect waves-light pink darken-1">
						<i class="material-icons">close</i>
					</button>
				</div>

				<div class="container col s12 m12 l12">
				<div class="card z-depth-2 hoverable"">
							<div class="card-content">
								<form id="sendVDWithAttachment" class="sendMgsForm">
									<div class="row">
											<input type="hidden" name="applicationID" value="{{finalMsgApplicationId}}">
											<div class="input-field col s12 m12 l12">
												<input id="vdsendfrom" name="vdsendfrom" type="hidden" value=""
													class="validate" size="320" maxlength="64"> 
											</div>
											<div class="input-field col s12 m12 l12">
												<input id="vdsendto" name="vdsendto" type="email" required
													class="validate" size="320" maxlength="64"> <label
													for="vdsendto">To:</label>
											</div>
											<div class="input-field col s12 m12 l12">
												<input id="vdsendcc" name="vdsendcc" type="email" required
													class="validate" size="320" maxlength="64"> <label
													for="vdsendcc">CC:</label>
											</div>
											<div class="input-field col s12 m12 l12">
												<input id="vdsendsubject" name="vdsendsubject" type="text" required
													class="validate" size="320" maxlength="64"> <label
													for="vdsendsubject">Subject:</label>
											</div>
											<div class="input-field col s12 m12 l12">
											<textarea id="vdsendmsgBody" name="vdsendmsgBody" ng-model="finalVeriMsg"
												class="materialize-textarea" value="" required></textarea>
											<label for="vdsendmsgBody">Message:</label>
											</div>
											<div class="file-field input-field col s12 m12 l12">
												<div class="btn">
													<span>Verification Document </span> <input
														type="file" accept="application/pdf"
														name="verificationDocument">
												</div>
												<div class="file-path-wrapper">
													<input class="file-path validate" type="text">
												</div>
											</div>
											<input type="reset"
												class="waves-effect waves-light btn #e8eaf6 indigo lighten-5"
												value="Reset"> 
											<input type="submit"
												class="waves-effect waves-light btn light-blue darken-5"
												value="submit">
									</div>
								</form>
							</div>
						</div>
						

				</div>
			</div>
		</div>
	</div>

	<!--  SEND FINAL MESSAGE with a pdf attachment-->
	<div id="viewVeriMessage" class="modal">
		<div class="row">
			<div class="col s8 m8 l8">
				<iframe src="{{VeripdfURLInMessage}}" width="100%" height="500px"></iframe>
			</div>
			<div class="col s4 m4 l4">
				<div class="col s10 m10 l10">
					<br>
					<h6>Send final message to the applicant</h6>
				</div>
				<div class="col s2 m2 l2">
					<br>
					<button
						class="modal-close btn waves-effect waves-light pink darken-1">
						<i class="material-icons">close</i>
					</button>
				</div>

				<div class="container col s12 m12 l12">
				<div class=" card z-depth-2 hoverable">
							<div class="card-content">
								<p class="flow text" style="color: black;">
									<b>IMPORTANT:</b>&nbsp&nbsp Please insert the details of issued 
									verification documents. Such as Sent Date, Time, Sent Address, etc.
								</p>
							</div>
						</div>
				<div class="card z-depth-2 hoverable" style="height: 250px;">
							<div class="card-content">
								<form class="sendMgsForm">
						<div class="row">
							<div class=" input-field col s12 m12 l12">
								<textarea id="message" name="message" ng-model="finalVeriMsg" 
									class="materialize-textarea" value="" required></textarea>
								<label for="message">Message:</label>
								
								
								<button
									class="btn modal-close waves-effect waves-light #424242 blue darken-3"
									type="submit" name="action" id="rejectApplication"
									ng-click="emailVerificationDocument(finalMsgApplicationId, finalVeriMsg)">
									SEND MESSGAE<i class=" material-icons right ">send</i>
								</button>
								
							</div>
						</div>
					</form>
							</div>
						</div>
						

				</div>
			</div>
		</div>
	</div>


	<!--  VIEW APPLICATION WITHOUT ANY OPPTIONS-->
	<div id="VerificationApplicationView2" class="modal">
		<div class="row">
			<div class="col s12 m12 l12">
				<div class="col s11 m11 l11">
						<br>
						<h6>Application View</h6>
					</div>
					<div class="col s1 m1 l1">
						<br>
						<button
							class="modal-close btn waves-effect waves-light pink darken-1 right">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>

			<div class="col s12 m12 l12">
				<iframe src="{{verificationPDFviewURL3}}" width="100%" height="500px"></iframe>
			</div>
		</div>
	</div>
		
	<!--  PREPARE VERIFICATION DOCUMENTS -->
	<div id="prepareVeriDocuments" class="modal">
			<div class="row">
				<div class="col s12 m8 l8">
					<iframe src="{{verificationPDFviewURL2}}" width="100%" height="500px"></iframe>
				</div>
				<div class="col s12 m4 l4">
					<div class="col s10 m10 l10">
						<br>
						<h6>Upload Documents</h6>
					</div>
					<div class="col s2 m2 l2">
						<br>
						<button
							class="modal-close btn waves-effect waves-light pink darken-1 right">
							<i class="material-icons">close</i>
						</button>
					</div>
					<br><br>
					<div class="container col s12 m12 l12">
						<br> 
						<br>
						<div class="divider"></div>
						
						<form id="formU"  enctype="multipart/form-data" class="blurforms">
							 	<input type="hidden" name="applicationID" value="{{upload_VeriapplicationId}}">
								<div class="file-field input-field">
							      <div class="btn btn-small">
							        <span>Verification Document</span>
									<input type="file" accept=".pdf" name="verificationDocument" ng-model="verificationDocument">
								  </div>
							      <div class="file-path-wrapper">
							        <input class="file-path validate" type="text">
							      </div>
							    </div>
							     <input type="submit"  onclick="upload()"
									class="modal-close  waves-effect waves-light btn light-blue darken-1"
									value="submit pdf document/s" />
								 
							</form>
						<br> 
						<br>
						<br>
				<p> ---------------------- OR ----------------------</p>
							<form>
							Completed the process: 
							<a href=""
								class="modal-close waves-effect waves-light btn-small #424242 green darken-3"><i
									ng-click="checkedOKVeriApplication(upload_VeriapplicationId)"
									class="material-icons">done</i></a>
									</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<!-- Loading image -->
	<div class="modal-looading">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1 right" 
							ng-click="closeLoading()" onclick="closeLoading()">
							<i class="material-icons">close</i>
						</button>
	</div>
	

</body>
<script>

$('#sendVDWithAttachment').submit(function(e) {
	$.ajax({
		url : '/officerDashboard/sendVeriDocument/',
		type : 'POST',
		data : new FormData(this),
		processData : false,
		contentType : false
	}).done(function(data){
		showToastr('success', 'Successfully sent!', "<br><br><b> Dismiss </b>");
		$("#sendVDWithAttachment")[0].reset();
		window.location.reload();
		})
     .fail(function(errMsg) {showToastr('error', 'Something went wrong!', "Please try again! <br><br><b> Dismiss </b>");});
	e.preventDefault();
});

function upload(){
	    var data = new FormData(document.getElementById("formU")); 
	    var formFields = document.getElementById('formU').elements;
	    if(formFields["verificationDocument"].files.length == true){
	    	
	    	$.ajax({
				url : '/officerDashboard/uploadVeriDocument/',
				type : 'POST',
				data : data,
				processData : false,
				contentType : false
			}).done(function(data){
				showToastr('success', 'Successfully uploaded!', "<br><br><b> Dismiss </b>", "one");
				//$('html').load("/officerDashboard/VerificationApplication#2")
				$("#formU")[0].reset();
				//window.location.replace("/officerDashboard/VerificationApplication#2");
				
				//location.reload();
				//$(document).scrollTop(0);
				window.location.reload();
				//window.location.hash = hash;
				})
		     .fail(function(errMsg) {
		    	 showToastr('error', 'Something went wrong!', "Please try again! <br><br><b> Dismiss </b>");
		    	 
		     });
	    }else{
	    	alert("Files need to upload!");
	    }		
}

$body = $("body");

$(document).on({
	ajaxStart : function() {
		$body.addClass("loading");
	},
	ajaxStop : function() {
		$body.removeClass("loading");
		//showToastr('success', 'Successfully sent!', "Dismiss");
	}
});

function closeLoading(){
	$body.removeClass("loading");
}

function showToastr(type, title, messsage, hash) {
	let body;
	toastr.options = {
		"closeButton" : false,
		"debug" : false,
		"newestOnTop" : false,
		"progressBar" : false,
		"positionClass" : 'toast-top-center',
		"preventDuplicates" : true,
		"showDuration" : "300",
		"hideDuration" : "1000",
		"timeOut" : 5000,
		"onclick" : null,
		"onCloseClick" : null,
		"extendedTimeOut" : 1000,
		"showEasing" : "swing",
		"hideEasing" : "linear",
		"showMethod" : "fadeIn",
		"hideMethod" : "fadeOut",
		"tapToDismiss" : true
	};
	toastr.options.onHidden = function(){
		};
	switch (type) {
	case "info":
		body = "<span> <i class='fa fa-spinner fa-pulse'></i></span>";
		break;
	default:
		body = '';
	}
	const content = messsage + body;
	toastr[type](content, title)
}



</script>
</html>