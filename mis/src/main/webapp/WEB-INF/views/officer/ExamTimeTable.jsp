
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="PageDetails">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/officer.css" rel="stylesheet">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link rel="stylesheet" href="css/toastr.min.css">

<script type="text/javascript" src="js/Sortable.min.js"></script>
<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript" src="js/jquery-sortable.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$('.modal').modal();
		/* $('#AddEmployee').on('click', function() {
		});
		$("#AddEmployee").modal({
			dismissible : false
		});
		
		$('#EditEmployee').on('click', function() {
		});
		$("#EditEmployee").modal({
			dismissible : false
		});

		$('#DeleteEmployee').on('click', function() {
		});
		$("#DeleteEmployee").modal({
			dismissible : false
		}); */
		//$('select').formSelect();
		$('.tabs').tabs();
		$('.sidenav').sidenav({
			draggable : true,
		});
		$('.dropdown-trigger').dropdown();
		
		
		$('#exam').formSelect();
		
	});
</script>
<style>
.main_page {
	margin: 0% 2% 0% 2%;
}

.sidenav-trigger {
	display: block !important;
}

/* .card {
	width: 100%;
	padding: 0% 0% 15% 0%;
} */

/* .input-field label {
	color: #212121 !important;
	font-size: 20px !important;
} */

/* .input-field input[type=text]:focus {
	border-bottom: 1px solid #212121 !important;
	box-shadow: 0 1px 0 0 #212121 !important;
} */
disabled {
	pointer-events: none;
}

.ghost {
	opacity: 0.4;
}
.list-group {
	margin: 20px;
}
.highlight{ 
  background:#00FF00; 
  padding:1px; 
  border:#00CC00 dotted 1px; 
}
</style>

<body ng-controller="officerPageData">

	<div id="top">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="/logout">Logout</a></li>
		</ul>
		<nav class="navbar">
			<div class="nav-wrapper">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i
					class="material-icons">menu</i></a>
				<ul class="right">
					<li><a class="dropdown-trigger" href="#!"
						data-target="dropdown1">{{firstName}} {{lastName}}<i
							class="material-icons right">arrow_drop_down</i></a></li>
				</ul>

			</div>

		</nav>

	</div>

	<div id="left-side">
		<ul id="slide-out" class="sidenav z-depth-4">
			<li>

				<p class="MIS-logo">MIS</p>
			</li>
			<li><div class="divider"></div></li>
			<li class="nav-item active "><a class="dashboard_link nav-link"
				href="/officerDashboard"> <i class="material-icons">dashboard</i>
					<p>Dashboard</p>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="/officerDashboard/myAccount"> <i class="material-icons">person</i>
					<p>My Account</p>
			</a></li>
			<!-- <li class="nav-item"><a class="nav-link"
			href="./notifications.html"> <i class="material-icons">notifications</i>
				<p>Notifications</p>
		</a></li> -->
		</ul>
	</div>


	<div id="main_page" class="main_page">



		<h4>Exam Time Table Creation</h4>
		<div class="divider"></div>

		<form id="form">
			<div class="col s12 m12 l12">
				<div class="file-field input-field">
					<div class="btn">
						<span>Exam Documents (Class Lists)</span> <input type="file"
							accept=".xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
							name="examData" multiple required="required">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					</div>
				</div>
			</div>
			<div class="input-field col s12 m10 l10">
				<input id="examName" name="examName" required
					pattern="[0-9A-Za-z '()\/\-&:.,]{3,}"
					title="Please enter english characters, numbers and ' ( ) / - & : . and , only."
					type="text" class="validate"> <label for="examName">Exam
					Name:</label>
			</div>

			<div class="input-field col s12 m12 l12 right-align">

				<input type="reset"
					class="waves-effect waves-light btn #e8eaf6 indigo lighten-5"
					value="Reset"> <input type="submit"
					class="waves-effect waves-light btn light-blue darken-5"
					value="submit">

			</div>
		</form>


		<form id="selectExamForm">
			<h4>Exam</h4>
			<div class="col s12 m12 l12">
				<select ng-model="selectExam" id="exam" class="browser-default">
					<option value="" disabled selected>Select Exam</option>
					<option ng-repeat="i in listofExams">{{i}}</option>
				</select>

			</div>
			<div class="input-field col s12 m12 l12 right-align">
				<button class="btn waves-effect waves-light" type="submit"
					name="action" id="selectExambtn"
					ng-click="showSubjects(selectExam)">
					search <i class="material-icons right">search</i>
				</button>
			</div>
		</form>
		<table>
		<tr ng-repeat="i in listofCourses">
			<td>
			{{i}}
			</td>
		</tr>
		</table>

		<form id="getTimeslots">
			<h4>Exam</h4>
			<div class="col s12 m12 l12">
				<select ng-model="selectExamtogetSlots" id="setExam" class="browser-default">
					<option value="" disabled selected>Select Exam</option>
					<option ng-repeat="i in listofExams">{{i}}</option>
				</select>

			</div>
			<div class="input-field col s12 m12 l12 right-align">
				<button class="btn waves-effect waves-light" type="submit"
					name="action" id="selectExambtn"
					ng-click="showSubjectsInSolts(selectExamtogetSlots)">
					search <i class="material-icons right">search</i>
				</button>
			</div>
		</form>
		
		<!-- <table>
		<tr ng-repeat="i in setofCourses">
			<td>
			{{i}}
			</td>
		</tr>
		</table>
		<br> -->
		<!-- https://github.com/SortableJS/jquery-sortablejs -->

		<div id="timeslots" class="row">
			<div id="{{i}}" class="list-group slot col" ng-repeat="(i,set) in setofSlots">
					<div id="{{key}}"  class="list-group-item slot-item nested-1 card" ng-repeat="(key,subject) in set">{{subject}}</div>
			</div>
		</div>
		
		
		
		<script>
		
		
		
		
		//var list = document.getElementsByClassName("slot");
		
		//console.log(list);
		
		
		</script>

	</div>
</body>

<script>



	$('#form')
			.submit(
					function(e) {
						$
								.ajax(
										{
											url : '/officerDashboard/exam/uploadExamData',
											type : 'POST',
											data : new FormData(this),
											processData : false,
											contentType : false
										})
								.done(
										function(data) {
											showToastr('success',
													'Successfully uploaded!',
													"<br><br><b> Dismiss </b>");
											$("#form")[0].reset();
											location.reload();
										})
								.fail(
										function(errMsg) {
											showToastr('error',
													'Something went wrong!',
													"Please try again! <br><br><b> Dismiss </b>");
										});
						e.preventDefault();
					});


	$body = $("body");

	$(document).on({
		ajaxStart : function() {
			$body.addClass("loading");
		},
		ajaxStop : function() {
			$body.removeClass("loading");
		}
	});

	function showToastr(type, title, messsage) {
		let body;
		toastr.options = {
			"closeButton" : false,
			"debug" : false,
			"newestOnTop" : false,
			"progressBar" : false,
			"positionClass" : 'toast-bottom-center',
			"preventDuplicates" : true,
			"showDuration" : "300",
			"hideDuration" : "1000",
			"timeOut" : 5000,
			"onclick" : null,
			"onCloseClick" : null,
			"extendedTimeOut" : 1000,
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut",
			"tapToDismiss" : true
		};
		switch (type) {
		case "info":
			body = "<span> <i class='fa fa-spinner fa-pulse'></i></span>";
			break;
		default:
			body = '';
		}
		const content = messsage + body;
		toastr[type](content, title)
	}
</script>
</html>