<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="PageDetails">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MIS</title>
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/officer.css" rel="stylesheet">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/materialize.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link rel="stylesheet" href="css/toastr.min.css">

<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<!-- <script type="text/javascript" src="js/officer.js"></script> -->
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/toastr.min.js"></script>
<script type="text/javascript" src="js/lodash.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.modal').modal();
		$('#AddEmployee').on('click', function() {
		});
		$("#AddEmployee").modal({
			dismissible : false
		});
		
		$('#EditEmployee').on('click', function() {
		});
		$("#EditEmployee").modal({
			dismissible : false
		});

		$('#DeleteEmployee').on('click', function() {
		});
		$("#DeleteEmployee").modal({
			dismissible : false
		});
		$('select').formSelect();

	});
	$(document).ready(function() {
		$('.sidenav').sidenav({
			draggable : true,
		});
		$('.modal').modal();
		$('.dropdown-trigger').dropdown();
	});
	$(document).ready(function() {
		$('.tabs').tabs();

	});
</script>
<style>
.main_page {
	margin: 0% 2% 0% 2%;
}

.sidenav-trigger {
	display: block !important;
}

/* .card {
	width: 100%;
	padding: 0% 0% 15% 0%;
} */

.input-field label {
	color: #212121 !important;
	font-size: 20px !important;
}

.input-field input[type=text]:focus {
	border-bottom: 1px solid #212121 !important;
	box-shadow: 0 1px 0 0 #212121 !important;
}

disabled {
    pointer-events: none;
}
</style>
<body ng-controller="officerPageData">

	<div id="top">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="/logout">Logout</a></li>
		</ul>
		<nav class="navbar">
			<div class="nav-wrapper">
				<a href="#" data-target="slide-out" class="sidenav-trigger"><i
					class="material-icons">menu</i></a>
				<ul class="right">
					<li><a class="dropdown-trigger" href="#!"
						data-target="dropdown1">{{firstName}} {{lastName}}<i
							class="material-icons right">arrow_drop_down</i></a></li>
				</ul>

			</div>

		</nav>

	</div>

	<div id="left-side">
		<ul id="slide-out" class="sidenav z-depth-4">
			<li>

				<p class="MIS-logo">MIS</p>
			</li>
			<li><div class="divider"></div></li>
			<li class="nav-item active "><a class="dashboard_link nav-link"
				href="/officerDashboard"> <i class="material-icons">dashboard</i>
					<p>Dashboard</p>
			</a></li>
			<li class="nav-item"><a class="nav-link"
				href="/officerDashboard/myAccount"> <i class="material-icons">person</i>
					<p>My Account</p>
			</a></li>
			<!-- <li class="nav-item"><a class="nav-link"
			href="./notifications.html"> <i class="material-icons">notifications</i>
				<p>Notifications</p>
		</a></li> -->
		</ul>
	</div>


	<div id="main_page" class="main_page">
		<!-- <h5> Employee Accounts</h5>
	<div class="divider"></div> -->
		<div class="row">
			<div class="col s12 m12 l4">
				<div class="card cardHover">
					<!-- <div class="card-image waves-effect waves-block waves-light">
                   image link 
                 </div> -->
					<a href="#AddEmployee" class="black-text modal-trigger"
						ng-click="openAddEmployeeForm()">
						<div class="card-content  waves-effect waves-block waves-light">
							<span class="card-title activator text-darken-4"> ADD
								EMPLOYEES<i class="material-icons right"
								style="font-size: 55px;">group_add</i>
							</span>
						</div>
					</a>
				</div>
				<blockquote>
					<div style="">
						<h5>{{pleaseTryAgain}}</h5>
					</div>
				</blockquote>
			</div>
		</div>

		<!-- Employee data and employee data by department -->
		<div class="row">

			<div class="col s12 m12 l12">
				<ul class="tabs" style="font: bold;">
					<li class="tab col s2"><a class="active" href="#one">{{employee_departmentCode}}</a></li>
					<li class="tab col s2"><a href="#two">All</a></li>
				</ul>
			</div>

		</div>

		<div id="one">
			<h4>Employee List</h4>
			<div class="divider"></div>
			<div class="row">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb blue lighten-4">
							<th>Emp. No</th>
							<th>title</th>
							<th>Surname With Initials</th>
							<th>Names Denoted By Initials</th>
							<th>NIC</th>
							<th>Designation</th>
							<th>Present Division1</th>
							<th>Department Code</th>
							<th>Telephone</th>
							<th>Type</th>
							<th>Email</th>
							<th>Mobile Number</th>
							<th>Address</th>
							<th>Availability</th>
							<th>Remark</th>
						</tr>
						<tr ng-repeat="i in employeesByDepartment">
							<td>{{i.employeeNumber}}</td>
							<td>{{i.title}}</td>
							<td>{{i.surnameWithInitials}}</td>
							<td>{{i.namesDenotedByInitials}}</td>
							<td>{{i.nic}}</td>
							<td>{{i.designation}}</td>
							<td>{{i.presentDivision1}}</td>
							<td>{{i.departmentCode}}
							<td>{{i.telephone}}</td>
							<td>{{i.type}}</td>
							<td>{{i.email}}</td>
							<td>{{i.mobileNumber}}</td>
							<td>{{i.address}}</td>
							<td>{{i.availability}}</td>
							<td>{{i.remark}}</td>
							

							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  blue darken-1"
								href="#EditEmployee"
								ng-click="getEmployeeByEmpNumber(i.employeeNumber)"><i
									class="material-icons">edit</i></a></td>
							<td><a
								class="waves-effect waves-light btn modal-trigger modal-close  red darken-1"
								href="#DeleteEmployee"
								ng-click="deleteEmployeeByEmpNo(i.employeeNumber)"><i
									class="material-icons">delete</i></a></td>

						</tr>
						<tr style="border-bottom: 1px solid white;">
							<td colspan="19" class="center-align"><b>{{NotFoundError}}</b></td>
						</tr>
					</table>
				</div>
			</div>
	</div>
	<div id="two">
		<h4>Employee List</h4>
			<div class="divider"></div>
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="card z-depth-4">
						<div class="card-content">
							<span class="card-title"><b>Search Employees</b></span>
							<form>
								<div class="row">
									<div class="input-field col s12 m12 l4">
										<input id="employeeNumber" name="employeeNumber" type="text"
											value="" placeholder="" class="validate" ng-model="emNumber">
										<label for="employeeNumber"> Employee No.</label>
									</div>
									<div class="input-field col s12 m12 l8">
										<input id="surnameWithInitials" name="surnameWithInitials"
											value="" placeholder="" type="text" class="validate"
											ng-model="empSurname"> <label
											for="surnameWithInitials"> Surname</label>
									</div>
									<div class="input-field col s12 m12 l6">
										<input id="designation" name="designation" value=""
											placeholder="" type="text" class="validate"
											ng-model="empDesig"> <label>Designation</label>
									</div>
									<div class="input-field col s12 m12 l6">
										<input id="presentDivision" name="presentDivision" value=""
											placeholder="" type="text" class="validate"
											ng-model="presentDivi"> <label>Present
											Division</label>
									</div>
									<div class="input-field col s12 m12 l6">
										<select id="type" name="type" ng-model="type">
											<option value="" disabled selected>Choose Type</option>
											<option value="" selected>--Any--</option>
											<option value="Academic">Academic</option>
											<option value="Non Academic">Non-Academic</option>
											<option value="Academic Supportive">Academic
												Supportive</option>
											<option value="Other">Other</option>
										</select> <label>Staff Type</label>
									</div>
									<div class="input-field col s12 m12 l12 right-align">
										<button class="btn waves-effect waves-light" type="submit"
											name="action" id="empSearch"
											ng-click="empSearch(emNumber,empSurname,empDesig,presentDivi,type)">
											search <i class="material-icons right">search</i>
										</button>
										<a href="{{employeesSheetURL}}"
											class="btn {{disabled}} waves-effect waves-light"
											name="action" id="empSearchDownload"
											ng-click="empSearchDownload(emNumber,empSurname,empDesig,presentDivi,type)">
											download<i class="material-icons right">download</i>
										</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<button
				class="waves-effect waves-light btn-large #1a237e indigo darken-4 {{disabled}}"
				id="summary_btn">
				<i class="material-icons left">summarize</i>Summary
			</button>
			<script>
				$('#summary_btn').on('click', function() {
					$('#summary').toggle(1500);
					$("html").animate({
						scrollTop : $("#summary_btn").offset().top
					}, 1200);
				});
			</script>
			<div class="divider"></div>
			<br>
			<div id="summary" style="display: none;">
				<div class="row">
					<div class="col s12 m12 l12">
						<div class="card z-depth-4">
							<div class="card-content">
								<span class="card-title"><b>Summary</b></span> <br>
								<div class="row">
									<div class="col s6 m6 l6">
										<table class="responsive-table">
											<tr class="#e0e0e0 grey lighten-2">
												<th>Type</th>
												<th>Count</th>
											</tr>
											<tr ng-repeat="(key,value) in groupedbytype">
												<td>{{key}}</td>
												<td>{{value}}</td>
											</tr>
										</table>
									</div>

									<div class="col s6 m6 l6">
										<table class="responsive-table">
											<tr class="#e0e0e0 grey lighten-2">
												<th>Designation</th>
												<th>Count</th>
											</tr>
											<tr ng-repeat="(key,value) in groupedbydesignation">
												<td>{{key}}</td>
												<td>{{value}}</td>
											</tr>
										</table>
									</div>

									<div class="col s12 m12 l12">
										<div class="right-align">
											<h4>
												Total: <b>{{Total_count}}</b>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l12">
					<table class="responsive-table">

						<tr class="#bbdefb blue lighten-4">
							<th>Emp. No</th>
							<th>title</th>
							<th>Surname With Initials</th>
							<th>Names Denoted By Initials</th>
							<th>Designation</th>
							<th>Present Division1</th>
							<th>Telephone</th>
							<th>Type</th>
							<th>Email</th>
							<th>Mobile Number</th>
							<th>Address</th>
							<th>Availability</th>
							<th>Remark</th>
						</tr>
						<tr ng-repeat="i in employees">
							<td>{{i.employeeNumber}}</td>
							<td>{{i.title}}</td>
							<td>{{i.surnameWithInitials}}</td>
							<td>{{i.namesDenotedByInitials}}</td>
							<td>{{i.designation}}</td>
							<td>{{i.presentDivision1}}</td>
							<td>{{i.telephone}}</td>
							<td>{{i.type}}</td>
							<td>{{i.email}}</td>
							<td>{{i.mobileNumber}}</td>
							<td>{{i.address}}</td>
							<td>{{i.availability}}</td>
							<td>{{i.remark}}</td>
						</tr>
						<tr style="border-bottom: 1px solid white;">
							<td colspan="19" class="center-align"><b>{{NotFoundError}}</b></td>
						</tr>
					</table>
				</div>
			</div>
	</div>
		

	<!-- ADD EMPLOYEES -->
	<div id="AddEmployee" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12 m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
					<h5>ADD EMPLOYEES</h5>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s3"><a class="active" href="#single">Add
								Employee</a></li>
						<li class="tab col s3"><a href="#more">More Employees</a></li>
					</ul>
				</div>
				<div id="single" class="col s12">
					<form id="one_form">
						<div class="row">
							<div class="input-field col s12 m12 l4">
								<input id="employeeNumber" name="employeeNumber" type="text"
									class="validate" ng-model="employee_employeeNumber" placeholder="" > <label
									for="employeeNumber"> emp. Number</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="title" name="title" type="text" class="validate"
									ng-model="employee_title" placeholder="" > <label for="title">
									title</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="surnameWithInitials" name="surnameWithInitials"
									type="text" class="validate"
									ng-model="employee_surnameWithInitials" placeholder="" > <label
									for="surnameWithInitials"> Surname With Initials</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="namesDenotedByInitials" name="namesDenotedByInitials"
									type="text" class="validate"
									ng-model="employee_namesDenotedByInitials" placeholder="" > <label
									for="namesDenotedByInitials"> Names Denoted By Initials</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="nic" name="nic" type="text" class="validate"
									ng-model="employee_nic" placeholder="" > <label for="nic" > NIC</label>
							</div>
							<div class="input-field col s12 m12 l6">
							
									<input id="designation" name="designation" value=""
										type="text" class="validate" placeholder=""
										ng-model="employee_designation">
									<label for="designation">Designation</label>
								</div>
							
							<div class="input-field col s12 m12 l6">
									<input id="presentDivision1" name="presentDivision1" value="" placeholder=""
										type="text" class="validate" ng-model="employee_presentDivision1">
								    <label for="presentDivision1">Present Division</label>
								</div>
							
							<div class="input-field col s12 m12 l4">
								<input id="departmentCode" name="departmentCode" type="text"
									 ng-model="employee_departmentCode" placeholder="" readonly> <label
									for="departmentCode"> Department Code</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="telephone" name="telephone" type="text"
									class="validate" ng-model="employee_telephone" placeholder="" > <label
									for="telephone"> Telephone</label>
							</div>
							<div class="input-field col s12 m12 l6">
								    <select id="type" name="type" ng-model="employee_type" >
								    	<option value="" disabled selected>Choose Type</option>
								    	<option value="" selected>--Any--</option>
										<option value="Academic">Academic</option>
										<option value="Non Academic">Non-Academic</option>
										<option value="Academic Supportive">Academic Supportive</option>
										<option value="Other">Other</option>
								    </select>
								    <label for="type">Staff Type</label>
								</div>
							
							<div class="input-field col s12 m12 l4">
								<input id="email" name="email" type="email" class="validate"
									ng-model="employee_email" placeholder="" > <label for="email">
									Email</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="mobileNumber" name="mobileNumber" type="text"
									class="validate" ng-model="employee_mobileNumber" placeholder="" > <label
									for="mobileNumber"> Mobile Number</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="address" name="address" type="text" class="validate"
									ng-model="employee_address" placeholder="" > <label for="address">
									Address</label>
							</div>
							<div class="input-field col s12 m12 l4">
								  <select id="availability" name="availability" ng-model="employee_availability" >
								    	<option value="" disabled selected>--Select--</option>
										<option value="yes">Available</option>
										<option value="no">Not-Available</option>
								    </select> <label for="availability">
									Availability<label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="remark" name="remark" type="text" class="validate"
									ng-model="employee_remark" placeholder="" > <label for="remark">Remark (Other details)<label>
							</div>
							
						</div>
						<div class="raw right-align">
								
								<input type="submit" class="btn waves-effect waves-light modal-close" name="action" id="addAction"
									ng-click="addEmployee(employee_employeeNumber,
															employee_title,
															employee_surnameWithInitials,
															employee_namesDenotedByInitials,
															employee_nic,
															employee_designation,
															employee_presentDivision1,
															employee_departmentCode,
															employee_telephone,
															employee_type,
															employee_email,
															employee_mobileNumber,
															employee_address,
															employee_availability,
															employee_remark)">
								
							</div>
					</form>
				</div>
				<div id="more" class="col s12">
					<h5>Upload Excel sheet here!</h5>
					<form id="form" enctype="multipart/form-data">
						<div class="file-field input-field">
							<div class="btn">
								<span>Excel sheet of Employee Details</span> <input type="file"
									accept=".xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
									name="employeeExcel" ng-model="employeeExcel" required>
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>

						</div>

						<div class="input-field col s12 m12 l12">
							<div class="modal-footer">

								<input type="submit"
									class="waves-effect waves-light modal-close btn light-blue darken-1"
									value="submit">

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- EDIT EMPLOYEES -->
	<div id="EditEmployee" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12 m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
					<h5>EDIT EMPLOYEES</h5>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s3"><a class="active" href="#one">Edit
								Employee</a></li>
						<!-- <li class="tab col s3"><a href="#two">More Employees</a></li> -->
					</ul>
				</div>
				<div id="one" class="col s12">
					<form id="one_form">
						<div class="row">
							<div class="input-field col s12 m12 l4">
								<input id="employeeNumber" name="employeeNumber" type="text" placeholder ="" 
									class="validate" ng-model="e_empNo" value="{{e_empNo}}" > <label
									for="employeeNumber"> Emp. Number</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="title" name="title" type="text" class="validate"
									ng-model="e_empTitle" placeholder ="" value="{{e_empTitle}}" required> <label for="title">
									title</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="surnameWithInitials" name="surnameWithInitials"
									type="text" class="validate"
									ng-model="e_empsurnameWithInitials" placeholder ="" value="{{e_empsurnameWithInitials}}" required> <label
									for="surnameWithInitials"> Surname With Initials</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="namesDenotedByInitials" name="namesDenotedByInitials"
									type="text" class="validate"
									ng-model="e_empnamesDenotedByInitials" placeholder ="" value="{{e_empnamesDenotedByInitials}}" required> <label
									for="namesDenotedByInitials"> Names Denoted By Initials</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="nic" name="nic" type="text" class="validate"
									ng-model="e_empnic" placeholder ="" value="{{e_empnic}}" required> <label for="nic" > NIC</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="designation" name="designation" type="text"
									class="validate" ng-model="e_empdesignation"  placeholder ="" value="{{e_empdesignation}}"  required> <label
									for="designation"> Designation</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="presentDivision1" name="presentDivision1" type="text"
									class="validate" ng-model="e_emppresentDivision1" placeholder ="" value="{{e_emppresentDivision1}}"  required>
								<label for="presentDivision1"> Present Division1</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="e_departmentCode" name="departmentCode" type="text"
									 ng-model="e_departmentCode" placeholder="" readonly> <label
									for="e_departmentCode"> Department Code</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="telephone" name="telephone" type="text"
									class="validate" ng-model="e_emptelephone" placeholder ="" value="{{e_emptelephone}}" required> <label
									for="telephone"> Telephone</label>
							</div>
							<!-- <div class="input-field col s12 m12 l4">
							Type ( Academic / Non Academic / Academic Supportive / Other)
								<input id="type" name="type" type="text" class="validate"
									ng-model="e_emptype"  placeholder ="" value="{{e_emptype}}" required> 
									
							</div> -->
							<div class="input-field col s12 m12 l6">
								    <select id="type" name="type" ng-model="e_emptype" >
								    	<option value="" disabled selected>Choose Type</option>
										<option value="Academic">Academic</option>
										<option value="Non Academic">Non-Academic</option>
										<option value="Academic Supportive">Academic Supportive</option>
										<option value="Other">Other</option>
								    </select>
								    <label for="type">Staff Type</label>
								</div>
							
							
							
							<div class="input-field col s12 m12 l4">
								<input id="email" name="email" type="text" class="validate"
									ng-model="e_empemail" placeholder ="" value="{{e_empemail}}" required> <label for="email">
									Email</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="mobileNumber" name="mobileNumber" type="text"
									class="validate" ng-model="e_empmobileNumber" placeholder ="" value="{{e_empmobileNumber}}" required> <label
									for="mobileNumber"> Mobile Number</label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="address" name="address" type="text" class="validate"
									ng-model="e_empaddress" placeholder ="" value="{{e_empaddress}}" required> <label for="address">
									Address</label>
							</div>
							<div class="input-field col s12 m12 l4">
								  <select id="e_availability" name="availability" ng-model="e_availability" >
								    	<option value="" disabled selected>--Select--</option>
										<option value="yes">Available</option>
										<option value="no">Not-Available</option>
								    </select> <label for="e_availability">
									Availability<label>
							</div>
							<div class="input-field col s12 m12 l4">
								<input id="e_remark" name="remark" type="text" class="validate"
									ng-model="e_remark" placeholder="" > <label for="e_remark">Remark (Other details)<label>
							</div>
							<div class="input-field col s12 m12 l4">
								
								<input type="submit" class="btn waves-effect waves-light modal-close" name="action" id="addAction"
									ng-click="editEmployee(e_empNo,
															e_empTitle,
															e_empsurnameWithInitials,
															e_empnamesDenotedByInitials,
															e_empnic,
															e_empdesignation,
															e_emppresentDivision1,
															e_departmentCode,
															e_emptelephone,
															e_emptype,
															e_empemail,
															e_empmobileNumber,
															e_empaddress,
															e_availability,
															e_remark
															)">
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- DELETE EMPLOYEE -->
	<div id="DeleteEmployee" class="modal">
		<div class="modal-content">
			<div class="raw">
				<div class="col s12  m12 l12">
					<div class="right-align">
						<button
							class="modal-close btn waves-effect waves-light pink darken-1">
							<i class="material-icons">close</i>
						</button>
					</div>
				</div>
				<div class="col s11 m11 l11">
					<h5>Do you need to delete this Employee's Account?</h5>
					<br> Employee No : {{deleteEmployeeEmpNo}}<br>
				</div>
			</div>
			<div class="raw">
				<form>
					<div class="right-align">
						<button
							class="btn waves-effect waves-light modal-close blue-text blue lighten-5">
							No</button>
						<button
							class="btn waves-effect waves-light modal-close red darken-1"
							type="submit" name="action"
							ng-click="deleteEmployee(deleteEmployeeEmpNo)">Delete</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

<script>
	$('#form')
			.submit(
					function(e) {
						$
								.ajax(
										{
											url : '/officerDashboard/employeeDetails/employeeExcel',
											type : 'POST',
											data : new FormData(this),
											processData : false,
											contentType : false
										})
								.done(
										function(data) {
											showToastr('success',
													'Successfully uploaded!',
													"<br><br><b> Dismiss </b>");
											$("#form")[0].reset();
											location.reload();
										})
								.fail(
										function(errMsg) {
											showToastr('error',
													'Something went wrong!',
													"Please try again! <br><br><b> Dismiss </b>");
										});
						e.preventDefault();
					});
	
	$('#one_form')
	.submit(
			function(e) {
				$("#one_form")[0].reset();
				e.preventDefault();
			});
	
	
	$body = $("body");

	$(document).on({
		ajaxStart : function() {
			$body.addClass("loading");
		},
		ajaxStop : function() {
			$body.removeClass("loading");
		}
	});

	function showToastr(type, title, messsage) {
		let body;
		toastr.options = {
			"closeButton" : false,
			"debug" : false,
			"newestOnTop" : false,
			"progressBar" : false,
			"positionClass" : 'toast-bottom-center',
			"preventDuplicates" : true,
			"showDuration" :"300",
			"hideDuration" :"1000",
			"timeOut" : 5000,
			"onclick" : null,
			"onCloseClick" : null,
			"extendedTimeOut" : 1000,
			"showEasing" :"swing",
			"hideEasing" :"linear",
			"showMethod" :"fadeIn",
			"hideMethod" :"fadeOut",
			"tapToDismiss" : true
		};
		switch (type) {
		case"info":
			body ="<span> <i class='fa fa-spinner fa-pulse'></i></span>";
			break;
		default:
			body = '';
		}
		const content = messsage + body;
		toastr[type](content, title)
	}
</script>
</html>