package lk.ac.pdn.eng.mis.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table (name = "pgstudent")
public class PGStudent {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int studentDataId;
	
	int year;
	String type; //degree level - PhD/M Phil/MSc/Diploma
	String department;
	String registrationNo;
	String title;
	String namewithinitials;
	String namedenotedbyInitials;
	String dateofRegistration;
	String id; //NIC
	
	@Lob 
	private byte[] photo;
	
	String contactNumber_s; //number(s)
	String emailAddress_s; //address(s)
	String postalAddress;
	String undergraduateDegree;
	String undergraduateClass;
	String undergraduateUniversity;
	String experienceinfield;
	String designation;
	String mScOrMScEng;
	String partTimeOrFullTime;
	String hDCMeetingNumber;
	String affiliatedLaboratory;
	String researchTitle;
	String supervisor_s; //supervisor(s)
	String examiner_s; //examiner(s)
	String stream;
	
	String programme;
	double courseFee;
	double libraryFee;
	double tuitionFee;
	double registrationFee;
	double firstYearPaymeent;
	double secondYearPaymeent;
	double thirdYearPaymeent;
	double fourthYearPaymeent;
	double programmeFee;
	double totalPaymentMade;
	
	
	public PGStudent() {
		super();
	}

	public PGStudent(int studentDataId, int year, String type, String department, String registrationNo, String title,
			String namewithinitials, String namedenotedbyInitials, String dateofRegistration, String id, byte[] photo,
			String contactNumber_s, String emailAddress_s, String postalAddress, String undergraduateDegree,
			String undergraduateClass, String undergraduateUniversity, String experienceinfield, String designation,
			String mScOrMScEng, String partTimeOrFullTime, String hDCMeetingNumber, String affiliatedLaboratory,
			String researchTitle, String supervisor_s, String examiner_s, String stream, int payment) {
		super();
		this.studentDataId = studentDataId;
		this.year = year;
		this.type = type;
		this.department = department;
		this.registrationNo = registrationNo;
		this.title = title;
		this.namewithinitials = namewithinitials;
		this.namedenotedbyInitials = namedenotedbyInitials;
		this.dateofRegistration = dateofRegistration;
		this.id = id;
		this.photo = photo;
		this.contactNumber_s = contactNumber_s;
		this.emailAddress_s = emailAddress_s;
		this.postalAddress = postalAddress;
		this.undergraduateDegree = undergraduateDegree;
		this.undergraduateClass = undergraduateClass;
		this.undergraduateUniversity = undergraduateUniversity;
		this.experienceinfield = experienceinfield;
		this.designation = designation;
		this.mScOrMScEng = mScOrMScEng;
		this.partTimeOrFullTime = partTimeOrFullTime;
		this.hDCMeetingNumber = hDCMeetingNumber;
		this.affiliatedLaboratory = affiliatedLaboratory;
		this.researchTitle = researchTitle;
		this.supervisor_s = supervisor_s;
		this.examiner_s = examiner_s;
		this.stream = stream;
		this.totalPaymentMade = payment;
	}

	public int getStudentDataId() {
		return studentDataId;
	}

	public void setStudentDataId(int studentDataId) {
		this.studentDataId = studentDataId;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNamewithinitials() {
		return namewithinitials;
	}

	public void setNamewithinitials(String namewithinitials) {
		this.namewithinitials = namewithinitials;
	}

	public String getNamedenotedbyInitials() {
		return namedenotedbyInitials;
	}

	public void setNamedenotedbyInitials(String namedenotedbyInitials) {
		this.namedenotedbyInitials = namedenotedbyInitials;
	}

	public String getDateofRegistration() {
		return dateofRegistration;
	}

	public void setDateofRegistration(String dateofRegistration) {
		this.dateofRegistration = dateofRegistration;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getContactNumber_s() {
		return contactNumber_s;
	}

	public void setContactNumber_s(String contactNumber_s) {
		this.contactNumber_s = contactNumber_s;
	}

	public String getEmailAddress_s() {
		return emailAddress_s;
	}

	public void setEmailAddress_s(String emailAddress_s) {
		this.emailAddress_s = emailAddress_s;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getUndergraduateDegree() {
		return undergraduateDegree;
	}

	public void setUndergraduateDegree(String undergraduateDegree) {
		this.undergraduateDegree = undergraduateDegree;
	}

	public String getUndergraduateClass() {
		return undergraduateClass;
	}

	public void setUndergraduateClass(String undergraduateClass) {
		this.undergraduateClass = undergraduateClass;
	}

	public String getUndergraduateUniversity() {
		return undergraduateUniversity;
	}

	public void setUndergraduateUniversity(String undergraduateUniversity) {
		this.undergraduateUniversity = undergraduateUniversity;
	}

	public String getExperienceinfield() {
		return experienceinfield;
	}

	public void setExperienceinfield(String experienceinfield) {
		this.experienceinfield = experienceinfield;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getmScOrMScEng() {
		return mScOrMScEng;
	}

	public void setmScOrMScEng(String mScOrMScEng) {
		this.mScOrMScEng = mScOrMScEng;
	}

	public String getPartTimeOrFullTime() {
		return partTimeOrFullTime;
	}

	public void setPartTimeOrFullTime(String partTimeOrFullTime) {
		this.partTimeOrFullTime = partTimeOrFullTime;
	}

	public String gethDCMeetingNumber() {
		return hDCMeetingNumber;
	}

	public void sethDCMeetingNumber(String hDCMeetingNumber) {
		this.hDCMeetingNumber = hDCMeetingNumber;
	}

	public String getAffiliatedLaboratory() {
		return affiliatedLaboratory;
	}

	public void setAffiliatedLaboratory(String affiliatedLaboratory) {
		this.affiliatedLaboratory = affiliatedLaboratory;
	}

	public String getResearchTitle() {
		return researchTitle;
	}

	public void setResearchTitle(String researchTitle) {
		this.researchTitle = researchTitle;
	}

	public String getSupervisor_s() {
		return supervisor_s;
	}

	public void setSupervisor_s(String supervisor_s) {
		this.supervisor_s = supervisor_s;
	}

	public String getExaminer_s() {
		return examiner_s;
	}

	public void setExaminer_s(String examiner_s) {
		this.examiner_s = examiner_s;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	
	public String getProgramme() {
		return programme;
	}

	public void setProgramme(String programme) {
		this.programme = programme;
	}

	public double getCourseFee() {
		return courseFee;
	}

	public void setCourseFee(double courseFee) {
		this.courseFee = courseFee;
	}

	public double getLibraryFee() {
		return libraryFee;
	}

	public void setLibraryFee(double libraryFee) {
		this.libraryFee = libraryFee;
	}

	public double getTuitionFee() {
		return tuitionFee;
	}

	public void setTuitionFee(double tuitionFee) {
		this.tuitionFee = tuitionFee;
	}

	public double getRegistrationFee() {
		return registrationFee;
	}

	public void setRegistrationFee(double registrationFee) {
		this.registrationFee = registrationFee;
	}

	public double getFirstYearPaymeent() {
		return firstYearPaymeent;
	}

	public void setFirstYearPaymeent(double firstYearPaymeent) {
		this.firstYearPaymeent = firstYearPaymeent;
	}

	public double getSecondYearPaymeent() {
		return secondYearPaymeent;
	}

	public void setSecondYearPaymeent(double secondYearPaymeent) {
		this.secondYearPaymeent = secondYearPaymeent;
	}

	public double getThirdYearPaymeent() {
		return thirdYearPaymeent;
	}

	public void setThirdYearPaymeent(double thirdYearPaymeent) {
		this.thirdYearPaymeent = thirdYearPaymeent;
	}

	public double getFourthYearPaymeent() {
		return fourthYearPaymeent;
	}

	public void setFourthYearPaymeent(double fourthYearPaymeent) {
		this.fourthYearPaymeent = fourthYearPaymeent;
	}

	public double getProgrammeFee() {
		return programmeFee;
	}

	public void setProgrammeFee(double programmeFee) {
		this.programmeFee = programmeFee;
	}

	public double getTotalPaymentMade() {
		return totalPaymentMade;
	}

	public void setTotalPaymentMade(double totalPaymentMade) {
		this.totalPaymentMade = totalPaymentMade;
	}

	public double getPayment() {
		return totalPaymentMade;
	}

	public void setPayment(double payment) {
		this.totalPaymentMade = payment;
	}	
}



/*

ATTRIBUTES

Year
Type
Department
Registration No
Title
Name with initials
Name denoted by Initials
Date of Registration
ID
photo
Contact Number(s)
E-mail Address(s)
Postal Address
Undergraduate Degree
Undergraduate Class
Undergraduate University
Expererience in field
Designation
M.Sc. or M.Sc.Eng.
Part Time/Full Time
HDC Meeting Number
Affiliated Laboratory
Research Title
Supervisor(s)
Examiner(s)
Stream
Payment

*/