package lk.ac.pdn.eng.mis.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "classlistsubject")		
public class Class_List_Subject {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int clSubjectID;
	
	
	private String course_code;
	private String course_name;
	private int no_std;
	private String hall;
	private String date;
	private String time;
	private String exam_name;
	
	public Class_List_Subject(String course_code, String course_name, int no_std, String hall, String date,
			String time, String exam_name) {
		super();
		this.course_code = course_code;
		this.course_name = course_name;
		this.no_std = no_std;
		this.hall = hall;
		this.date = date;
		this.time = time;
		this.exam_name = exam_name;
	}

	//To update
	public Class_List_Subject(int clSubjectID, String course_code, String course_name, int no_std, String hall,
			String date, String time, String exam_name) {
		super();
		this.clSubjectID = clSubjectID;
		this.course_code = course_code;
		this.course_name = course_name;
		this.no_std = no_std;
		this.hall = hall;
		this.date = date;
		this.time = time;
		this.exam_name = exam_name;
	}
	
	
	public Class_List_Subject() {
		super();
	}
	
	public String getCourse_code() {
		return course_code;
	}

	public void setCourse_code(String course_code) {
		this.course_code = course_code;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	public int getNo_std() {
		return no_std;
	}

	public void setNo_std(int no_std) {
		this.no_std = no_std;
	}

	public String getHall() {
		return hall;
	}

	public void setHall(String hall) {
		this.hall = hall;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}
	
	
	
}
