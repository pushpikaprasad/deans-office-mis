package lk.ac.pdn.eng.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.Class_List_Subject;

@Repository
public interface ClassListSubjectRepository extends JpaRepository <Class_List_Subject, Integer> {
		
	@Query("select s from Class_List_Subject s where s.course_code = ?1 and s.exam_name = ?2")
	Class_List_Subject findSubject(String course_code, String exam_name);

	@Query("select s.exam_name from Class_List_Subject s group by s.exam_name")
	String[] getListofExams();
	
	@Query("select s.course_code from Class_List_Subject s where s.exam_name = ?1")
	String[] getListofCourses(String exan_name);
	
	@Query("select s.course_code from Class_List_Subject s where s.exam_name = ?1 order by s.no_std DESC")
	String[] getListofCoursesbyChangedOrder(String exan_name);
	
}
