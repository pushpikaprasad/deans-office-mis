package lk.ac.pdn.eng.mis.entity;

public class WorkCategory {
	
	int workCategoryID;
	String workCategoryHTML;
	
	public int getWorkCategoryID() {
		return workCategoryID;
	}
	public void setWorkCategoryID(int workCategoryID) {
		this.workCategoryID = workCategoryID;
	}
	public String getWorkCategoryHTML(int id) {
		if(id == 1) {
			this.workCategoryHTML = "<div id=\"view_applications\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #b9f6ca green accent-1 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/Applications\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>APPLICATIONS</b><i class=\"material-icons left\"\r\nstyle=\"font-size: 40px; padding:12px;\">note_add</i></span>\r\n"
					+ "								<div style=\"font-size: 75%\"><p>NEW/COMPLETED TRANSCRIPT/ACADEMIC RANK APPLICATIONS</p></div>\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 2) {
			this.workCategoryHTML = "<div id=\"accepted_transcript_academic_applications\" class=\"col s12 m12 l4\">\r\n"
					+ "				<div class=\"card cardHover #81d4fa light-blue lighten-3 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/accepted_transcript_academic_applications\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>APPLICATION PROCESS</b><i class=\"material-icons left\"\r\nstyle=\"font-size: 40px; padding:12px;\">file_copy</i></span>\r\n"
					+ "								<p>TRANSCRIPT/ACADEMIC RANK APPLICATIONS</p>\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 3) {
			this.workCategoryHTML = "<div id=\"check_transcript_academic_applications\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #bcaaa4 brown lighten-3 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/Check_transcript_academic_documents\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>CHECK APPLICATIONS</b><i class=\"material-icons left\"\r\n"
					+ "								style=\"font-size: 40px; padding:12px;\">task</i></span>\r\n"
					+ "								<p>TRANSCRIPT/ACADEMIC RANK DOCUMENTS</p>\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 4) {
			this.workCategoryHTML = "<div id=\"employee_btn\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #f3e5f5 purple lighten-5 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/employees\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>EMPLOYEE DETAILS</b><i class=\"material-icons left\"\r\n"
					+ "								style=\"font-size: 40px; padding:12px;\">badge</i></span>\r\n"
					+ "								\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 5) {
			this.workCategoryHTML = "<div id=\"employee_btn\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #f8bbd0 pink lighten-4 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/PGStudents\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>PG STUDENTS</b><i class=\"material-icons left\"\r\n"
					+ "								style=\"font-size: 40px; padding:12px;\">people</i></span>\r\n"
					+ "								\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 6) {
			this.workCategoryHTML = "<div id=\"employee_btn\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #e6ee9c lime lighten-3 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/VerificationApplication\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>VERIFICATION APPLICATIONS</b><i class=\"material-icons left\"\r\n"
					+ "								style=\"font-size: 40px; padding:12px;\">how_to_reg</i></span>\r\n"
					+ "								\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 7) {
			this.workCategoryHTML = "<div id=\"employee_btn\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #e6hh9c lime lighten-3 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/CheckVerificationApplication\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>CHECK VERIFICATION APPLICATIONS</b><i class=\"material-icons left\"\r\n"
					+ "								style=\"font-size: 40px; padding:12px;\">how_to_reg</i></span>\r\n"
					+ "								\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else if(id == 8) {
			this.workCategoryHTML = "<div id=\"employee_btn\" class=\"col s12 m12 l4 \">\r\n"
					+ "				<div class=\"card cardHover #e6hh9c green lighten-3 hoverable\">\r\n"
					+ "					<a href=\"/officerDashboard/ExamTimeTable\"\r\n"
					+ "						class=\"btn_load_student black-text\">\r\n"
					+ "						<div class=\"card-content  waves-effect waves-block waves-light\">\r\n"
					+ "							<span class=\"card-title activator text-darken-4\"><b>EXAM TIME TABLE</b><i class=\"material-icons left\"\r\n"
					+ "								style=\"font-size: 40px; padding:12px;\"><span class=\"material-icons-outlined\">\r\n"
					+ "view_timeline\r\n"
					+ "</span></i></span>\r\n"
					+ "								\r\n"
					+ "							\r\n"
					+ "						</div>\r\n"
					+ "					</a>\r\n"
					+ "				</div>\r\n"
					+ "			</div>";
		}else {
			workCategoryHTML = "";
		}
		
		return this.workCategoryHTML;
	}
	public void setWorkCategoryHTML(String workCategoryHTML) {
		this.workCategoryHTML = workCategoryHTML;
	}
	

	/*
	 * <div class="col s12 m6 l4"> <div class="card cardHover"> <!-- <div
	 * class="card-image waves-effect waves-block waves-light"> image link </div>
	 * --> <a href="/adminDashboard/employees" class="black-text"> <div
	 * class="card-content  waves-effect waves-block waves-light"> <span
	 * class="card-title activator text-darken-4"> EMPLOYEE<i
	 * class="material-icons right" style="font-size: 55px;">badge</i></span> </div>
	 * </a> </div> </div>
	 */
}
