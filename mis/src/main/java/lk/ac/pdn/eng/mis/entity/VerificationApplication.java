package lk.ac.pdn.eng.mis.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "verificationapp")
public class VerificationApplication {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int applicationId;

	private String applicantName;			// private or government sector name
	private String applicantAddress;
	private String applicantMobile;			// Phone number or mobile number
	private String applicantEmail;

	private String methodOfcollection;
	private String postAddress;

	private String postEmail;

	@Lob
	private byte[] coverLetter;

	private String methodOfPayment;
	
	@Lob
	private byte[] paymentImage;
	
	@Lob
	private byte[] paymentPdf;
	
	private String paymentVerifyCode;

	//private int[] studentDataIDs;
	private String applicationDataID;

	private String submissionDate;
	
	private String applicationStatus;
	private String applicationStatusDetails;

	
	// verification document
	@Lob
	private byte[] verificationDocument;
	
	
	public VerificationApplication() {
		super();
	}

	public VerificationApplication( String applicantName, String applicantAddress, String applicantMobile,
			String applicantEmail, String methodOfcollection, String postAddress, String postEmail, byte[] coverLetter,
			String methodOfPayment, byte[] paymentImage, byte[] paymentPdf, String paymentVerifyCode, String applicationDataID,//int[] studentDataIDs,
			String submissionDate, String applicationStatus, String applicationDetails) {
		super();
		this.applicantName = applicantName;
		this.applicantAddress = applicantAddress;
		this.applicantMobile = applicantMobile;
		this.applicantEmail = applicantEmail;
		this.methodOfcollection = methodOfcollection;
		this.postAddress = postAddress;
		this.postEmail = postEmail;
		this.coverLetter = coverLetter;
		this.methodOfPayment = methodOfPayment;
		this.paymentImage = paymentImage;
		this.paymentPdf = paymentPdf;
		this.paymentVerifyCode = paymentVerifyCode;
		//this.studentDataIDs = studentDataIDs;
		this.applicationDataID = applicationDataID;
		this.submissionDate = submissionDate;
		this.applicationStatus = applicationStatus;
		this.applicationStatusDetails = applicationDetails;
	}
	
	public VerificationApplication( int applicationId, String applicantName, String applicantAddress, String applicantMobile,
			String applicantEmail, String methodOfcollection, String postAddress, String postEmail,
			String methodOfPayment,  String paymentVerifyCode, String applicationDataID,
			String submissionDate, String applicationStatus, String applicationDetails) {
		super();
		this.applicationId = applicationId;
		this.applicantName = applicantName;
		this.applicantAddress = applicantAddress;
		this.applicantMobile = applicantMobile;
		this.applicantEmail = applicantEmail;
		this.methodOfcollection = methodOfcollection;
		this.postAddress = postAddress;
		this.postEmail = postEmail;
		this.methodOfPayment = methodOfPayment;
		this.paymentVerifyCode = paymentVerifyCode;
		this.applicationDataID = applicationDataID;
		this.submissionDate = submissionDate;
		this.applicationStatus = applicationStatus;
		this.applicationStatusDetails = applicationDetails;
	}

	
	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public String getapplicantName() {
		return applicantName;
	}

	public void setapplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getapplicantAddress() {
		return applicantAddress;
	}

	public void setapplicantAddress(String applicantAddress) {
		this.applicantAddress = applicantAddress;
	}

	public String getapplicantMobile() {
		return applicantMobile;
	}

	public void setapplicantMobile(String applicantMobile) {
		this.applicantMobile = applicantMobile;
	}

	public String getapplicantEmail() {
		return applicantEmail;
	}

	public void setapplicantEmail(String applicantEmail) {
		this.applicantEmail = applicantEmail;
	}

	public String getMethodOfcollection() {
		return methodOfcollection;
	}

	public void setMethodOfcollection(String methodOfcollection) {
		this.methodOfcollection = methodOfcollection;
	}

	public String getPostAddress() {
		return postAddress;
	}

	public void setPostAddress(String postAddress) {
		this.postAddress = postAddress;
	}

	public String getPostEmail() {
		return postEmail;
	}

	public void setPostEmail(String postEmail) {
		this.postEmail = postEmail;
	}

	public byte[] getCoverLetter() {
		return coverLetter;
	}

	public void setCoverLetter(byte[] coverLetter) {
		this.coverLetter = coverLetter;
	}

	public String getMethodOfPayment() {
		return methodOfPayment;
	}

	public void setMethodOfPayment(String methodOfPayment) {
		this.methodOfPayment = methodOfPayment;
	}

	public byte[] getPaymentImage() {
		return paymentImage;
	}

	public void setPaymentImage(byte[] paymentImage) {
		this.paymentImage = paymentImage;
	}
	
	

	public byte[] getPaymentPdf() {
		return paymentPdf;
	}

	public void setPaymentPdf(byte[] paymentPdf) {
		this.paymentPdf = paymentPdf;
	}

	public String getPaymentVerifyCode() {
		return paymentVerifyCode;
	}

	public void setPaymentVerifyCode(String paymentVerifyCode) {
		this.paymentVerifyCode = paymentVerifyCode;
	}

	/*
	 * public int[] getStudentDataIDs() { return studentDataIDs; }
	 * 
	 * public void setStudentDataIDs(int[] studentDataIDs) { this.studentDataIDs =
	 * studentDataIDs; }
	 */
	public String getApplicationDataID() {
		return applicationDataID;
	}

	public void setApplicationDataID(String applicationDataID) {
		this.applicationDataID = applicationDataID;
	}
	
	public String getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getApplicationStatusDetails() {
		return applicationStatusDetails;
	}

	public void setApplicationStatusDetails(String applicationStatusDetails) {
		this.applicationStatusDetails = applicationStatusDetails;
	}

	public byte[] getVerificationDocument() {
		return verificationDocument;
	}

	public void setVerificationDocument(byte[] verificationDocument) {
		this.verificationDocument = verificationDocument;
	}
	
	
}