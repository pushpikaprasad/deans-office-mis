package lk.ac.pdn.eng.mis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.PGStudent;


@Repository
public interface PGStudentRepository extends JpaRepository<PGStudent,Integer>{

	@Query(value = "SELECT e FROM pgstudent as e WHERE "
			+ "(:registrationNo is null or e.registrationNo=:registrationNo ) and "
			+ "(:Title is null or e.title=:Title) and "
			+ "(:Namewithinitials is null or e.namewithinitials=:Namewithinitials) and "
			+ "(:NamedenotedbyInitials is null or e.namedenotedbyInitials=:NamedenotedbyInitials) and "
			+ "(:DateofRegistration is null or e.dateofRegistration=:DateofRegistration) and "
			+ "(:ID is null or e.id=:ID) and "
			+ "(:ContactNumber_s is null or e.ontactNumber_s=:ContactNumber_s) and "
			+ "(:EmailAddress_s is null or e.emailAddress_s=:EmailAddress_s) and "
			+ "(:PostalAddress is null or e.postalAddress=:PostalAddress) and "
			+ "(:UndergraduateDegree is null or e.undergraduateDegree=:UndergraduateDegree) and "
			+ "(:UndergraduateClass is null or e.undergraduateClass=:UndergraduateClass) and "
			+ "(:UndergraduateUniversity is null or e.undergraduateUniversity=:UndergraduateUniversity) and "
			+ "(:Experienceinfield is null or e.experienceinfield=:Experienceinfield) and "
			+ "(:Designation is null or e.designation=:Designation) and "
			+ "(:Stream is null or e.stream=:Stream) and "
			+ "(:MScorMScEng is null or e.mScOrMScEng=:MScorMScEng) and "
			+ "(:PartTimeFullTime is null or e.partTimeOrFullTime=:PartTimeFullTime) and "
			+ "(:HDCMeetingNumber is null or e.hDCMeetingNumber=:HDCMeetingNumber) and "
			+ "(:AffiliatedLaboratory is null or e.affiliatedLaboratory=:AffiliatedLaboratory) and "
			+ "(:ResearchTitle is null or e.researchTitle=:ResearchTitle) and "
			+ "(:Supervisor_s is null or e.supervisor_s=:Supervisor_s) and "
			+ "(:Examiner_s is null or e.examiner_s=:Examiner_s)"
			+ "(:programme is null or e.programme=:programme) and "
			+ "(:courseFee is null or e.courseFee=:courseFee) and "
			+ "(:libraryFee is null or e.libraryFee=:libraryFee) and "
			+ "(:tuitionFee is null or e.tuitionFee=:tuitionFee) and "
			+ "(:registrationFee is null or e.registrationFee=:registrationFee) and "
			+ "(:firstYearPaymeent is null or e.firstYearPaymeent=:firstYearPaymeent) and "
			+ "(:secondYearPaymeent is null or e.secondYearPaymeent=:secondYearPaymeent) and "
			+ "(:thirdYearPaymeent is null or e.thirdYearPaymeent=:thirdYearPaymeent) and "
			+ "(:fourthYearPaymeent is null or e.fourthYearPaymeent=:fourthYearPaymeent) and "
			+ "(:programmeFee is null or e.programmeFee=:programmeFee) and "
			+ "(:totalPaymentMade is null or e.totalPaymentMade=:totalPaymentMade) and "
			, nativeQuery = true)
	
	List<PGStudent> findAllStudents(
			String registrationNo,
			String Title,
			String Namewithinitials,
			String NamedenotedbyInitials,
			String DateofRegistration,
			String ID,
			String ContactNumber_s,
			String EmailAddress_s,
			String PostalAddress,
			String UndergraduateDegree,
			String UndergraduateClass,
			String UndergraduateUniversity,
			String Experienceinfield,
			String Designation,
			String Stream,
			String MScorMScEng,
			String PartTimeFullTime,
			String HDCMeetingNumber,
			String AffiliatedLaboratory,
			String ResearchTitle,
			String Supervisor_s,
			String Examiner_s,
			String programme,
			double courseFee,
			double libraryFee,
			double tuitionFee,
			double registrationFee,
			double firstYearPaymeent,
			double secondYearPaymeent,
			double thirdYearPaymeent,
			double fourthYearPaymeent,
			double programmeFee,
			double totalPaymentMade);
	
	List<PGStudent> findByRegistrationNo(String regNo);

	
	@Query(value = "SELECT * FROM pgstudent as e WHERE "
		+ "(:registrationNo is null or e.registration_no  like %:registrationNo%  ) and "
		+ "(:name is null or e.namewithinitials like %:name% or e.namedenotedby_initials like %:name% )and "
		+ "(:department is null or e.department like %:department% ) and "
		+ "(:degreeLevel is null or e.type like %:degreeLevel%) and "
		+ "(e.year=:year)"
			, nativeQuery = true)
	List<PGStudent> findPGStudentsByData(String registrationNo, String name, String department, String degreeLevel,
			int year);

}
