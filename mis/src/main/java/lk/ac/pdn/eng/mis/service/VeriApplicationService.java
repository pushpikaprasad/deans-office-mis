package lk.ac.pdn.eng.mis.service;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import lk.ac.pdn.eng.mis.entity.Application;
import lk.ac.pdn.eng.mis.entity.StudentVeriData;
import lk.ac.pdn.eng.mis.entity.VerificationApplication;
import lk.ac.pdn.eng.mis.repository.StudentVeriRepository;
import lk.ac.pdn.eng.mis.repository.VeriApplicarionRepository;

@Service
public class VeriApplicationService {

	@Autowired
	private VeriApplicarionRepository veriAppRepo;

	@Autowired
	private StudentVeriRepository stdVeriRepo;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private OfficerService officerService;

	// Save verification application
	@SuppressWarnings("null")
	public void saveVerificationApplication(String applicantName, String applicantAddress, String applicantMobile,
			String applicantEmail, String methodOfcollection, String postAddress, String postEmail, byte[] coverLetter,
			String methodOfPayment, byte[] payingImage, byte[] paymentPdf, String paymentVerifyCode,
			String submissionDate, StudentVeriData[] studentData) throws MessagingException {

		String dateTime = new Date().toString().replaceAll(" ", "");

		String applicationDataID = "" + applicantEmail + "" + dateTime;

		if (studentData != null) {
			if (studentData.length > 0) {

				for (StudentVeriData std : studentData) {
					std.setDate(submissionDate.substring(0, 10));
					std.setApplicationDataID(applicationDataID);
					stdVeriRepo.save(std);
				}
			}
		}

		// Status
		String statusDetails = "New application - Received on " + submissionDate.substring(0, 10);

		if (paymentPdf.length == 0) {
			paymentPdf = null;
		}
		if (coverLetter.length == 0) {
			coverLetter = null;
		}
		if (payingImage.length == 0) {
			payingImage = null;
		}

		VerificationApplication newVeriApp = new VerificationApplication(applicantName, applicantAddress,
				applicantMobile, applicantEmail, methodOfcollection, postAddress, postEmail, coverLetter,
				methodOfPayment, payingImage, paymentPdf, paymentVerifyCode, applicationDataID, // studentDataIDs,
				submissionDate, "new", statusDetails);

		veriAppRepo.save(newVeriApp);

		// send email to relevant applicant
		sendEmailHTML(applicantEmail, "Successfully submitted Verification Document",
				"Dear Sir/Madam,<br><br> Your application for the verification document is successfull submitted to the system."
						+ "<br><br> Thank you" + "<br><br> Best regards<br>"
						+ "Note: this is a automated email from the system.");

		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(6),
				"Received new verification application",
				"Dear Officer, <br><br>You have received new Verification application.<br><br> Thank you ");

	}

	// save application
	public void save(VerificationApplication application) {
		veriAppRepo.save(application);
	}

	// find student data by date and reg no
	public StudentVeriData findstdDataByDateAndRegNo(String date, String regNo) {
		return stdVeriRepo.findByDateAndStudentreg(date, regNo);
	}

	// Get All applications
	public List<VerificationApplication> getAllApplications() {
		return veriAppRepo.findAll();
	}

	// Get Application By ApplicationID
	public VerificationApplication getApplicationByID(int applicationID) {
		// return veriAppRepo.getOne(applicationID);
		return veriAppRepo.getApplicationByID(applicationID);
	}

	// Get New applications
	public List<VerificationApplication> getNewApplications() {
		return veriAppRepo.findAllNewApplications();
	}

	// Get Accepted applications
	public List<VerificationApplication> getAcceptedApplications() {
		return veriAppRepo.findAllAcceptedApplications();
	}

	// Get Completed applications
	public List<VerificationApplication> getCompletedApplications() {
		return veriAppRepo.findAllCompletedApplications();
	}

	// Get Rejected applications
	public List<VerificationApplication> getRejectedApplications() {
		return veriAppRepo.findAllRejectedApplications();
	}

	// Get prepared applications
	public List<VerificationApplication> getPreparedApplications() {
		return veriAppRepo.findAllPreparedApplications();
	}

	// Get checked OK verification application
	public List<VerificationApplication> getOKApplications() {
		return veriAppRepo.findAllOKApplications();
	}

	// Get checked NOT verification application
	public List<VerificationApplication> getNotOKApplications() {
		return veriAppRepo.findAllNOTOKApplications();
	}

	// Get students data
	public List<StudentVeriData> getStudentData(int applicationID) {
		VerificationApplication application = veriAppRepo.getOne(applicationID);
		return stdVeriRepo.findStuddentByDataID(application.getApplicationDataID());
	}

	// View selected application in PDF mode
	public static ByteArrayInputStream verificationPDFView(VerificationApplication verificationApplication,
			List<StudentVeriData> students) {

		// System.out.println(students.size());

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		PdfWriter.getInstance(document, out);
		PdfWriter writer = PdfWriter.getInstance(document, out);

		try {

			document.open();
			document.bottom(1);

			// Header
			Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
			font.setSize(14);
			font.setColor(Color.BLACK);
			font.setStyle(2);
			Paragraph p = new Paragraph("APPLICATION FOR RESULT VERIFICATION DOCUMENT", font);
			p.setAlignment(Paragraph.ALIGN_CENTER);
			p.setFont(font);
			document.add(p);

			// Application view table
			// Cell Formats
			// Normal cell
			PdfPCell cell = new PdfPCell();
			cell.setBackgroundColor(Color.WHITE);
			cell.setPadding(5);
			cell.setBorderWidth(0);
			Font cellfont = FontFactory.getFont(FontFactory.HELVETICA);
			cellfont.setColor(Color.DARK_GRAY);
			Font contentfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
			contentfont.setSize(12);
			contentfont.setColor(Color.BLACK);

			// Status Cell
			PdfPCell cellStatus = new PdfPCell();
			cellStatus.setBackgroundColor(Color.WHITE);
			cellStatus.setPadding(10);
			cellStatus.setBorderWidth(1);
			cellStatus.setVerticalAlignment(1);
			Font cellStatusfont = FontFactory.getFont(FontFactory.HELVETICA);
			cellStatusfont.setColor(Color.DARK_GRAY);

			// Sub heading Cell
			PdfPCell cellheading = new PdfPCell();
			Font subHeadingfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
			subHeadingfont.setSize(14);
			subHeadingfont.setColor(Color.BLACK);
			subHeadingfont.setStyle(2);
			cellheading.setColspan(2);
			cellheading.setBorderWidth(1);
			cellheading.setBorderColorBottom(Color.BLACK);

			// Student data Cell
			PdfPCell stdDataCell = new PdfPCell();
			Font stdDataCellfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
			stdDataCellfont.setSize(11);
			stdDataCellfont.setColor(Color.BLACK);
			stdDataCellfont.setStyle(2);
			stdDataCell.setBorderWidth(1);
			stdDataCell.setBorderColorBottom(Color.BLACK);
			Font stdDataContentFont = FontFactory.getFont(FontFactory.COURIER_BOLD);
			stdDataContentFont.setSize(11);
			stdDataContentFont.setColor(Color.BLACK);

			// START table
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 5f, 7.5f });
			table.setSpacingBefore(10);

			// Header
			cellStatus.setPhrase(new Phrase("Application Status: ", cellStatusfont));
			table.addCell(cellStatus);
			cellStatus.setPhrase(new Phrase(verificationApplication.getApplicationStatusDetails() + "\n "));
			table.addCell(cellStatus);

			cell.setPhrase(new Phrase("", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("", cellfont));
			table.addCell(cell);

			// Applicant details
			cellheading.setPhrase(new Phrase("Applicant Details", subHeadingfont));
			table.addCell(cellheading);

			cell.setPhrase(new Phrase("1.\tName of the Applicant: ", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getapplicantName(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("2.\tEmail: ", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getapplicantEmail(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("3.\tMobile: ", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getapplicantMobile(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("4.\tAddress : ", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getapplicantAddress() + "\n\n", contentfont));
			table.addCell(cell);

			// Student details
			cellheading.setPhrase(new Phrase("Student Details", subHeadingfont));
			table.addCell(cellheading);

			// Student data table
			PdfPTable stdDataTable = new PdfPTable(3); // number of column is 3
			stdDataTable.setWidthPercentage(100f);
			// stdDataTable.setWidths(new float[] { 2f, 2f,2f, 2f,2f, 2f, 2f });
			stdDataTable.setWidths(new float[] { 4f, 4f, 4f });
			stdDataTable.setSpacingBefore(10);

			stdDataCell.setPhrase(new Phrase("Name:", stdDataCellfont));
			stdDataTable.addCell(stdDataCell);
			stdDataCell.setPhrase(new Phrase("Reg. No.:", stdDataCellfont));
			stdDataTable.addCell(stdDataCell);
			// stdDataCell.setPhrase(new Phrase("Stream:", stdDataCellfont));
			// stdDataTable.addCell(stdDataCell);
			stdDataCell.setPhrase(new Phrase("Effective Date:", stdDataCellfont));
			stdDataTable.addCell(stdDataCell);
			// stdDataCell.setPhrase(new Phrase("Email:", stdDataCellfont));
			// stdDataTable.addCell(stdDataCell);
			// stdDataCell.setPhrase(new Phrase("Mobile:", stdDataCellfont));
			// stdDataTable.addCell(stdDataCell);
			// stdDataCell.setPhrase(new Phrase("Date of Birth:", stdDataCellfont));
			// stdDataTable.addCell(stdDataCell);

			PdfPCell stdDataTableCell = new PdfPCell();
			stdDataTableCell.setColspan(2);

			for (StudentVeriData s : students) {
				// Name
				stdDataCell.setPhrase(new Phrase(s.getStudentName() + "\n\n", stdDataContentFont));
				stdDataTable.addCell(stdDataCell);

				// Registration No
				stdDataCell.setPhrase(new Phrase(s.getStudentReg() + "\n\n", stdDataContentFont));
				stdDataTable.addCell(stdDataCell);

				// Stream
				// stdDataCell.setPhrase(new Phrase(s.getStream()+"\n\n",stdDataContentFont));
				// stdDataTable.addCell(stdDataCell);

				// Effective Date
				stdDataCell.setPhrase(new Phrase(s.geteffectiveDate() + "\n\n", stdDataContentFont));
				stdDataTable.addCell(stdDataCell);

				// Email
				// stdDataCell.setPhrase(new Phrase(s.getEmail()+"\n\n",stdDataContentFont));
				// stdDataTable.addCell(stdDataCell);

				// Mobile
				// stdDataCell.setPhrase(new
				// Phrase(s.getContactNumber()+"\n\n",stdDataContentFont));
				// stdDataTable.addCell(stdDataCell);

				// Date of Birth
				// stdDataCell.setPhrase(new Phrase(s.getDob()+"\n\n",stdDataContentFont));
				// stdDataTable.addCell(stdDataCell);

				// Transcript

				// Degree Certificate
			}
			stdDataTableCell.addElement(stdDataTable);
			table.addCell(stdDataTableCell);

			cell.setPhrase(new Phrase("", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("", cellfont));
			table.addCell(cell);

			// Payment details
			cellheading.setPhrase(new Phrase("Payment Details", subHeadingfont));
			table.addCell(cellheading);

			cell.setPhrase(new Phrase("1.\tMethod Of Payment:", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getMethodOfPayment(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("2.\tPayment Verify Code:", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getPaymentVerifyCode(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("", cellfont));
			table.addCell(cell);

			// Collection details
			cellheading.setPhrase(new Phrase("Collection Details", subHeadingfont));
			table.addCell(cellheading);

			cell.setPhrase(new Phrase("1.\tMethod Of Collection:", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getMethodOfcollection(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("2.\tPostal Address:", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getPostAddress(), contentfont));
			table.addCell(cell);

			cell.setPhrase(new Phrase("3.\tPostal Email:", cellfont));
			table.addCell(cell);
			cell.setPhrase(new Phrase(verificationApplication.getPostEmail(), contentfont));
			table.addCell(cell);

			document.add(table);
			document.newPage();

			// get paying in slip images
			if (verificationApplication.getPaymentImage() != null) {

				document.newPage();

				// Paying in slip
				PdfPTable payingsliptable = new PdfPTable(1);
				payingsliptable.setWidthPercentage(100f);
				payingsliptable.setWidths(new float[] { 12f });
				payingsliptable.setSpacingBefore(10);
				PdfPCell payingInCell = new PdfPCell();
				payingInCell.setPhrase(new Phrase("Payment Image:", cellfont));
				payingsliptable.addCell(payingInCell);

				Image img = null;
				try {
					// document.add(new Paragraph("Deposit Slip:"));
					img = Image.getInstance(verificationApplication.getPaymentImage());

					payingInCell.addElement(img);
					// payingInCell.setColspan(2);
					payingInCell.setBorderWidth(1);

					payingsliptable.addCell(payingInCell);
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}

				document.add(payingsliptable);

			}

			// get paying in slip PDF
			if (verificationApplication.getPaymentPdf() != null) {
				document.newPage();
				PdfContentByte pageContentByte = writer.getDirectContent();
				int currentPdfReaderPage = 1;

				PdfReader reader = null;
				try {
					byte dataPayment[] = verificationApplication.getPaymentPdf();
					reader = new PdfReader(dataPayment);
					PdfImportedPage pdfImportpage;

					while (currentPdfReaderPage <= reader.getNumberOfPages()) {
						document.newPage();
						pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
						pageContentByte.addTemplate(pdfImportpage, 0, 0);
						currentPdfReaderPage++;
					}

					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			document.newPage();

			// cover letter
			if (verificationApplication.getCoverLetter() != null) {
				PdfContentByte pageContentByte = writer.getDirectContent();
				int currentPdfReaderPage = 1;

				// InputStream inputStream = new ByteArrayInputStream(data1);

				PdfReader reader = null;
				try {
					byte dataCoverLetter[] = verificationApplication.getCoverLetter();
					reader = new PdfReader(dataCoverLetter);
					PdfImportedPage pdfImportpage;

					while (currentPdfReaderPage <= reader.getNumberOfPages()) {
						document.newPage();
						pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
						pageContentByte.addTemplate(pdfImportpage, 0, 0);
						currentPdfReaderPage++;
					}

					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			for (StudentVeriData s : students) {

				document.newPage();

				// transcript document
				if (s.getTranscriptFile() != null) {
					PdfContentByte pageContentByte = writer.getDirectContent();
					int currentPdfReaderPage = 1;

					// InputStream inputStream = new ByteArrayInputStream(data1);

					PdfReader reader = null;
					try {
						byte dataTranscriptFile[] = s.getTranscriptFile();
						reader = new PdfReader(dataTranscriptFile);
						PdfImportedPage pdfImportpage;

						while (currentPdfReaderPage <= reader.getNumberOfPages()) {
							document.newPage();
							pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
							pageContentByte.addTemplate(pdfImportpage, 0, 0);
							currentPdfReaderPage++;
						}

						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// degree certificate document
				if (s.getDegreeCertificateFile() != null) {
					document.newPage();
					PdfContentByte pageContentByte = writer.getDirectContent();
					int currentPdfReaderPage = 1;

					byte dataDegreeCertificateFile[] = s.getDegreeCertificateFile();
					// InputStream inputStream = new ByteArrayInputStream(data1);

					PdfReader reader = null;
					try {
						reader = new PdfReader(dataDegreeCertificateFile);
						PdfImportedPage pdfImportpage;

						while (currentPdfReaderPage <= reader.getNumberOfPages()) {
							document.newPage();
							pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
							pageContentByte.addTemplate(pdfImportpage, 0, 0);
							currentPdfReaderPage++;
						}

						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}

			// verification document
			if (verificationApplication.getVerificationDocument() != null) {
				PdfContentByte pageContentByte = writer.getDirectContent();
				int currentPdfReaderPage = 1;

				byte dataVerificationDocument[] = verificationApplication.getVerificationDocument();
				PdfReader reader = null;
				try {
					reader = new PdfReader(dataVerificationDocument);
					PdfImportedPage pdfImportpage;

					while (currentPdfReaderPage <= reader.getNumberOfPages()) {
						document.newPage();
						pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
						pageContentByte.addTemplate(pdfImportpage, 0, 0);
						currentPdfReaderPage++;
					}

					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			document.close();

		} catch (DocumentException ex) {

			System.out.println(ex.getMessage());
		}

		return new ByteArrayInputStream(out.toByteArray());

	}

	// Download PDF document
	public void export(HttpServletResponse response) throws SocketTimeoutException, DocumentException, IOException {

	}

	// Accept Application
	public void setAcceptVeriApplication(int applicationID, String officerName)
			throws AddressException, MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationID);
		veriApp.setApplicationStatus("accepted");

		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);

		String statusDetails = "Accepted by " + officerName + " on " + dateTime;

		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		veriAppRepo.save(veriApp);

		// send email to relevant applicant
		String applicantemail = veriApp.getapplicantEmail();
		sendEmailHTML(applicantemail, "Accepted Verification Document",
				"Dear Sir/Madam,<br><br> Your application has been accepted and will start the verification process."
						+ "<br><br> Thank you" + "<br><br> Best regards<br>"
						+ "Note: this is a automated email from the system.");
	}

	// Reject Application
	public void setRejectVeriApplication(int applicationID, String officerName, String reason)
			throws MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationID);
		veriApp.setApplicationStatus("rejected");
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);
		String statusDetails = "Rejected by " + officerName + " on " + dateTime + "\n Reason: " + reason;
		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		veriAppRepo.save(veriApp);

		// send email to relevant applicant
		String applicantemail = veriApp.getapplicantEmail();
		sendEmailHTML(applicantemail, "Rejected Verification Document",
				"Dear Sir/Madam,<br><br> Your application has been rejected due to following reason. Please submit the application "
						+ "again." + "" + "<br><br>Reason: " + reason + "<br><br> Thank you"
						+ "<br><br> Best regards<br>" + "Note: this is a automated email from the system.");
	}

	// Prepare Document
	public void setPrepareVeriDocuments(int applicationID, String officerName)
			throws AddressException, MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationID);
		veriApp.setApplicationStatus("prepared");
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);
		String statusDetails = "Prepared by " + officerName + " on " + dateTime;
		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		veriAppRepo.save(veriApp);

		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(7),
				"Received Prepared verification application",
				"Dear Officer, <br><br>You have received prepared Verification application.<br><br> Thank you ");

	}

	// Set OK verification application
	public void setOKVeriDocuments(int applicationID, String officerName) throws AddressException, MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationID);
		veriApp.setApplicationStatus("marked_OK");
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);
		String statusDetails = "Marked as OK by " + officerName + " on " + dateTime;
		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		veriAppRepo.save(veriApp);

		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(6),
				"Received Marked-OK verification application",
				"Dear Officer, <br><br>You have received Marked-OK Verification application.<br><br> Thank you ");

	}

	// Set NOT OK verification application
	public void setNOTOKVeriDocuments(int applicationID, String message, String officerName)
			throws AddressException, MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationID);
		veriApp.setApplicationStatus("marked_NOT_OK");
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);
		String statusDetails = "Marked as NOT OK by " + officerName + " on " + dateTime + " due to the reason: \n "
				+ message;
		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		veriAppRepo.save(veriApp);

		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(6),
				"Received Marked-NOT OK verification application",
				"Dear Officer, <br><br>You have received Marked-NOT OK Verification application.<br><br> Thank you ");

	}

	// Duplicate Application
	public void setDuplicateVeriApplication(int applicationID, String officerName)
			throws AddressException, MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationID);
		veriApp.setApplicationStatus("duplicate");

		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);

		String statusDetails = "Marked as duplicate application by " + officerName + " on " + dateTime;

		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		veriAppRepo.save(veriApp);

	}

	// Send/email final message to applicant
	public void setfinalMsgVeriDocuments(int applicationId, String message, String officer) throws MessagingException {
		VerificationApplication veriApp = veriAppRepo.getOne(applicationId);
		veriApp.setApplicationStatus("done");

		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);

		String statusDetails = "Final message sent by " + officer + " on " + dateTime + "\n\n **Message** \n\n"
				+ message;
		String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
		veriApp.setApplicationStatusDetails(updateStatus);
		String applicantemail = veriApp.getapplicantEmail();
		String finalmsg = "Dear Sir/Madam, <br><br>Your Verification document is ready. "
				+ "Please refer the below notice from the relavant officer to collect the document."
				+ "<br><br> Message from officer: " + message + "" + "<br><br> Thank you.";
		sendEmailHTML(applicantemail, "Verification Document", finalmsg);
		veriAppRepo.save(veriApp);
	}
	
	// Snd final verification document with an attachment
	public void setStatusOnSendingFinalVeriDoc(int applicationId, String message, String officer) throws MessagingException {
			VerificationApplication veriApp = veriAppRepo.getOne(applicationId);
			veriApp.setApplicationStatus("done");

			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
			String dateTime = df.format(date);

			String statusDetails = "Final message sent by " + officer + " on " + dateTime + "\n\n **Message** \n\n"
					+ message;
			String updateStatus = veriApp.getApplicationStatusDetails() + "\n\n" + statusDetails;
			veriApp.setApplicationStatusDetails(updateStatus);
			veriAppRepo.save(veriApp);
		}

	// Search by Applicant name
	public List<VerificationApplication> searchByName(String searchName){
		return veriAppRepo.getApplicationsBySearchName(searchName);
	}
	// ----------------------------------------------------------------------------------------
	// send email with HTML
	public void sendEmailHTML(String toSend, String title, String message) throws MessagingException {
		MimeMessage msg = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		helper.setFrom("noreply@engmis.pdn.ac.lk");
		helper.setTo(toSend);
		helper.setSubject(title);
		helper.setText(message, true);
		javaMailSender.send(msg);
	}

}
