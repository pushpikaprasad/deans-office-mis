package lk.ac.pdn.eng.mis.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.activation.URLDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import lk.ac.pdn.eng.mis.entity.Application;
import lk.ac.pdn.eng.mis.repository.ApplicationRepository;

@Service
public class ApplicationService {

	@Autowired
	private ApplicationRepository applicationRepository;
	
	@Autowired
	private JavaMailSender javaMailSender;
	 
	@Autowired
	private OfficerService officerService;
 
	 
	public void save(Application application) {
		applicationRepository.save(application);
	}
	
	//get all application details
	public List<Application> getApplications(){
		return applicationRepository.findAll();
	}
	
	//get specific application details
	public Application getApplicationByID(int id) {
		return applicationRepository.findById(id).get(); //CHECK get()
	}
	
	//get application by registration No
	public List<Application> getApplicationByRegNo(String regNo) {
		return applicationRepository.findApplicationByRegNo(regNo);
	}
	
	//Set status of Application
	public void setStatusApplication(int id, String status) {
		Application application = applicationRepository.findByApplicationId(id);
		application.setStatus(status);
		applicationRepository.save(application);
	}
	
	public void setStatusApplicationDetails(int id, String status) {
		Application application = applicationRepository.findByApplicationId(id);
		String currentStatus = application.getStatusDetails();
		application.setStatusDetails(currentStatus+status);
		applicationRepository.save(application);
	}
	
	//Get applications based on status
	public List<Application> getApplicationByStatus(String status){
		return applicationRepository.findByStatus(status);
	}
	
	//Create Application
	public void createApplication(Application application) throws AddressException, MessagingException {
		
		/*
		 * Date date = new Date(); DateFormat df = new
		 * SimpleDateFormat("yyyy-MM-dd ");
		 * df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo")); String submissionDate =
		 * df.format(date);
		 */
		
		String submissionDate = application.getDate();
		
		application.setStatusDetails("\u2022 New application received on "+submissionDate.substring(0, 10));
		applicationRepository.save(application);
		
		int applicationId = getApplicationByDate(application.getDate()).getApplicationId();

		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(1), "Received new application",
				"Dear Officer, <br><br>You have received new Transcript/Academic Rank Certificate application.<br><br> Thank you ");

		// send with attachment int applicationId
		getApplicationByDate(application.getDate()).getApplicationId();
		String title2 = "TRANSCRIPT/ACADEMIC RANK CERTIFICATE APPLICATION";
		String msg2 = "Dear Student,\n\nYou have successfully submitted the application.";
		try {
			sendEmailWithPDF(application.getEmail(), title2, msg2, applicationId);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
	}
	
	//find application by Date
	public Application getApplicationByDate(String date) {
		return applicationRepository.findByDate(date);
	}
	
	// send pdf attachment
	public void sendEmailWithPDF(String toSend, String title, String messageText, int id) throws MessagingException {

			MimeMessage msg = javaMailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setFrom("noreply@engmis.pdn.ac.lk");
			helper.setTo(toSend);

			//URL fileURL = null;
			//try {
			//	fileURL = new URL("http://localhost:8080/studentDashboard/downloadApplication/" + id);
			//} catch (MalformedURLException e) {
			//	e.printStackTrace();
			//}  
			//URLDataSource file = new URLDataSource(fileURL);
			//helper.addAttachment("Application-" + id + ".pdf", file);
			helper.setSubject(title);
			helper.setText(messageText);
			javaMailSender.send(msg);

		}
}
