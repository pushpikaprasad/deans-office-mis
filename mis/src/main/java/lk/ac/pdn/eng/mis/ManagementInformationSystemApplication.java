package lk.ac.pdn.eng.mis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import lk.ac.pdn.eng.mis.entity.Admin;
import lk.ac.pdn.eng.mis.service.AdminService;
import lk.ac.pdn.eng.mis.service.UserService;

@SpringBootApplication
public class ManagementInformationSystemApplication extends SpringBootServletInitializer {
//public class ManagementInformationSystemApplication{

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) 
    {
        return application.sources(ManagementInformationSystemApplication.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(ManagementInformationSystemApplication.class, args);
	}
	
}

@org.springframework.stereotype.Component
class CommandLineRun implements CommandLineRunner{
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	
	@Override
	public void run(String... args) throws Exception {
		
		boolean checkAdminUser = userService.isUserDoesNotExists("admin");
        if (checkAdminUser == true) {
        	Admin adminUser= new Admin("admin","123456","Admin","Account","Peradeniya","cp.eng.pdn@gmail.com","0719794452", null,"199136300726");
    		adminService.createAdmin(adminUser);
        }
		
		/*Ar arUser = new Ar("ar","ar1234@","AR","Account","Dean's Office, Faculty of Engineering","arengpdn@gmail.com","0706138846", null, null);
		//ArService.updateAr(arUser);
		adminService.createAr(arUser);
		
		Officer officerUser = new Officer("sanjeewa","sanjeewa1234@","Sanjeewa","Karunarathne","Dean's Office, Faculty of Engineering","arengpdn@gmail.com","0706138846", null, null, "4");
		//OfficerService.updateOfficer(officerUser);
		adminService.createOfficer(officerUser);*/
	}
}
