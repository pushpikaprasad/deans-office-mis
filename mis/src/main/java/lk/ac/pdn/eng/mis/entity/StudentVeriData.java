package lk.ac.pdn.eng.mis.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table (name = "studentveridata")
public class StudentVeriData {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int dataId;
	private String applicationDataID;
	
	private String studentName;
	private String studentreg;
	private String Stream;
	private String email;
	private String contactNumber;
	private String Address;
	
	private String Dob;
	private String effectiveDate;
	private String date;
	@SuppressWarnings("unused")
	@JsonProperty("transcript")
	private String transcript;
	@SuppressWarnings("unused")
	@JsonProperty("degreeCertificate")
	private String degreeCertificate;
	
	@Lob
	private byte[] transcriptFile;
	
	
	@Lob
	private byte[] degreeCertificateFile;

	public StudentVeriData() {
		super();
	}

	

	public StudentVeriData(int dataId, String studentName, String studentReg, String stream, String email,
			String contactNumber, String address, String dob, String effectiveDate, String date, String transcript,
			String degreeCertificate) {
		super();
		this.dataId = dataId;
		this.studentName = studentName;
		this.studentreg = studentReg;
		Stream = stream;
		this.email = email;
		this.contactNumber = contactNumber;
		Address = address;
		Dob = dob;
		this.effectiveDate = effectiveDate;
		this.date = date;
		this.transcript = transcript;
		this.degreeCertificate = degreeCertificate;
	}


	public int getDataId() {
		return dataId;
	}

	public void setDataId(int dataId) {
		this.dataId = dataId;
	}
	
	public String getApplicationDataID() {
		return applicationDataID;
	}

	public void setApplicationDataID(String applicationDataID) {
		this.applicationDataID = applicationDataID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentReg() {
		return studentreg;
	}

	public void setStudentReg(String studentReg) {
		this.studentreg = studentReg;
	}

	public String getStream() {
		return Stream;
	}

	public void setStream(String stream) {
		Stream = stream;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getDob() {
		return Dob;
	}

	public void setDob(String dob) {
		Dob = dob;
	}

	public String geteffectiveDate() {
		return effectiveDate;
	}

	public void seteffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public byte[] getTranscriptFile() {
		return transcriptFile;
	}

	public void setTranscriptFile(byte[] transcriptFile) {
		this.transcriptFile = transcriptFile;
	}

	public byte[] getDegreeCertificateFile() {
		return degreeCertificateFile;
	}

	public void setDegreeCertificateFile(byte[] degreeCertificateFile) {
		this.degreeCertificateFile = degreeCertificateFile;
	}

	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}

	public void setDegreeCertificate(String degreeCertificate) {
		this.degreeCertificate = degreeCertificate;
	}



	public String getTranscript() {
		return transcript;
	}



	public String getDegreeCertificate() {
		return degreeCertificate;
	}
	
	
	
}
