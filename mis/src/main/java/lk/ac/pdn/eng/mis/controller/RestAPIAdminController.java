package lk.ac.pdn.eng.mis.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lk.ac.pdn.eng.mis.entity.Admin;
import lk.ac.pdn.eng.mis.entity.Ar;
import lk.ac.pdn.eng.mis.entity.Employee;
import lk.ac.pdn.eng.mis.entity.Notice;
import lk.ac.pdn.eng.mis.entity.Officer;
import lk.ac.pdn.eng.mis.entity.PGStudent;
import lk.ac.pdn.eng.mis.entity.Student;
import lk.ac.pdn.eng.mis.entity.User;
import lk.ac.pdn.eng.mis.service.AdminService;
import lk.ac.pdn.eng.mis.service.EmployeeService;
import lk.ac.pdn.eng.mis.service.ExcelHelper;
import lk.ac.pdn.eng.mis.service.PGStudentService;

@RestController
public class RestAPIAdminController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private PGStudentService pgStudentService;
	
	@Autowired
	private EmployeeService employeeService;
	
	
	// Get Student in JSON
	@GetMapping(value = "/adminDashboard/studentList")
	public List<Student> getStudents(Model model) {

		return adminService.getStudents();
	}

	// Add Student Details
	@PostMapping(value = "/adminDashboard/studentList/addstudent", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addStudent(@RequestBody Student newStudent) {
		adminService.createStudent(newStudent);
	}
	
	

	// Get Ar in JSON
	@GetMapping(value = "/adminDashboard/arList")
	public List<Ar> getAr(Model model) {
		return adminService.getAr();
	}

	// Add Ar Details
	@PostMapping(value = "/adminDashboard/arList/addar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addAr(@RequestBody Ar newAr) {
		adminService.createAr(newAr);
	}

	// Get Admin details
	@GetMapping(value = "/adminDashboard/getAdmin")
	public Admin getAdmin(Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		return adminService.getAdmin(userDetails.getUsername());
	}

	// update Admin
	@PostMapping(value = "/adminDashboard/updateAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateAdmin(@RequestBody Admin editAdmin, Authentication authentication) {
		adminService.updateAdmin(editAdmin);
	}

	// reset password
	@PostMapping(value = "/adminDashboard/resetPassword", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void resetPassword(@RequestParam("email") String email, @RequestParam("newPassword") String newPassword) {
		User user = adminService.getUserByEmail(email);
		adminService.setPassword(user.getUsername(), newPassword);
	}

	// delete Student
	@DeleteMapping("/adminDashboard/deleteStudent/{email}")
	public void deleteStudent(@PathVariable("email") String email) {
		adminService.deleteStudent(email);
	}

	/********************************************
	 * Officer Details
	 ******************************************/
	// Get officer in JSON
	@GetMapping(value = "/adminDashboard/officerList")
	public List<Officer> getOfficers(Model model) {
		return adminService.getOfficers();
	}

	// Get officer By Email
	@GetMapping(value = "/adminDashboard/officerList/{Email}")
	public Officer getOfficerByEmail(@PathVariable("Email") String email) {
		return adminService.getOfficerByEmail(email);
	}

	// Add Officer Details
	@PostMapping(value = "/adminDashboard/officerList/addofficer/{workcategoryID}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addOfficer(@RequestBody Officer newOfficer, @PathVariable("workcategoryID") String workcategoryID) {
		newOfficer.setWorkCategoryId(workcategoryID);
		adminService.createOfficer(newOfficer);
	}

	// Update Officer Details
	@PostMapping(value = "/adminDashboard/officerList/updateOfficer", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addOfficer(@RequestBody Officer newOfficer) {
		adminService.updateOfficer(newOfficer);
	}

	// delete Officer
	@DeleteMapping("/adminDashboard/deleteOfficer/{email}")
	public void deleteOfficer(@PathVariable("email") String email) {
		adminService.deleteOfficer(email);
	}

	/********************************************
	 * Employee Details
	 ******************************************/
	// Get all employees
	@GetMapping(value = "/adminDashboard/employeeDetails")
	public List<Employee> getEmployees(Model model) {
		return adminService.getEmployees();
	}
	
	// Get employee by Employee Number
	@GetMapping(value = "/adminDashboard/employeeDetails/getEmployee/{empNum}")
	public Employee getEmployee(@PathVariable("empNum") String empNum) {
		return employeeService.findByEmployeeNumber(empNum);
	}

	// Add Employee Details
	@PostMapping(value = "/adminDashboard/employeeDetails/addEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addEmployee(@RequestBody Employee newEmployee) {
		adminService.createEmployee(newEmployee);
	}
	
	// Edit Employee
	@PutMapping(value = "/adminDashboard/employeeDetails/editEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void editEmployee(@RequestBody Employee editEmployee) {
		
		Employee emp = new Employee();
		try {
			emp = employeeService.findByEmployeeNumber(editEmployee.getEmployeeNumber());
			
			emp.setTitle(editEmployee.getTitle());
			emp.setSurnameWithInitials(editEmployee.getSurnameWithInitials());
			emp.setNamesDenotedByInitials(editEmployee.getNamesDenotedByInitials());
			emp.setNic(editEmployee.getNic());
			emp.setDesignation(editEmployee.getDesignation());
			emp.setPresentDivision1(editEmployee.getPresentDivision1());
			emp.setTelephone(editEmployee.getTelephone());
			emp.setEmail(editEmployee.getEmail());
			emp.setMobileNumber(editEmployee.getMobileNumber());
			emp.setAddress(editEmployee.getAddress());
			
			employeeService.updateEmployee(emp);
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
	}
	

	/*
	 * // delete employee
	 * 
	 * @DeleteMapping("/adminDashboard/employeeDetails/deleteEmployee/{email}")
	 * public void deleteEmployee(@PathVariable("email") String email) {
	 * adminService.deleteEmployee(email); }
	 */
	
	// delete employee by employee number
	@DeleteMapping("/adminDashboard/employeeDetails/deleteEmployee/{empNo}")
	public void deleteEmployee(@PathVariable("empNo") String empNo) {
		employeeService.deleteEmployeeByEmpNo(empNo);
	}

	// Create employee accounts using excel sheet
	@PostMapping(value = "/adminDashboard/employeeDetails/employeeExcel", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void submitApplication(HttpServletResponse response,
			@RequestParam("employeeExcel") MultipartFile employeeExcel) throws IOException {
		// Upload excel data to database
		adminService.setEmployeeDetailsbySheet(employeeExcel);
	}

	// search employee
	@GetMapping(value = "/adminDashboard/employeeDetails/searchEmployee/{emNumber}/{empSurname}/{empDesig}/{presentDivi}/{type}")
	public List<Employee> searchEmployees(@PathVariable("emNumber") String employeeNumber,
			@PathVariable("empSurname") String surnameWithInitials, @PathVariable("empDesig") String designation,
			@PathVariable("presentDivi") String presentDivision1, @PathVariable("type") String type) {

		if (employeeNumber.equals("NA")) {
			employeeNumber = "";
		}
		if (surnameWithInitials.equals("NA")) {
			surnameWithInitials = "";
		}
		if (designation.equals("NA")) {
			designation = "";
		}
		if (presentDivision1.equals("NA")) {
			presentDivision1 = "";
		}
		if (type.equals("NA")) {
			type = "";
		}

		return adminService.searchEmployee(employeeNumber, surnameWithInitials, designation, presentDivision1, type);
	}

	// download searched employee
	@GetMapping(value = "/adminDashboard/employeeDetails/downloadSearchedEmployees/{emNumber}/{empSurname}/{empDesig}/{presentDivi}/{type}")
	public ResponseEntity<Resource> downloadSearchedEmployees(@PathVariable("emNumber") String employeeNumber,
			@PathVariable("empSurname") String surnameWithInitials, @PathVariable("empDesig") String designation,
			@PathVariable("presentDivi") String presentDivision1, @PathVariable("type") String type) {

		if (employeeNumber.equals("NA")) {
			employeeNumber = "";
		}
		if (surnameWithInitials.equals("NA")) {
			surnameWithInitials = "";
		}
		if (designation.equals("NA")) {
			designation = "";
		}
		if (presentDivision1.equals("NA")) {
			presentDivision1 = "";
		}
		if (type.equals("NA")) {
			type = "";
		}

		String filename = "Employee details.xlsx";
		InputStreamResource file = new InputStreamResource(adminService.downloadSearchEmployee(employeeNumber,
				surnameWithInitials, designation, presentDivision1, type));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	/************************************************************
	 * Post Graduate Student details
	 * 
	 */

	// Add PG student
	@PostMapping(value = "/adminDashboard/addPGStudent", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void addPGStudent(

			@RequestParam("dlevel") String dlevel, @RequestParam("year") int year,
			@RequestParam("department") String department, @RequestParam("registrationNo") String registrationNo,
			@RequestParam("Title") String Title, @RequestParam("Namewithinitials") String Namewithinitials,
			@RequestParam("NamedenotedbyInitials") String NamedenotedbyInitials,
			@RequestParam("DateofRegistration") String DateofRegistration, @RequestParam("ID") String ID,
			@RequestParam("ContactNumber_s") String ContactNumber_s,
			@RequestParam("EmailAddress_s") String EmailAddress_s, @RequestParam("PostalAddress") String PostalAddress,
			@RequestParam("UndergraduateDegree") String UndergraduateDegree,
			@RequestParam("UndergraduateClass") String UndergraduateClass,
			@RequestParam("UndergraduateUniversity") String UndergraduateUniversity,
			@RequestParam("Experienceinfield") String Experienceinfield,
			@RequestParam("Designation") String Designation, @RequestParam("Stream") String Stream,
			@RequestParam("MScorMScEng") String MScorMScEng, @RequestParam("PartTimeFullTime") String PartTimeFullTime,
			@RequestParam("HDCMeetingNumber") String HDCMeetingNumber,
			@RequestParam("AffiliatedLaboratory") String AffiliatedLaboratory,
			@RequestParam("ResearchTitle") String ResearchTitle, @RequestParam("Supervisor_s") String Supervisor_s,
			@RequestParam("Examiner_s") String Examiner_s, @RequestParam("programme") String programme,
			@RequestParam("courseFee") int courseFee, @RequestParam("libraryFee") int libraryFee,
			@RequestParam("tuitionFee") int tuitionFee, @RequestParam("registrationFee") int registrationFee,
			@RequestParam("firstYearPaymeent") int firstYearPaymeent,
			@RequestParam("secondYearPaymeent") int secondYearPaymeent,
			@RequestParam("thirdYearPaymeent") int thirdYearPaymeent,
			@RequestParam("fourthYearPaymeent") int fourthYearPaymeent,
			@RequestParam("programmeFee") int programmeFee, @RequestParam("totalPaymentMade") int totalPaymentMade

	) {

		PGStudent newStudent = new PGStudent();

		newStudent.setType(dlevel);
		newStudent.setYear(year);
		newStudent.setDepartment(department);

		newStudent.setRegistrationNo(registrationNo);
		newStudent.setTitle(Title);
		newStudent.setNamewithinitials(Namewithinitials);
		newStudent.setNamedenotedbyInitials(NamedenotedbyInitials);
		newStudent.setDateofRegistration(DateofRegistration);
		newStudent.setId(ID);
		newStudent.setContactNumber_s(ContactNumber_s);
		newStudent.setEmailAddress_s(EmailAddress_s);
		newStudent.setPostalAddress(PostalAddress);
		newStudent.setUndergraduateDegree(UndergraduateDegree);
		newStudent.setUndergraduateClass(UndergraduateClass);
		newStudent.setUndergraduateUniversity(UndergraduateUniversity);
		newStudent.setExperienceinfield(Experienceinfield);
		newStudent.setDesignation(Designation);
		newStudent.setStream(Stream);
		newStudent.setmScOrMScEng(MScorMScEng);
		newStudent.setPartTimeOrFullTime(PartTimeFullTime);
		newStudent.sethDCMeetingNumber(HDCMeetingNumber);
		newStudent.setAffiliatedLaboratory(AffiliatedLaboratory);
		newStudent.setResearchTitle(ResearchTitle);
		newStudent.setSupervisor_s(Supervisor_s);
		newStudent.setExaminer_s(Examiner_s);
		newStudent.setProgramme(programme);
		newStudent.setCourseFee(courseFee);
		newStudent.setLibraryFee(libraryFee);
		newStudent.setTuitionFee(tuitionFee);
		newStudent.setRegistrationFee(registrationFee);
		newStudent.setFirstYearPaymeent(firstYearPaymeent);
		newStudent.setSecondYearPaymeent(secondYearPaymeent);
		newStudent.setThirdYearPaymeent(thirdYearPaymeent);
		newStudent.setFourthYearPaymeent(fourthYearPaymeent);
		newStudent.setProgrammeFee(programmeFee);
		newStudent.setTotalPaymentMade(totalPaymentMade);
		
		pgStudentService.addPGStudent(newStudent);
	}

	// view All PG Students
	@GetMapping(value = "/adminDashboard/pgStudentDetails")
	public List<PGStudent> getPGStudents() {
		return adminService.getPGStudents();
	}

	// Add students by Excel
	@PostMapping(value = "/adminDashboard/addPGStudentsByExcel", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void addPGStudentsByExcel(HttpServletResponse response, @RequestParam("pgStudentExcel") MultipartFile pgStudentExcel)
			throws IOException {
		pgStudentService.addPGStudentsByExcel(pgStudentExcel.getInputStream());
		
	}

	// Edit PG student
	@PutMapping(value = "/adminDashboard/editPGStudent", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void editPGStudent(
			//@RequestParam("year") int year,
			@RequestParam("registrationNo") String registrationNo,
			//@RequestParam("studentDataId") String stdId,
			@RequestParam(value="title", required = false) String Title, 
			@RequestParam(value="namewithinitials", required = false) String Namewithinitials,
			@RequestParam(value="namedenotedbyInitials", required = false) String NamedenotedbyInitials,
			@RequestParam(value="type", required = false) String dlevel, 
			@RequestParam(value="partTimeOrFullTime", required = false) String PartTimeFullTime,
			@RequestParam(value="department", required = false) String department, 
			@RequestParam(value="hDCMeetingNumber", required = false) String HDCMeetingNumber,
			@RequestParam(value="stream", required = false) String Stream,
			@RequestParam(value="affiliatedLaboratory", required = false) String AffiliatedLaboratory,
			@RequestParam(value="programme", required = false) String programme,
			@RequestParam(value="researchTitle", required = false) String ResearchTitle, 
			@RequestParam(value="dateofRegistration", required = false) String DateofRegistration, 
			@RequestParam(value="supervisor_s", required = false) String Supervisor_s,
			@RequestParam(value="examiner_s", required = false) String Examiner_s, 
			@RequestParam(value="postalAddress", required = false) String PostalAddress,
			@RequestParam(value="id", required = false) String ID,
			@RequestParam(value="emailAddress_s", required = false) String EmailAddress_s, 
			@RequestParam(value="designation", required = false) String Designation, 
			@RequestParam(value="contactNumber_s", required = false) String ContactNumber_s,
			@RequestParam(value="undergraduateDegree", required = false) String UndergraduateDegree,
			@RequestParam(value="undergraduateUniversity", required = false) String UndergraduateUniversity,
			@RequestParam(value="undergraduateClass", required = false) String UndergraduateClass,
			@RequestParam(value="experienceinfield", required = false) String Experienceinfield,
			@RequestParam(value="courseFee", required = false) int courseFee, 
			@RequestParam(value="firstYearPaymeent", required = false) int firstYearPaymeent,
			@RequestParam(value="libraryFee", required = false) int libraryFee,
			@RequestParam(value="secondYearPaymeent", required = false) int secondYearPaymeent,
			@RequestParam(value="tuitionFee", required = false) int tuitionFee, 
			@RequestParam(value="thirdYearPaymeent", required = false) int thirdYearPaymeent,
			@RequestParam(value="registrationFee", required = false) int registrationFee,
			@RequestParam(value="fourthYearPaymeent", required = false) int fourthYearPaymeent,
			@RequestParam(value="programmeFee", required = false) int programmeFee, 
			@RequestParam(value="totalPaymentMade", required = false) int totalPaymentMade
			//@RequestParam("MScorMScEng") String MScorMScEng, 
			
			) 
	{
		PGStudent editStudent = pgStudentService.viewPGStudent(registrationNo);
		
		//editStudent.setStudentDataId(Integer.parseInt(stdId));
		editStudent.setType(dlevel);
		//newStudent.setYear(year);
		editStudent.setDepartment(department);

		//newStudent.setRegistrationNo(registrationNo);
		editStudent.setTitle(Title);
		editStudent.setNamewithinitials(Namewithinitials);
		editStudent.setNamedenotedbyInitials(NamedenotedbyInitials);
		editStudent.setDateofRegistration(DateofRegistration);
		editStudent.setId(ID);
		editStudent.setContactNumber_s(ContactNumber_s);
		editStudent.setEmailAddress_s(EmailAddress_s);
		editStudent.setPostalAddress(PostalAddress);
		editStudent.setUndergraduateDegree(UndergraduateDegree);
		editStudent.setUndergraduateClass(UndergraduateClass);
		editStudent.setUndergraduateUniversity(UndergraduateUniversity);
		editStudent.setExperienceinfield(Experienceinfield);
		editStudent.setDesignation(Designation);
		editStudent.setStream(Stream);
		//editStudent.setmScOrMScEng(MScorMScEng);
		editStudent.setPartTimeOrFullTime(PartTimeFullTime);
		editStudent.sethDCMeetingNumber(HDCMeetingNumber);
		editStudent.setAffiliatedLaboratory(AffiliatedLaboratory);
		editStudent.setResearchTitle(ResearchTitle);
		editStudent.setSupervisor_s(Supervisor_s);
		editStudent.setExaminer_s(Examiner_s);
		editStudent.setProgramme(programme);
		editStudent.setCourseFee(courseFee);
		editStudent.setLibraryFee(libraryFee);
		editStudent.setTuitionFee(tuitionFee);
		editStudent.setRegistrationFee(registrationFee);
		editStudent.setFirstYearPaymeent(firstYearPaymeent);
		editStudent.setSecondYearPaymeent(secondYearPaymeent);
		editStudent.setThirdYearPaymeent(thirdYearPaymeent);
		editStudent.setFourthYearPaymeent(fourthYearPaymeent);
		editStudent.setProgrammeFee(programmeFee);
		editStudent.setTotalPaymentMade(totalPaymentMade);
		
		pgStudentService.updatePGStudent(editStudent);
	}
	
	//Delete PG Student By Registration No
	@DeleteMapping("/adminDashboard/deletePGStudent/{registrationNo}")
	public void deletePGStudent(@PathVariable("registrationNo") String registrationNo) {
		registrationNo = registrationNo.replaceAll("_", "/");
		pgStudentService.deletePGStudentByRegistrationNo(registrationNo);
	}
	
	//Search PG Students : By Registration no, Name (2 type), Degree Level, Department, Year,
	@GetMapping(value = "/adminDashboard/searchPGStudents/{registrationNo}/{name}/{department}/{degreeLevel}/{year}")
	public List<PGStudent> searchPGStudents(@PathVariable("registrationNo") String registrationNo,
			@PathVariable("name") String name, @PathVariable("department") String department,
			@PathVariable("degreeLevel") String degreeLevel, @PathVariable("year") String year) {
		
		registrationNo = registrationNo.replaceAll("_", "/");
		
		if (registrationNo.equals("NA")) {
			registrationNo = "";
		}
		if (name.equals("NA")) {
			name = "";
		}
		if (department.equals("NA")) {
			department = "";
		}
		if (degreeLevel.equals("NA")) {
			degreeLevel = "";
		}
		if (year.equals("NA")) {
			year = "";
		}
		
		return pgStudentService.search_pgsByData(registrationNo,name,department,degreeLevel,year);
	}

	// download PG students excel sheet
	/*
	 * @SuppressWarnings("static-access")
	 * 
	 * @GetMapping(value = "/adminDashboard/downloadPGStudents", consumes =
	 * MediaType.APPLICATION_JSON_VALUE) public ResponseEntity<Resource>
	 * downloadPGStudents(@RequestBody PGStudent searchStudent) {
	 * 
	 * String filename = "PG Students.xlsx"; InputStreamResource file = new
	 * InputStreamResource(
	 * pgStudentService.downloadPGStudentsAsExcel(pgStudentService.searchPGStudents(
	 * searchStudent)));
	 * 
	 * return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
	 * "attachment; filename=" + filename)
	 * .contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file)
	 * ; }
	 */
	
	// download searched PG Student
	@GetMapping(value = "/adminDashboard/downloadPGStudents/{registrationNo}/{name}/{department}/{degreeLevel}/{year}")
	public ResponseEntity<Resource> downloadSearchedPGS( @PathVariable("registrationNo") String registrationNo,
			@PathVariable("name") String name, @PathVariable("department") String department,
			@PathVariable("degreeLevel") String degreeLevel, @PathVariable("year") String year) {
		
		registrationNo = registrationNo.replaceAll("_", "/");
		
		if (registrationNo.equals("NA")) {
			registrationNo = "";
		}
		if (name.equals("NA")) {
			name = "";
		}
		if (department.equals("NA")) {
			department = "";
		}
		if (degreeLevel.equals("NA")) {
			degreeLevel = "";
		}
		if (year.equals("NA")) {
			year = "";
		}

		String filename = "Postgraduate Student details.xlsx";
		
		InputStreamResource file = new InputStreamResource(pgStudentService.downloadSearchPGS(registrationNo,name,department,degreeLevel,year));

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
					.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
		}

	
	// view PG student
	@GetMapping(value = "/adminDashboard/viewPGStudent/{registrationNo}")
	public PGStudent viewPGStudent(@PathVariable("registrationNo") String registrationNo) {
		registrationNo = registrationNo.replaceAll("_", "/");
		return pgStudentService.viewPGStudent(registrationNo);
	}
	
	/************************************************************
	 * Manage notices
	 * 
	 */
	
	// Create new notice
	@PostMapping(value = "/adminDashboard/Notices/createNotice", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void createNotice(@RequestBody Notice newNotice) {
		String notice = newNotice.getNotice();
		String noticeType = newNotice.getNoticeType();
		String noticeSection = newNotice.getNoticeSection();
		adminService.createNotice(notice, noticeType,noticeSection); // can change the method by having initial constructor in entity level
	}
	
	// Update notice
	@PostMapping(value = "/adminDashboard/Notices/updateNotice", consumes = { MediaType.APPLICATION_JSON_VALUE})
	public void updateNotice(@RequestBody Notice updatedNotice) {
		int noticeId = updatedNotice.getNoticeID();
		String noticeUpdate = updatedNotice.getNotice();
		String noticeUpdateType = updatedNotice.getNoticeType();
		adminService.updateNotice(noticeUpdate, noticeId, noticeUpdateType); // not similar to Employee update function.
	}
	
	// Delete notice
	@DeleteMapping("/adminDashboard/Notices/deleteNotice/{noticeId}")
	public void deleteNotice(@PathVariable("noticeId") int noticeId) {
		adminService.deleteNotice(noticeId);
	}
	
	// Get Notice by ID
	@GetMapping(value = "/adminDashboard/Notices/{noticeId}")
	public Notice getNoticeByID(@PathVariable("noticeId") int noticeId) {
		return adminService.getNoticeById(noticeId);
	}
	
	// Get Important notices
	@GetMapping(value = "/adminDashboard/Notices/importantNotices")
	public List<Notice> getImportantNotices() {
		return adminService.getImportantNotices();
	}
	
	// Get Not important notices
	@GetMapping(value = "/adminDashboard/Notices/GeneralNotices") // Not important
	public List<Notice> getNotImportantNotices() {
		return adminService.getNotImportantNotices();
	}
	
	// Get notice by section
	@GetMapping(value = "/adminDashboard/Notices/getNotices/{section}")
	public List<Notice> getNoticesBySection(@PathVariable("section") String section) {
		return adminService.getNoticesBySection(section);
	}
}
