package lk.ac.pdn.eng.mis.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lowagie.text.DocumentException;

import lk.ac.pdn.eng.mis.entity.Application;
import lk.ac.pdn.eng.mis.entity.Ar;
import lk.ac.pdn.eng.mis.entity.Employee;
import lk.ac.pdn.eng.mis.entity.Officer;
import lk.ac.pdn.eng.mis.entity.PGStudent;
import lk.ac.pdn.eng.mis.entity.VerificationApplication;
import lk.ac.pdn.eng.mis.service.ApplicationPDFExporter;
import lk.ac.pdn.eng.mis.service.ArService;
import lk.ac.pdn.eng.mis.service.PGStudentService;

@RestController
public class RestAPIArController {

	@Autowired
	private ArService arService;

	@Autowired
	private PGStudentService pgStudentService;

	/* APPLICATIONS */
	// Get all applications
	@GetMapping(value = "/arDashboard/allApplications")
	public List<Application> getAllApllications() {
		return arService.getAllTanAndAcaApps();
	}

	// Get all applications by Registration No
	@GetMapping(value = "/arDashboard/searchApplications/{registrationNo}")
	public List<Application> getApplicationByRegNo(@PathVariable("registrationNo") String registrationNo) {
		String regNo = registrationNo.replaceAll("_", "/");
		return arService.getAllApplicationsByRegNo(regNo);
	}

	// Get all new applications
	@GetMapping(value = "/arDashboard/allNewApplications")
	public List<Application> getAllNewApllications() {
		return arService.getAllNewApplications();
	}

	// Get all accepted applications
	@GetMapping(value = "/arDashboard/allAcceptedApplications")
	public List<Application> getAllAcceptedTranAcadApllications() {
		return arService.getAllAcceptedApplication();
	}

	// Get all accepted applications
	@GetMapping(value = "/arDashboard/allCompletedTranAcadApplications")
	public List<Application> getAllCompletedTranAcadApllications() {
		return arService.getAllCompletedTranAcadApplication();
	}

	// Get all rejected applications
	@GetMapping(value = "/arDashboard/allRejectedApplications")
	public List<Application> getAllRejectedTranAcadApllications() {
		return arService.getAllRejectedApplication();
	}

	// download application
	@GetMapping(value = "/arDashboard/allApplications/{applicationId}")
	public void downloadApplication(@PathVariable("applicationId") int applicationId, HttpServletResponse response)
			throws DocumentException, IOException {

		arService.downloadPDFApplication(applicationId, response);
	}

	// view application
	@GetMapping(value = "/arDashboard/Applications/{applicationId}.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> pdfReport(@PathVariable("applicationId") int applicationId) {

		ByteArrayInputStream bis = ApplicationPDFExporter.pdfDocument(arService.getApplication(applicationId));

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

	/* EMPLOYEES */

	// Get all employees
	@GetMapping(value = "/arDashboard/employeeDetails")
	public List<Employee> getEmployees(Model model) {
		return arService.getEmployees();
	}

	// search employee
	@GetMapping(value = "/arDashboard/employeeDetails/searchEmployee/{emNumber}/{empSurname}/{empDesig}/{presentDivi}/{type}")
	public List<Employee> searchEmployees(@PathVariable("emNumber") String employeeNumber,
			@PathVariable("empSurname") String surnameWithInitials, @PathVariable("empDesig") String designation,
			@PathVariable("presentDivi") String presentDivision1, @PathVariable("type") String type) {

		if (employeeNumber.equals("NA")) {
			employeeNumber = "";
		}
		if (surnameWithInitials.equals("NA")) {
			surnameWithInitials = "";
		}
		if (designation.equals("NA")) {
			designation = "";
		}
		if (presentDivision1.equals("NA")) {
			presentDivision1 = "";
		}
		if (type.equals("NA")) {
			type = "";
		}

		return arService.searchEmployee(employeeNumber, surnameWithInitials, designation, presentDivision1, type);
	}

	// download searched employee
	@GetMapping(value = "/arDashboard/employeeDetails/downloadSearchedEmployees/{emNumber}/{empSurname}/{empDesig}/{presentDivi}/{type}")
	public ResponseEntity<Resource> downloadSearchedEmployees(@PathVariable("emNumber") String employeeNumber,
			@PathVariable("empSurname") String surnameWithInitials, @PathVariable("empDesig") String designation,
			@PathVariable("presentDivi") String presentDivision1, @PathVariable("type") String type) {

		if (employeeNumber.equals("NA")) {
			employeeNumber = "";
		}
		if (surnameWithInitials.equals("NA")) {
			surnameWithInitials = "";
		}
		if (designation.equals("NA")) {
			designation = "";
		}
		if (presentDivision1.equals("NA")) {
			presentDivision1 = "";
		}
		if (type.equals("NA")) {
			type = "";
		}

		String filename = "Employee details.xlsx";
		InputStreamResource file = new InputStreamResource(arService.downloadSearchEmployee(employeeNumber,
				surnameWithInitials, designation, presentDivision1, type));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);

	}

	/* MY ACCOUNT */
	// Get Ar
	@GetMapping(value = "/arDashboard/getAr")
	public Ar getAr(Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		return arService.getAr(userDetails.getUsername());
	}

	// update ar
	@PostMapping(value = "/arDashboard/updateAr", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateOfficer(@RequestBody Ar editAr, Authentication authentication) {
		arService.updateAr(editAr);

	}

	/************************************************************
	 * Post Graduate Student details
	 * 
	 */
	// Add PG student
	@PostMapping(value = "/arDashboard/addPGStudent", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void addPGStudent(

			@RequestParam("dlevel") String dlevel, @RequestParam("year") int year,
			@RequestParam("department") String department, @RequestParam("registrationNo") String registrationNo,
			@RequestParam("Title") String Title, @RequestParam("Namewithinitials") String Namewithinitials,
			@RequestParam("NamedenotedbyInitials") String NamedenotedbyInitials,
			@RequestParam("DateofRegistration") String DateofRegistration, @RequestParam("ID") String ID,
			@RequestParam("ContactNumber_s") String ContactNumber_s,
			@RequestParam("EmailAddress_s") String EmailAddress_s, @RequestParam("PostalAddress") String PostalAddress,
			@RequestParam("UndergraduateDegree") String UndergraduateDegree,
			@RequestParam("UndergraduateClass") String UndergraduateClass,
			@RequestParam("UndergraduateUniversity") String UndergraduateUniversity,
			@RequestParam("Experienceinfield") String Experienceinfield,
			@RequestParam("Designation") String Designation, @RequestParam("Stream") String Stream,
			@RequestParam("MScorMScEng") String MScorMScEng, @RequestParam("PartTimeFullTime") String PartTimeFullTime,
			@RequestParam("HDCMeetingNumber") String HDCMeetingNumber,
			@RequestParam("AffiliatedLaboratory") String AffiliatedLaboratory,
			@RequestParam("ResearchTitle") String ResearchTitle, @RequestParam("Supervisor_s") String Supervisor_s,
			@RequestParam("Examiner_s") String Examiner_s, @RequestParam("programme") String programme,
			@RequestParam("courseFee") int courseFee, @RequestParam("libraryFee") int libraryFee,
			@RequestParam("tuitionFee") int tuitionFee, @RequestParam("registrationFee") int registrationFee,
			@RequestParam("firstYearPaymeent") int firstYearPaymeent,
			@RequestParam("secondYearPaymeent") int secondYearPaymeent,
			@RequestParam("thirdYearPaymeent") int thirdYearPaymeent,
			@RequestParam("fourthYearPaymeent") int fourthYearPaymeent,
			@RequestParam("programmeFee") int programmeFee, @RequestParam("totalPaymentMade") int totalPaymentMade

	) {

		PGStudent newStudent = new PGStudent();

		newStudent.setType(dlevel);
		newStudent.setYear(year);
		newStudent.setDepartment(department);

		newStudent.setRegistrationNo(registrationNo);
		newStudent.setTitle(Title);
		newStudent.setNamewithinitials(Namewithinitials);
		newStudent.setNamedenotedbyInitials(NamedenotedbyInitials);
		newStudent.setDateofRegistration(DateofRegistration);
		newStudent.setId(ID);
		newStudent.setContactNumber_s(ContactNumber_s);
		newStudent.setEmailAddress_s(EmailAddress_s);
		newStudent.setPostalAddress(PostalAddress);
		newStudent.setUndergraduateDegree(UndergraduateDegree);
		newStudent.setUndergraduateClass(UndergraduateClass);
		newStudent.setUndergraduateUniversity(UndergraduateUniversity);
		newStudent.setExperienceinfield(Experienceinfield);
		newStudent.setDesignation(Designation);
		newStudent.setStream(Stream);
		newStudent.setmScOrMScEng(MScorMScEng);
		newStudent.setPartTimeOrFullTime(PartTimeFullTime);
		newStudent.sethDCMeetingNumber(HDCMeetingNumber);
		newStudent.setAffiliatedLaboratory(AffiliatedLaboratory);
		newStudent.setResearchTitle(ResearchTitle);
		newStudent.setSupervisor_s(Supervisor_s);
		newStudent.setExaminer_s(Examiner_s);
		newStudent.setProgramme(programme);
		newStudent.setCourseFee(courseFee);
		newStudent.setLibraryFee(libraryFee);
		newStudent.setTuitionFee(tuitionFee);
		newStudent.setRegistrationFee(registrationFee);
		newStudent.setFirstYearPaymeent(firstYearPaymeent);
		newStudent.setSecondYearPaymeent(secondYearPaymeent);
		newStudent.setThirdYearPaymeent(thirdYearPaymeent);
		newStudent.setFourthYearPaymeent(fourthYearPaymeent);
		newStudent.setProgrammeFee(programmeFee);
		newStudent.setTotalPaymentMade(totalPaymentMade);

		pgStudentService.addPGStudent(newStudent);
	}

	// view All PG Students
	@GetMapping(value = "/arDashboard/pgStudentDetails")
	public List<PGStudent> getPGStudents() {
		return arService.getPGStudents();
	}

	// Add students by Excel
	@PostMapping(value = "/arDashboard/addPGStudentsByExcel", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void addPGStudentsByExcel(HttpServletResponse response,
			@RequestParam("pgStudentExcel") MultipartFile pgStudentExcel) throws IOException {
		pgStudentService.addPGStudentsByExcel(pgStudentExcel.getInputStream());

	}

	// Edit PG student
	@PutMapping(value = "/arDashboard/editPGStudent", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void editPGStudent(
			// @RequestParam("year") int year,
			@RequestParam("registrationNo") String registrationNo,
			// @RequestParam("studentDataId") String stdId,
			@RequestParam(value = "title", required = false) String Title,
			@RequestParam(value = "namewithinitials", required = false) String Namewithinitials,
			@RequestParam(value = "namedenotedbyInitials", required = false) String NamedenotedbyInitials,
			@RequestParam(value = "type", required = false) String dlevel,
			@RequestParam(value = "partTimeOrFullTime", required = false) String PartTimeFullTime,
			@RequestParam(value = "department", required = false) String department,
			@RequestParam(value = "hDCMeetingNumber", required = false) String HDCMeetingNumber,
			@RequestParam(value = "stream", required = false) String Stream,
			@RequestParam(value = "affiliatedLaboratory", required = false) String AffiliatedLaboratory,
			@RequestParam(value = "programme", required = false) String programme,
			@RequestParam(value = "researchTitle", required = false) String ResearchTitle,
			@RequestParam(value = "dateofRegistration", required = false) String DateofRegistration,
			@RequestParam(value = "supervisor_s", required = false) String Supervisor_s,
			@RequestParam(value = "examiner_s", required = false) String Examiner_s,
			@RequestParam(value = "postalAddress", required = false) String PostalAddress,
			@RequestParam(value = "id", required = false) String ID,
			@RequestParam(value = "emailAddress_s", required = false) String EmailAddress_s,
			@RequestParam(value = "designation", required = false) String Designation,
			@RequestParam(value = "contactNumber_s", required = false) String ContactNumber_s,
			@RequestParam(value = "undergraduateDegree", required = false) String UndergraduateDegree,
			@RequestParam(value = "undergraduateUniversity", required = false) String UndergraduateUniversity,
			@RequestParam(value = "undergraduateClass", required = false) String UndergraduateClass,
			@RequestParam(value = "experienceinfield", required = false) String Experienceinfield,
			@RequestParam(value = "courseFee", required = false) int courseFee,
			@RequestParam(value = "firstYearPaymeent", required = false) int firstYearPaymeent,
			@RequestParam(value = "libraryFee", required = false) int libraryFee,
			@RequestParam(value = "secondYearPaymeent", required = false) int secondYearPaymeent,
			@RequestParam(value = "tuitionFee", required = false) int tuitionFee,
			@RequestParam(value = "thirdYearPaymeent", required = false) int thirdYearPaymeent,
			@RequestParam(value = "registrationFee", required = false) int registrationFee,
			@RequestParam(value = "fourthYearPaymeent", required = false) int fourthYearPaymeent,
			@RequestParam(value = "programmeFee", required = false) int programmeFee,
			@RequestParam(value = "totalPaymentMade", required = false) int totalPaymentMade
	// @RequestParam("MScorMScEng") String MScorMScEng,

	) {
		PGStudent editStudent = pgStudentService.viewPGStudent(registrationNo);

		// editStudent.setStudentDataId(Integer.parseInt(stdId));
		editStudent.setType(dlevel);
		// newStudent.setYear(year);
		editStudent.setDepartment(department);

		// newStudent.setRegistrationNo(registrationNo);
		editStudent.setTitle(Title);
		editStudent.setNamewithinitials(Namewithinitials);
		editStudent.setNamedenotedbyInitials(NamedenotedbyInitials);
		editStudent.setDateofRegistration(DateofRegistration);
		editStudent.setId(ID);
		editStudent.setContactNumber_s(ContactNumber_s);
		editStudent.setEmailAddress_s(EmailAddress_s);
		editStudent.setPostalAddress(PostalAddress);
		editStudent.setUndergraduateDegree(UndergraduateDegree);
		editStudent.setUndergraduateClass(UndergraduateClass);
		editStudent.setUndergraduateUniversity(UndergraduateUniversity);
		editStudent.setExperienceinfield(Experienceinfield);
		editStudent.setDesignation(Designation);
		editStudent.setStream(Stream);
		// editStudent.setmScOrMScEng(MScorMScEng);
		editStudent.setPartTimeOrFullTime(PartTimeFullTime);
		editStudent.sethDCMeetingNumber(HDCMeetingNumber);
		editStudent.setAffiliatedLaboratory(AffiliatedLaboratory);
		editStudent.setResearchTitle(ResearchTitle);
		editStudent.setSupervisor_s(Supervisor_s);
		editStudent.setExaminer_s(Examiner_s);
		editStudent.setProgramme(programme);
		editStudent.setCourseFee(courseFee);
		editStudent.setLibraryFee(libraryFee);
		editStudent.setTuitionFee(tuitionFee);
		editStudent.setRegistrationFee(registrationFee);
		editStudent.setFirstYearPaymeent(firstYearPaymeent);
		editStudent.setSecondYearPaymeent(secondYearPaymeent);
		editStudent.setThirdYearPaymeent(thirdYearPaymeent);
		editStudent.setFourthYearPaymeent(fourthYearPaymeent);
		editStudent.setProgrammeFee(programmeFee);
		editStudent.setTotalPaymentMade(totalPaymentMade);

		pgStudentService.updatePGStudent(editStudent);
	}

	// Delete PG Student By Registration No
	@DeleteMapping("/arDashboard/deletePGStudent/{registrationNo}")
	public void deletePGStudent(@PathVariable("registrationNo") String registrationNo) {
		registrationNo = registrationNo.replaceAll("_", "/");
		pgStudentService.deletePGStudentByRegistrationNo(registrationNo);
	}

	// Search PG Students : By Registration no, Name (2 type), Degree Level,
	// Department, Year,
	@GetMapping(value = "/arDashboard/searchPGStudents/{registrationNo}/{name}/{department}/{degreeLevel}/{year}")
	public List<PGStudent> searchPGStudents(@PathVariable("registrationNo") String registrationNo,
			@PathVariable("name") String name, @PathVariable("department") String department,
			@PathVariable("degreeLevel") String degreeLevel, @PathVariable("year") String year) {

		registrationNo = registrationNo.replaceAll("_", "/");

		if (registrationNo.equals("NA")) {
			registrationNo = "";
		}
		if (name.equals("NA")) {
			name = "";
		}
		if (department.equals("NA")) {
			department = "";
		}
		if (degreeLevel.equals("NA")) {
			degreeLevel = "";
		}
		if (year.equals("NA")) {
			year = "";
		}

		return pgStudentService.search_pgsByData(registrationNo, name, department, degreeLevel, year);
	}

	// download searched PG Student
	@GetMapping(value = "/arDashboard/downloadPGStudents/{registrationNo}/{name}/{department}/{degreeLevel}/{year}")
	public ResponseEntity<Resource> downloadSearchedPGS(@PathVariable("registrationNo") String registrationNo,
			@PathVariable("name") String name, @PathVariable("department") String department,
			@PathVariable("degreeLevel") String degreeLevel, @PathVariable("year") String year) {

		registrationNo = registrationNo.replaceAll("_", "/");

		if (registrationNo.equals("NA")) {
			registrationNo = "";
		}
		if (name.equals("NA")) {
			name = "";
		}
		if (department.equals("NA")) {
			department = "";
		}
		if (degreeLevel.equals("NA")) {
			degreeLevel = "";
		}
		if (year.equals("NA")) {
			year = "";
		}

		String filename = "Postgraduate Student details.xlsx";

		InputStreamResource file = new InputStreamResource(
				pgStudentService.downloadSearchPGS(registrationNo, name, department, degreeLevel, year));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	// view PG student
	@GetMapping(value = "/arDashboard/viewPGStudent/{registrationNo}")
	public PGStudent viewPGStudent(@PathVariable("registrationNo") String registrationNo) {
		registrationNo = registrationNo.replaceAll("_", "/");
		return pgStudentService.viewPGStudent(registrationNo);
	}
	
	/************************************************************
	 * Verification Document
	 * 
	 */
	
	// Get all verification documents
	@GetMapping(value = "/arDashboard/VerificationApplications/viewALL")
	public List<VerificationApplication> getAllVeriApplications(){
		return arService.getAllVeriApplications();
	}
	
	// Get PDF view of the verification document
	@SuppressWarnings("static-access")
	@GetMapping(value = "/arDashboard/VerificationApplications/{applicationId}.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> verificationApplicationPDFView(
			@PathVariable("applicationId") int applicationId) {

		ByteArrayInputStream bytesofPDF = arService.verificationPDFView(arService.getVeriApplicationByID(applicationId));

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=application_" + applicationId + ".pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bytesofPDF));
	}
}
