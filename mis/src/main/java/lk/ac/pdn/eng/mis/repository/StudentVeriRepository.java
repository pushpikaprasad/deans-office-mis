package lk.ac.pdn.eng.mis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.StudentVeriData;

@Repository
public interface StudentVeriRepository extends JpaRepository<StudentVeriData, Integer>{

	StudentVeriData findByDateAndStudentreg(String date, String regNo);

	@Query("select s from StudentVeriData s where s.applicationDataID = ?1")
	List<StudentVeriData> findStuddentByDataID(String applicationDataID);

}
