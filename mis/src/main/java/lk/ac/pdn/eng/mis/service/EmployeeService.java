package lk.ac.pdn.eng.mis.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.ac.pdn.eng.mis.entity.Employee;
import lk.ac.pdn.eng.mis.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	//Find employee by employee number
	public Employee findByEmployeeNumber(String empNo) {
		Employee e = new Employee();
		try {
			e = employeeRepository.findEmployeeByEmployeeNumber(empNo);
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		return e; 
	}
	
	
	//Update Employee
	public void updateEmployee(Employee e) {
		employeeRepository.save(e);
	}
	
	
	//delete employee by employee no
	public void deleteEmployeeByEmpNo(String empNo) {
		try {
			Employee e = findByEmployeeNumber(empNo);
			employeeRepository.delete(e);
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
	}
	
	// Add employee by Excell
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] HEADERs = { "Emp. No","Title","Name","Names of Initials","NIC","Designation","Present Division 1","Department Code",
									"Telephone","Type","Email","Mobile Number","Address","Availability","Remark"};
	static String SHEET = "Employees";

	@SuppressWarnings("unused")
	public void excelToEmployees(InputStream inputStream) {
		try {
			Workbook workbook = new XSSFWorkbook(inputStream);

			Sheet sheet = workbook.getSheet(SHEET);
			Iterator<Row> rows = sheet.iterator();

			List<Employee> employees = new ArrayList<Employee>();

			int rowNumber = 0;
			while (rows.hasNext()) {
				Row currentRow = rows.next();

				// skip header
				if (rowNumber == 0) {
					rowNumber++;
					continue;
				}

				Iterator<Cell> cellsInRow = currentRow.iterator();
				
				Employee employee = new Employee();
				int numberOfColumns = 12;
				int cellIdx = 0;
				//while (cellsInRow.hasNext()) {
				while (cellIdx < numberOfColumns) {
					//System.out.println("cellIdx :"+cellIdx);
					Cell currentCell = currentRow.getCell(cellIdx, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);//cellsInRow.next();
					String cellValue;
					////System.out.print(currentCell.getCellType().toString());
					
					switch (cellIdx) {
					case 0:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setEmployeeNumber"+cellValue);
						employee.setEmployeeNumber(cellValue);
						break;
					case 1:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setTitle"+cellValue);
						employee.setTitle(cellValue);
						break;
					case 2:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setSurnameWithInitials"+cellValue);
						employee.setSurnameWithInitials(cellValue);
						break;
					case 3:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println(currentCell.getStringCellValue());
						//System.out.println("setNamesDenotedByInitials"+cellValue);
						employee.setNamesDenotedByInitials(cellValue);
						break;
					case 4:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setNic"+cellValue);
						employee.setNic(cellValue);
						break;
					case 5:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setDesignation"+cellValue);
						employee.setDesignation(cellValue);
						break;
					case 6:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setPresentDivision1"+cellValue);
						employee.setPresentDivision1(cellValue);
						break;
					case 7:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setPresentDivision1"+cellValue);
						employee.setDepartmentCode(cellValue);
						break;
						
					case 8:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setTelephone"+cellValue);
						employee.setTelephone(cellValue);
						break;
					case 9:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setType"+cellValue);
						employee.setType(cellValue);
						break;
					case 10:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setEmail"+cellValue);
						employee.setEmail(cellValue);
						break;
					case 11:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setMobileNumber"+cellValue);
						employee.setMobileNumber(cellValue);
						break;
					case 12:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setAddress"+cellValue);
						employee.setAddress(cellValue);
						break;
					case 13:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setAddress"+cellValue);
						employee.setAvailability(cellValue);
						break;
					case 14:
						if(currentCell.getStringCellValue().equals("")) {
							cellValue = null;
						}else {
							cellValue= currentCell.getStringCellValue();
						}
						//System.out.println("setAddress"+cellValue);
						employee.setRemark(cellValue);
						break;
					default:
						break;
					}

					cellIdx++;
				}
				
				Employee e = new Employee();
				try {
					e = employeeRepository.findEmployeeByEmployeeNumber(employee.getEmployeeNumber());
					if(e == null) {
						employees.add(employee);
					}
				}catch(Exception ex) {
					System.out.println(ex.getMessage());
				}
			}

			workbook.close();

			//return employees;
			employeeRepository.saveAll(employees);
		} catch (IOException e) {
			throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		}
	}
}
