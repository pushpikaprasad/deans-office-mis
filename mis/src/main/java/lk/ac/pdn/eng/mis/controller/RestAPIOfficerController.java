package lk.ac.pdn.eng.mis.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lowagie.text.DocumentException;

import lk.ac.pdn.eng.mis.entity.Application;
import lk.ac.pdn.eng.mis.entity.Employee;
import lk.ac.pdn.eng.mis.entity.Officer;
import lk.ac.pdn.eng.mis.entity.PGStudent;
import lk.ac.pdn.eng.mis.entity.VerificationApplication;
import lk.ac.pdn.eng.mis.repository.EmployeeRepository;
import lk.ac.pdn.eng.mis.service.ApplicationPDFExporter;
import lk.ac.pdn.eng.mis.service.EmployeeService;
import lk.ac.pdn.eng.mis.service.ExamTimeTableService;
import lk.ac.pdn.eng.mis.service.OfficerService;
import lk.ac.pdn.eng.mis.service.PGStudentService;

@RestController
public class RestAPIOfficerController {

	@Autowired
	private OfficerService officerService;

	@Autowired
	private PGStudentService pgStudentService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ExamTimeTableService examTimeTableService;

	// Get Officer
	@GetMapping(value = "/officerDashboard/getOfficer")
	public Officer getOfficer(Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		return officerService.getOfficer(userDetails.getUsername());
	}

	// update officer
	@PostMapping(value = "/officerDashboard/updateOfficer", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateOfficer(@RequestBody Officer editOfficer, Authentication authentication) {
		officerService.updateOfficer(editOfficer);

	}

	// ###############################################################################################################################
	/* APPLICATION PROCESS */

	// Get all applications
	@GetMapping(value = "/officerDashboard/allApplications")
	public List<Application> getAllApllications() {
		return officerService.getAllTanAndAcaApps();
	}

	// Get all applications by Registration No
	@GetMapping(value = "/officerDashboard/searchApplications/{registrationNo}")
	public List<Application> getApplicationByRegNo(@PathVariable("registrationNo") String registrationNo) {
		String regNo = registrationNo.replaceAll("_", "/");
		return officerService.getAllApplicationsByRegNo(regNo);
	}

	// Get all new applications
	@GetMapping(value = "/officerDashboard/allNewApplications")
	public List<Application> getAllNewApllications() {
		return officerService.getAllNewApplications();
	}

	// Get all accepted applications
	@GetMapping(value = "/officerDashboard/allAcceptedApplications")
	public List<Application> getAllAcceptedTranAcadApllications() {
		return officerService.getAllAcceptedApplication();
	}

	// Get all rejected applications
	@GetMapping(value = "/officerDashboard/allRejectedApplications")
	public List<Application> getAllRejectedTranAcadApllications() {
		return officerService.getAllRejectedApplication();
	}

	// Get all prepared applications
	@GetMapping(value = "/officerDashboard/allPreparedApplications")
	public List<Application> getAllPreparedApplications() {
		return officerService.getAllPreparedApplication();
	}

	// Get all checked OK applications
	@GetMapping(value = "/officerDashboard/allCheckedOKApplications")
	public List<Application> getAllCheckedOKApplications() {
		return officerService.getAllCheckedOKApplication();
	}

	// Get all checked Not OK applications
	@GetMapping(value = "/officerDashboard/allCheckedNotOKApplications")
	public List<Application> getAllCheckedNotOKApplications() {
		return officerService.getAllCheckedNotOKApplication();
	}

	// Get all completed applications
	@GetMapping(value = "/officerDashboard/allCompletedTranAcadApplications")
	public List<Application> getAllCompletedTranAcadApllications() {
		return officerService.getAllCompletedTranAcadApplication();
	}

	// Put application as accepted
	@PutMapping("/officerDashboard/allApplications/setNew/{applicationId}")
	public void setAcceptTranAcadApplication(@PathVariable("applicationId") int applicationId,
			Authentication authentication) throws AddressException, MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.acceptApplication(applicationId, officer);
		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(2),
				"Received accepted new application",
				"Dear Officer, <br><br>You have received accepted new Transcript/Academic Rank Certificate application.<br><br> Thank you ");
	}

	// Put application as rejected
	@PutMapping("/officerDashboard/allApplications/setReject/{applicationId}/{reason}")
	public void setRejectTranAcadApplication(@PathVariable("applicationId") int applicationId,
			@PathVariable("reason") String reason, Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.rejectApplication(applicationId, reason, officer);
	}

	// Put application as duplicate
	@PutMapping("/officerDashboard/allApplications/setDuplicate/{applicationId}")
	public void setDuplicateTranAcadApplication(@PathVariable("applicationId") int applicationId,
			Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.duplicateApplication(applicationId, officer);
	}

	// Put application as prepared
	// upload transcript and academic documents
	@PostMapping(value = "/officerDashboard/allApplications/uploadDocuments/", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE })
	public void uploadDocuments(HttpServletResponse response, @RequestParam("applicationID") int applicationID,
			@RequestParam(value = "transcriptDocument", required = false) MultipartFile transcriptDocument,
			@RequestParam(value = "AcademicDocument", required = false) MultipartFile AcademicDocument,
			Authentication authentication) throws IOException, AddressException, MessagingException {

		// get officer
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		if (!transcriptDocument.isEmpty()) {
			try {
				// Upload transcript document data to database
				officerService.setTranscriptDocumentData(applicationID, transcriptDocument);
				// change status
				officerService.preparedT_Documents(applicationID, officer);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (!AcademicDocument.isEmpty()) {
			try {
				// Upload transcript document data to database
				officerService.setAcademicDocumentData(applicationID, AcademicDocument);
				// change status
				officerService.preparedA_Documents(applicationID, officer);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(3), "Received prepared document/s",
				"Dear Officer, <br><br>You have received prepared document(s) of Transcript/Academic Rank Certificate. <br> Please check those documents and mark them OK or NOT OK (with proper message).<br><br> Thank you ");

	}

	// Put application as checked OK
	@PutMapping("/officerDashboard/allApplications/setCheckedOK/{applicationId}")
	public void setCheckedOKApplication(@PathVariable("applicationId") int applicationId, Authentication authentication)
			throws AddressException, MessagingException {

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		officerService.checkedOKApplication(applicationId, officer);
		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(2),
				"Received Checked-OK application",
				"Dear Officer, <br><br>You have received checked OK application.<br>Application ID: " + applicationId
						+ "<br><br> Thank you ");
	}

	// Put application as checked Not OK
	@PutMapping("/officerDashboard/allApplications/setCheckedNotOK/{applicationId}/{reason}")
	public void setCheckedNotOKApplication(@PathVariable("applicationId") int applicationId,
			@PathVariable("reason") String reason, Authentication authentication)
			throws AddressException, MessagingException {

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		officerService.checkedNotOKApplication(applicationId, reason, officer);
		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(2),
				"Received Checked-Not OK application",
				"Dear Officer, <br><br> The Application ID: " + applicationId
						+ " is checked as NOT OK due to the follwing reason" + "<br><br> Reason: " + reason
						+ "<br><br> Thank you ");
	}

	// Put application as completed
	@PutMapping("/officerDashboard/allApplications/setCompleted/{applicationId}")
	public void setCompletedTranAcadApplication(@PathVariable("applicationId") int applicationId,
			Authentication authentication) throws AddressException, MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.completedApplication(applicationId, officer);

		// send email to relevant officer
		officerService.sendEmailToList(officerService.getEmailListofOfficersByWCID(1), "Received completed application",
				"Dear Officer, <br><br>You have received completed Transcript/Academic Rank Certificate application.<br><br> Thank you ");

	}

	// Put application as done
	@PutMapping("/officerDashboard/allApplications/setDone/{applicationId}/{message}")
	public void setDoneTranAcadApplication(@PathVariable("applicationId") int applicationId,
			@PathVariable("message") String message, Authentication authentication) throws MessagingException {
		String msg = message.replaceAll("_", "/");
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		officerService.doneApplication(applicationId, msg, officer);
	}

	// download application
	@GetMapping(value = "/officerDashboard/allApplications/{applicationId}")
	public void downloadApplication(@PathVariable("applicationId") int applicationId, HttpServletResponse response)
			throws DocumentException, IOException {

		/*
		 * Date date = new Date(); DateFormat df = new
		 * SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
		 * df.setTimeZone(TimeZone.getTimeZone("GMT+5:30")); String submissionDate =
		 * df.format(date); System.out.print(submissionDate);
		 */

		officerService.downloadPDFApplication(applicationId, response);
	}

	// view application
	@GetMapping(value = "/officerDashboard/Applications/{applicationId}.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> pdfReport(@PathVariable("applicationId") int applicationId) {

		ByteArrayInputStream bis = ApplicationPDFExporter.pdfDocument(officerService.getApplication(applicationId));

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=application_" + applicationId + ".pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

	// view application without documents
	@GetMapping(value = "/officerDashboard/InitialApplications/{applicationId}.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> initialpdfReport(@PathVariable("applicationId") int applicationId) {

		ByteArrayInputStream bis = ApplicationPDFExporter
				.initialpdfDocument(officerService.getApplication(applicationId));

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=application_" + applicationId + ".pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

	/********************************************
	 * Employee Details
	 ******************************************/
	// Get department code of the current user
	@GetMapping(value = "/officerDashboard/getDepartmentCode")
	public List<String> getDepartmentCode(Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String departmentCode = officerService.getOfficer(userDetails.getUsername()).getDepartmentCode();
		//System.out.println(departmentCode);
		List<String> newList = new ArrayList<>();
		newList.add(departmentCode);
		return newList;
	}
	
	// Get all employees
	@GetMapping(value = "/officerDashboard/employeeDetails")
	public List<Employee> getEmployees(Model model) {
		return officerService.getEmployees();
	}
	
	// Get all employees by department code
	@SuppressWarnings("unlikely-arg-type")
	@GetMapping(value = "/officerDashboard/employeeDetailsByDepartment")
	public List<Employee> employeeDetailsByDepartment(Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String departmentCode = officerService.getOfficer(userDetails.getUsername()).getDepartmentCode();
		System.out.println(departmentCode);
		List<Employee> newList = new ArrayList<>();
		List<Employee> employeesList = officerService.getEmployees();
		for(int i = 0; i < officerService.getEmployees().size(); i++) {
			if(employeesList.get(i).getDepartmentCode() != null) {
				if(employeesList.get(i).getDepartmentCode().equals(departmentCode)) {
					newList.add(employeesList.get(i));
				}
			}
		}
		return newList;
	}

	// Get employee by Employee Number
	@GetMapping(value = "/officerDashboard/employeeDetails/getEmployee/{empNum}")
	public Employee getEmployee(@PathVariable("empNum") String empNum) {
		return employeeService.findByEmployeeNumber(empNum);
	}

	// Add Employee Details
	@PostMapping(value = "/officerDashboard/employeeDetails/addEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addEmployee(@RequestBody Employee newEmployee) {
		officerService.createEmployee(newEmployee);
	}
	
	/*@PostMapping(value = "/officerDashboard/employeeDetails/addEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addEmployee(@RequestBody Employee newEmployee) {
		officerService.createEmployee(newEmployee);
	}*/

	// Edit Employee
	@PutMapping(value = "/officerDashboard/employeeDetails/editEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void editEmployee(@RequestBody Employee editEmployee) {

		Employee emp = new Employee();
		try {
			emp = employeeService.findByEmployeeNumber(editEmployee.getEmployeeNumber());
			emp.setTitle(editEmployee.getTitle());
			emp.setSurnameWithInitials(editEmployee.getSurnameWithInitials());
			emp.setNamesDenotedByInitials(editEmployee.getNamesDenotedByInitials());
			emp.setNic(editEmployee.getNic());
			emp.setDesignation(editEmployee.getDesignation());
			emp.setPresentDivision1(editEmployee.getPresentDivision1());
			emp.setDepartmentCode(editEmployee.getDepartmentCode());
			emp.setTelephone(editEmployee.getTelephone());
			emp.setEmail(editEmployee.getEmail());
			emp.setMobileNumber(editEmployee.getMobileNumber());
			emp.setAddress(editEmployee.getAddress());
			emp.setAvailability(editEmployee.getAvailability());
			emp.setRemark(editEmployee.getRemark());
			emp.setType(editEmployee.getType());
			

			employeeService.updateEmployee(emp);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	// delete employee
	/*
	 * @DeleteMapping("/officerDashboard/employeeDetails/deleteEmployee/{email}")
	 * public void deleteEmployee(@PathVariable("email") String email) {
	 * officerService.deleteEmployee(email); }
	 */

	// delete employee by employee number
	@DeleteMapping("/officerDashboard/employeeDetails/deleteEmployee/{empNo}")
	public void deleteEmployee(@PathVariable("empNo") String empNo) {
		employeeService.deleteEmployeeByEmpNo(empNo);
	}

	// Create employee accounts using excel sheet
	@PostMapping(value = "/officerDashboard/employeeDetails/employeeExcel", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE })
	public void submitApplication(HttpServletResponse response,
			@RequestParam("employeeExcel") MultipartFile employeeExcel) throws IOException {
		// Upload excel data to database
		officerService.setEmployeeDetailsbySheet(employeeExcel);

	}

	// search employee
	@GetMapping(value = "/officerDashboard/employeeDetails/searchEmployee/{emNumber}/{empSurname}/{empDesig}/{presentDivi}/{type}")
	public List<Employee> searchEmployees(@PathVariable("emNumber") String employeeNumber,
			@PathVariable("empSurname") String surnameWithInitials, @PathVariable("empDesig") String designation,
			@PathVariable("presentDivi") String presentDivision1, @PathVariable("type") String type) {

		if (employeeNumber.equals("NA")) {
			employeeNumber = "";
		}
		if (surnameWithInitials.equals("NA")) {
			surnameWithInitials = "";
		}
		if (designation.equals("NA")) {
			designation = "";
		}
		if (presentDivision1.equals("NA")) {
			presentDivision1 = "";
		}
		if (type.equals("NA")) {
			type = "";
		}

		return officerService.searchEmployee(employeeNumber, surnameWithInitials, designation, presentDivision1, type);
	}

	// download searched employee
	@GetMapping(value = "/officerDashboard/employeeDetails/downloadSearchedEmployees/{emNumber}/{empSurname}/{empDesig}/{presentDivi}/{type}")
	public ResponseEntity<Resource> downloadSearchedEmployees(@PathVariable("emNumber") String employeeNumber,
			@PathVariable("empSurname") String surnameWithInitials, @PathVariable("empDesig") String designation,
			@PathVariable("presentDivi") String presentDivision1, @PathVariable("type") String type) {

		if (employeeNumber.equals("NA")) {
			employeeNumber = "";
		}
		if (surnameWithInitials.equals("NA")) {
			surnameWithInitials = "";
		}
		if (designation.equals("NA")) {
			designation = "";
		}
		if (presentDivision1.equals("NA")) {
			presentDivision1 = "";
		}
		if (type.equals("NA")) {
			type = "";
		}

		String filename = "Employee details.xlsx";
		InputStreamResource file = new InputStreamResource(officerService.downloadSearchEmployee(employeeNumber,
				surnameWithInitials, designation, presentDivision1, type));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	/************************************************************
	 * Post Graduate Student details
	 * 
	 */

	// Add PG student
	@PostMapping(value = "/officerDashboard/addPGStudent", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void addPGStudent(

			@RequestParam("dlevel") String dlevel, @RequestParam("year") int year,
			@RequestParam("department") String department, @RequestParam("registrationNo") String registrationNo,
			@RequestParam("Title") String Title, @RequestParam("Namewithinitials") String Namewithinitials,
			@RequestParam("NamedenotedbyInitials") String NamedenotedbyInitials,
			@RequestParam("DateofRegistration") String DateofRegistration, @RequestParam("ID") String ID,
			@RequestParam("ContactNumber_s") String ContactNumber_s,
			@RequestParam("EmailAddress_s") String EmailAddress_s, @RequestParam("PostalAddress") String PostalAddress,
			@RequestParam("UndergraduateDegree") String UndergraduateDegree,
			@RequestParam("UndergraduateClass") String UndergraduateClass,
			@RequestParam("UndergraduateUniversity") String UndergraduateUniversity,
			@RequestParam("Experienceinfield") String Experienceinfield,
			@RequestParam("Designation") String Designation, @RequestParam("Stream") String Stream,
			@RequestParam("MScorMScEng") String MScorMScEng, @RequestParam("PartTimeFullTime") String PartTimeFullTime,
			@RequestParam("HDCMeetingNumber") String HDCMeetingNumber,
			@RequestParam("AffiliatedLaboratory") String AffiliatedLaboratory,
			@RequestParam("ResearchTitle") String ResearchTitle, @RequestParam("Supervisor_s") String Supervisor_s,
			@RequestParam("Examiner_s") String Examiner_s, @RequestParam("programme") String programme,
			@RequestParam("courseFee") int courseFee, @RequestParam("libraryFee") int libraryFee,
			@RequestParam("tuitionFee") int tuitionFee, @RequestParam("registrationFee") int registrationFee,
			@RequestParam("firstYearPaymeent") int firstYearPaymeent,
			@RequestParam("secondYearPaymeent") int secondYearPaymeent,
			@RequestParam("thirdYearPaymeent") int thirdYearPaymeent,
			@RequestParam("fourthYearPaymeent") int fourthYearPaymeent, @RequestParam("programmeFee") int programmeFee,
			@RequestParam("totalPaymentMade") int totalPaymentMade

	) {

		PGStudent newStudent = new PGStudent();

		newStudent.setType(dlevel);
		newStudent.setYear(year);
		newStudent.setDepartment(department);

		newStudent.setRegistrationNo(registrationNo);
		newStudent.setTitle(Title);
		newStudent.setNamewithinitials(Namewithinitials);
		newStudent.setNamedenotedbyInitials(NamedenotedbyInitials);
		newStudent.setDateofRegistration(DateofRegistration);
		newStudent.setId(ID);
		newStudent.setContactNumber_s(ContactNumber_s);
		newStudent.setEmailAddress_s(EmailAddress_s);
		newStudent.setPostalAddress(PostalAddress);
		newStudent.setUndergraduateDegree(UndergraduateDegree);
		newStudent.setUndergraduateClass(UndergraduateClass);
		newStudent.setUndergraduateUniversity(UndergraduateUniversity);
		newStudent.setExperienceinfield(Experienceinfield);
		newStudent.setDesignation(Designation);
		newStudent.setStream(Stream);
		newStudent.setmScOrMScEng(MScorMScEng);
		newStudent.setPartTimeOrFullTime(PartTimeFullTime);
		newStudent.sethDCMeetingNumber(HDCMeetingNumber);
		newStudent.setAffiliatedLaboratory(AffiliatedLaboratory);
		newStudent.setResearchTitle(ResearchTitle);
		newStudent.setSupervisor_s(Supervisor_s);
		newStudent.setExaminer_s(Examiner_s);
		newStudent.setProgramme(programme);
		newStudent.setCourseFee(courseFee);
		newStudent.setLibraryFee(libraryFee);
		newStudent.setTuitionFee(tuitionFee);
		newStudent.setRegistrationFee(registrationFee);
		newStudent.setFirstYearPaymeent(firstYearPaymeent);
		newStudent.setSecondYearPaymeent(secondYearPaymeent);
		newStudent.setThirdYearPaymeent(thirdYearPaymeent);
		newStudent.setFourthYearPaymeent(fourthYearPaymeent);
		newStudent.setProgrammeFee(programmeFee);
		newStudent.setTotalPaymentMade(totalPaymentMade);

		pgStudentService.addPGStudent(newStudent);
	}

	// view All PG Students
	@GetMapping(value = "/officerDashboard/pgStudentDetails")
	public List<PGStudent> getPGStudents() {
		return officerService.getPGStudents();
	}

	// Add students by Excel
	@PostMapping(value = "/officerDashboard/addPGStudentsByExcel", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void addPGStudentsByExcel(HttpServletResponse response,
			@RequestParam("pgStudentExcel") MultipartFile pgStudentExcel) throws IOException {
		pgStudentService.addPGStudentsByExcel(pgStudentExcel.getInputStream());

	}

	// Edit PG student
	@PutMapping(value = "/officerDashboard/editPGStudent", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void editPGStudent(
			// @RequestParam("year") int year,
			@RequestParam("registrationNo") String registrationNo,
			// @RequestParam("studentDataId") String stdId,
			@RequestParam(value = "title", required = false) String Title,
			@RequestParam(value = "namewithinitials", required = false) String Namewithinitials,
			@RequestParam(value = "namedenotedbyInitials", required = false) String NamedenotedbyInitials,
			@RequestParam(value = "type", required = false) String dlevel,
			@RequestParam(value = "partTimeOrFullTime", required = false) String PartTimeFullTime,
			@RequestParam(value = "department", required = false) String department,
			@RequestParam(value = "hDCMeetingNumber", required = false) String HDCMeetingNumber,
			@RequestParam(value = "stream", required = false) String Stream,
			@RequestParam(value = "affiliatedLaboratory", required = false) String AffiliatedLaboratory,
			@RequestParam(value = "programme", required = false) String programme,
			@RequestParam(value = "researchTitle", required = false) String ResearchTitle,
			@RequestParam(value = "dateofRegistration", required = false) String DateofRegistration,
			@RequestParam(value = "supervisor_s", required = false) String Supervisor_s,
			@RequestParam(value = "examiner_s", required = false) String Examiner_s,
			@RequestParam(value = "postalAddress", required = false) String PostalAddress,
			@RequestParam(value = "id", required = false) String ID,
			@RequestParam(value = "emailAddress_s", required = false) String EmailAddress_s,
			@RequestParam(value = "designation", required = false) String Designation,
			@RequestParam(value = "contactNumber_s", required = false) String ContactNumber_s,
			@RequestParam(value = "undergraduateDegree", required = false) String UndergraduateDegree,
			@RequestParam(value = "undergraduateUniversity", required = false) String UndergraduateUniversity,
			@RequestParam(value = "undergraduateClass", required = false) String UndergraduateClass,
			@RequestParam(value = "experienceinfield", required = false) String Experienceinfield,
			@RequestParam(value = "courseFee", required = false) int courseFee,
			@RequestParam(value = "firstYearPaymeent", required = false) int firstYearPaymeent,
			@RequestParam(value = "libraryFee", required = false) int libraryFee,
			@RequestParam(value = "secondYearPaymeent", required = false) int secondYearPaymeent,
			@RequestParam(value = "tuitionFee", required = false) int tuitionFee,
			@RequestParam(value = "thirdYearPaymeent", required = false) int thirdYearPaymeent,
			@RequestParam(value = "registrationFee", required = false) int registrationFee,
			@RequestParam(value = "fourthYearPaymeent", required = false) int fourthYearPaymeent,
			@RequestParam(value = "programmeFee", required = false) int programmeFee,
			@RequestParam(value = "totalPaymentMade", required = false) int totalPaymentMade
	// @RequestParam("MScorMScEng") String MScorMScEng,

	) {
		PGStudent editStudent = pgStudentService.viewPGStudent(registrationNo);

		// editStudent.setStudentDataId(Integer.parseInt(stdId));
		editStudent.setType(dlevel);
		// newStudent.setYear(year);
		editStudent.setDepartment(department);

		// newStudent.setRegistrationNo(registrationNo);
		editStudent.setTitle(Title);
		editStudent.setNamewithinitials(Namewithinitials);
		editStudent.setNamedenotedbyInitials(NamedenotedbyInitials);
		editStudent.setDateofRegistration(DateofRegistration);
		editStudent.setId(ID);
		editStudent.setContactNumber_s(ContactNumber_s);
		editStudent.setEmailAddress_s(EmailAddress_s);
		editStudent.setPostalAddress(PostalAddress);
		editStudent.setUndergraduateDegree(UndergraduateDegree);
		editStudent.setUndergraduateClass(UndergraduateClass);
		editStudent.setUndergraduateUniversity(UndergraduateUniversity);
		editStudent.setExperienceinfield(Experienceinfield);
		editStudent.setDesignation(Designation);
		editStudent.setStream(Stream);
		// editStudent.setmScOrMScEng(MScorMScEng);
		editStudent.setPartTimeOrFullTime(PartTimeFullTime);
		editStudent.sethDCMeetingNumber(HDCMeetingNumber);
		editStudent.setAffiliatedLaboratory(AffiliatedLaboratory);
		editStudent.setResearchTitle(ResearchTitle);
		editStudent.setSupervisor_s(Supervisor_s);
		editStudent.setExaminer_s(Examiner_s);
		editStudent.setProgramme(programme);
		editStudent.setCourseFee(courseFee);
		editStudent.setLibraryFee(libraryFee);
		editStudent.setTuitionFee(tuitionFee);
		editStudent.setRegistrationFee(registrationFee);
		editStudent.setFirstYearPaymeent(firstYearPaymeent);
		editStudent.setSecondYearPaymeent(secondYearPaymeent);
		editStudent.setThirdYearPaymeent(thirdYearPaymeent);
		editStudent.setFourthYearPaymeent(fourthYearPaymeent);
		editStudent.setProgrammeFee(programmeFee);
		editStudent.setTotalPaymentMade(totalPaymentMade);

		pgStudentService.updatePGStudent(editStudent);
	}

	// Delete PG Student By Registration No
	@DeleteMapping("/officerDashboard/deletePGStudent/{registrationNo}")
	public void deletePGStudent(@PathVariable("registrationNo") String registrationNo) {
		registrationNo = registrationNo.replaceAll("_", "/");
		pgStudentService.deletePGStudentByRegistrationNo(registrationNo);
	}

	// Search PG Students : By Registration no, Name (2 type), Degree Level,
	// Department, Year,
	@GetMapping(value = "/officerDashboard/searchPGStudents/{registrationNo}/{name}/{department}/{degreeLevel}/{year}")
	public List<PGStudent> searchPGStudents(@PathVariable("registrationNo") String registrationNo,
			@PathVariable("name") String name, @PathVariable("department") String department,
			@PathVariable("degreeLevel") String degreeLevel, @PathVariable("year") String year) {

		registrationNo = registrationNo.replaceAll("_", "/");

		if (registrationNo.equals("NA")) {
			registrationNo = "";
		}
		if (name.equals("NA")) {
			name = "";
		}
		if (department.equals("NA")) {
			department = "";
		}
		if (degreeLevel.equals("NA")) {
			degreeLevel = "";
		}
		if (year.equals("NA")) {
			year = "";
		}

		return pgStudentService.search_pgsByData(registrationNo, name, department, degreeLevel, year);
	}

	// download searched PG Student
	@GetMapping(value = "/officerDashboard/downloadPGStudents/{registrationNo}/{name}/{department}/{degreeLevel}/{year}")
	public ResponseEntity<Resource> downloadSearchedPGS(@PathVariable("registrationNo") String registrationNo,
			@PathVariable("name") String name, @PathVariable("department") String department,
			@PathVariable("degreeLevel") String degreeLevel, @PathVariable("year") String year) {

		registrationNo = registrationNo.replaceAll("_", "/");

		if (registrationNo.equals("NA")) {
			registrationNo = "";
		}
		if (name.equals("NA")) {
			name = "";
		}
		if (department.equals("NA")) {
			department = "";
		}
		if (degreeLevel.equals("NA")) {
			degreeLevel = "";
		}
		if (year.equals("NA")) {
			year = "";
		}

		String filename = "Postgraduate Student details.xlsx";

		InputStreamResource file = new InputStreamResource(
				pgStudentService.downloadSearchPGS(registrationNo, name, department, degreeLevel, year));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	// view PG student
	@GetMapping(value = "/officerDashboard/viewPGStudent/{registrationNo}")
	public PGStudent viewPGStudent(@PathVariable("registrationNo") String registrationNo) {
		registrationNo = registrationNo.replaceAll("_", "/");
		return pgStudentService.viewPGStudent(registrationNo);
	}

	/************************************************************
	 * Verification Application details
	 * 
	 */

	// Get all verification Applications
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewALL")
	public List<VerificationApplication> getAllVeriApplications() {
		return officerService.getAllVeriApplications();
	}

	// Get New Verification Applications
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewNewApplications")
	public List<VerificationApplication> getNewVeriApplications() {
		return officerService.getNewVeriApplications();
	}

	// Get Accepted Verification Applications
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewAcceptedApplications")
	public List<VerificationApplication> getAcceptedVeriApplications() {
		return officerService.getAcceptedVeriApplications();
	}

	// Get Completed Verification Applications
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewCompletedApplications")
	public List<VerificationApplication> getCompletedVeriApplications() {
		return officerService.getCompletedVeriApplications();
	}

	// Get Rejected Verification Applications
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewRejectedApplications")
	public List<VerificationApplication> getRejectedVeriApplications() {
		return officerService.getRejectedVeriApplications();
	}

	// Get Prepared Verification Applications
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewPreparedApplications")
	public List<VerificationApplication> getPreparedVeriApplications() {
		return officerService.getPreparedVeriApplications();
	}

	// Get OK verification application
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewOKApplications")
	public List<VerificationApplication> getOKVeriApplications() {
		return officerService.getOKVeriApplications();
	}

	// Get Not OK verification application
	@GetMapping(value = "/officerDashboard/VerificationApplications/viewNOTOKApplications")
	public List<VerificationApplication> getNotOKVeriApplications() {
		return officerService.getNOTOKVeriApplications();
	}

	// View PDF of application
	@SuppressWarnings("static-access")
	@GetMapping(value = "/officerDashboard/VerificationApplications/{applicationId}.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> verificationApplicationPDFView(
			@PathVariable("applicationId") int applicationId) {

		ByteArrayInputStream bytesofPDF = officerService
				.verificationPDFView(officerService.getVeriApplicationByID(applicationId));

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=application_" + applicationId + ".pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bytesofPDF));
	}

	// Accept verification Application
	@PutMapping(value = "/officerDashboard/VerificationApplication/Accept/{applicationId}")
	public void setAcceptVeriApplication(@PathVariable("applicationId") int applicationId,
			Authentication authentication) throws AddressException, MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.acceptVeriApplication(applicationId, officer);
	}

	// Duplicate verification Application
	@PutMapping(value = "/officerDashboard/VerificationApplication/Duplicate/{applicationId}")
	public void setDuplicateVeriApplication(@PathVariable("applicationId") int applicationId,
			Authentication authentication) throws AddressException, MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.duplicateVeriApplication(applicationId, officer);
	}

	// Reject verification Application
	@PutMapping(value = "/officerDashboard/VerificationApplication/Reject/{applicationId}/{reason}")
	public void setRejectVeriApplication(@PathVariable("applicationId") int applicationId,
			Authentication authentication, @PathVariable("reason") String reason) throws MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.rejectVeriApplication(applicationId, officer, reason);
	}

	// Set OK verification Application
	@PutMapping(value = "/officerDashboard/VerificationApplication/MarkOK/{applicationId}")
	public void setOKVeriApplication(@PathVariable("applicationId") int applicationId, Authentication authentication)
			throws AddressException, MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.setOKveriapplication(applicationId, officer);
	}

	// Set ONT OK verification Application
	@PutMapping(value = "/officerDashboard/VerificationApplication/MarkNOTOK/{applicationId}/{reason}")
	public void setNOTOKVeriApplication(@PathVariable("applicationId") int applicationId, Authentication authentication,
			@PathVariable("reason") String reason) throws AddressException, MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();
		officerService.setNotOkverificationApplication(applicationId, reason, officer);
	}

	// Upload Verification document
	@PostMapping(value = "/officerDashboard/uploadVeriDocument/", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void uploadVeriDocuments(HttpServletResponse response, @RequestParam("applicationID") int applicationID,
			@RequestParam(value = "verificationDocument", required = false) MultipartFile verificationDocument,
			Authentication authentication) throws IOException, AddressException, MessagingException {

		// get officer
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		if (!verificationDocument.isEmpty()) {
			// Upload verification document data to database
			officerService.setVerificationDocumentData(applicationID, verificationDocument);

			// change status
			officerService.preparedVerificationDocument(applicationID, officer);
		}

	}

	// Send/email final message to applicant
	@PutMapping(value = "/officerDashboard/VerificationApplication/finalVerificationMessage/{applicationId}/{message}")
	public void setfinalVeriAppMessage(@PathVariable("applicationId") int applicationId, Authentication authentication,
			@PathVariable("message") String message) throws MessagingException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		officerService.setFinalMsgVerificationApplication(applicationId, message, officer);
	}
	
	// Send Verification document to applicant
	@PostMapping(value = "/officerDashboard/sendVeriDocument/", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void sendEmailwithFileAttachmentandCC(HttpServletResponse response,
			@RequestParam("applicationID") int applicationID,
			@RequestParam(value = "verificationDocument", required = false) MultipartFile verificationDocument,
			@RequestParam(value = "vdsendto", required = false) String to,
			@RequestParam(value = "vdsendfrom", required = false) String from,
			@RequestParam(value = "vdsendcc", required = false) String cc,
			@RequestParam(value = "vdsendsubject", required = false) String subject,
			@RequestParam(value = "vdsendmsgBody", required = false) String msgBody, Authentication authentication)
			throws IOException, MessagingException {

		// get officer
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String officer = officerService.getOfficer(userDetails.getUsername()).getFirstName();

		officerService.sendVerificationDocumentWithPDFAndCC(applicationID, to, from, cc, subject, msgBody,
				verificationDocument, officer);

	}
	
	// Search by Applicant name
	@GetMapping(value = "/officerDashboard/searchVerificationByName/{searchName}")
	public List<VerificationApplication> searchVerificationByName(@PathVariable("searchName") String searchName) {
		System.out.println(searchName);
		return officerService.searchVerificationByName(searchName);
	}

	/************************************************************
	 * Exam Time Table Processing
	 * 
	 */

	// Create Subject data and Create Student data
	@PostMapping(value = "/officerDashboard/exam/uploadExamData", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void uploadExamData(@RequestParam("examData") MultipartFile[] excelFiles,
			@RequestParam("examName") String examName) {
		examTimeTableService.setClassListData(excelFiles, examName);
	}

	// Get list of exams
	@GetMapping(value = "/officerDashboard/exam/listofExams")
	public String[] getListofExams() {
		return examTimeTableService.getListofExams();
	}

	// Get list of courses
	@GetMapping(value = "/officerDashboard/exam/listofCourses/{examName}")
	public String[] getListofCourses(@PathVariable("examName") String examName) {
		return examTimeTableService.getListofCourses(examName);
	}

	// Get connected list for each subject
	@GetMapping(value = "/officerDashboard/exam/getCoonectedForeach/{examName}")
	public String[][] getCoonectedForeach(@PathVariable("examName") String examName) {
		return examTimeTableService.getCoonectedForeach(examName);
	}

	// Get list of courses in slots
	@GetMapping(value = "/officerDashboard/exam/setOfCourses/{examName}")
	public String[][] getSetofCourses(@PathVariable("examName") String examName) {
		return examTimeTableService.generate_timeslots(examName);
	}

}
