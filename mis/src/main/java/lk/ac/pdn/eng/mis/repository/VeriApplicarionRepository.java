package lk.ac.pdn.eng.mis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.VerificationApplication;

@Repository
public interface VeriApplicarionRepository extends JpaRepository<VerificationApplication, Integer>{

	
	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId, a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) from VerificationApplication a where a.applicationStatus = 'new'")
	List<VerificationApplication> findAllNewApplications();

	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId,a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) from VerificationApplication a where a.applicationStatus = 'accepted'")
	List<VerificationApplication> findAllAcceptedApplications();

	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId,a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) from VerificationApplication a where a.applicationStatus = 'completed'")
	List<VerificationApplication> findAllCompletedApplications();

	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId,a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) from VerificationApplication a where a.applicationStatus = 'rejected'")
	List<VerificationApplication> findAllRejectedApplications();

	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId,a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) from VerificationApplication a where a.applicationStatus = 'prepared'")
	List<VerificationApplication> findAllPreparedApplications();

	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId,a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) "
			+ "from VerificationApplication a where a.applicationStatus = 'marked_OK'")
	List<VerificationApplication> findAllOKApplications();
	
	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId,a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) "
			+ "from VerificationApplication a where a.applicationStatus = 'marked_NOT_OK'")
	List<VerificationApplication> findAllNOTOKApplications();
	
	
	@Query("select new lk.ac.pdn.eng.mis.entity.VerificationApplication(a.applicationId, a.applicantName, a.applicantAddress, a.applicantMobile,"
			+ "	a.applicantEmail, a.methodOfcollection, a.postAddress, a.postEmail,"
			+ "	a.methodOfPayment,  a.paymentVerifyCode, a.applicationDataID,"
			+ "	a.submissionDate, a.applicationStatus, a.applicationStatusDetails) from VerificationApplication a")
	List<VerificationApplication> findAll();

	@Query("select a from VerificationApplication a where a.applicationId = ?1")
	VerificationApplication getApplicationByID(int applicationId);
	
	@Query("select a from VerificationApplication a where a.applicantName like %?1%")
	List<VerificationApplication> getApplicationsBySearchName(String searchName);
	
}
