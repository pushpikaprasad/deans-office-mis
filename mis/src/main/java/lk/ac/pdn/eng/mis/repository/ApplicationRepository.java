package lk.ac.pdn.eng.mis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer>{
	
	@Query("select a from Application a where a.applicationId = ?1")
	Application findByApplicationId(int id);
	
	@Query("select a from Application a where a.registrationNo = ?1")
	List<Application> findApplicationByRegNo(String regNo);

	@Query("select a from Application a where a.date = ?1")
	Application findByDate(String date);

	@Query("select new lk.ac.pdn.eng.mis.entity.Application(a.applicationId, a.applicationType, a.email, a.mobileNumber,"
			+ "a.surnameWithInitials, a.NamesDenotedByInitials, a.titlesPrefixing, a.postalAddress,"
			+ "a.registrationNo, a.field, a.cus_generalProgramme, a.cus_specializationProgramme,"
			+ "a.ore_firstExaminEng, a.ore_finalPart1Exam, a.ore_finalPart2Exam, a.ore_finalPart3Exam,"
			+ "a.pge_nameOfdegreeOrDipl, a.pge_yearCompleted,"
			+ "a.requestMadefor, a.payment, a.collectionMethod, a.paymentPDFId, a.clearenceFormPDFId,"
			+ "a.addressToSendCertificate, a.localOrForieng, a.numberOfCopies, a.date, a.status) from Application a")
	List<Application> findAll();
	
	
	@Query("select new lk.ac.pdn.eng.mis.entity.Application(a.applicationId, a.applicationType, a.email, a.mobileNumber,"
			+ "a.surnameWithInitials, a.NamesDenotedByInitials, a.titlesPrefixing, a.postalAddress,"
			+ "a.registrationNo, a.field, a.cus_generalProgramme, a.cus_specializationProgramme,"
			+ "a.ore_firstExaminEng, a.ore_finalPart1Exam, a.ore_finalPart2Exam, a.ore_finalPart3Exam,"
			+ "a.pge_nameOfdegreeOrDipl, a.pge_yearCompleted,"
			+ "a.requestMadefor, a.payment, a.collectionMethod, a.paymentPDFId, a.clearenceFormPDFId,"
			+ "a.addressToSendCertificate, a.localOrForieng, a.numberOfCopies, a.date, a.status) from Application a where a.status = ?1")
	List<Application> findByStatus(String status);
	
}
