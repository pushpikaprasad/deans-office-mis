package lk.ac.pdn.eng.mis.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.ac.pdn.eng.mis.entity.PGStudent;
import lk.ac.pdn.eng.mis.repository.PGStudentRepository;

@Service
public class PGStudentService {

	@Autowired
	private PGStudentRepository pgStudentRepository;

	// Add student
	public void addPGStudent(PGStudent std) {
		pgStudentRepository.save(std);
	}

	// Add students by Excel
	// Sheets: PG.Dip. / M.Sc. or M.Sc.Eng. / M.Phil. / ph.D.
	
	/* Attributes:
	 * 
	 * PG.Dip.:- Registration No Title Name with initials Name denoted by Initials
	 * Date of Registration ID Contact Number(s) E-mail Address(s) Postal Address
	 * Undergraduate Degree Undergraduate Class Undergraduate University
	 * Expererience in field Designation Stream Payment
	 * 
	 * M.Sc. or M.Sc.Eng.:-  M.Sc. or M.Sc.Eng.
	 * 
	 * M.Phil. / ph.D.:-  Part Time/Full Time HDC Meeting Number Affiliated Laboratory
	 * Research Title Supervisor(s) Examiner(s)
	 */
	
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	
	static String[] HEADERs = { "Year", "Department", "Type", "Registration No", "Title", "Name with initials",
			"Name denoted by Initials", "Date of Registration", "ID", "Contact Number(s)", "E-mail Address(s)",
			"Postal Address", "Undergraduate Degree", "Undergraduate Class", "Undergraduate University",
			"Expererience in field", "Designation", "Stream", "Payment", "M.Sc. or M.Sc.Eng.", "Part Time/Full Time",
			"HDC Meeting Number", "Affiliated Laboratory", "Research Title", "Supervisor(s)", "Examiner(s)" };
	static String SHEET1 = "PG.Dip.";
	static String SHEET2 = "M.Sc. or M.Sc.Eng.";
	static String SHEET3 = "M.Phil.";
	static String SHEET4 = "ph.D.";

	boolean isEmptyRow(Row row) {
	    boolean isEmpty=true;
	    String data="";
	    for(Cell cell:row) {
	    	//System.out.println(cell.getCellType().toString());
	    	if(cell.getCellType() == CellType.NUMERIC) {
				if (cell.getNumericCellValue() == 0) {
					data=data.concat(""+0);
				} else {
					data=data.concat(String.valueOf(cell.getNumericCellValue()));
				}
			}else {
				if (cell.getStringCellValue().equals("")) {
					data=data.concat("");
				} else {
					data=data.concat(cell.getStringCellValue());
				}
			}
	    }
	    if(!data.trim().isEmpty()) {
	        isEmpty=false;
	    }
	    return isEmpty;
	}
	
	public void addPGStudentsByExcel(InputStream inputStream) {
		try {
			
			Workbook workbook = new XSSFWorkbook(inputStream);
			List<PGStudent> stdSet = new ArrayList<PGStudent>();

			// For PG.Dip.
			System.out.println("In Sheet 1");
			Sheet sheet1 = workbook.getSheet(SHEET1);
			if (sheet1 != null) {
				//System.out.println("Entered to the sheet");
				Iterator<Row> rowSet1 = sheet1.iterator();
				int rowNumber = 0;
				int count1=0;
				while (rowSet1.hasNext()) {
					/*
					 * // skip header if (rowNumber == 0) { rowNumber++; continue; }
					 */
					//System.out.println(count1++);
					
					Row currentRowOfSet1 = rowSet1.next();
					
					// skip header
					if (rowNumber == 0) {
						rowNumber++;
						continue;
					}
					
					Iterator<Cell> cellsInRow = currentRowOfSet1.iterator();
					PGStudent std = new PGStudent();
					
					//add degree level, year and department
					//std.setType("PG.Dip.");
					//std.setYear(year);
					//std.setDepartment(department);
											
					int numberOfColumns = 30;
					int cellIdx = 0;
					
					//while (cellsInRow2.hasNext()) {
					while (cellIdx < numberOfColumns) {
						
						Cell currentCell = currentRowOfSet1.getCell(cellIdx, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
						
						//System.out.println("Cell IDx:" + cellIdx);
						String cellValue;
						switch (cellIdx) {
						case 0:
							/* Registration No */ 
							 if (currentCell == null) { cellValue = null;break; // Register number should not null
							 } else if (currentCell.getCellType() == CellType.NUMERIC) {
								if (currentCell.getNumericCellValue() == 0) {
									cellValue = null;break;
								} else {
									cellValue = String.valueOf(currentCell.getNumericCellValue());
								}
							} else {
								if (currentCell.getStringCellValue().equals("")
										|| currentCell.getCellType() == CellType.BLANK) {
									cellValue = null;
								} else {
									cellValue = currentCell.getStringCellValue();
									std.setRegistrationNo(cellValue);
								}
							}
							
							break;
						case 1: /*Title*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setTitle(cellValue);break;
						case 2: /*Name with initials*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setNamewithinitials(cellValue);break;
						case 3: /*Name denoted by Initials*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setNamedenotedbyInitials(cellValue);break;
						case 4: /*Date of Registration*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setDateofRegistration(cellValue);break;
						case 5: /*ID*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setId(cellValue);break;
						case 6: /*Contact Number(s)*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setContactNumber_s(cellValue);break;
						case 7: /*E-mail Address(s)*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setEmailAddress_s(cellValue);break;
						case 8: /*Postal Address*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setPostalAddress(cellValue);break;
						case 9: /*Undergraduate Degree*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setUndergraduateDegree(cellValue);break;
						case 10: /*Undergraduate Class*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setUndergraduateClass(cellValue);break;
						case 11: /*Undergraduate University*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setUndergraduateUniversity(cellValue);break;
						case 12: /*Experience in field*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setExperienceinfield(cellValue);break;
						case 13: /*Designation*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setDesignation(cellValue);break;
						case 14: /*Stream*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setStream(cellValue);break;
						case 15: /*examiner*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setExaminer_s(cellValue);break;
						case 16: /*department*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setDepartment(cellValue);break;
						case 17: /*PhD/Mphil/MSc/Diploma*/	if (currentCell == null) { cellValue = null; } else if(currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}}else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setType(cellValue);break;
						case 18:/* programme */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	if (currentCell.getNumericCellValue() == 0) {		cellValue = null;	} else {		cellValue = String.valueOf(currentCell.getNumericCellValue());	}} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		cellValue = currentCell.getStringCellValue();	}}std.setProgramme(cellValue);break;
						case 19:/* courseFee */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setCourseFee(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setCourseFee(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 20:/* libraryFee */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setLibraryFee(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setLibraryFee(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 21:/* tuitionFee */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setTuitionFee(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setTuitionFee(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 22:/* registrationFee */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setRegistrationFee(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setRegistrationFee(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 23:/* firstYearPaymeent */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setFirstYearPaymeent(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setFirstYearPaymeent(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 24:/* secondYearPaymeent */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setSecondYearPaymeent(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setSecondYearPaymeent(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 25:/* thirdYearPaymeent */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setThirdYearPaymeent(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setThirdYearPaymeent(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 26:/* fourthYearPaymeent */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setFourthYearPaymeent(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setFourthYearPaymeent(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 27:/* programmeFee */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setProgrammeFee(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setProgrammeFee(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						case 28:/* totalPaymentMade */	 if (currentCell == null) { cellValue = null; } else if (currentCell.getCellType() == CellType.NUMERIC) {	std.setTotalPaymentMade(currentCell.getNumericCellValue());} else {	if (currentCell.getStringCellValue().equals("")  || currentCell.getCellType() == CellType.BLANK) {		cellValue = null;	} else {		std.setTotalPaymentMade(Double.valueOf(currentCell.getNumericCellValue()));	}}break;
						default:
							break;
						}
						cellIdx++;
					}
					/*
					 * if (std.getRegistrationNo() != null ||
					 * !pgStudentRepository.findByRegistrationNo(std.getRegistrationNo()).isEmpty())
					 * { //System.out.println(std.getRegistrationNo()); stdSet.add(std); }
					 */
					if (std.getRegistrationNo() != null) {
						try {
							List<PGStudent> check_stds = pgStudentRepository.findByRegistrationNo(std.getRegistrationNo());  
							if(check_stds.isEmpty()) {
								//System.out.println(std4.getRegistrationNo());
								stdSet.add(std);
							}
						}catch(Exception e) {
							System.out.println("PGStudentService.addPGStudentsByExcel()");
						}
					}
					//System.out.println("done 1");
					
				}
			}
			// For M.Sc.
			System.out.println("In Sheet 2");
			Sheet sheet2 = workbook.getSheet(SHEET2);
			if (sheet2 != null) {
				Iterator<Row> rowSet2 = sheet2.iterator();
				int rowNumber2 = 0;
				int count = 0;
				while (rowSet2.hasNext()) {
					//if(!isEmptyRow(rowSet2.next()) || rowSet2.next() != null) {
					//System.out.println(count++);	
					Row currentRowOfSet2 = rowSet2.next();
					// skip header
					if (rowNumber2 == 0) {
						rowNumber2++;
						continue;
					}
					Iterator<Cell> cellsInRow2 = currentRowOfSet2.iterator();
					PGStudent std2 = new PGStudent();
					
					//add degree level, year and department
					//std2.setType("M.Sc. or M.Sc.Eng.");
					//std2.setYear(year);
					//std2.setDepartment(department);
					
					int numberOfColumns = 30;
					int cellIdx2 = 0;
					
					//while (cellsInRow2.hasNext()) {
					while (cellIdx2<numberOfColumns) {
						
						Cell currentCell2 = currentRowOfSet2.getCell(cellIdx2, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
						/*
						 * if(cellsInRow2.hasNext()) { currentCell2 = cellsInRow2.next(); }else {
						 * cellIdx2++; }
						 */
						
						
						//System.out.println("for cell"+cellIdx2+" - "+currentCell2.getCellType());
						
						
						String cellValue2;
						switch (cellIdx2) {
						case 0:
							/* Registration No */if (currentCell2 == null) {
								cellValue2 = null; break; // register number should not null
							} else if (currentCell2.getCellType() == CellType.NUMERIC) {
								if (currentCell2.getNumericCellValue() == 0) {
									cellValue2 = null;break;
								} else {
									cellValue2 = String.valueOf(currentCell2.getNumericCellValue());
								}
							} else {
								if (currentCell2.getStringCellValue().equals("")
										|| currentCell2.getCellType() == CellType.BLANK) {
									cellValue2 = null;
								} else {
									cellValue2 = currentCell2.getStringCellValue();
									std2.setRegistrationNo(cellValue2);
								}
							}
							break;
						case 1:
							/* Title */ 
							if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {
								if (currentCell2.getNumericCellValue() == 0) {
									cellValue2 = null;
								} else {
									cellValue2 = String.valueOf(currentCell2.getNumericCellValue());
								}
							} else {
								if (currentCell2.getStringCellValue().equals("	")
										|| currentCell2.getCellType() == CellType.BLANK) {
									cellValue2 = null;
								} else {
									cellValue2 = currentCell2.getStringCellValue();
								}
							}
							std2.setTitle(cellValue2);
							break;
						case 2: /*Name with initials*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setNamewithinitials(cellValue2);break;
						case 3:
							/* Name denoted by Initials */ 
							if (currentCell2 == null) { cellValue2 = null; } else 
							if (currentCell2.getCellType() == CellType.NUMERIC) {
								if (currentCell2.getNumericCellValue() == 0) {
									cellValue2 = null;
								} else {
									cellValue2 = String.valueOf(currentCell2.getNumericCellValue());
								}
							} else {
								if (currentCell2.getStringCellValue().equals("")
										|| currentCell2.getCellType() == CellType.BLANK) {
									cellValue2 = null;
								} else {
									cellValue2 = currentCell2.getStringCellValue();
								}
							}
							std2.setNamedenotedbyInitials(cellValue2);
							break;
						case 4: /*Date of Registration*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setDateofRegistration(cellValue2);break;
						case 5: /*ID*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setId(cellValue2);break;
						case 6: /* Contact Number(s)*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setContactNumber_s(cellValue2);break;
						case 7: /*E-mail Address(s)*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setEmailAddress_s(cellValue2);break;
						case 8: /*Postal Address*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setPostalAddress(cellValue2);break;
						case 9: /*Undergraduate Degree*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setUndergraduateDegree(cellValue2);break;
						case 10: /*Undergraduate Class*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setUndergraduateClass(cellValue2);break;
						case 11: /*Undergraduate University*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setUndergraduateUniversity(cellValue2);break;
						case 12: /*Experience in field*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setExperienceinfield(cellValue2);break;
						case 13: /*Designation*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setDesignation(cellValue2);break;
						case 14: /* M.Sc. or M.Sc.Eng.*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setmScOrMScEng(cellValue2);break;
						case 15: /*Stream*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setStream(cellValue2);break;
						//case 16: /*Payment*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setTotalPaymentMade(Integer.valueOf(cellValue2));break;
						case 16: /*examiner*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue().replaceAll("[\n\r]", "");;	}}std2.setExaminer_s(cellValue2);break;
						case 17: /*department*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setDepartment(cellValue2);break;
						case 18: /*PhD/Mphil/MSc/Diploma*/	if (currentCell2 == null) { cellValue2 = null; } else if(currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}}else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setType(cellValue2);break;
						case 19:/* programme */	if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	if (currentCell2.getNumericCellValue() == 0) {		cellValue2 = null;	} else {		cellValue2 = String.valueOf(currentCell2.getNumericCellValue());	}} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		cellValue2 = currentCell2.getStringCellValue();	}}std2.setProgramme(cellValue2);break;
						case 20:/* courseFee */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setCourseFee(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setCourseFee(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 21:/* libraryFee */	if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setLibraryFee(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setLibraryFee(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 22:/* tuitionFee */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setTuitionFee(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setTuitionFee(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 23:/* registrationFee */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setRegistrationFee(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setRegistrationFee(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 24:/* firstYearPaymeent */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setFirstYearPaymeent(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setFirstYearPaymeent(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 25:/* secondYearPaymeent */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setSecondYearPaymeent(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setSecondYearPaymeent(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 26:/* thirdYearPaymeent */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setThirdYearPaymeent(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setThirdYearPaymeent(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 27:/* fourthYearPaymeent */	if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setFourthYearPaymeent(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setFourthYearPaymeent(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 28:/* programmeFee */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setProgrammeFee(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setProgrammeFee(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						case 29:/* totalPaymentMade */	 if (currentCell2 == null) { cellValue2 = null; } else if (currentCell2.getCellType() == CellType.NUMERIC) {	std2.setTotalPaymentMade(currentCell2.getNumericCellValue());} else {	if (currentCell2.getStringCellValue().equals("")  || currentCell2.getCellType() == CellType.BLANK) {		cellValue2 = null;	} else {		std2.setTotalPaymentMade(Double.valueOf(currentCell2.getNumericCellValue()));	}}break;
						default:
							break;
						}
						cellIdx2++;
					}

					
					/*
					 * if (std2.getRegistrationNo() != null ||
					 * !pgStudentRepository.findByRegistrationNo(std2.getRegistrationNo()).isEmpty()
					 * ) { //System.out.println(std2.getRegistrationNo()); stdSet.add(std2); }
					 */
					
					if (std2.getRegistrationNo() != null) {
						try {
							List<PGStudent> check_stds = pgStudentRepository.findByRegistrationNo(std2.getRegistrationNo());  
							if(check_stds.isEmpty()) {
								//System.out.println(std4.getRegistrationNo());
								stdSet.add(std2);
							}
						}catch(Exception e) {
							System.out.println("PGStudentService.addPGStudentsByExcel()");
						}
					}
					//System.out.println("done 2");
					//}
				}
			}
			// For M.Phil.
			System.out.println("In Sheet 3");
			Sheet sheet3 = workbook.getSheet(SHEET3);
			if (sheet3 != null) {
				Iterator<Row> rowSet3 = sheet3.iterator();
				int rowNumber3 = 0;
				int row_num = 0;
				while (rowSet3.hasNext()) {
					
					Row currentRowOfSet3 = rowSet3.next();
					// skip header 
					if (rowNumber3 == 0) {
						rowNumber3++;
						continue;
					}
					Iterator<Cell> cellsInRow3 = currentRowOfSet3.iterator();
					PGStudent std3 = new PGStudent();
					
					//add degree level, year and department
					//std3.setType("M.Phil.");
					//std3.setYear(year);
					//std3.setDepartment(department);
					
					int numberOfColumns = 30;
					int cellIdx3 = 0;
					
					row_num++;
					System.out.println("Row num "+ row_num);
					//while (cellsInRow2.hasNext()) {
					while (cellIdx3<numberOfColumns) {
						
						Cell currentCell3 = currentRowOfSet3.getCell(cellIdx3, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
						String cellValue3;
						switch (cellIdx3) {
						case 0:
							/* Registration No */ if (currentCell3 == null) {
								cellValue3 = null; break; // register number should not null
							} else if (currentCell3.getCellType() == CellType.NUMERIC) {
								if (currentCell3.getNumericCellValue() == 0) {
									cellValue3 = null;break;
								} else {
									cellValue3 = String.valueOf(currentCell3.getNumericCellValue());
								}
							} else {
								if (currentCell3.getStringCellValue().equals("")
										|| currentCell3.getCellType() == CellType.BLANK) {
									cellValue3 = null;
								} else {
									cellValue3 = currentCell3.getStringCellValue();
									std3.setRegistrationNo(cellValue3);
								}
							}
							
							break;
						//case 1: /*Title*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setTitle(cellValue3);break;
						case 1: /*Name with initials*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setNamewithinitials(cellValue3);break;
						case 2: /*Name denoted by Initials*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setNamedenotedbyInitials(cellValue3);break;
						case 3: /*Date of Registration*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setDateofRegistration(cellValue3);break;
						case 4: /*Part Time/Full Time*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setPartTimeOrFullTime(cellValue3);break;
						case 5: /* HDC Meeting Number - integer*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.sethDCMeetingNumber(cellValue3);break;
						case 6: /*Postal Address*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setPostalAddress(cellValue3);break;
						case 7: /*E-mail Address(s)*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setEmailAddress_s(cellValue3);break;
						case 8: /* Contact Number(s)*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setContactNumber_s(cellValue3);break;
						case 9: /*Affiliated Laboratory*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setAffiliatedLaboratory(cellValue3);break;
						case 10: /*Research Title*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setResearchTitle(cellValue3);break;
						case 11: /*Supervisor(s)*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setSupervisor_s(cellValue3);break;
						case 12: /*Examiner(s)*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setExaminer_s(cellValue3);break;
						case 13: /*Stream*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setStream(cellValue3);break;
						//case 15: /*Payment*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setTotalPaymentMade(Integer.valueOf(cellValue3));break; 
						//case 16: /*examiner*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setExaminer_s(cellValue3);break;
						case 14: /*department*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setDepartment(cellValue3);break;
						case 15: /*PhD/Mphil/MSc/Diploma*/	if (currentCell3 == null) { cellValue3 = null; } else if(currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}}else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setType(cellValue3);break;
						case 16:/* programme */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	if (currentCell3.getNumericCellValue() == 0) {		cellValue3 = null;	} else {		cellValue3 = String.valueOf(currentCell3.getNumericCellValue());	}} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		cellValue3 = currentCell3.getStringCellValue();	}}std3.setProgramme(cellValue3);break;
						case 17:/* courseFee */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setCourseFee(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setCourseFee(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 18:/* libraryFee */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setLibraryFee(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setLibraryFee(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 19:/* tuitionFee */	if (currentCell3 == null) { cellValue3 = null; } else  if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setTuitionFee(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setTuitionFee(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 20:/* registrationFee */	if (currentCell3 == null) { cellValue3 = null; } else  if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setRegistrationFee(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setRegistrationFee(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 21:/* firstYearPaymeent */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setFirstYearPaymeent(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setFirstYearPaymeent(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 22:/* secondYearPaymeent */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setSecondYearPaymeent(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setSecondYearPaymeent(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 23:/* thirdYearPaymeent */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setThirdYearPaymeent(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setThirdYearPaymeent(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 24:/* fourthYearPaymeent */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setFourthYearPaymeent(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setFourthYearPaymeent(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 25:/* programmeFee */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setProgrammeFee(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setProgrammeFee(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						case 26:/* totalPaymentMade */	 if (currentCell3 == null) { cellValue3 = null; } else if (currentCell3.getCellType() == CellType.NUMERIC) {	std3.setTotalPaymentMade(currentCell3.getNumericCellValue());} else {	if (currentCell3.getStringCellValue().equals("")  || currentCell3.getCellType() == CellType.BLANK) {		cellValue3 = null;	} else {		std3.setTotalPaymentMade(Double.valueOf(currentCell3.getNumericCellValue()));	}}break;
						
						
						default:
							break;
						}
						cellIdx3++;
					}
					/*
					 * if (std3.getRegistrationNo() != null ||
					 * !pgStudentRepository.findByRegistrationNo(std3.getRegistrationNo()).isEmpty()
					 * ) { //System.out.println(std3.getRegistrationNo()); stdSet.add(std3); }
					 */
					
					if (std3.getRegistrationNo() != null) {
						try {
							List<PGStudent> check_stds = pgStudentRepository.findByRegistrationNo(std3.getRegistrationNo());  
							if(check_stds.isEmpty()) {
								//System.out.println(std4.getRegistrationNo());
								stdSet.add(std3);
							}
						}catch(Exception e) {
							System.out.println("PGStudentService.addPGStudentsByExcel()");
						}
					}
					//System.out.println("done 3");
					
				}
			}
			// For ph.D
			System.out.println("In Sheet 4");
			Sheet sheet4 = workbook.getSheet(SHEET4);
			if (sheet4 != null) {
				Iterator<Row> rowSet4 = sheet4.iterator();

				int rowNumber4 = 0;
				while (rowSet4.hasNext()) {
					
					Row currentRowOfSet4 = rowSet4.next();

					// skip header
					if (rowNumber4 == 0) {
						rowNumber4++;
						continue;
					}

					Iterator<Cell> cellsInRow4 = currentRowOfSet4.iterator();

					PGStudent std4 = new PGStudent();

					
					//add degree level, year and department
					//std4.setType("Ph.D.");
					//std4.setYear(year);
					//std4.setDepartment(department);
					
					
					int numberOfColumns = 30;
					int cellIdx4 = 0;
					
					//while (cellsInRow2.hasNext()) {
					while (cellIdx4<numberOfColumns) {
						
						Cell currentCell4 = currentRowOfSet4.getCell(cellIdx4, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
						String cellValue4 = "";
						
						switch (cellIdx4) {
						case 0:
							/* Registration No */ if (currentCell4 == null) {
								cellValue4 = null; break; // register number should not null
							} else if (currentCell4.getCellType() == CellType.NUMERIC) {
								if (currentCell4.getNumericCellValue() == 0) {
									cellValue4 = null;break;
								} else {
									cellValue4 = String.valueOf(currentCell4.getNumericCellValue());
								}
							} else {
								if (currentCell4.getStringCellValue().equals("")
										|| currentCell4.getCellType() == CellType.BLANK) {
									cellValue4 = null;
								} else {
									cellValue4 = currentCell4.getStringCellValue();
									
								}
							}
							
							std4.setRegistrationNo(cellValue4);
							break;
						//case 1: /*Title*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setTitle(cellValue4);break;
						
						case 1:
							/* Name with initials */ if (currentCell4 == null) {
								cellValue4 = null;
							} else if (currentCell4.getCellType() == CellType.NUMERIC) {
								if (currentCell4.getNumericCellValue() == 0) {
									cellValue4 = null;
								} else {
									cellValue4 = String.valueOf(currentCell4.getNumericCellValue());
								}
							} else {
								if (currentCell4.getStringCellValue().equals("")
										|| currentCell4.getCellType() == CellType.BLANK) {
									cellValue4 = null;
								} else {
									cellValue4 = currentCell4.getStringCellValue();
								}
							}
							//System.out.println(cellValue4);
							std4.setNamewithinitials(cellValue4);
							break;

						case 2: /*Name denoted by Initials*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setNamedenotedbyInitials(cellValue4);break;
						case 3: /*Date of Registration*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setDateofRegistration(cellValue4);break;
						case 4: /*Part Time/Full Time*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setPartTimeOrFullTime(cellValue4);break;
						case 5: /*HDC Meeting Number - integer*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.sethDCMeetingNumber(cellValue4);break;
						case 6: /*Postal Address*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setPostalAddress(cellValue4);break;
						case 7: /*E-mail Address(s)*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setEmailAddress_s(cellValue4);break;
						case 8: /*Contact Number(s)*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setContactNumber_s(cellValue4);break;
						case 9: /*Affiliated Laboratory*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setAffiliatedLaboratory(cellValue4);break;
						
						case 10: /*Research Title*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setResearchTitle(cellValue4);break;
						case 11: /*Supervisor(s)*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setSupervisor_s(cellValue4);break;
						case 12: /*Examiner(s)*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4 = null;	} else {		cellValue4 = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4 = null;	} else {		cellValue4 = currentCell4.getStringCellValue();	}}std4.setExaminer_s(cellValue4);break;
						
						case 13: /*department*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4  = null;	} else {		cellValue4  = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		cellValue4  = currentCell4.getStringCellValue();	}}std4.setDepartment(cellValue4 );break;
						case 14: /*PhD/Mphil/MSc/Diploma*/	if (currentCell4 == null) { cellValue4 = null; } else if(currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4  = null;	} else {		cellValue4  = String.valueOf(currentCell4.getNumericCellValue());	}}else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		cellValue4  = currentCell4.getStringCellValue();	}}std4.setType(cellValue4 );break;
						case 15:/* programme */	if (currentCell4 == null) { cellValue4 = null; } else  if (currentCell4.getCellType() == CellType.NUMERIC) {	if (currentCell4.getNumericCellValue() == 0) {		cellValue4  = null;	} else {		cellValue4  = String.valueOf(currentCell4.getNumericCellValue());	}} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		cellValue4  = currentCell4.getStringCellValue();	}}std4.setProgramme(cellValue4 );break;
						case 16:/* courseFee */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setCourseFee(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setCourseFee(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						case 17:/* libraryFee */	if (currentCell4 == null) { cellValue4 = null; } else  if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setLibraryFee(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setLibraryFee(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						case 18:/* tuitionFee */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setTuitionFee(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setTuitionFee(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						case 19:/* registrationFee */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setRegistrationFee(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setRegistrationFee(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						case 20:/* firstYearPaymeent */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setFirstYearPaymeent(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setFirstYearPaymeent(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						case 21:/* secondYearPaymeent */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setSecondYearPaymeent(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setSecondYearPaymeent(Double.valueOf(currentCell4.getNumericCellValue()));	}}break;
						case 22:/* thirdYearPaymeent */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setThirdYearPaymeent(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setThirdYearPaymeent(Double.valueOf(currentCell4.getNumericCellValue()));	}}break;
						case 23:/* fourthYearPaymeent */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setFourthYearPaymeent(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setFourthYearPaymeent(Double.valueOf(currentCell4.getNumericCellValue()));	}}break;
						case 24:/* programmeFee */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setProgrammeFee(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setProgrammeFee(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						case 25:/* totalPaymentMade */	 if (currentCell4 == null) { cellValue4 = null; } else if (currentCell4.getCellType() == CellType.NUMERIC) {	std4.setTotalPaymentMade(currentCell4.getNumericCellValue());} else {	if (currentCell4.getStringCellValue().equals("")  || currentCell4.getCellType() == CellType.BLANK) {		cellValue4  = null;	} else {		std4.setTotalPaymentMade(Double.valueOf(currentCell4.getNumericCellValue()));}	}break;
						
						default:
							break;
						
						}
						
						
						//System.out.println("Cell"+cellIdx4+" "+cellValue4);
						cellIdx4++;
					}
					if (std4.getRegistrationNo() != null) {
						try {
							List<PGStudent> check_stds = pgStudentRepository.findByRegistrationNo(std4.getRegistrationNo());  
							if(check_stds.isEmpty()) {
								//System.out.println(std4.getRegistrationNo());
								stdSet.add(std4);
							}
						}catch(Exception e) {
							System.out.println("PGStudentService.addPGStudentsByExcel()");
						}
					}
					//System.out.println("done 4");
					
				}
			}
			workbook.close();
			//System.out.println(stdSet.toString());
			
			//save all
			pgStudentRepository.saveAll(stdSet);
			
		} catch (IOException e) {
			throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		}
	}

	// Edit student
	public void editPGStudent(PGStudent std) {
		pgStudentRepository.save(std);
	}

	// Delete student
	public void deletePGStudent(int stdID) {
		pgStudentRepository.deleteById(stdID);
	}

	// Search students
	
	public List<PGStudent> search_pgsByData(String registrationNo, String name, String department, String degreeLevel,
			String year) {
		
		if(registrationNo.isEmpty()) {
			registrationNo = null;
		}
		if(name.isEmpty()) {
			name = null;
		}
		if(department.isEmpty()) {
			department = null;
		}
		if(degreeLevel.isEmpty()) {
			degreeLevel = null;
		}
		if(year.isEmpty()) {
			year = "0";
		}
		
		return pgStudentRepository.findPGStudentsByData(registrationNo,name,department,degreeLevel,Integer.parseInt(year));
	}
	
	public List<PGStudent> searchPGStudents(PGStudent std) { //get all search data as an object 
		
		String registrationNo = std.getRegistrationNo();
		String Title = std.getTitle();
		String Namewithinitials = std.getNamewithinitials();
		String NamedenotedbyInitials = std.getNamedenotedbyInitials();
		String DateofRegistration = std.getDateofRegistration();
		String ID = std.getId();
		String ContactNumber_s = std.getContactNumber_s();
		String EmailAddress_s = std.getEmailAddress_s();
		String PostalAddress = std.getPostalAddress();
		String UndergraduateDegree = std.getUndergraduateDegree();
		String UndergraduateClass = std.getUndergraduateClass();
		String UndergraduateUniversity = std.getUndergraduateUniversity();
		String Experienceinfield = std.getExperienceinfield();
		String Designation = std.getDesignation();
		String Stream = std.getStream();
		String MScorMScEng = std.getmScOrMScEng();
		String PartTimeFullTime = std.getPartTimeOrFullTime();
		String HDCMeetingNumber = std.gethDCMeetingNumber();
		String AffiliatedLaboratory = std.getAffiliatedLaboratory();
		String ResearchTitle = std.getResearchTitle();
		String Supervisor_s = std.getSupervisor_s();
		String Examiner_s = std.getExaminer_s();
		String programme = std.getProgramme();
		double courseFee = std.getCourseFee();
		double libraryFee = std.getLibraryFee();
		double tuitionFee = std.getTuitionFee();
		double registrationFee = std.getRegistrationFee();
		double firstYearPaymeent = std.getFirstYearPaymeent();
		double secondYearPaymeent = std.getSecondYearPaymeent();
		double thirdYearPaymeent = std.getThirdYearPaymeent();
		double fourthYearPaymeent = std.getFourthYearPaymeent();
		double programmeFee = std.getProgrammeFee();
		double totalPaymentMade = std.getTotalPaymentMade();
		
		return pgStudentRepository.findAllStudents(
				registrationNo,
				Title,
				Namewithinitials,
				NamedenotedbyInitials,
				DateofRegistration,
				ID,
				ContactNumber_s,
				EmailAddress_s,
				PostalAddress,
				UndergraduateDegree,
				UndergraduateClass,
				UndergraduateUniversity,
				Experienceinfield,
				Designation,
				Stream,
				MScorMScEng,
				PartTimeFullTime,
				HDCMeetingNumber,
				AffiliatedLaboratory,
				ResearchTitle,
				Supervisor_s,
				Examiner_s,
				programme,
				courseFee,
				libraryFee,
				tuitionFee,
				registrationFee,
				firstYearPaymeent,
				secondYearPaymeent,
				thirdYearPaymeent,
				fourthYearPaymeent,
				programmeFee,
				totalPaymentMade);
	}

	// Download student
	
	public ByteArrayInputStream downloadSearchPGS(String registrationNo, String name, String department, String degreeLevel, String year) {
		
		
		if(registrationNo.isEmpty()) {
			registrationNo = null;
		}
		if(name.isEmpty()) {
			name = null;
		}
		if(department.isEmpty()) {
			department = null;
		}
		if(degreeLevel.isEmpty()) {
			degreeLevel = null;
		}
		if(year.isEmpty()) {
			year = "0";
		}
		
		List<PGStudent> stdSets = pgStudentRepository.findPGStudentsByData(registrationNo, name, department, degreeLevel, Integer.parseInt(year));
			
		//InputStream in = null;
			
		
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			
			
			//For PGDip
			Sheet sheet1 = workbook.createSheet("PG.Dip.");
			
			String[] HEADERsset1 = { "Registration No", "Title", "Name with initials", "Name denoted by Initials",
					"Date of Registration", "ID", "Contact Number(s)", "E-mail Address(s)", "Postal Address",
					"Undergraduate Degree", "Undergraduate Class", "Undergraduate University", "Expererience in field",
					"Designation", "Stream", "Examiners", "Department", "PhD/Mphil/MSc/Diploma", "Programme",
					"Course Fee", "Library Fee", "Tution Fee", "Registration fee", "Payment(first year)",
					"Payment(second year)", "Payment(third year)", "Payment(fourth year", "Prgramme fee",
					"Total Payment Made" };
			
			Row headerRowSet1 = sheet1.createRow(0);
			for (int col = 0; col < HEADERsset1.length; col++) {
				Cell cellSet1 = headerRowSet1.createCell(col);
				cellSet1.setCellValue(HEADERsset1[col]);
			}
			
			//For MSc
			Sheet sheet2 = workbook.createSheet("M.Sc. or M.Sc.Eng.");
			
			String[] HEADERsset2 = {"Registration No", "Title", "Name with initials", "Name denoted by Initials",
					"Date of Registration", "ID", "Contact Number(s)", "E-mail Address(s)", "Postal Address",
					"Undergraduate Degree", "Undergraduate Class", "Undergraduate University", "Expererience in field",
					"Designation","M.Sc. or M.Sc.Eng.", "Stream", "Examiners", "Department", "PhD/Mphil/MSc/Diploma", "Programme",
					"Course Fee", "Library Fee", "Tution Fee", "Registration fee", "Payment(first year)",
					"Payment(second year)", "Payment(third year)", "Payment(fourth year", "Prgramme fee",
					"Total Payment Made" };
			
			Row headerRowSet2 = sheet2.createRow(0);
			for (int col = 0; col < HEADERsset2.length; col++) {
				Cell cellSet2 = headerRowSet2.createCell(col);
				cellSet2.setCellValue(HEADERsset2[col]);
			}
			
			//For MPhil
			Sheet sheet3 = workbook.createSheet("M.Phil.");
			
			String[] HEADERsset3 = { "Registration No.", "Name with Initials", "Name denoted by Initials",
					"Date of Registration", "Part Time/Full Time", "HDC Meeting Number", "Postal Address",
					"E-mail Address(s)", "Contact Number(s)", "Affiliated Laboratory", "Research Title",
					"Supervisor(s)", "Examiner(s)", "Stream", "Department", "PhD/Mphil/MSc/Diploma", "Programme",
					"Course Fee", "Library Fee", "Tution Fee", "Registration fee", "Payment(first year)",
					"Payment(second year)", "Payment(third year)", "Payment(fourth year", "Prgramme fee",
					"Total Payment Made" };

			Row headerRowSet3 = sheet3.createRow(0);
			for (int col = 0; col < HEADERsset3.length; col++) {
				Cell cellSet3 = headerRowSet3.createCell(col);
				cellSet3.setCellValue(HEADERsset3[col]);
			}
			
			//For phD
			Sheet sheet4 = workbook.createSheet("ph.D.");
			String[] HEADERsset4 = {"Registration No.", "Name with Initials", "Name denoted by Initials",
					"Date of Registration", "Part Time/Full Time", "HDC Meeting Number", "Postal Address",
					"E-mail Address(s)", "Contact Number(s)", "Affiliated Laboratory", "Research Title",
					"Supervisor(s)", "Examiner(s)", "Department", "PhD/Mphil/MSc/Diploma", "Programme",
					"Course Fee", "Library Fee", "Tution Fee", "Registration fee", "Payment(first year)",
					"Payment(second year)", "Payment(third year)", "Payment(fourth year", "Prgramme fee",
					"Total Payment Made" };
			
			Row headerRowSet4 = sheet4.createRow(0);
			for (int col = 0; col < HEADERsset4.length; col++) {
				Cell cellSet4 = headerRowSet4.createCell(col);
				cellSet4.setCellValue(HEADERsset4[col]);
			}
			
			int rowIdx1 = 1;
			int rowIdx2 = 1;
			int rowIdx3 = 1;
			int rowIdx4 = 1;
			
			for (PGStudent std : stdSets) {
				
				//Types:  PGDip MSc MPhil phD
				if(std.getType().equalsIgnoreCase("Diploma")) { Row row = sheet1.createRow(rowIdx1++); 
				// new row 
				// Registration No
				if (std.getRegistrationNo() != null) {
					
					row.createCell(0).setCellValue(std.getRegistrationNo());
				} else {
					row.createCell(0).setCellValue("");
				}
				// Title 
				if (std.getTitle() != null) { 	row.createCell(1).setCellValue(std.getTitle()); } else { 	row.createCell(1).setCellValue(""); } 
				// Name with initials 
				if (std.getNamewithinitials() != null) { 	row.createCell(2).setCellValue(std.getNamewithinitials()); } else { 	row.createCell(2).setCellValue(""); } 
				// Name denoted by Initials 
				if (std.getNamedenotedbyInitials() != null) { 	row.createCell(3).setCellValue(std.getNamedenotedbyInitials()); } else { 	row.createCell(3).setCellValue(""); } 
				// Date of Registration 
				if (std.getDateofRegistration() != null) { 	row.createCell(4).setCellValue(std.getDateofRegistration()); } else { 	row.createCell(4).setCellValue(""); } 
				// ID 
				if (std.getId() != null) { 	row.createCell(5).setCellValue(std.getId()); } else { 	row.createCell(5).setCellValue(""); } 
				// Contact Number(s) 
				if (std.getContactNumber_s() != null) { 	row.createCell(6).setCellValue(std.getContactNumber_s()); } else { 	row.createCell(6).setCellValue(""); } 
				// E-mail Address(s) 
				if (std.getEmailAddress_s() != null) { 	row.createCell(7).setCellValue(std.getEmailAddress_s()); } else { 	row.createCell(7).setCellValue(""); } 
				// Postal Address 
				if (std.getPostalAddress() != null) { 	row.createCell(8).setCellValue(std.getPostalAddress()); } else { 	row.createCell(8).setCellValue(""); } 
				// Undergraduate Degree 
				if (std.getUndergraduateDegree() != null) { 	row.createCell(9).setCellValue(std.getUndergraduateDegree()); } else { 	row.createCell(9).setCellValue(""); } 
				// Undergraduate Class 
				if (std.getUndergraduateClass() != null) { 	row.createCell(10).setCellValue(std.getUndergraduateClass()); } else { 	row.createCell(10).setCellValue(""); } 
				// Undergraduate University 
				if (std.getUndergraduateUniversity() != null) { 	row.createCell(11).setCellValue(std.getUndergraduateUniversity()); } else { 	row.createCell(11).setCellValue(""); } 
				// Expererience in field 
				if (std.getExperienceinfield() != null) { 	row.createCell(12).setCellValue(std.getExperienceinfield()); } else { 	row.createCell(12).setCellValue(""); } 
				// Designation 
				if (std.getDesignation() != null) { 	row.createCell(13).setCellValue(std.getDesignation()); } else { 	row.createCell(13).setCellValue(""); } 
				// Stream 
				if (std.getStream() != null) { 	row.createCell(14).setCellValue(std.getStream()); } else { 	row.createCell(14).setCellValue(""); } 
				// Examiner(s) 
				if (std.getExaminer_s() != null) { 	row.createCell(15).setCellValue(std.getExaminer_s()); } else { 	row.createCell(15).setCellValue(""); } 
				//Department	
				if (std.getDepartment() != null) { 	row.createCell(16).setCellValue(std.getDepartment()); } else { 	row.createCell(16).setCellValue(""); } 
				//PhD/Mphil/MSc/Diploma	
				if (std.getType() != null) { 	row.createCell(17).setCellValue(std.getType()); } else { 	row.createCell(17).setCellValue(""); } 
				//Programme	
				if (std.getProgramme() != null) { 	row.createCell(18).setCellValue(std.getProgramme()); } else { 	row.createCell(18).setCellValue(""); } 
				//Course Fee
				if (std.getCourseFee() != 0) { 	row.createCell(19).setCellValue(std.getCourseFee()); } else { 	row.createCell(19).setCellValue(0); } 
				//Library Fee
				if (std.getLibraryFee() != 0) { 	row.createCell(20).setCellValue(std.getLibraryFee()); } else { 	row.createCell(20).setCellValue(0); } 
				//Tution Fee
				if (std.getTuitionFee() != 0) { 	row.createCell(21).setCellValue(std.getTuitionFee()); } else { 	row.createCell(21).setCellValue(0); } 
				//Registration fee	
				if (std.getRegistrationFee() != 0) { 	row.createCell(22).setCellValue(std.getRegistrationFee()); } else { 	row.createCell(22).setCellValue(0); } 
				//"Payment(first year)"	
				if (std.getFirstYearPaymeent() != 0) { 	row.createCell(23).setCellValue(std.getFirstYearPaymeent()); } else { 	row.createCell(23).setCellValue(0); } 
				//"Payment(second year)"	
				if (std.getSecondYearPaymeent() != 0) { 	row.createCell(24).setCellValue(std.getSecondYearPaymeent()); } else { 	row.createCell(24).setCellValue(0); } 
				//"Payment(third year)"	
				if (std.getThirdYearPaymeent() != 0) { 	row.createCell(25).setCellValue(std.getThirdYearPaymeent()); } else { 	row.createCell(25).setCellValue(0); } 
				//"Payment(fourth year"	
				if (std.getFourthYearPaymeent() != 0) { 	row.createCell(26).setCellValue(std.getFourthYearPaymeent()); } else { 	row.createCell(26).setCellValue(0); } 
				//Prgramme fee
				if (std.getProgrammeFee() != 0) { 	row.createCell(27).setCellValue(std.getProgrammeFee()); } else { 	row.createCell(27).setCellValue(0); } 
				//Total Payment Made
				if (std.getTotalPaymentMade() != 0) { 	row.createCell(28).setCellValue(std.getTotalPaymentMade()); } else { 	row.createCell(28).setCellValue(0); } 
				

				}else 
				if(std.getType().equalsIgnoreCase("MSc")) { Row row = sheet2.createRow(rowIdx2++); 
				// new row  
				// Registration No 
				if (std.getRegistrationNo() != null) { 	row.createCell(0).setCellValue(std.getRegistrationNo()); } else { 	row.createCell(0).setCellValue(""); } 
				// Title 
				if (std.getTitle() != null) { 	row.createCell(1).setCellValue(std.getTitle()); } else { 	row.createCell(1).setCellValue(""); } 
				// Name with initials 
				if (std.getNamewithinitials() != null) { 	row.createCell(2).setCellValue(std.getNamewithinitials()); } else { 	row.createCell(2).setCellValue(""); } 
				// Name denoted by Initials 
				if (std.getNamedenotedbyInitials() != null) { 	row.createCell(3).setCellValue(std.getNamedenotedbyInitials()); } else { 	row.createCell(3).setCellValue(""); } 
				// Date of Registration 
				if (std.getDateofRegistration() != null) { 	row.createCell(4).setCellValue(std.getDateofRegistration()); } else { 	row.createCell(4).setCellValue(""); } 
				// ID 
				if (std.getId() != null) { 	row.createCell(5).setCellValue(std.getId()); } else { 	row.createCell(5).setCellValue(""); } 
				// Contact Number(s) 
				if (std.getContactNumber_s() != null) { 	row.createCell(6).setCellValue(std.getContactNumber_s()); } else { 	row.createCell(6).setCellValue(""); } 
				// E-mail Address(s) 
				if (std.getEmailAddress_s() != null) { 	row.createCell(7).setCellValue(std.getEmailAddress_s()); } else { 	row.createCell(7).setCellValue(""); } 
				// Postal Address 
				if (std.getPostalAddress() != null) { 	row.createCell(8).setCellValue(std.getPostalAddress()); } else { 	row.createCell(8).setCellValue(""); } 
				// Undergraduate Degree 
				if (std.getUndergraduateDegree() != null) { 	row.createCell(9).setCellValue(std.getUndergraduateDegree()); } else { 	row.createCell(9).setCellValue(""); } 
				// Undergraduate Class 
				if (std.getUndergraduateClass() != null) { 	row.createCell(10).setCellValue(std.getUndergraduateClass()); } else { 	row.createCell(10).setCellValue(""); } 
				// Undergraduate University 
				if (std.getUndergraduateUniversity() != null) { 	row.createCell(11).setCellValue(std.getUndergraduateUniversity()); } else { 	row.createCell(11).setCellValue(""); } 
				// Expererience in field 
				if (std.getExperienceinfield() != null) { 	row.createCell(12).setCellValue(std.getExperienceinfield()); } else { 	row.createCell(12).setCellValue(""); } 
				// Designation 
				if (std.getDesignation() != null) { 	row.createCell(13).setCellValue(std.getDesignation()); } else { 	row.createCell(13).setCellValue(""); } 
				// M.Sc. or M.Sc.Eng. 
				if (std.getmScOrMScEng() != null) { 	row.createCell(14).setCellValue(std.getmScOrMScEng()); } else { 	row.createCell(14).setCellValue(""); } 
				// Stream 
				if (std.getStream() != null) { 	row.createCell(15).setCellValue(std.getStream()); } else { 	row.createCell(15).setCellValue(""); } 
				// Examiner(s) 
				if (std.getExaminer_s() != null) { 	row.createCell(16).setCellValue(std.getExaminer_s()); } else { 	row.createCell(16).setCellValue(""); } 
				//Department	
				if (std.getDepartment() != null) { 	row.createCell(17).setCellValue(std.getDepartment()); } else { 	row.createCell(17).setCellValue(""); } 
				//PhD/Mphil/MSc/Diploma	
				if (std.getType() != null) { 	row.createCell(18).setCellValue(std.getType()); } else { 	row.createCell(18).setCellValue(""); } 
				//Programme	
				if (std.getProgramme() != null) { 	row.createCell(19).setCellValue(std.getProgramme()); } else { 	row.createCell(19).setCellValue(""); } 
				//Course Fee
				if (std.getCourseFee() != 0) { 	row.createCell(20).setCellValue(std.getCourseFee()); } else { 	row.createCell(20).setCellValue(0); } 
				//Library Fee
				if (std.getLibraryFee() != 0) { 	row.createCell(21).setCellValue(std.getLibraryFee()); } else { 	row.createCell(21).setCellValue(0); } 
				//Tution Fee
				if (std.getTuitionFee() != 0) { 	row.createCell(22).setCellValue(std.getTuitionFee()); } else { 	row.createCell(22).setCellValue(0); } 
				//Registration fee	
				if (std.getRegistrationFee() != 0) { 	row.createCell(23).setCellValue(std.getRegistrationFee()); } else { 	row.createCell(23).setCellValue(0); } 
				//"Payment(first year)"	
				if (std.getFirstYearPaymeent() != 0) { 	row.createCell(24).setCellValue(std.getFirstYearPaymeent()); } else { 	row.createCell(24).setCellValue(0); } 
				//"Payment(second year)"	
				if (std.getSecondYearPaymeent() != 0) { 	row.createCell(25).setCellValue(std.getSecondYearPaymeent()); } else { 	row.createCell(25).setCellValue(0); } 
				//"Payment(third year)"	
				if (std.getThirdYearPaymeent() != 0) { 	row.createCell(26).setCellValue(std.getThirdYearPaymeent()); } else { 	row.createCell(26).setCellValue(0); } 
				//"Payment(fourth year"	
				if (std.getFourthYearPaymeent() != 0) { 	row.createCell(27).setCellValue(std.getFourthYearPaymeent()); } else { 	row.createCell(27).setCellValue(0); } 
				//Prgramme fee
				if (std.getProgrammeFee() != 0) { 	row.createCell(28).setCellValue(std.getProgrammeFee()); } else { 	row.createCell(28).setCellValue(0); } 
				//Total Payment Made
				if (std.getTotalPaymentMade() != 0) { 	row.createCell(29).setCellValue(std.getTotalPaymentMade()); } else { 	row.createCell(29).setCellValue(0); } 
											
				}else 
				if(std.getType().equalsIgnoreCase("MPhil")) { Row row = sheet3.createRow(rowIdx3++); 
				// new row  
				// Registration No 
				if (std.getRegistrationNo() != null) { 	row.createCell(0).setCellValue(std.getRegistrationNo()); } else { 	row.createCell(0).setCellValue(""); } 
				// Name with initials 
				if (std.getNamewithinitials() != null) { 	row.createCell(1).setCellValue(std.getNamewithinitials()); } else { 	row.createCell(1).setCellValue(""); } 
				// Name denoted by Initials 
				if (std.getNamedenotedbyInitials() != null) { 	row.createCell(2).setCellValue(std.getNamedenotedbyInitials()); } else { 	row.createCell(2).setCellValue(""); } 
				// Date of Registration 
				if (std.getDateofRegistration() != null) { 	row.createCell(3).setCellValue(std.getDateofRegistration()); } else { 	row.createCell(3).setCellValue(""); } 
				// Part Time/Full Time 
				if (std.getPartTimeOrFullTime() != null) { 	row.createCell(4).setCellValue(std.getPartTimeOrFullTime()); } else { 	row.createCell(4).setCellValue(""); } 
				// HDC Meeting Number 
				if (std.gethDCMeetingNumber() != null) { 	row.createCell(5).setCellValue(std.gethDCMeetingNumber()); } else { 	row.createCell(5).setCellValue(""); }
				// Postal Address 
				if (std.getPostalAddress() != null) { 	row.createCell(6).setCellValue(std.getPostalAddress()); } else { 	row.createCell(6).setCellValue(""); } 
				// E-mail Address(s) 
				if (std.getEmailAddress_s() != null) { 	row.createCell(7).setCellValue(std.getEmailAddress_s()); } else { 	row.createCell(7).setCellValue(""); } 
				// Contact Number(s) 
				if (std.getContactNumber_s() != null) { 	row.createCell(8).setCellValue(std.getContactNumber_s()); } else { 	row.createCell(8).setCellValue(""); } 
				// Affiliated Laboratory 
				if (std.getAffiliatedLaboratory() != null) { 	row.createCell(9).setCellValue(std.getAffiliatedLaboratory()); } else { 	row.createCell(9).setCellValue(""); } 
				// Research Title 
				if (std.getResearchTitle() != null) { 	row.createCell(10).setCellValue(std.getResearchTitle()); } else { 	row.createCell(10).setCellValue(""); } 
				// Supervisor(s) 
				if (std.getSupervisor_s() != null) { 	row.createCell(11).setCellValue(std.getSupervisor_s()); } else { 	row.createCell(11).setCellValue(""); } 
				// Examiner(s) 
				if (std.getExaminer_s() != null) { 	row.createCell(12).setCellValue(std.getExaminer_s()); } else { 	row.createCell(12).setCellValue(""); } 
				// Stream 
				if (std.getStream() != null) { 	row.createCell(13).setCellValue(std.getStream()); } else { 	row.createCell(13).setCellValue(""); } 
				//Department	
				if (std.getDepartment() != null) { 	row.createCell(14).setCellValue(std.getDepartment()); } else { 	row.createCell(14).setCellValue(""); } 
				//PhD/Mphil/MSc/Diploma	
				if (std.getType() != null) { 	row.createCell(15).setCellValue(std.getType()); } else { 	row.createCell(15).setCellValue(""); } 
				//Programme	
				if (std.getProgramme() != null) { 	row.createCell(16).setCellValue(std.getProgramme()); } else { 	row.createCell(16).setCellValue(""); } 
				//Course Fee
				if (std.getCourseFee() != 0) { 	row.createCell(17).setCellValue(std.getCourseFee()); } else { 	row.createCell(17).setCellValue(0); } 
				//Library Fee
				if (std.getLibraryFee() != 0) { 	row.createCell(18).setCellValue(std.getLibraryFee()); } else { 	row.createCell(18).setCellValue(0); } 
				//Tution Fee
				if (std.getTuitionFee() != 0) { 	row.createCell(19).setCellValue(std.getTuitionFee()); } else { 	row.createCell(19).setCellValue(0); } 
				//Registration fee	
				if (std.getRegistrationFee() != 0) { 	row.createCell(20).setCellValue(std.getRegistrationFee()); } else { 	row.createCell(20).setCellValue(0); } 
				//"Payment(first year)"	
				if (std.getFirstYearPaymeent() != 0) { 	row.createCell(21).setCellValue(std.getFirstYearPaymeent()); } else { 	row.createCell(21).setCellValue(0); } 
				//"Payment(second year)"	
				if (std.getSecondYearPaymeent() != 0) { 	row.createCell(22).setCellValue(std.getSecondYearPaymeent()); } else { 	row.createCell(22).setCellValue(0); } 
				//"Payment(third year)"	
				if (std.getThirdYearPaymeent() != 0) { 	row.createCell(23).setCellValue(std.getThirdYearPaymeent()); } else { 	row.createCell(23).setCellValue(0); } 
				//"Payment(fourth year"	
				if (std.getFourthYearPaymeent() != 0) { 	row.createCell(24).setCellValue(std.getFourthYearPaymeent()); } else { 	row.createCell(24).setCellValue(0); } 
				//Prgramme fee
				if (std.getProgrammeFee() != 0) { 	row.createCell(25).setCellValue(std.getProgrammeFee()); } else { 	row.createCell(25).setCellValue(0); } 
				//Total Payment Made
				if (std.getTotalPaymentMade() != 0) { 	row.createCell(26).setCellValue(std.getTotalPaymentMade()); } else { 	row.createCell(26).setCellValue(0); } 

				}else 
				if(std.getType().equalsIgnoreCase("PhD")) {  Row row = sheet4.createRow(rowIdx4++); 
				// new row  
				// Registration No 
				if (std.getRegistrationNo() != null) { 	row.createCell(0).setCellValue(std.getRegistrationNo()); } else { 	row.createCell(0).setCellValue(""); } 
				// Name with initials 
				if (std.getNamewithinitials() != null) { 	row.createCell(1).setCellValue(std.getNamewithinitials()); } else { 	row.createCell(1).setCellValue(""); } 
				// Name denoted by Initials 
				if (std.getNamedenotedbyInitials() != null) { 	row.createCell(2).setCellValue(std.getNamedenotedbyInitials()); } else { 	row.createCell(2).setCellValue(""); } 
				// Date of Registration 
				if (std.getDateofRegistration() != null) { 	row.createCell(3).setCellValue(std.getDateofRegistration()); } else { 	row.createCell(3).setCellValue(""); } 
				// Part Time/Full Time 
				if (std.getPartTimeOrFullTime() != null) { 	row.createCell(4).setCellValue(std.getPartTimeOrFullTime()); } else { 	row.createCell(4).setCellValue(""); } 
				// HDC Meeting Number 
				if (std.gethDCMeetingNumber() != null) { 	row.createCell(5).setCellValue(std.gethDCMeetingNumber()); } else { 	row.createCell(5).setCellValue(""); }
				// Postal Address 
				if (std.getPostalAddress() != null) { 	row.createCell(6).setCellValue(std.getPostalAddress()); } else { 	row.createCell(6).setCellValue(""); } 
				// E-mail Address(s) 
				if (std.getEmailAddress_s() != null) { 	row.createCell(7).setCellValue(std.getEmailAddress_s()); } else { 	row.createCell(7).setCellValue(""); } 
				// Contact Number(s) 
				if (std.getContactNumber_s() != null) { 	row.createCell(8).setCellValue(std.getContactNumber_s()); } else { 	row.createCell(8).setCellValue(""); } 
				// Affiliated Laboratory 
				if (std.getAffiliatedLaboratory() != null) { 	row.createCell(9).setCellValue(std.getAffiliatedLaboratory()); } else { 	row.createCell(9).setCellValue(""); } 
				// Research Title 
				if (std.getResearchTitle() != null) { 	row.createCell(10).setCellValue(std.getResearchTitle()); } else { 	row.createCell(10).setCellValue(""); } 
				// Supervisor(s) 
				if (std.getSupervisor_s() != null) { 	row.createCell(11).setCellValue(std.getSupervisor_s()); } else { 	row.createCell(11).setCellValue(""); } 
				// Examiner(s) 
				if (std.getExaminer_s() != null) { 	row.createCell(12).setCellValue(std.getExaminer_s()); } else { 	row.createCell(12).setCellValue(""); } 
				//Department	
				if (std.getDepartment() != null) { 	row.createCell(13).setCellValue(std.getDepartment()); } else { 	row.createCell(13).setCellValue(""); } 
				//PhD/Mphil/MSc/Diploma	
				if (std.getType() != null) { 	row.createCell(14).setCellValue(std.getType()); } else { 	row.createCell(14).setCellValue(""); } 
				//Programme	
				if (std.getProgramme() != null) { 	row.createCell(15).setCellValue(std.getProgramme()); } else { 	row.createCell(15).setCellValue(""); } 
				//Course Fee
				if (std.getCourseFee() != 0) { 	row.createCell(16).setCellValue(std.getCourseFee()); } else { 	row.createCell(16).setCellValue(0); } 
				//Library Fee
				if (std.getLibraryFee() != 0) { 	row.createCell(17).setCellValue(std.getLibraryFee()); } else { 	row.createCell(17).setCellValue(0); } 
				//Tution Fee
				if (std.getTuitionFee() != 0) { 	row.createCell(18).setCellValue(std.getTuitionFee()); } else { 	row.createCell(18).setCellValue(0); } 
				//Registration fee	
				if (std.getRegistrationFee() != 0) { 	row.createCell(19).setCellValue(std.getRegistrationFee()); } else { 	row.createCell(19).setCellValue(0); } 
				//"Payment(first year)"	
				if (std.getFirstYearPaymeent() != 0) { 	row.createCell(20).setCellValue(std.getFirstYearPaymeent()); } else { 	row.createCell(20).setCellValue(0); } 
				//"Payment(second year)"	
				if (std.getSecondYearPaymeent() != 0) { 	row.createCell(21).setCellValue(std.getSecondYearPaymeent()); } else { 	row.createCell(21).setCellValue(0); } 
				//"Payment(third year)"	
				if (std.getThirdYearPaymeent() != 0) { 	row.createCell(22).setCellValue(std.getThirdYearPaymeent()); } else { 	row.createCell(22).setCellValue(0); } 
				//"Payment(fourth year"	
				if (std.getFourthYearPaymeent() != 0) { 	row.createCell(23).setCellValue(std.getFourthYearPaymeent()); } else { 	row.createCell(23).setCellValue(0); } 
				//Prgramme fee
				if (std.getProgrammeFee() != 0) { 	row.createCell(24).setCellValue(std.getProgrammeFee()); } else { 	row.createCell(24).setCellValue(0); } 
				//Total Payment Made
				if (std.getTotalPaymentMade() != 0) { 	row.createCell(25).setCellValue(std.getTotalPaymentMade()); } else { 	row.createCell(25).setCellValue(0); } 
				}
				
			}
			/*
			 * //Remove sheet if there are no data if(rowIdx1 == 1) {
			 * workbook.removeSheetAt(0); }else if(rowIdx2 == 1) {
			 * workbook.removeSheetAt(1); }else if(rowIdx3 == 1) {
			 * workbook.removeSheetAt(2); }else if(rowIdx4 == 1) {
			 * workbook.removeSheetAt(3); }
			 */

			workbook.write(out);
			ByteArrayInputStream outIn = new ByteArrayInputStream(out.toByteArray());
			return outIn;
			
		} catch (IOException e) {
			throw new RuntimeException("fail to export data to Excel file: " + e.getMessage());
		}
		
	}
	
	/*
	 * public ByteArrayInputStream downloadPGStudentsAsExcel(List<PGStudent>
	 * stdSets) { try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream
	 * out = new ByteArrayOutputStream();) { //For PGDip Sheet sheet1 =
	 * workbook.createSheet("PG.Dip."); String[] HEADERsset1 =
	 * {"Registration No","Title","Name with initials","Name denoted by Initials"
	 * ,"Date of Registration"
	 * ,"ID","Contact Number(s)","E-mail Address(s)","Postal Address"
	 * ,"Undergraduate Degree","Undergraduate Class"
	 * ,"Undergraduate University","Expererience in field","Designation","Stream",
	 * "Payment"};
	 * 
	 * Row headerRowSet1 = sheet1.createRow(0); for (int col = 0; col <
	 * HEADERsset1.length; col++) { Cell cellSet1 = headerRowSet1.createCell(col);
	 * cellSet1.setCellValue(HEADERsset1[col]); }
	 * 
	 * //For MSc Sheet sheet2 = workbook.createSheet("M.Sc. or M.Sc.Eng."); String[]
	 * HEADERsset2 =
	 * {"Registration No","Title","Name with initials","Name denoted by Initials"
	 * ,"Date of Registration","ID","Contact Number(s)","E-mail Address(s)"
	 * ,"Postal Address"
	 * ,"Undergraduate Degree","Undergraduate Class","Undergraduate University"
	 * ,"Expererience in field"
	 * ,"Designation","M.Sc. or M.Sc.Eng.","Stream","Payment"};
	 * 
	 * Row headerRowSet2 = sheet2.createRow(0); for (int col = 0; col <
	 * HEADERsset2.length; col++) { Cell cellSet2 = headerRowSet2.createCell(col);
	 * cellSet2.setCellValue(HEADERsset2[col]); }
	 * 
	 * //For MPhil Sheet sheet3 = workbook.createSheet("M.Phil."); String[]
	 * HEADERsset3 =
	 * {"Registration No.","Title","Name with Initials","Name denoted by Initials"
	 * ,"Date of Registration","Part Time/Full Time","HDC Meeting Number"
	 * ,"Postal Address"
	 * ,"E-mail Address(s)","Contact Number(s)","Affiliated Laboratory"
	 * ,"Research Title" ,"Supervisor(s)","Examiner(s)","Stream","Payment"};
	 * 
	 * Row headerRowSet3 = sheet3.createRow(0); for (int col = 0; col <
	 * HEADERsset3.length; col++) { Cell cellSet3 = headerRowSet3.createCell(col);
	 * cellSet3.setCellValue(HEADERsset3[col]); }
	 * 
	 * //For phD Sheet sheet4 = workbook.createSheet("ph.D."); String[] HEADERsset4
	 * = {"Registration No.","Title","Name with Initials","Name denoted by Initials"
	 * ,"Date of Registration","Part Time/Full Time","HDC Meeting Number"
	 * ,"Postal Address"
	 * ,"E-mail Address(s)","Contact Number(s)","Affiliated Laboratory"
	 * ,"Research Title" ,"Supervisor(s)","Examiner(s)","Stream","Payment"};
	 * 
	 * Row headerRowSet4 = sheet4.createRow(0); for (int col = 0; col <
	 * HEADERsset4.length; col++) { Cell cellSet4 = headerRowSet4.createCell(col);
	 * cellSet4.setCellValue(HEADERsset4[col]); } int rowIdx1 = 1; int rowIdx2 = 1;
	 * int rowIdx3 = 1; int rowIdx4 = 1;
	 * 
	 * for (PGStudent std : stdSets) {
	 * 
	 * //Types: PGDip MSc MPhil phD if(std.getType() == "PGDip") { Row row =
	 * sheet1.createRow(rowIdx1++); // new row
	 * 
	 * // Registration No if (std.getRegistrationNo() != null) {
	 * row.createCell(0).setCellValue(std.getRegistrationNo()); } else {
	 * row.createCell(0).setCellValue(""); } // Title if (std.getTitle() != null) {
	 * row.createCell(1).setCellValue(std.getTitle()); } else {
	 * row.createCell(1).setCellValue(""); } // Name with initials if
	 * (std.getNamewithinitials() != null) {
	 * row.createCell(2).setCellValue(std.getNamewithinitials()); } else {
	 * row.createCell(2).setCellValue(""); } // Name denoted by Initials if
	 * (std.getNamedenotedbyInitials() != null) {
	 * row.createCell(3).setCellValue(std.getNamedenotedbyInitials()); } else {
	 * row.createCell(3).setCellValue(""); } // Date of Registration if
	 * (std.getDateofRegistration() != null) {
	 * row.createCell(4).setCellValue(std.getDateofRegistration()); } else {
	 * row.createCell(4).setCellValue(""); } // ID if (std.getId() != null) {
	 * row.createCell(5).setCellValue(std.getId()); } else {
	 * row.createCell(5).setCellValue(""); } // Contact Number(s) if
	 * (std.getContactNumber_s() != null) {
	 * row.createCell(6).setCellValue(std.getContactNumber_s()); } else {
	 * row.createCell(6).setCellValue(""); } // E-mail Address(s) if
	 * (std.getEmailAddress_s() != null) {
	 * row.createCell(7).setCellValue(std.getEmailAddress_s()); } else {
	 * row.createCell(7).setCellValue(""); } // Postal Address if
	 * (std.getPostalAddress() != null) {
	 * row.createCell(8).setCellValue(std.getPostalAddress()); } else {
	 * row.createCell(8).setCellValue(""); } // Undergraduate Degree if
	 * (std.getUndergraduateDegree() != null) {
	 * row.createCell(9).setCellValue(std.getUndergraduateDegree()); } else {
	 * row.createCell(9).setCellValue(""); } // Undergraduate Class if
	 * (std.getUndergraduateClass() != null) {
	 * row.createCell(10).setCellValue(std.getUndergraduateClass()); } else {
	 * row.createCell(10).setCellValue(""); } // Undergraduate University if
	 * (std.getUndergraduateUniversity() != null) {
	 * row.createCell(11).setCellValue(std.getUndergraduateUniversity()); } else {
	 * row.createCell(11).setCellValue(""); } // Expererience in field if
	 * (std.getExpererienceinfield() != null) {
	 * row.createCell(12).setCellValue(std.getExpererienceinfield()); } else {
	 * row.createCell(12).setCellValue(""); } // Designation if
	 * (std.getDesignation() != null) {
	 * row.createCell(13).setCellValue(std.getDesignation()); } else {
	 * row.createCell(13).setCellValue(""); } // Stream if (std.getStream() != null)
	 * { row.createCell(14).setCellValue(std.getStream()); } else {
	 * row.createCell(14).setCellValue(""); } // Payment if
	 * (std.getTotalPaymentMade() != 0) {
	 * row.createCell(15).setCellValue(std.getTotalPaymentMade()); } else {
	 * row.createCell(15).setCellValue(""); } }else if(std.getType() == "MSc") { Row
	 * row = sheet2.createRow(rowIdx2++); // new row
	 * 
	 * // Registration No if (std.getRegistrationNo() != null) {
	 * row.createCell(0).setCellValue(std.getRegistrationNo()); } else {
	 * row.createCell(0).setCellValue(""); } // Title if (std.getTitle() != null) {
	 * row.createCell(1).setCellValue(std.getTitle()); } else {
	 * row.createCell(1).setCellValue(""); } // Name with initials if
	 * (std.getNamewithinitials() != null) {
	 * row.createCell(2).setCellValue(std.getNamewithinitials()); } else {
	 * row.createCell(2).setCellValue(""); } // Name denoted by Initials if
	 * (std.getNamedenotedbyInitials() != null) {
	 * row.createCell(3).setCellValue(std.getNamedenotedbyInitials()); } else {
	 * row.createCell(3).setCellValue(""); } // Date of Registration if
	 * (std.getDateofRegistration() != null) {
	 * row.createCell(4).setCellValue(std.getDateofRegistration()); } else {
	 * row.createCell(4).setCellValue(""); } // ID if (std.getId() != null) {
	 * row.createCell(5).setCellValue(std.getId()); } else {
	 * row.createCell(5).setCellValue(""); } // Contact Number(s) if
	 * (std.getContactNumber_s() != null) {
	 * row.createCell(6).setCellValue(std.getContactNumber_s()); } else {
	 * row.createCell(6).setCellValue(""); } // E-mail Address(s) if
	 * (std.getEmailAddress_s() != null) {
	 * row.createCell(7).setCellValue(std.getEmailAddress_s()); } else {
	 * row.createCell(7).setCellValue(""); } // Postal Address if
	 * (std.getPostalAddress() != null) {
	 * row.createCell(8).setCellValue(std.getPostalAddress()); } else {
	 * row.createCell(8).setCellValue(""); } // Undergraduate Degree if
	 * (std.getUndergraduateDegree() != null) {
	 * row.createCell(9).setCellValue(std.getUndergraduateDegree()); } else {
	 * row.createCell(9).setCellValue(""); } // Undergraduate Class if
	 * (std.getUndergraduateClass() != null) {
	 * row.createCell(10).setCellValue(std.getUndergraduateClass()); } else {
	 * row.createCell(10).setCellValue(""); } // Undergraduate University if
	 * (std.getUndergraduateUniversity() != null) {
	 * row.createCell(11).setCellValue(std.getUndergraduateUniversity()); } else {
	 * row.createCell(11).setCellValue(""); } // Expererience in field if
	 * (std.getExpererienceinfield() != null) {
	 * row.createCell(12).setCellValue(std.getExpererienceinfield()); } else {
	 * row.createCell(12).setCellValue(""); } // Designation if
	 * (std.getDesignation() != null) {
	 * row.createCell(13).setCellValue(std.getDesignation()); } else {
	 * row.createCell(13).setCellValue(""); } // M.Sc. or M.Sc.Eng. if
	 * (std.getmScOrMScEng() != null) {
	 * row.createCell(14).setCellValue(std.getmScOrMScEng()); } else {
	 * row.createCell(14).setCellValue(""); } // Stream if (std.getStream() != null)
	 * { row.createCell(15).setCellValue(std.getStream()); } else {
	 * row.createCell(15).setCellValue(""); } // Payment if
	 * (std.getTotalPaymentMade() != 0) {
	 * row.createCell(16).setCellValue(std.getTotalPaymentMade()); } else {
	 * row.createCell(16).setCellValue(""); }
	 * 
	 * }else if(std.getType() == "MPhil") { Row row = sheet3.createRow(rowIdx3++);
	 * // new row
	 * 
	 * // Registration No if (std.getRegistrationNo() != null) {
	 * row.createCell(0).setCellValue(std.getRegistrationNo()); } else {
	 * row.createCell(0).setCellValue(""); } // Title if (std.getTitle() != null) {
	 * row.createCell(1).setCellValue(std.getTitle()); } else {
	 * row.createCell(1).setCellValue(""); } // Name with initials if
	 * (std.getNamewithinitials() != null) {
	 * row.createCell(2).setCellValue(std.getNamewithinitials()); } else {
	 * row.createCell(2).setCellValue(""); } // Name denoted by Initials if
	 * (std.getNamedenotedbyInitials() != null) {
	 * row.createCell(3).setCellValue(std.getNamedenotedbyInitials()); } else {
	 * row.createCell(3).setCellValue(""); } // Date of Registration if
	 * (std.getDateofRegistration() != null) {
	 * row.createCell(4).setCellValue(std.getDateofRegistration()); } else {
	 * row.createCell(4).setCellValue(""); } // Part Time/Full Time if
	 * (std.getPartTimeOrFullTime() != null) {
	 * row.createCell(5).setCellValue(std.getPartTimeOrFullTime()); } else {
	 * row.createCell(5).setCellValue(""); } // HDC Meeting Number if
	 * (std.gethDCMeetingNumber() != null) {
	 * row.createCell(6).setCellValue(std.gethDCMeetingNumber()); } else {
	 * row.createCell(6).setCellValue(""); }
	 * 
	 * // Postal Address if (std.getPostalAddress() != null) {
	 * row.createCell(7).setCellValue(std.getPostalAddress()); } else {
	 * row.createCell(7).setCellValue(""); } // E-mail Address(s) if
	 * (std.getEmailAddress_s() != null) {
	 * row.createCell(8).setCellValue(std.getEmailAddress_s()); } else {
	 * row.createCell(8).setCellValue(""); } // Contact Number(s) if
	 * (std.getContactNumber_s() != null) {
	 * row.createCell(9).setCellValue(std.getContactNumber_s()); } else {
	 * row.createCell(9).setCellValue(""); } // Affiliated Laboratory if
	 * (std.getAffiliatedLaboratory() != null) {
	 * row.createCell(10).setCellValue(std.getAffiliatedLaboratory()); } else {
	 * row.createCell(10).setCellValue(""); } // Research Title if
	 * (std.getResearchTitle() != null) {
	 * row.createCell(11).setCellValue(std.getResearchTitle()); } else {
	 * row.createCell(11).setCellValue(""); } // Supervisor(s) if
	 * (std.getSupervisor_s() != null) {
	 * row.createCell(12).setCellValue(std.getSupervisor_s()); } else {
	 * row.createCell(12).setCellValue(""); } // Examiner(s) if (std.getExaminer_s()
	 * != null) { row.createCell(13).setCellValue(std.getSupervisor_s()); } else {
	 * row.createCell(13).setCellValue(""); } // Stream if (std.getStream() != null)
	 * { row.createCell(14).setCellValue(std.getStream()); } else {
	 * row.createCell(14).setCellValue(""); } // Payment if
	 * (std.getTotalPaymentMade() != 0) {
	 * row.createCell(15).setCellValue(std.getTotalPaymentMade()); } else {
	 * row.createCell(15).setCellValue(""); } }else if(std.getType() == "phD") {
	 * 
	 * Row row = sheet4.createRow(rowIdx4++); // new row
	 * 
	 * // Registration No if (std.getRegistrationNo() != null) {
	 * row.createCell(0).setCellValue(std.getRegistrationNo()); } else {
	 * row.createCell(0).setCellValue(""); } // Title if (std.getTitle() != null) {
	 * row.createCell(1).setCellValue(std.getTitle()); } else {
	 * row.createCell(1).setCellValue(""); } // Name with initials if
	 * (std.getNamewithinitials() != null) {
	 * row.createCell(2).setCellValue(std.getNamewithinitials()); } else {
	 * row.createCell(2).setCellValue(""); } // Name denoted by Initials if
	 * (std.getNamedenotedbyInitials() != null) {
	 * row.createCell(3).setCellValue(std.getNamedenotedbyInitials()); } else {
	 * row.createCell(3).setCellValue(""); } // Date of Registration if
	 * (std.getDateofRegistration() != null) {
	 * row.createCell(4).setCellValue(std.getDateofRegistration()); } else {
	 * row.createCell(4).setCellValue(""); } // Part Time/Full Time if
	 * (std.getPartTimeOrFullTime() != null) {
	 * row.createCell(5).setCellValue(std.getPartTimeOrFullTime()); } else {
	 * row.createCell(5).setCellValue(""); } // HDC Meeting Number if
	 * (std.gethDCMeetingNumber() != null) {
	 * row.createCell(6).setCellValue(std.gethDCMeetingNumber()); } else {
	 * row.createCell(6).setCellValue(""); }
	 * 
	 * // Postal Address if (std.getPostalAddress() != null) {
	 * row.createCell(7).setCellValue(std.getPostalAddress()); } else {
	 * row.createCell(7).setCellValue(""); } // E-mail Address(s) if
	 * (std.getEmailAddress_s() != null) {
	 * row.createCell(8).setCellValue(std.getEmailAddress_s()); } else {
	 * row.createCell(8).setCellValue(""); } // Contact Number(s) if
	 * (std.getContactNumber_s() != null) {
	 * row.createCell(9).setCellValue(std.getContactNumber_s()); } else {
	 * row.createCell(9).setCellValue(""); } // Affiliated Laboratory if
	 * (std.getAffiliatedLaboratory() != null) {
	 * row.createCell(10).setCellValue(std.getAffiliatedLaboratory()); } else {
	 * row.createCell(10).setCellValue(""); } // Research Title if
	 * (std.getResearchTitle() != null) {
	 * row.createCell(11).setCellValue(std.getResearchTitle()); } else {
	 * row.createCell(11).setCellValue(""); } // Supervisor(s) if
	 * (std.getSupervisor_s() != null) {
	 * row.createCell(12).setCellValue(std.getSupervisor_s()); } else {
	 * row.createCell(12).setCellValue(""); } // Examiner(s) if (std.getExaminer_s()
	 * != null) { row.createCell(13).setCellValue(std.getSupervisor_s()); } else {
	 * row.createCell(13).setCellValue(""); } // Stream if (std.getStream() != null)
	 * { row.createCell(14).setCellValue(std.getStream()); } else {
	 * row.createCell(14).setCellValue(""); } // Payment if
	 * (std.getTotalPaymentMade() != 0) {
	 * row.createCell(15).setCellValue(std.getTotalPaymentMade()); } else {
	 * row.createCell(15).setCellValue(""); } } }
	 * 
	 * //Remove sheet if there are no data if(rowIdx1 == 1) {
	 * workbook.removeSheetAt(0); }else if(rowIdx2 == 1) {
	 * workbook.removeSheetAt(1); }else if(rowIdx3 == 1) {
	 * workbook.removeSheetAt(2); }else if(rowIdx4 == 1) {
	 * workbook.removeSheetAt(3); }
	 * 
	 * workbook.write(out); return new ByteArrayInputStream(out.toByteArray()); }
	 * catch (IOException e) { throw new
	 * RuntimeException("fail to export data to Excel file: " + e.getMessage()); } }
	 */

	// View/Get student
	public PGStudent viewPGStudent(String registrationNo) {
		List<PGStudent> stds = pgStudentRepository.findByRegistrationNo(registrationNo);
		return stds.get(0);
	}
	
	//Edit Student
	public void updatePGStudent(PGStudent std) {
		try {
		pgStudentRepository.save(std);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Delete Student
	public void deletePGStudentByRegistrationNo(String registrationNo) {
		//Get student
		try {
		List<PGStudent> stds = pgStudentRepository.findByRegistrationNo(registrationNo);
		pgStudentRepository.deleteById(stds.get(0).getStudentDataId());
		}catch(Exception e) {
			System.out.println("Student cannot found");
		}
		
	}

	


	
	

	

}
