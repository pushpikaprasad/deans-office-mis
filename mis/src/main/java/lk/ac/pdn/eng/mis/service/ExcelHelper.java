
package lk.ac.pdn.eng.mis.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.ac.pdn.eng.mis.entity.Employee;
import lk.ac.pdn.eng.mis.entity.PGStudent;
import lk.ac.pdn.eng.mis.repository.EmployeeRepository;

@Service
public class ExcelHelper {
	
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] HEADERs = { "Emp. No","Title","Name","Names of Initials","NIC","Designation","Present Division 1","Department Code",
			"Telephone","Type","Email","Mobile Number","Address","Availability","Remark"};
	static String SHEET = "Employees";

	public static ByteArrayInputStream employeesToExcel(List<Employee> employees) {
		
		try (
				Workbook workbook = new XSSFWorkbook(); 
				ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		       
			Sheet sheet = workbook.createSheet(SHEET);

		      // Header
		      Row headerRow = sheet.createRow(0);

		      for (int col = 0; col < HEADERs.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(HEADERs[col]);
		      }

		      int rowIdx = 1;
		      for (Employee emp : employees) {
		        Row row = sheet.createRow(rowIdx++);

		        if(emp.getEmployeeNumber() != null) {
		        	row.createCell(0).setCellValue(emp.getEmployeeNumber());
		        }else {
		        	row.createCell(0).setCellValue("");
		        }
		        if(emp.getTitle() != null) {
		        	row.createCell(1).setCellValue(emp.getTitle());
		        }else {
		        	row.createCell(1).setCellValue("");
		        }
		        
		        if(emp.getSurnameWithInitials() != null) {
		        	row.createCell(2).setCellValue(emp.getSurnameWithInitials());
		        }else {
		        	row.createCell(2).setCellValue("");
		        }
		        
		        if(emp.getNamesDenotedByInitials() != null) {
		        	row.createCell(3).setCellValue(emp.getNamesDenotedByInitials());
		        }else {
		        	row.createCell(3).setCellValue("");
		        }
		        
		        if(emp.getNic() != null) {
		        	row.createCell(4).setCellValue(emp.getNic());
		        }else {
		        	row.createCell(4).setCellValue("");
		        }
		        
		       
		        if(emp.getDesignation() != null) {
		        	 row.createCell(5).setCellValue(emp.getDesignation());
		        }else {
		        	 row.createCell(5).setCellValue("");
		        }
		        
		       
		        if(emp.getPresentDivision1() != null) {
		        	 row.createCell(6).setCellValue(emp.getPresentDivision1());
		        }else {
		        	 row.createCell(6).setCellValue("");
		        }
		        
		        if(emp.getDepartmentCode() != null) {
		        	 row.createCell(7).setCellValue(emp.getDepartmentCode());
		        }else {
		        	 row.createCell(7).setCellValue("");
		        }
		       
		        if(emp.getTelephone() != null) {
		        	row.createCell(8).setCellValue(emp.getTelephone());
		        }else {
		        	row.createCell(8).setCellValue("");
		        }
		        
		        
		        if(emp.getType() != null) {
		        	 row.createCell(9).setCellValue(emp.getType());
		        }else {
		        	 row.createCell(9).setCellValue("");
		        }
		       
		        
		        if(emp.getEmail() != null) {
		        	row.createCell(10).setCellValue(emp.getEmail());
		        }else {
		        	row.createCell(10).setCellValue("");
		        }
		        
		        
		        if(emp.getMobileNumber() != null) {
		        	row.createCell(11).setCellValue(emp.getMobileNumber());
		        }else {
		        	row.createCell(11).setCellValue("");
		        }
		        
		        
		        if(emp.getAddress() != null) {
		        	row.createCell(12).setCellValue(emp.getAddress());
		        }else {
		        	row.createCell(12).setCellValue("");
		        }
		        
		        if(emp.getAvailability() != null) {
		        	row.createCell(13).setCellValue(emp.getAvailability());
		        }else {
		        	row.createCell(13).setCellValue("");
		        }
		        
		        if(emp.getRemark() != null) {
		        	row.createCell(14).setCellValue(emp.getRemark());
		        }else {
		        	row.createCell(14).setCellValue("");
		        }
		        
		      }

		      workbook.write(out);
		      return new ByteArrayInputStream(out.toByteArray());
		    } catch (IOException e) {
		      throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		    }
	}

}
