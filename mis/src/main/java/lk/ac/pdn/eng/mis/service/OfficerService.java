package lk.ac.pdn.eng.mis.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lowagie.text.DocumentException;

import lk.ac.pdn.eng.mis.entity.Application;
import lk.ac.pdn.eng.mis.entity.Employee;
import lk.ac.pdn.eng.mis.entity.Officer;
import lk.ac.pdn.eng.mis.entity.PGStudent;
import lk.ac.pdn.eng.mis.entity.StudentVeriData;
import lk.ac.pdn.eng.mis.entity.User;
import lk.ac.pdn.eng.mis.entity.VerificationApplication;
import lk.ac.pdn.eng.mis.repository.ApplicationRepository;
import lk.ac.pdn.eng.mis.repository.EmployeeRepository;
import lk.ac.pdn.eng.mis.repository.OfficerRepository;
import lk.ac.pdn.eng.mis.repository.PGStudentRepository;
import lk.ac.pdn.eng.mis.repository.StudentVeriRepository;

@Service
public class OfficerService {

	@Autowired
	private OfficerRepository officerRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private UserService userService;

	@Autowired
	private ApplicationService applicationService;

	@Autowired
	private PGStudentRepository pgStudentRepository;

	@Autowired
	private VeriApplicationService veriApplicationService;

	@Autowired
	private StudentVeriRepository stdVeriRepo;

	// get Officer
	public Officer getOfficer(String username) {
		return officerRepository.getOfficerByUsername(username);
	}

	// ###############################################################################################################################
	/* APPLICATION PROCESS */

	// View Application
	public Application getApplication(int id) {
		return applicationService.getApplicationByID(id);
	}

	// Get all transcript/academic rank certificate applications
	public List<Application> getAllTanAndAcaApps() {
		return applicationService.getApplications();
	}

	// Get all applications by Registration no
	public List<Application> getAllApplicationsByRegNo(String regNo) {
		return applicationService.getApplicationByRegNo(regNo);
	}

	// Get all new applications
	public List<Application> getAllNewApplications() {
		return applicationService.getApplicationByStatus("new");
	}

	// Get all accepted applications
	public List<Application> getAllAcceptedApplication() {
		return applicationService.getApplicationByStatus("accepted");
	}

	// Get all rejected applications
	public List<Application> getAllRejectedApplication() {
		return applicationService.getApplicationByStatus("rejected");
	}

	// Get all prepared applications
	public List<Application> getAllPreparedApplication() {
		return applicationService.getApplicationByStatus("prepared");
	}

	// Get all checked-OK applications
	public List<Application> getAllCheckedOKApplication() {
		return applicationService.getApplicationByStatus("checked_OK");
	}

	// Get all checked-Not OK applications
	public List<Application> getAllCheckedNotOKApplication() {
		return applicationService.getApplicationByStatus("checked_Not_OK");
	}

	// Get all completed applications
	public List<Application> getAllCompletedTranAcadApplication() {
		return applicationService.getApplicationByStatus("completed");
	}

	// Get all finished applications
	public List<Application> getAllFinishedTranAcadApplication() {
		return applicationService.getApplicationByStatus("done");
	}

	// Accept application ---> Status: Transcript/Aca-Rank documents accepted
	public void acceptApplication(int id, String officerName) {

		Application application = applicationService.getApplicationByID(id);

		applicationService.setStatusApplication(id, "accepted");

		String title = "Your application has been accepted";
		String msg = "Dear Student,\n\nYour application has been accepted and process is started." + "\n\nThank you.";
		String emailAddress = application.getEmail();
		sendEmail(emailAddress, title, msg);

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(id, "\n\u2022 Accepted by " + officerName + " on " + date);
	}

	// Reject application ---> Status: Transcript/Aca-Rank documents rejected
	public void rejectApplication(int id, String reason, String OfficerName) {
		Application application = applicationService.getApplicationByID(id);
		String title = "Your application has been rejected";
		String msg = "Dear Student,\n\nYour application has been rejected due to follwing reason.\nREASON: \n\t"
				+ reason
				+ "\n\nPlease contact AR office to futher details and please resubmit your application with the corrections."
				+ "\n\nThank you.";
		String emailAddress = application.getEmail();
		sendEmail(emailAddress, title, msg);
		applicationService.setStatusApplication(id, "rejected");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(id,
				"\n\u2022 Rejected by " + OfficerName + " on " + date + ". Due to the reason of \"" + reason + "\".");
	}

	// Set duplicate application
	public void duplicateApplication(int id, String OfficerName) {
		Application application = applicationService.getApplicationByID(id);
		applicationService.setStatusApplication(id, "duplicate");
		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);
		applicationService.setStatusApplicationDetails(id,
				"\n\u2022 Marked as duplicate by " + OfficerName + " on " + date);
	}

	// prepared transcript document of Application
	public void preparedT_Documents(int applicationId, String officerName) throws MessagingException {
		applicationService.setStatusApplication(applicationId, "prepared");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(applicationId,
				"\n\u2022 Prepared Transcript document by " + officerName + " on " + date);
	}

	// prepared transcript document of Application
	public void preparedA_Documents(int applicationId, String officerName) throws MessagingException {
		applicationService.setStatusApplication(applicationId, "prepared");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(applicationId,
				"\n\u2022 Prepared Academic Rank document by " + officerName + " on " + date);
	}

	// checked OK
	public void checkedOKApplication(int applicationId, String officerName) {
		applicationService.setStatusApplication(applicationId, "checked_OK");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(applicationId,
				"\n\u2022 Checked - OK by " + officerName + " on " + date);
	}

	// checked Not OK
	public void checkedNotOKApplication(int applicationId, String reason, String officerName) {
		applicationService.setStatusApplication(applicationId, "checked_Not_OK");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(applicationId, "\n\u2022 Checked - NOT OK by " + officerName
				+ " on " + date + ". Due to the reason of \"" + reason + "\".");
	}

	// complete application ---> Status: Transcript/Aca-Rank documents completed
	public void completedApplication(int applicationId, String officerName) {
		applicationService.setStatusApplication(applicationId, "completed");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(applicationId,
				"\n\u2022 Document creation is completed by " + officerName + " on " + date);
	}

	// Done Application
	public void doneApplication(int applicationId, String message, String officerName) throws MessagingException {
		Application application = applicationService.getApplicationByID(applicationId);
		String title = "Transcript/Academic Rank Certificate process is completed.";
		String msg = "Dear Student,<br><br>Your application process is completed.<br>You can contact Ar office for futher information."
				+ "<br><br> Message: " + message
				+ "<br><br>Thank you.<br><br>Note: This is an automated email through the system.";
		String emailAddress = application.getEmail();
		sendEmailHTML(emailAddress, title, msg);
		applicationService.setStatusApplication(applicationId, "done");

		Date dateObj = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String date = df.format(dateObj);

		// String date = new Date().toLocaleString();
		applicationService.setStatusApplicationDetails(applicationId,
				"\n\u2022 Process is completed and notification is sent by " + officerName + " to email: "
						+ emailAddress + " on " + date + "\n\n*********************\n\nFinal Message to the student: \n"
						+ message);
	}

	// download application
	public void downloadPDFApplication(int id, HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=application_" + id + ".pdf";
		response.setHeader(headerKey, headerValue);

		ApplicationPDFExporter exporter = new ApplicationPDFExporter(applicationService.getApplicationByID(id));

		exporter.export(response);
	}

	// Upload transcript documents
	public void setTranscriptDocumentData(int applicationID, MultipartFile transcriptDocument) {

		// get application
		Application application = applicationService.getApplicationByID(applicationID);

		try {
			if (transcriptDocument.getBytes().length > 0) {
				application.setTranscriptDocument(transcriptDocument.getBytes());
				applicationService.save(application);
			}

		} catch (IOException e) {
			throw new RuntimeException("fail to store transcript word data: " + e.getMessage());
		}

	}

	// Upload academic rank documents
	public void setAcademicDocumentData(int applicationID, MultipartFile academicDocument) {
		// get application
		Application application = applicationService.getApplicationByID(applicationID);

		try {
			if (academicDocument.getBytes().length > 0) {
				application.setAcademicRankDocument(academicDocument.getBytes());
				applicationService.save(application);
			}
		} catch (IOException e) {
			throw new RuntimeException("fail to store transcript word data: " + e.getMessage());
		}
	}

	// ###############################################################################################################################

	// send email to student ---> Status: Transcript/Aca-Rank documents completed
	public void sendEmail(String emailAddress, String title, String msg) {
		userService.sendEmail(emailAddress, title, msg);
	}

	// send email to student(with HTML) ---> Status: Transcript/Aca-Rank documents
	// completed
	public void sendEmailHTML(String emailAddress, String title, String msg) throws MessagingException {
		userService.sendEmailHTML(emailAddress, title, msg);
	}

	// send email to list
	public void sendEmailToList(InternetAddress[] internetAddresses, String title, String msg)
			throws MessagingException {
		userService.sendEmailToList(internetAddresses, title, msg);

	}

	// update officer
	public void updateOfficer(Officer editOfficer) {
		try {

			officerRepository.save(editOfficer);
		} catch (NullPointerException e) {
			System.out.println(e.getMessage() + " updateOfficer() gives null object!");
		}
	}

	public InternetAddress[] getEmailListofOfficersByWCID(int i) throws AddressException {
		List<User> listOfUsers = userService.getEmailListofOfficersByWCID(i);

		// List<InternetAddress> listOfToAddress = new List<InternetAddress>();
		InternetAddress[] addresses = new InternetAddress[listOfUsers.size()];
		for (int u = 0; u < listOfUsers.size(); u++) {
			addresses[u] = new InternetAddress(listOfUsers.get(u).getEmail());
		}
		System.out.println(addresses.toString());
		return addresses;
	}

	// set work number for officer
	/*
	 * public void setWorkNo(String username, int n) { Officer officer =
	 * officerRepository.getOfficerByUsername(username); officer.setWork_no(n); }
	 */

	// get work number of officer
	/*
	 * public int getWorkNo(String username) { Officer officer =
	 * officerRepository.getOfficerByUsername(username); return
	 * officer.getWork_no(); }
	 */

	/********************************************
	 * Employee Details
	 ******************************************/
	// Get all employees
	public List<Employee> getEmployees() {
		// return employeeRepository.findAll();
		return employeeRepository.findOrderByEmployeeNumber();
	}
	
	//Get all employee By Department
	public List<Employee> getEmployeesByDept(String department) { //presentDivision1
		// return employeeRepository.findAll();
		return employeeRepository.findOrderByDepartment(department);
	}

	public void createEmployee(Employee newEmployee) {
		try {
			employeeRepository.save(newEmployee);
		} catch (DataIntegrityViolationException e) {
			System.out.println(e.getMessage());
		}
	}

	public void deleteEmployee(String email) {
		Employee employee = employeeRepository.findEmployeeByEmail(email);
		try {
			employeeRepository.delete(employee);
		} catch (DataIntegrityViolationException e) {
			System.out.println(e.getMessage());
		}
	}

	public void setEmployeeDetailsbySheet(MultipartFile employeeExcel) {
		try {
			employeeService.excelToEmployees(employeeExcel.getInputStream());
		} catch (IOException e) {
			throw new RuntimeException("fail to store excel data: " + e.getMessage());
		}
	}

	public List<Employee> searchEmployee(String emNumber, String empSurname, String empDesig, String presentDivi,
			String type) {

		if (emNumber.isEmpty()) {
			emNumber = null;
		}
		if (empSurname.isEmpty()) {
			empSurname = null;
		}
		if (empDesig.isEmpty()) {
			empDesig = null;
		}
		if (presentDivi.isEmpty()) {
			presentDivi = null;
		}
		if (type.isEmpty()) {
			type = null;
		}

		return employeeRepository.findAllByInputString(emNumber, empSurname, empDesig, presentDivi, type);
	}

	public InputStream downloadSearchEmployee(String emNumber, String empSurname, String empDesig, String presentDivi,
			String type) {
		List<Employee> employee;

		if (emNumber.isEmpty()) {
			emNumber = null;
		}
		if (empSurname.isEmpty()) {
			empSurname = null;
		}
		if (empDesig.isEmpty()) {
			empDesig = null;
		}
		if (presentDivi.isEmpty()) {
			presentDivi = null;
		}
		if (type.isEmpty()) {
			type = null;
		}

		employee = employeeRepository.findAllByInputString(emNumber, empSurname, empDesig, presentDivi, type);

		ByteArrayInputStream in = ExcelHelper.employeesToExcel(employee);
		return in;
	}

	/*******************************************
	 * 
	 * PG Students'details
	 * 
	 */

	public List<PGStudent> getPGStudents() {
		return pgStudentRepository.findAll();
	}

	/*******************************************
	 * 
	 * Verification Process
	 * 
	 */

	// Get all verification applications
	public List<VerificationApplication> getAllVeriApplications() {
		return veriApplicationService.getAllApplications();
	}

	// Get a verification Application by ApplicationID
	public VerificationApplication getVeriApplicationByID(int applicationID) {
		return veriApplicationService.getApplicationByID(applicationID);
	}

	// Get Student data
	public List<StudentVeriData> getVeriStudentData(int applicationID) {
		return veriApplicationService.getStudentData(applicationID);
	}

	// Get New Applications
	public List<VerificationApplication> getNewVeriApplications() {
		return veriApplicationService.getNewApplications();
	}

	// Get Accepted Applications
	public List<VerificationApplication> getAcceptedVeriApplications() {
		return veriApplicationService.getAcceptedApplications();
	}

	// Get Completed Applications
	public List<VerificationApplication> getCompletedVeriApplications() {
		return veriApplicationService.getCompletedApplications();
	}

	// Get Rejected Applications
	public List<VerificationApplication> getRejectedVeriApplications() {
		return veriApplicationService.getRejectedApplications();
	}

	// Get Prepared Applications
	public List<VerificationApplication> getPreparedVeriApplications() {
		return veriApplicationService.getPreparedApplications();
	}

	// Get checked OK verification application
	public List<VerificationApplication> getOKVeriApplications() {
		return veriApplicationService.getOKApplications();
	}

	// Get checked NOT OK verification application
	public List<VerificationApplication> getNOTOKVeriApplications() {
		return veriApplicationService.getNotOKApplications();
	}

	// View PDF of verification application
	@SuppressWarnings("static-access")
	public ByteArrayInputStream verificationPDFView(VerificationApplication verificationApplication) {

		return veriApplicationService.verificationPDFView(verificationApplication,
				stdVeriRepo.findStuddentByDataID(verificationApplication.getApplicationDataID()));

		/*
		 * System.out.println(stdVeriRepo.findStuddentByDataID(verificationApplication.
		 * getApplicationDataID())); =
		 * stdVeriRepo.findStuddentByDataID(verificationApplication.getApplicationDataID
		 * ());
		 */

	}

	// Accept verification application
	public void acceptVeriApplication(int applicationID, String officerName)
			throws AddressException, MessagingException {
		veriApplicationService.setAcceptVeriApplication(applicationID, officerName);
		;
	}

	// Duplicate verification application
	public void duplicateVeriApplication(int applicationID, String officerName)
			throws AddressException, MessagingException {
		veriApplicationService.setDuplicateVeriApplication(applicationID, officerName);
		;
	}

	// Reject verification application
	public void rejectVeriApplication(int applicationID, String officerName, String reason) throws MessagingException {
		veriApplicationService.setRejectVeriApplication(applicationID, officerName, reason);
	}

	// Upload verification Document data to Database
	public void setVerificationDocumentData(int applicationID, MultipartFile verificationDocument) {
		// get verification application
		VerificationApplication application = veriApplicationService.getApplicationByID(applicationID);

		try {
			if (verificationDocument.getBytes().length > 0) {
				application.setVerificationDocument(verificationDocument.getBytes());
				veriApplicationService.save(application);
			}
		} catch (IOException e) {
			throw new RuntimeException("fail to store transcript word data: " + e.getMessage());
		}
	}

	// Set prepared verification Document
	public void preparedVerificationDocument(int applicationID, String officer)
			throws AddressException, MessagingException {
		veriApplicationService.setPrepareVeriDocuments(applicationID, officer);
	}

	// Set OK verification application
	public void setOKveriapplication(int applicationID, String officer) throws AddressException, MessagingException {
		veriApplicationService.setOKVeriDocuments(applicationID, officer);
	}

	// Set NOT OK verification application
	public void setNotOkverificationApplication(int applicationID, String message, String officer)
			throws AddressException, MessagingException {
		veriApplicationService.setNOTOKVeriDocuments(applicationID, message, officer);
	}

	// Send final message to applicant
	public void setFinalMsgVerificationApplication(int applicationId, String message, String officer)
			throws MessagingException {
		veriApplicationService.setfinalMsgVeriDocuments(applicationId, message, officer);
	}
	
	public void sendVerificationDocumentWithPDFAndCC(int applicationId, String to,String from,String cc,String sub,String msgBody, MultipartFile verificationDocument, String officer) throws IOException, MessagingException {
		byte[] content = verificationDocument.getBytes();
		userService.sendEmailwithFileAttachmentandCC(to, from, cc, sub, msgBody, content, applicationId);
		veriApplicationService.setStatusOnSendingFinalVeriDoc(applicationId, "Send Verification Document of application id:"+applicationId, officer);
	}
	
	// Search by Applicant name
	public List<VerificationApplication> searchVerificationByName(String searchName){
		return veriApplicationService.searchByName(searchName);
	}

}
