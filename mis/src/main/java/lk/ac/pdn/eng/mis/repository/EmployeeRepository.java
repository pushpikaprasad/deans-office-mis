package lk.ac.pdn.eng.mis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer>{

	@Query("select emp from Employee emp where emp.email = ?1")
	Employee findEmployeeByEmail(String email);
	
	@Query("select e from Employee e ORDER BY CAST(e.employeeNumber AS int)")
	List<Employee> findOrderByEmployeeNumber();

	@Query("select emp from Employee emp where emp.employeeNumber = ?1")
	Employee findEmployeeByEmployeeNumber(String enpNo);

	@Query("SELECT e FROM Employee as e WHERE (:emNumber is null or e.employeeNumber=:emNumber) and (:empSurname is null or e.surnameWithInitials like %:empSurname%) and " +
			"(:empDesig is null or e.designation like %:empDesig%) and (:presentDivi is null or e.presentDivision1 like %:presentDivi%) and " +
			" (:type is null or e.type=:type) ORDER BY CAST(e.employeeNumber AS int)"
	)
	
	List<Employee> findAllByInputString(String emNumber, String empSurname, String empDesig, String presentDivi, String type);

	@Query("select emp from Employee emp where emp.presentDivision1 = ?1")
	List<Employee> findOrderByDepartment(String department);

}

