package lk.ac.pdn.eng.mis.controller;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.xmlbeans.impl.common.IOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import lk.ac.pdn.eng.mis.entity.Application;
import lk.ac.pdn.eng.mis.entity.StudentVeriData;
import lk.ac.pdn.eng.mis.entity.VerificationApplication;
import lk.ac.pdn.eng.mis.service.ApplicationService;
import lk.ac.pdn.eng.mis.service.OfficerService;
import lk.ac.pdn.eng.mis.service.VeriApplicationService;

@RestController
public class webAPIController {

	@Autowired
	private ApplicationService applicationService;

	
	@Autowired
	private VeriApplicationService veriApplicationService;

	/* APIs for common use */

	@PostMapping(value = "/sendApplication", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void submitApplication(HttpServletResponse response, @RequestParam("email") String email,
			@RequestParam("mobile") String mobile,

			@RequestParam("titlesPrefixing") String titlesPrefixing,
			@RequestParam("surnameWithInitials") String surnameWithInitials,
			@RequestParam("NamesDenotedByInitials") String NamesDenotedByInitials,
			@RequestParam("postalAddress") String postalAddress, @RequestParam("registrationNo") String registrationNo,
			@RequestParam("field") String field, @RequestParam("cus_generalProgramme") String cus_generalProgramme,
			@RequestParam("cus_specializationProgramme") String cus_specializationProgramme,
			@RequestParam("ore_firstExaminEng") String ore_firstExaminEng,
			@RequestParam("ore_finalPart1Exam") String ore_finalPart1Exam,
			@RequestParam("ore_finalPart2Exam") String ore_finalPart2Exam,
			@RequestParam("ore_finalPart3Exam") String ore_finalPart3Exam,
			@RequestParam("pge_nameOfdegreeOrDipl") String pge_nameOfdegreeOrDipl,
			@RequestParam("pge_yearCompleted") String pge_yearCompleted,
			@RequestParam("requestMadefor") String requestMadefor, @RequestParam("payment") double payment,
			@RequestParam("collectionMethod") String collectionMethod,
			@RequestParam("addressToSendCertificate") String addressToSendCertificate,
			
			@RequestParam(value = "additionalDocuments", required = false) MultipartFile[] additionalDocuments,
			@RequestParam(value = "remark", required = false) String remark,
			
			@RequestParam(value = "localOrForieng", required = false) String localOrForieng,
			@RequestParam("numberOfCopies") int numberOfCopies,
			@RequestParam(value = "paymentReceipt", required = false) MultipartFile paymentReceipt,
			@RequestParam(value = "paymentReceipt2", required = false) MultipartFile paymentReceipt2,
			@RequestParam(value = "clearenceForm", required = false) MultipartFile clearenceForm)
			throws IOException, AddressException, MessagingException {

		Application application = new Application();
		
		
		// upload images into db
		//application.setPaymentImg(paymentReceipt.getBytes());
		if (!paymentReceipt.isEmpty() && !paymentReceipt.getOriginalFilename().split("\\.")[1].equals("pdf")) {
			application.setPaymentImg(paymentReceipt.getBytes());
		}else if (!paymentReceipt.isEmpty() && paymentReceipt.getOriginalFilename().split("\\.")[1].equals("pdf")) {
			application.setPaymentPDF(paymentReceipt.getBytes());
		}
		
		if (!paymentReceipt2.isEmpty()) {
			application.setPaymentImg2(paymentReceipt2.getBytes());
		}
		
		
		if (!clearenceForm.isEmpty()) {
			application.setClearenceFormImg(clearenceForm.getBytes());
		}

		application.setTitlesPrefixing(titlesPrefixing);
		application.setSurnameWithInitials(surnameWithInitials);
		application.setStatus("new");
		application.setRequestMadefor(requestMadefor);
		application.setRegistrationNo(registrationNo);
		application.setPostalAddress(postalAddress);
		application.setPge_yearCompleted(pge_yearCompleted);
		application.setPge_nameOfdegreeOrDipl(pge_nameOfdegreeOrDipl);
		application.setPayment(payment);
		application.setOre_firstExaminEng(ore_firstExaminEng);
		application.setOre_finalPart3Exam(ore_finalPart3Exam);
		application.setOre_finalPart2Exam(ore_finalPart2Exam);
		application.setOre_finalPart1Exam(ore_finalPart1Exam);
		application.setNumberOfCopies(numberOfCopies);
		application.setNamesDenotedByInitials(NamesDenotedByInitials);
		System.out.println(localOrForieng);
		application.setMobileNumber(mobile);
		if (localOrForieng != null) {
			application.setLocalOrForieng(localOrForieng);
		} else {
			application.setLocalOrForieng("");
		}
		application.setField(field);
		application.setEmail(email);
		
		
		//Create formatter
		DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mm a");

		//Local date time instance
		LocalDateTime localDateTime = LocalDateTime.now();

		//Get formatted String
		String submissionDate =  FOMATTER.format(localDateTime);
		
		application.setDate(submissionDate);
		application.setCus_specializationProgramme(cus_specializationProgramme);
		application.setCus_generalProgramme(cus_generalProgramme);

		application.setApplicationType("TRANSCRIPT/ACADEMIC RANK");

		String ifEmail = "";
		if (collectionMethod.equals("From Email")) {
			ifEmail = " (To " + email + " )";
		}
		application.setCollectionMethod(collectionMethod + ifEmail);
		application.setAddressToSendCertificate(addressToSendCertificate);
		
		
		// Other documents and Remark
		application.setRemark(remark);
		ByteArrayInputStream otherdocumentsPDFs = mergeallPDFfiles(additionalDocuments);
		byte[] fileStream = IOUtils.toByteArray(otherdocumentsPDFs);
		application.setAdditionalDocuments(fileStream);
		
		applicationService.createApplication(application);

	}

	@PostMapping(value = "/sendVerificationApplication", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public void sendVerificationApplication(@RequestParam("applicantName") String applicantName,
			@RequestParam("applicantAddress") String applicantAddress,
			@RequestParam("applicantMobile") String applicantMobile,
			@RequestParam("applicantEmail") String applicantEmail,
			@RequestParam("methodOfcollection") String methodOfcollection,
			@RequestParam(value = "postAddress", required = false) String postAddress, 
			@RequestParam(value = "postEmail", required = false) String postEmail,
			@RequestParam("coverLetter") MultipartFile[] coverLetterwithFiles,
			@RequestParam(value = "methodOfPayment" , required = false) String methodOfPayment,
			@RequestParam(value = "paymentImage", required = false) MultipartFile paymentImage,
			@RequestParam(value = "paymentPdf", required = false) MultipartFile paymentPdf,
			@RequestParam(value = "paymentVerifyCode", required = false) String paymentVerifyCode, 
			@RequestParam(value = "stdData", required = false) String stdData)
			throws IOException, MessagingException {
		
		StudentVeriData[] studentData;
		if(stdData.length() > 0) {
			studentData = studentStringToJson(stdData);
		
			for(StudentVeriData s: studentData) {
				if(s.getTranscript() != null) {
					byte[] transcript = DatatypeConverter.parseBase64Binary(s.getTranscript().substring(s.getTranscript().indexOf(",") + 1));
					s.setTranscriptFile(transcript);
				}
				if(s.getDegreeCertificate() != null) {
					byte[] degreecertificate = DatatypeConverter.parseBase64Binary(s.getDegreeCertificate().substring(s.getDegreeCertificate().indexOf(",") + 1));
					s.setDegreeCertificateFile(degreecertificate);
				}
				s.setTranscript(null);
				s.setDegreeCertificate(null);
			}
		
		}
		else {
			studentData = null;
		}
		
		/*
		 * Date date = new Date(); DateFormat df = new
		 * SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
		 * df.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		 */
		//String submissionDate = df.format(date);
		
		//Create formatter
				DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mm a");

				//Local date time instance
				LocalDateTime localDateTime = LocalDateTime.now();

				//Get formatted String
				String submissionDate =  FOMATTER.format(localDateTime);
		
		// merge files into together and create single file of input stream
		ByteArrayInputStream coverletterwithfile = mergePDFFiles(coverLetterwithFiles);
		byte[] fileStream = IOUtils.toByteArray(coverletterwithfile);
		
		veriApplicationService.saveVerificationApplication(applicantName, applicantAddress, applicantMobile,
				applicantEmail, methodOfcollection, postAddress, postEmail, 
				//coverLetter.getBytes(), 
				//coverletterwithfile.readAllBytes(),
				fileStream,
				methodOfPayment,
				paymentImage.getBytes(), paymentPdf.getBytes(), paymentVerifyCode, submissionDate, studentData);

	}
	
	// merge files together to get single byte array
	public static ByteArrayInputStream mergePDFFiles(MultipartFile[] files) {
		
		/*
		 * //System.out.println(students.size()); for(MultipartFile e: files) {
		 * System.out.println(e.getSize()); }
		 */
			
		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		PdfWriter.getInstance(document, out);
		PdfWriter writer = PdfWriter.getInstance(document, out);
		try {
			document.open();
			document.bottom(1);
			
			// Header
			Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
			font.setSize(14);
			font.setColor(Color.BLACK);
			font.setStyle(2);
			Paragraph p = new Paragraph("Cover letter and other documents", font);
			p.setAlignment(Paragraph.ALIGN_CENTER);
			p.setFont(font);
			document.add(p);
			
			
			// cover letter
			if (files.length > 0 ) {
				PdfContentByte pageContentByte = writer.getDirectContent();
				int currentPdfReaderPage = 1;

				// InputStream inputStream = new ByteArrayInputStream(data1);

				PdfReader reader = null;
				try {
					byte dataCoverLetter[] = files[0].getBytes();
					reader = new PdfReader(dataCoverLetter);
					PdfImportedPage pdfImportpage;

					while (currentPdfReaderPage <= reader.getNumberOfPages()) {
						document.newPage();
						pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
						pageContentByte.addTemplate(pdfImportpage, 0, 0);
						currentPdfReaderPage++;
					}

					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			// get paying in slip PDF
			for(int i = 1; i<files.length; i++) {
				
				if (files[i] != null) {
					document.newPage();
					PdfContentByte pageContentByte = writer.getDirectContent();
					int currentPdfReaderPage = 1;

					PdfReader reader = null;
					try {
						byte dataPayment[] = files[i].getBytes();
						reader = new PdfReader(dataPayment);
						PdfImportedPage pdfImportpage;

						while (currentPdfReaderPage <= reader.getNumberOfPages()) {
							document.newPage();
							pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
							pageContentByte.addTemplate(pdfImportpage, 0, 0);
							currentPdfReaderPage++;
						}

						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
				
			}
			document.close();
			
		}catch (DocumentException ex) {

			System.out.println(ex.getMessage());

		}catch (Exception e) {
			e.printStackTrace();
		}

		return new ByteArrayInputStream(out.toByteArray());
			
	}
	
	
	@SuppressWarnings({ "null" })
	public StudentVeriData[] studentStringToJson(String stdData)
			throws IOException {

		StudentVeriData[] studentData = null;
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		studentData = mapper.readValue(stdData, StudentVeriData[].class);
				
		return studentData;
	}
	
	// merge files together to get single byte array
	public static ByteArrayInputStream mergeallPDFfiles(MultipartFile[] files) {
			
			Document document = new Document();
			ByteArrayOutputStream out = new ByteArrayOutputStream();

			PdfWriter.getInstance(document, out);
			PdfWriter writer = PdfWriter.getInstance(document, out);
			try {
				document.open();
				document.bottom(1);
				
				// Header
				Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
				font.setSize(14);
				font.setColor(Color.BLACK);
				font.setStyle(2);
				Paragraph p = new Paragraph("other documents", font);
				p.setAlignment(Paragraph.ALIGN_CENTER);
				p.setFont(font);
				document.add(p);
				
				
				// cover letter
				if (files.length > 0 ) {
					PdfContentByte pageContentByte = writer.getDirectContent();
					int currentPdfReaderPage = 1;

					PdfReader reader = null;
					try {
						byte firstData[] = files[0].getBytes();
						reader = new PdfReader(firstData);
						PdfImportedPage pdfImportpage;

						while (currentPdfReaderPage <= reader.getNumberOfPages()) {
							document.newPage();
							pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
							pageContentByte.addTemplate(pdfImportpage, 0, 0);
							currentPdfReaderPage++;
						}

						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				// get and attach other documents
				for(int i = 1; i<files.length; i++) {
					
					if (files[i] != null) {
						document.newPage();
						PdfContentByte pageContentByte = writer.getDirectContent();
						int currentPdfReaderPage = 1;

						PdfReader reader = null;
						try {
							byte otherData[] = files[i].getBytes();
							reader = new PdfReader(otherData);
							PdfImportedPage pdfImportpage;

							while (currentPdfReaderPage <= reader.getNumberOfPages()) {
								document.newPage();
								pdfImportpage = writer.getImportedPage(reader, currentPdfReaderPage);
								pageContentByte.addTemplate(pdfImportpage, 0, 0);
								currentPdfReaderPage++;
							}

							reader.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						
					}
					
				}
				document.close();
				
			}catch (DocumentException ex) {

				System.out.println(ex.getMessage());

			}catch (Exception e) {
				e.printStackTrace();
			}

			return new ByteArrayInputStream(out.toByteArray());
				
		}

}
