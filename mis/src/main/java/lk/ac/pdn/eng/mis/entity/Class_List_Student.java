package lk.ac.pdn.eng.mis.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "classliststudent")
public class Class_List_Student {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int clStudentID;
	
	private int serial_no;
	private String reg_no;
	private String name;
	private String field;
	private String course_code;
	private String course_type;
	private String m_f;
	private int desk_no;
	private String exam_name;
	
	public Class_List_Student(int serial_no, String reg_no, String name, String field, String course_code,
			String course_type, String m_f, int desk_no, String exam_name) {
		super();
		this.serial_no = serial_no;
		this.reg_no = reg_no;
		this.name = name;
		this.field = field;
		this.course_code = course_code;
		this.course_type = course_type;
		this.m_f = m_f;
		this.desk_no = desk_no;
		this.exam_name = exam_name;
	}
	
	//To update
	public Class_List_Student(int clStudentID, int serial_no, String reg_no, String name, String field,
			String course_code, String course_type, String m_f, int desk_no, String exam_name) {
		super();
		this.clStudentID = clStudentID;
		this.serial_no = serial_no;
		this.reg_no = reg_no;
		this.name = name;
		this.field = field;
		this.course_code = course_code;
		this.course_type = course_type;
		this.m_f = m_f;
		this.desk_no = desk_no;
		this.exam_name = exam_name;
	}
	
	public Class_List_Student() {
		super();
	}
	
	public int getSerial_no() {
		return serial_no;
	}
	
	public void setSerial_no(int serial_no) {
		this.serial_no = serial_no;
	}
	public String getReg_no() {
		return reg_no;
	}
	public void setReg_no(String reg_no) {
		this.reg_no = reg_no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getCourse_code() {
		return course_code;
	}
	public void setCourse_code(String course_code) {
		this.course_code = course_code;
	}
	public String getCourse_type() {
		return course_type;
	}
	public void setCourse_type(String course_type) {
		this.course_type = course_type;
	}
	public String getM_f() {
		return m_f;
	}
	public void setM_f(String m_f) {
		this.m_f = m_f;
	}
	public int getDesk_no() {
		return desk_no;
	}
	public void setDesk_no(int desk_no) {
		this.desk_no = desk_no;
	}
	public String getExam_name() {
		return exam_name;
	}
	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}
	
	
}
