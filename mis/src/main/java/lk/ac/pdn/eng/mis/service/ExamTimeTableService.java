package lk.ac.pdn.eng.mis.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lk.ac.pdn.eng.mis.entity.Class_List_Student;
import lk.ac.pdn.eng.mis.entity.Class_List_Subject;
import lk.ac.pdn.eng.mis.repository.ClassListStudentRepository;
import lk.ac.pdn.eng.mis.repository.ClassListSubjectRepository;


@Service
public class ExamTimeTableService {
	
	@Autowired
	private ClassListSubjectRepository subjectRepo;
	
	@Autowired
	private ClassListStudentRepository studentRepo;
	
	//Create Subject data and Create Student data
	@SuppressWarnings("resource")
	public void setClassListData(MultipartFile files[], String exam_name) {
		
		List<Class_List_Subject> subjects = new ArrayList<Class_List_Subject>();
		
		for(MultipartFile f: files) {
			
			Workbook workbook  = new XSSFWorkbook();
			
			List<Class_List_Student> students = new ArrayList<Class_List_Student>();

			try {

				Class_List_Subject subject = new Class_List_Subject();
				subject.setExam_name(exam_name);

				
				if (f.toString().endsWith(".xlsx")) {
					workbook = new XSSFWorkbook(f.getInputStream());
					// Workbook workbook = new XSSFWorkbook(f.getInputStream());
					Sheet sheet = workbook.getSheetAt(0);
					Iterator<Row> rows = sheet.iterator();

					
					int rowNumber = 0;
					int i = 1;
					while (rows.hasNext()) {
						
						Row currentRow = rows.next();

						// skip header
						if (rowNumber == 0 || rowNumber == 2) {
							rowNumber++;
							continue;
						}

						// Iterator<Cell> cellsInRow = currentRow.iterator();

						
						String s_code = "";

						int c_numberOfColumns = 4;
						int c_cellIdx = 0;

						while (c_cellIdx < c_numberOfColumns) {

							Cell c_currentCell = currentRow.getCell(c_cellIdx,
									Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
							if (c_currentCell != null && c_currentCell.getCellType() == CellType.STRING) {
								System.out.println("rowNumber: " + rowNumber + " Cell number: " + c_cellIdx + " - "
										+ c_currentCell.getStringCellValue());
							}
							c_cellIdx++;
						}

						if (rowNumber == 1) {
							int numberOfColumns = 4;
							int cellIdx = 0;

							while (cellIdx < numberOfColumns) {

								Cell currentCell = currentRow.getCell(cellIdx,
										Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);

								String cellValue;

								switch (cellIdx) {
								case 1:
									if (currentCell.getStringCellValue().equals("")) {
										cellValue = null;
									} else {
										cellValue = currentCell.getStringCellValue();
									}
									subject.setCourse_code(cellValue);
									s_code = cellValue;
									break;
								case 3:
									if (currentCell.getStringCellValue().equals("")) {
										cellValue = null;
									} else {
										cellValue = currentCell.getStringCellValue();
									}
									subject.setCourse_name(cellValue);
									break;
								default:
									break;
								}
								cellIdx++;
							}
						}

						Class_List_Student student = new Class_List_Student();
						student.setCourse_code(s_code);
						student.setExam_name(exam_name);

						if (rowNumber > 2) {
							int numberOfColumns = 6;
							int cellIdx = 0;

							while (cellIdx < numberOfColumns) {

								Cell currentCell = currentRow.getCell(cellIdx,
										Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);

								String cellValue;

								switch (cellIdx) {
								case 0:
									if (currentCell != null
											&& !currentCell.getStringCellValue().equals("Total No. of Students :")) {
										// System.out.println(currentCell.getStringCellValue());
										if (currentCell.getStringCellValue().equals("")) {
											cellValue = null;
										} else {
											cellValue = currentCell.getStringCellValue();
										}
										student.setSerial_no(i);
										
										i++;
									}
									break;
								case 1:
									if (currentCell != null && currentCell.getCellType() == CellType.STRING
											&& !currentCell.getStringCellValue().equals("Total No. of Students :")) {
										if (currentCell.getStringCellValue().equals("")) {
											cellValue = null;
										} else {
											cellValue = currentCell.getStringCellValue();
										}
										student.setReg_no(cellValue);
									}
									break;
								case 2:
									if (currentCell != null
											&& !currentCell.getStringCellValue().equals("Total No. of Students :")) {
										if (currentCell.getStringCellValue().equals("")) {
											cellValue = null;
										} else {
											cellValue = currentCell.getStringCellValue();
										}
										student.setName(cellValue);
									}
									break;
								case 3:
									if (currentCell != null
											&& !currentCell.getStringCellValue().equals("Total No. of Students :")) {
										if (currentCell != null && currentCell.getStringCellValue().equals("")) {
											cellValue = null;
										} else {
											cellValue = currentCell.getStringCellValue();
										}
										student.setField(cellValue);
									}
									break;
								case 4:
									if (currentCell != null
											&& !currentCell.getStringCellValue().equals("Total No. of Students :")) {
										if (currentCell != null && currentCell.getStringCellValue().equals("")) {
											cellValue = null;
										} else {
											cellValue = currentCell.getStringCellValue();
										}
										student.setCourse_type(cellValue);
									}
									break;
								case 5:
									if (currentCell != null
											&& !currentCell.getStringCellValue().equals("Total No. of Students :")) {
										if (currentCell != null && currentCell.getStringCellValue().equals("")) {
											cellValue = null;
										} else {
											cellValue = currentCell.getStringCellValue();
										}
										student.setM_f(cellValue);
									}
									break;
								default:
									break;
								}
								cellIdx++;
								/*
								 * if(currentCell != null && currentCell.getCellType() == CellType.STRING) {
								 * System.out.println("rowNumber: "+ rowNumber + " Cell number: "+ c_cellIdx +
								 * " - " + currentCell.getStringCellValue()); }
								 */
							}

							Class_List_Student s = new Class_List_Student();
							try {
								s = studentRepo.findStudent(student.getReg_no(), student.getCourse_code(), exam_name);
								if (s == null || student.getName() != null) {
									students.add(student);
								}
							} catch (Exception ex) {
								System.out.println(ex.getMessage());
							}
						}
						rowNumber++;

					}
					workbook.close();

				} 
				else {

					// For xls or csv file format
					
					
					
					Scanner sc;
					try {
						sc = new Scanner(f.getInputStream());
						sc.useDelimiter(" "); 
						int rowNum = 0;
						String s_code = null;
						int i = 0;
						while (sc.hasNextLine()) // returns a boolean value
						{
							String textLine = sc.nextLine();
							/*if (textLine.equals("Class List") || textLine.isEmpty() || textLine
									.equals("Student Reg No	Name	Field of Specialization	Course Type	Sex	Remarks") || textLine.charAt(0) == 'T') {
								continue;
							}*/
							
							String subData[] = textLine.split("\t");
							
							if(subData.length != 0) {
								if(subData[0].equals("Course Code :")) {
									//System.out.println(subData[1]);
									subject.setCourse_code(subData[1]);
									s_code = subData[1];
									subject.setCourse_name(subData[3]);
									
									
								}else if (subData[0].contains("E/")) {
									Class_List_Student student = new Class_List_Student();
									student.setCourse_code(s_code);
									student.setExam_name(exam_name);
									student.setSerial_no(i);
									i++;
									student.setReg_no(subData[0]);
									//System.out.println(subData[1]);
									student.setName(subData[1]);
									//System.out.println(subData[2]);
									student.setField(subData[2]);
									//System.out.println(subData[3]);
									student.setCourse_type(subData[3]);
									student.setM_f(subData[4]);
	
									Class_List_Student s = new Class_List_Student();
									try {
										s = studentRepo.findStudent(student.getReg_no(), student.getCourse_code(),
												exam_name);
										if (s == null || student.getName() != null) {
											students.add(student);
											
										}
									} catch (Exception ex) {
										System.out.println(ex.getMessage());
									}
								} 
							}
							 
						}
						sc.close(); // closes the scanner
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
				int no_stds = students.size();
				// System.out.print(students.get(0).getCourse_code()+" -- ");
				//System.out.println(subject+" "+no_stds);
				subject.setNo_std(no_stds);
				
				
				if (no_stds < 195 && no_stds > 180) {
					subject.setHall("CR");
				} else if (no_stds <= 180 && no_stds > 155) {
					subject.setHall("DO II");
				} else if (no_stds <= 155 && no_stds > 87) {
					subject.setHall("DO I");
				} else if (no_stds <= 61 && no_stds > 26) {
					subject.setHall("MENB");
				} else if (no_stds <= 26) {
					subject.setHall("RNO14");
				} else {
					subject.setHall("");
				}
				
				Class_List_Subject s = new Class_List_Subject();
				try {
					s = subjectRepo.findSubject(subject.getCourse_code(), exam_name);
					if (s == null) {
						subjects.add(subject);
					}
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				
				
				subjectRepo.saveAll(subjects);
				
				studentRepo.saveAll(students);
				
			} catch (IOException e) {
				throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
			}
		}
	}
	
	//Get list of exams
	public String[] getListofExams() { //exam_name
		return subjectRepo.getListofExams();
	}
	
	//Get List of Subjects by Exam name
	public String[] getListofCourses(String exam_name) {
		return subjectRepo.getListofCourses(exam_name);
	}
	
	private String[] getListofCoursesbyChangedOrder(String examName) {
		return subjectRepo.getListofCoursesbyChangedOrder(examName);
	}
	
	//Get List of Student by course code
	public String[] getListofStudents(String course_code) {
		return studentRepo.getListofStudents(course_code);
	}
	
	// Check coonection
	public boolean isConnected(String A, String B) {
		String[] students_A = getListofStudents(A);
		String[] students_B = getListofStudents(B);
		
		boolean isConnect = false;
		for(String s1: students_A) {
			for(String s2: students_B) {
				if(s1.equals(s2)) {
					isConnect = true;
				}
			}
		}
		return isConnect;
	}
	
	// Is element in array
	public boolean isInArray(String element, String[] array) {
		boolean isIn = false;
		for(int i = 0; i<array.length; i++) {
			if(element.equals(array[i])) {
				isIn = true;
			}
		}
		return isIn;
	}
	
	//Get connected list by List of subjects
	@SuppressWarnings("null")
	public String[][] getConnectedSubList(String[] subjects) {
		
		String connectedList[][]=new String[subjects.length][subjects.length+1];
		int groupId = 0; // group number in connected list
		
		
		String checkedlist[] = new String[subjects.length];
		int a = 0; //index of checked list
		for(int i = 0; i<subjects.length-1; i++) {
			if(!isInArray(subjects[i], checkedlist)) {
				checkedlist[a] = subjects[i];
				a++;
				connectedList[groupId][0] =  subjects[i];
				
				int v = 1; // value index of connected list array
				for(int j= i+1; j<subjects.length; j++) {
					if(isConnected(subjects[i], subjects[j])) {
						v++;
						connectedList[groupId][v] =  subjects[j];
					}
				}
				groupId++;
			}
		}
		System.out.println();
		for(int i = 0; i<subjects.length; i++) {
			for(int j= 0; j<subjects.length+1; j++) {
				System.out.print(connectedList[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("checked list");
		for(int i = 0; i<subjects.length-1; i++) {
			System.out.print(checkedlist[i]+" ");
		}
		
		return connectedList;
	}
	
	//Get not connected list by List of subjects
	@SuppressWarnings("null")
	public String[][] getNotConnectedSubList(String[] subjects) {
		
		System.out.println("All list");
		for(int i = 0; i<subjects.length-1; i++) {
			System.out.print(subjects[i]+" ");
		}
		
		String notConnectedList[][]= new String[subjects.length][subjects.length+1];
		System.out.println("Number of subjects: "+subjects.length);
		int groupId = 0; // group number in connected list
		
		
		String checkedlist[] = new String[subjects.length];
		int a = 0; //index of checked list
		for(int i = 0; i<subjects.length-1; i++) {
			if(!isInArray(subjects[i], checkedlist)) {
				checkedlist[a] = subjects[i];
				a++;
				notConnectedList[groupId][0] =  subjects[i];
				
				int v = 1; // value index of not-connected list array
				System.out.print("\nchecked with: "+subjects[i]+" -- >");
				for(int j= i+1; j<subjects.length; j++) {
					System.out.println("\t"+subjects[j]);
					if(!isConnected(subjects[i], subjects[j])) {
						//System.out.print("\t"+subjects[j]);
						notConnectedList[groupId][v] =  subjects[j];
						v++;
					}
				}
				groupId++;
				
			}
		}
		System.out.println();
		for(int i = 0; i<subjects.length; i++) {
			for(int j= 0; j<subjects.length+1; j++) {
				System.out.print(notConnectedList[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("checked list");
		for(int i = 0; i<subjects.length-1; i++) {
			System.out.print(checkedlist[i]+" ");
		}
		return notConnectedList;
	}
	
	//Get keep List
	
	//Download Generated Time slots
	
	// Remove element from array
	public String[] removeSubject(String subject, String[] list) {
		String temp[] = new String[list.length];
		int a = 0;
		if(list.length != 0 ) {
			for(int i=0; i<list.length;i++) {
				if(list[i] != null) {
					if(list[i].equals(subject)) {
						continue;
					}
					temp[a] = list[i];
					a++;
				}
			}
		}else {
			System.out.println("Cannot remove element: null list in generate_timeslots()");
		}
		
		String[] temp2 = new String[a];
		for (int i = 0; i<a;i++) {
			temp2[i] = temp[i];
		}
		
		return temp2;
	}
	
	// Check whether subject to other subjects in same slot
	public boolean checkConnectionWithSubjects(int rowId, String[][] arrayOfSolts, String subject) {
		boolean isConnect =false;
		for(int i = 0; i<arrayOfSolts[0].length;i++) {
			if(isConnected(arrayOfSolts[rowId][i],subject)) {
				isConnect = true;
			}
		}
		return isConnect;
	}
	
	
	
	 // Check combinations of subjects to have minimum time slots
	 /*public void getAllRecursive(int n, String[] elements, List<String[]> combinations, int count, int limit) {
         	
	        if(n == 1) {
	            combinations.add(elements.clone());
	            count++;
	        } else {
	            if(count != limit) {
		        		for(int i = 0; i < n-1; i++) {
			            	
			                getAllRecursive(n - 1, elements, combinations, count,  limit);
			                if(n % 2 == 0) {
			                    swap(elements, i, n-1);
			                } else {
			                    swap(elements, 0, n-1);
			                }
			            }
			            getAllRecursive(n - 1, elements, combinations, count, limit );
		        }
	            else {
	            	return;
	            }
	        }
	    }
	    
	    private void swap(String[] input, int a, int b) {
	        String tmp = input[a];
	        input[a] = input[b];
	        input[b] = tmp;
	    }
	    
	    public List<String[]> getCombinations(String[] elements, List<String[]> combinations, int count, int limit){
	    	getAllRecursive(elements.length,  elements, combinations, count, limit);
	        return combinations;
	    }
	    */
	
	
	public String[][] getTimeslots(String[] subjects, String examName){
		
		//String[][] notConnectedList = getNotConnectedSubList(subjects);
				String[] toCheck = subjects;
				String[][] checkedNotConnectedlist = new String[subjects.length][subjects.length];
				int groupId = 0; // slot number from 0
				while(toCheck.length != 0) {
					
					//System.out.print("\nSubject list: ");
					//for(int i = 0; i < toCheck.length; i++) {
					//	System.out.print(" "+toCheck[i]);
					//}
					int c = 1; //slot value index
					checkedNotConnectedlist[groupId][0] = toCheck[0];
					toCheck = removeSubject(toCheck[0], toCheck);
					
					//System.out.println("\n" +groupId+": Added to final: "+checkedNotConnectedlist[groupId][0] );
					
					String[] updatedtoCheck = toCheck;
					
					//System.out.print("\nRemoved subjects:");
					
					for (int i = 1; i < toCheck.length; i++) {
						String subject = toCheck[i]; // read subject one by one
						if (!checkConnectionWithSubjects(groupId, checkedNotConnectedlist, subject)) {
							checkedNotConnectedlist[groupId][c] = subject;
							c++;
							updatedtoCheck = removeSubject(subject, updatedtoCheck);
							//System.out.print(" "+subject);
						}
					}
					
					//System.out.print("\nUpdated list: ");
					//for(int i = 0; i < updatedtoCheck.length; i++) {
					//	System.out.print(" "+updatedtoCheck[i]);
					//}
					toCheck = updatedtoCheck;
					groupId++;
				}
				
				int rows = 0;
				int columns = subjects.length;
				
				for(int i = 0; i < subjects.length; i++) {
					for(int j = 0; j < subjects.length; j++) {
						if(checkedNotConnectedlist[i][j] != null) {
							rows = i;
						}
					}
				}
				
				String[][] timeSlotsArray = new String[rows][columns];
				
				for(int i = 0; i < rows; i++) {
					for(int j = 0; j < columns; j++) {
						if(checkedNotConnectedlist[i][j] != null) {
						timeSlotsArray[i][j] = checkedNotConnectedlist[i][j];
						}
						else {
							timeSlotsArray[i][j] = "";
						}
					}
				}
				
				return timeSlotsArray;
	}
	
	//View generated Time slots
	public String[][] generate_timeslots(String examName) {
		
		String[] subjects = getListofCoursesbyChangedOrder(examName);
		
		//List<String[]> combinations = new ArrayList<>();
		
		//combinations = getCombinations(subjects,combinations, 0, 100);
		
		
		// Check minimum timeslots of all combinations
		/*int noOfrows = subjects.length;
		String[] minSubjectArray = new String[subjects.length];
		
		for(int i = 0; i< 20; i++){
			System.out.print(i+ " ");
			List<String> strList = Arrays.asList(subjects);
			Collections.shuffle(strList);
			subjects = strList.toArray(new String[strList.size()]);
			
            String[][] temp = getTimeslots(subjects,examName);
            if(noOfrows > temp.length) {
            	noOfrows = temp.length;
            	minSubjectArray = subjects;
            }
        }
		
		String[][] timeslots = getTimeslots(minSubjectArray,examName);
		*/
		String[][] timeslots = getTimeslots(subjects,examName);
		return timeslots;
		
		
	}

	public String[][] getCoonectedForeach(String examName) {

		String[] subjects = getListofCoursesbyChangedOrder(examName);
		String connectedList[][]=new String[subjects.length][subjects.length+1];
		int groupId = 0; // group number in connected list
		String checkedlist[] = new String[subjects.length];
		int a = 0; //index of checked list
		for(int i = 0; i<subjects.length-1; i++) {
			if(!isInArray(subjects[i], checkedlist)) {
				checkedlist[a] = subjects[i];
				a++;
				connectedList[groupId][0] =  subjects[i];
				
				int v = 1; // value index of connected list array
				for(int j= 0; j<subjects.length; j++) {
					if(isConnected(subjects[i], subjects[j]) && !subjects[i].equals(subjects[j])){
						v++;
						connectedList[groupId][v] =  subjects[j];
					}
				}
				groupId++;
			}
		}
		
		return connectedList;
	}

	
	
}
