package lk.ac.pdn.eng.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.Notice;

import java.util.Collection;
import java.util.List;

@Repository
public interface NoticeRepository extends JpaRepository<Notice, Integer>{

	@Query("select a from Notice a where a.noticeType = ?1")
	List<Notice> getImportantNotices(String noticeType);

	@Query("select a from Notice a where a.noticeType = ?1")
	List<Notice> getNotImportantNotices(String noticeType);

	@Query("select a from Notice a where a.noticeID = ?1")
	Notice findNoticeById(int noticeID);

	@Query("select a from Notice a where a.noticeSection = ?1")
	List<Notice> getNoticesBySection(String section);

	@Query("select a from Notice a where a.noticeSection = ?1 and a.noticeType = ?2")
	List<Notice> getNoticesBySectionAndType(String section, String type);
	
}
