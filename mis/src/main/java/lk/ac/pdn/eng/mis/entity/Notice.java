package lk.ac.pdn.eng.mis.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table (name = "notice")
public class Notice {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int noticeID;
	
	@Lob
	private String notice;
	private String noticeType; //Important or Not important
	private String noticeDateTime;
	private String noticeSection;
	
	public Notice(String notice, String noticeType, String noticeSection) {
		super();
		this.notice = notice;
		this.noticeType = noticeType;
		this.noticeSection = noticeSection;
	}
	
	public Notice() {
		super();
	}


	public String getNoticeSection() {
		return noticeSection;
	}

	public void setNoticeSection(String noticeSection) {
		this.noticeSection = noticeSection;
	}

	public int getNoticeID() {
		return noticeID;
	}

	public void setNoticeID(int noticeID) {
		this.noticeID = noticeID;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public String getNoticeType() {
		return noticeType;
	}
	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}
	public String getNoticeDateTime() {
		return noticeDateTime;
	}
	public void setNoticeDateTime(String noticeDateTime) {
		this.noticeDateTime = noticeDateTime;
	}
	
	
}
