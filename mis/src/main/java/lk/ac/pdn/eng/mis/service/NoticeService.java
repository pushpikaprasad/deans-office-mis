package lk.ac.pdn.eng.mis.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.ac.pdn.eng.mis.entity.Notice;
import lk.ac.pdn.eng.mis.repository.NoticeRepository;
import java.util.List;

@Service
public class NoticeService {
	
	@Autowired
	private NoticeRepository noticeRepository;
	
	// Create new Notice
	public void createNewNotice(String noticeString, String noticeType, String noticeSection) {
		Notice newNotice = new Notice(noticeString,noticeType,noticeSection);
		
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
		String dateTime = df.format(date);
		
		newNotice.setNoticeDateTime(dateTime);
		
		noticeRepository.save(newNotice);
	}
	
	// Edit notice
	public void updateNotice(String noticeUpdate, int noticeID, String noticeUpdateType) {
		Notice noticeObj = noticeRepository.findNoticeById(noticeID);
		if(noticeUpdate != null) {
			noticeObj.setNotice(noticeUpdate);
		}
		if(noticeUpdateType != null) {
			noticeObj.setNoticeType(noticeUpdateType);
		}
		noticeRepository.save(noticeObj);
	}
	
	
	// Delete notice
	public void deleteNoticeById(int noticeID) {
		noticeRepository.deleteById(noticeID);
	}
	
	// Get Important notices
	public List<Notice> getImportantNotices(){
		return noticeRepository.getImportantNotices("Important");
	}
	
	// Get Not important notices (General notices)
	public List<Notice> getNotImportantNotices() {
		return noticeRepository.getNotImportantNotices("Not important");
	}
	
	// Get Notice By ID
	public Notice getNoticeById(int noticeId) {
		return noticeRepository.findNoticeById(noticeId);
	}

	public List<Notice> getNoticesBySection(String section) {
		return noticeRepository.getNoticesBySection(section);
	}

	public List<Notice> getNoticesBySectionAndType(String section, String type) {
		return noticeRepository.getNoticesBySectionAndType(section,type);
	}

	
	
}
