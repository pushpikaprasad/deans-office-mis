package lk.ac.pdn.eng.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lk.ac.pdn.eng.mis.entity.Class_List_Student;

@Repository
public interface ClassListStudentRepository extends JpaRepository<Class_List_Student, Integer>{

	@Query("select s from Class_List_Student s where s.reg_no = ?1 and s.course_code = ?2 and s.exam_name = ?3")
	Class_List_Student findStudent(String reg_no, String course_code, String exam_name);
	
	@Query("select s.reg_no from Class_List_Student s where s.course_code = ?1")
	String[] getListofStudents(String course_code);
}
